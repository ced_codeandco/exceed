<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);
define('MAX_BANNER_IMAGE_SIZE',200);
define('IMAGE_ALLOWED_TYPES','gif|jpg|png|jpeg');
define('RECORD_PER_PAGE',10);
define('TIPS_PER_PAGE ',10);
define('PROJECT_NAME', 'exceed');
define('SITE_NAME', 'Exceed');
define('SITE_LINK_TEXT', 'exceed.ae');
define('DEFAULT_EMAIL', 'testing.webmunky@gmail.com');

if($_SERVER['SERVER_NAME']=='localhost'){
	define('ROOT_PATH', ROOT_DIRECTORY. '/'.PROJECT_NAME.'/');
	define('ROOT_URL', 'http://'.$_SERVER['SERVER_NAME']. '/'.PROJECT_NAME.'/');
	define('ROOT_URL_BASE', 'http://'.$_SERVER['SERVER_NAME']. '/'.PROJECT_NAME.'/');
	define('LOG_ENTRY', FALSE);
}else{
	/*define('ROOT_PATH', $_SERVER['DOCUMENT_ROOT']);
	define('ROOT_URL', 'http://'.$_SERVER['SERVER_NAME'].'/');*/
	define('ROOT_PATH', ROOT_DIRECTORY);
	define('ROOT_URL', 'http://'.$_SERVER['SERVER_NAME'].'/index.php/');
	define('ROOT_URL_BASE', 'http://'.$_SERVER['SERVER_NAME'].'/');
	define('LOG_ENTRY', TRUE);
}
define('CSS_PATH', ROOT_URL_BASE.'css/' );
define('JS_PATH', ROOT_URL_BASE.'js/' );

define('ADMIN_ROOT_URL', ROOT_URL.'administrator/' );
define('ADMIN_ROOT_PATH', ROOT_URL_BASE.'administrator/' );
define('ADMIN_CSS_PATH', CSS_PATH.'admin/' );
define('ADMIN_JS_PATH', JS_PATH.'admin/' );

define('MEMBER_ROOT_URL', ROOT_URL.'member/' );
define('MEMBER_ROOT_PATH', ROOT_PATH.'member/' );
define('MEMBER_CSS_PATH', CSS_PATH.'member/' );
define('MEMBER_JS_PATH', JS_PATH.'member/' );

define('DIR_UPLOAD_BANNER', ROOT_PATH.'uploads/banners/');
define('DIR_UPLOAD_BANNER_SHOW', ROOT_URL_BASE.'uploads/banners/');
define('DIR_UPLOAD_CLASSIFIED', ROOT_PATH.'uploads/classifieds/');
define('DIR_UPLOAD_CLASSIFIED_SHOW', ROOT_URL_BASE.'uploads/classifieds/');
define('DIR_UPLOAD_ADVERTIZE', ROOT_PATH.'uploads/advertize/');
define('DIR_UPLOAD_ADVERTIZE_SHOW', ROOT_URL_BASE.'uploads/advertize/');
/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');
/*
|--------------------------------------------------------------------------
| SMTP configurations
|--------------------------------------------------------------------------
|
| These values are used for esnding emails
|
*/
define('DEFAULT_FROM_EMAIL', 'classifieinfo@gmail.com');
define('DEFAULT_FROM_NAME', 'Exceed');
define('SMTP_PASSWORD', 'Q56pek5%u6');

/* End of file constants.php */
/* Location: ./application/config/constants.php */
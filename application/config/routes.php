<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/




$route['default_controller'] = 'home';
$route['search/(:any)'] = 'search/index/$1';
$route['cms/(:any)'] = 'cms/index/$1';

$route['404_override'] = '';

$route['administrator'] = 'administrator/dashboard';
$route['administrator/login'] = 'administrator/dashboard/login';
$route['administrator/admin_profile'] = 'administrator/dashboard/admin_profile';
$route['administrator/forgot_password'] = 'administrator/dashboard/forgot_password';
$route['administrator/reset_password/(:any)'] = 'administrator/dashboard/reset_password/$1';
$route['administrator/no_access'] = 'administrator/dashboard/no_access';
$route['administrator/logout'] = 'administrator/dashboard/logout';

$route['member'] = 'member/dashboard';
$route['login'] = 'member/dashboard/login';
$route['facebookLogin'] = 'member/dashboard/facebookLogin';

$route['sellers'] = 'home/sellers';
$route['location_listing'] = 'home/location_listing';
$route['categories_listing'] = 'home/categories_listing';
$route['member/profile'] = 'member/dashboard/profile';
$route['member/forgot_password'] = 'member/dashboard/forgot_password';
$route['member/reset_password/(:any)'] = 'member/dashboard/reset_password/$1';
$route['member/verify_email/(:any)'] = 'member/dashboard/verify_email/$1';
$route['member/my_adds'] = 'member/dashboard/my_adds';
$route['member/my_searches'] = 'member/dashboard/my_searches';
$route['member/my_profile'] = 'member/dashboard/my_profile';
$route['member/account_settings'] = 'member/dashboard/account_settings';
$route['create_classified'] = 'member/dashboard/create_classified';
$route['create_classified/(:any)'] = 'member/dashboard/create_classified/$1';
$route['details/(:any)'] = 'search/details/$1';
$route['details'] = 'search/details';
$route['read-blog/(:any)'] = 'blog/read_blog/$1';


$route['member/no_access'] = 'member/dashboard/no_access';
$route['member/logout'] = 'member/dashboard/logout';
$route['logout'] = 'member/dashboard/logout';

/* End of file routes.php */
/* Location: ./application/config/routes.php */
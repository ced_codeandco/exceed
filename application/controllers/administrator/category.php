<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH . 'libraries/Admin_controller.php';

class Category extends Admin_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 public $headerData;
	 public $contentData;
	 public $footerData;
	 public function __construct()
	 {
		parent::__construct();

		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->model('admin_model');
		$this->load->model('category_model');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->headerData['adminModuleList'] = $this->admin_model->getModuleList();
		$this->headerData['isAdminLogin'] = $this->admin_model->checkAdminLogin();
		$this->headerData['activeAdminDetails'] = $this->admin_model->activeAdminDetails();
		$this->headerData['noneEditPage'] = array('home');
		
	}
	function status_inactive(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
			$categoryId =  $this->uri->segment(4);
			if($categoryId == ''){
				redirect(ADMIN_ROOT_URL.'category');
			}else{
				$this->category_model->changeStatus(0,$categoryId);
				$this->session->set_flashdata('flash_success', 'Category Status changed successfully');
				redirect(ADMIN_ROOT_URL.'category');
			}
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}		
	}
	function status_active(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
			$categoryId =  $this->uri->segment(4);
			if($categoryId == ''){
				redirect(ADMIN_ROOT_URL.'category');
			}else{
				$this->category_model->changeStatus(1,$categoryId);
				$this->session->set_flashdata('flash_success', 'Category Status changed successfully');
				redirect(ADMIN_ROOT_URL.'category');
			}
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
		
	}
	function delete(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
			$categoryId =  $this->uri->segment(4);			
			$this->category_model->deleteRecord($categoryId);
			$this->session->set_flashdata('flash_success', 'Category deleted successfully');
			redirect(ADMIN_ROOT_URL.'category');

		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
	}
	
	function add(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
			$categoryId =  $this->uri->segment(4);
            $this->headerData['parentId']= $categoryId;
			$action = 'Add';
			if($categoryId == ''){
				$action = 'Add';
				$this->contentData['categoryDetails'] = array();
				$classifiedParentId = 0;
				$classifiedId = 0;
			}else{
				$action = 'Edit';
				$categoryDetails = $this->category_model->getDetails($categoryId);
				$this->contentData['categoryDetails'] = $categoryDetails;
				$classifiedParentId = $categoryDetails->parent_id;
				$classifiedId = $categoryDetails->id;
			}
			$this->load->library('ckeditor');
			$this->load->library('ckfinder');
			$this->ckeditor->basePath = base_url().'assets/ckeditor/';
			
			$this->ckeditor->config['language'] = 'en';
			$this->ckeditor->config['width'] = '1000px';
			$this->ckeditor->config['height'] = '300px';            
			
			//Add Ckfinder to Ckeditor
			$this->ckfinder->SetupCKEditor($this->ckeditor,'../../assets/ckfinder/'); 

			if($this->input->post()){
				
				$this->load->helper(array('form', 'url'));
				
				$this->form_validation->set_error_delimiters('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>', '</div>');
				$this->form_validation->set_rules('title', 'Title', 'trim|required');
				if(isset($_FILES['category_image']) && $_FILES['category_image']['name']!=''){
					$this->form_validation->set_rules('category_image', 'Image', 'trim|callback_upload_image');
				}else{
					$_POST['category_image']	= (isset($_POST['uploaded_file']) && $_POST['uploaded_file'] != '') ? $_POST['uploaded_file'] : '';	
				}
				if(isset($_FILES['category_icon_large']) && $_FILES['category_icon_large']['name']!=''){
					$this->form_validation->set_rules('category_icon_large', 'Image', 'trim|callback_upload_large_image');
				}else{
					$_POST['category_icon_large']	= (isset($_POST['uploaded_file_large']) && $_POST['uploaded_file_large'] != '') ? $_POST['uploaded_file_large'] : '';
				}
					
				if ($this->form_validation->run() == TRUE)
				{
					if($this->input->post('action') == 'Add') {					
						$_POST['category_slug'] = $this->category_model->generateCategorySlug($this->input->post('title'));
						$_POST['description'] = addslashes($_POST['description']);
						$insertedId = $this->category_model->addDetails();
						
						if($insertedId){
							$this->session->set_flashdata('flash_success', 'Category Details Added successfully');
							if(isset($_POST['parent_id']) && $_POST['parent_id'] != 0)
								redirect(ADMIN_ROOT_URL.'category/index/'.$_POST['parent_id']);
							else
								redirect(ADMIN_ROOT_URL.'category');
						}
					}else{
						
						$updateStatus = $this->category_model->updateDetails();
						if($updateStatus){
							$this->session->set_flashdata('flash_success', 'Category Details Updated successfully');
							
							if(isset($_POST['parent_id']) && $_POST['parent_id'] != 0)
								redirect(ADMIN_ROOT_URL.'category/index/'.$_POST['parent_id']);
							else
								redirect(ADMIN_ROOT_URL.'category');
						}
					}
				}else{
					$_SESSION = $_POST;
					$classifiedParentId = $_POST['parent_id'];
					$classifiedId =$_POST['id'];
				}
				
			}
			//$this->contentData['parentPageList'] = $this->category_model->getParentCategoryLists('id, title' ,' parent_id=0',' ORDER BY category_order ASC');
			$this->contentData['action'] = $action;
			$this->contentData['contentSelectData'] = $this->category_model->getParentCategoryLists($classifiedId,$classifiedParentId);
			$this->headerData['title']= $action.' Category | Admin Module';
			$this->load->view('admin/templates/header', $this->headerData);
			$this->load->view('admin/add_category', $this->contentData);
			$this->load->view('admin/templates/footer', $this->footerData);
			
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
	}
	function get_parent(){
		$this->category_model->getParentCategoryLists($this->input->get('id'),$this->input->get('current_parent_id'));
		exit;
		
	}
	function order(){
		
		$updateStatus = $this->category_model->changeOrder($this->input->get('id'),$this->input->get('category_order'),$this->input->get('position'));
		$this->session->set_flashdata('flash_success', 'Category Order Updated successfully');
		if(isset($_GET['parent']) && $_GET['parent'] != 0)
			redirect(ADMIN_ROOT_URL.'category/index/'.$_GET['parent']);
		else
			redirect(ADMIN_ROOT_URL.'category');
								
	}
	function upload_image(){
		$config['file_name'] = date('dmYHis').'_'.$_FILES['category_image']['name'];
		$_POST['category_image'] = $config['file_name'];
		$config['upload_path'] = DIR_UPLOAD_BANNER;
		$config['allowed_types'] = IMAGE_ALLOWED_TYPES;
		$config['max_size']	= MAX_BANNER_IMAGE_SIZE;				
		$this->load->library('upload', $config);		
		if ($this->upload->do_upload('category_image'))
		{
			if($this->input->post('action') == 'Edit') {
					if(isset($_POST['uploaded_file']) && $_POST['uploaded_file']!='' && file_exists(DIR_UPLOAD_BANNER.$_POST['uploaded_file'])){
						unlink(DIR_UPLOAD_BANNER.$_POST['uploaded_file']);
					}
			}
			return TRUE;
		}
		else
		{
			$this->form_validation->set_message('upload_image', $this->upload->display_errors());
			return FALSE;
		}
		
	}
	function upload_large_image(){
		$config['file_name'] = date('dmYHis').'_'.$_FILES['category_icon_large']['name'];
		$_POST['category_icon_large'] = $config['file_name'];
		$config['upload_path'] = DIR_UPLOAD_BANNER;
		$config['allowed_types'] = IMAGE_ALLOWED_TYPES;
		$config['max_size']	= MAX_BANNER_IMAGE_SIZE;
		$this->load->library('upload', $config);
		if ($this->upload->do_upload('category_icon_large'))
		{
			if($this->input->post('action') == 'Edit') {
                if(isset($_POST['uploaded_file_large']) && $_POST['uploaded_file_large']!='' && file_exists(DIR_UPLOAD_BANNER.$_POST['uploaded_file_large'])){
                    unlink(DIR_UPLOAD_BANNER.$_POST['uploaded_file_large']);
                }
			}
			return TRUE;
		}
		else
		{
			$this->form_validation->set_message('category_icon_large', $this->upload->display_errors());
			return FALSE;
		}

	}
	function email_exist($email){
		$alreadyExist = $this->admin_model->checkEmailExist($email,$_POST['id']);
		if(count($alreadyExist) > 0){
			$this->form_validation->set_message('email_exist', 'The %s is already registered !!!');
			return FALSE;
		}else{
			
			return TRUE;
		}
	}
	public function index()
	{
		$this->load->library('session');
		$pId = $this->uri->segment(4);
		$parentId =  (isset($pId) && $pId != '') ? $this->uri->segment(4) : 0;
		
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
		
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
		
		if($this->session->userdata('admin_id')==''){
			redirect(ADMIN_ROOT_URL.'login');
		}else{
            $this->headerData['parentId']= $parentId;
			$this->contentData['categoryList'] = $this->category_model->getAllRecords('id, title, category_slug,parent_id,category_image, is_active,created_date,category_order' ,'parent_id = '.$parentId,' ORDER BY category_order ASC');
			$succ_msg = $this->session->flashdata('flash_success');
			$err_msg = $this->session->flashdata('flash_error');
			if(isset($succ_msg) && $succ_msg != ''){				
				$this->contentData['successMsg'] = $this->session->flashdata('flash_success');				
			}
			if(isset($err_msg) && $err_msg != ''){				
				$this->contentData['errMsg'] = $this->session->flashdata('flash_error');				
			}
			$this->headerData['title']= 'Category List | Admin Module';
			$this->load->view('admin/templates/header', $this->headerData);
			$this->load->view('admin/category_list', $this->contentData);
			$this->load->view('admin/templates/footer', $this->footerData);
		}
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH . 'libraries/Admin_controller.php';

class Classified extends Admin_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 public $headerData;
	 public $contentData;
	 public $footerData;
	 public function __construct()
	 {
		parent::__construct();

		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->model('admin_model');
		$this->load->model('classified_model');
		$this->load->model('inquiry_model');
		$this->load->model('category_model');
		$this->load->model('brand_model');
		$this->load->model('member_model');
		$this->load->model('image_model');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->headerData['adminModuleList'] = $this->admin_model->getModuleList();
		$this->headerData['isAdminLogin'] = $this->admin_model->checkAdminLogin();
		$this->headerData['activeAdminDetails'] = $this->admin_model->activeAdminDetails();
		$this->headerData['noneEditPage'] = array('home');
		
	}
	function status_inactive(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
			$classifiedId =  $this->uri->segment(4);
			if($classifiedId == ''){
				redirect(ADMIN_ROOT_URL.'classified');
			}else{
				$this->classified_model->changeStatus(0,$classifiedId);
				$this->session->set_flashdata('flash_success', 'Classified Status changed successfully');
				redirect(ADMIN_ROOT_URL.'classified');
			}
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}		
	}
	function status_active(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
			$classifiedId =  $this->uri->segment(4);
			if($classifiedId == ''){
				redirect(ADMIN_ROOT_URL.'classified');
			}else{
				$this->classified_model->changeStatus(1,$classifiedId);
				$this->session->set_flashdata('flash_success', 'Classified Status changed successfully');
				redirect(ADMIN_ROOT_URL.'classified');
			}
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
		
	}
	function image_status_inactive(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
			$imageId =  $this->uri->segment(4);
			$imageDetails = $this->image_model->getDetails($imageId);
			if($imageId == ''){
				redirect(ADMIN_ROOT_URL.'classified');
			}else{
				$this->image_model->changeStatus(0,$imageId);
				$this->session->set_flashdata('flash_success', 'Classified Status changed successfully');
				redirect(ADMIN_ROOT_URL.'classified/image_list/'.$imageDetails->classified_id);
			}
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}		
	}
	function image_status_active(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
			$imageId =  $this->uri->segment(4);
			$imageDetails = $this->image_model->getDetails($imageId);
			if($imageId == ''){
				redirect(ADMIN_ROOT_URL.'classified');
			}else{
				$this->image_model->changeStatus(1,$imageId);
				$this->session->set_flashdata('flash_success', 'Classified Status changed successfully');
				redirect(ADMIN_ROOT_URL.'classified/image_list/'.$imageDetails->classified_id);
			}
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
		
	}
	function delete(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
			$classifiedId =  $this->uri->segment(4);
			
				$this->classified_model->deleteRecord($classifiedId);
				$this->session->set_flashdata('flash_success', 'Classified deleted successfully');
				redirect(ADMIN_ROOT_URL.'classified');
			
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
	}
	function image_delete(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
			$imageId =  $this->uri->segment(4);
				$imageDetails = $this->image_model->getDetails($imageId);
				if(isset($imageDetails->classified_image) && $imageDetails->classified_image!='' && file_exists(DIR_UPLOAD_CLASSIFIED.$imageDetails->classified_image)){
						unlink(DIR_UPLOAD_CLASSIFIED.$imageDetails->classified_image);
				}
				$this->image_model->deleteRecord($imageId);
				$this->session->set_flashdata('flash_success', 'Classified deleted successfully');
				redirect(ADMIN_ROOT_URL.'classified/image_list/'.$imageDetails->classified_id);
			
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
	}
	function add_image(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
			$classifiedId =  $this->uri->segment(4);
			if(isset($classifiedId) && $classifiedId != 0) {
                $classifiedDetails = $this->classified_model->getDetails($classifiedId);
				$action = 'Add';
				if($this->input->post()){
				$this->load->helper(array('form', 'url'));
				$this->form_validation->set_error_delimiters('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>', '</div>');
								
				if(isset($_FILES['classified_image']) && $_FILES['classified_image']['name']!=''){
                    if (!empty($classifiedDetails->sub_category_id) &&  $classifiedDetails->sub_category_id == CATEG_JOB_WANTED_ID){
                        $this->form_validation->set_rules('classified_image', 'Image', 'trim|callback_upload_files');
                    } else {
                        $this->form_validation->set_rules('classified_image', 'Image', 'trim|callback_upload_image');
                    }
				}
					
				if ($this->form_validation->run() == TRUE)
				{
					$insertedId = $this->image_model->addDetails();
					if($insertedId){
						$this->session->set_flashdata('flash_success', 'Image Details Added successfully');
						redirect(ADMIN_ROOT_URL.'classified/image_list/'.$classifiedId);
					}
					
				}else{
					$_SESSION = $_POST;				
				}
				
				}

				$this->contentData['classifiedDetails'] = $classifiedDetails;
				$this->headerData['title']= 'Add Image | Admin Module';
				$this->load->view('admin/templates/header', $this->headerData);
				$this->load->view('admin/add_image', $this->contentData);
				$this->load->view('admin/templates/footer', $this->footerData);
			}else{
				redirect(ADMIN_ROOT_URL.'classified');
			}
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}

	}
	function add(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
			$classifiedId =  $this->uri->segment(4);
			$action = 'Add';
			if($classifiedId == ''){
				$action = 'Add';
				$this->contentData['classifiedDetails'] = array();
				$classifiedCateId = 0;
			}else{
				$action = 'Edit';
				$classifiedDetails = $this->classified_model->getDetails($classifiedId);
				$this->contentData['classifiedDetails'] = $classifiedDetails;
				$classifiedCateId = $classifiedDetails->category_id;
			}
			$this->load->library('ckeditor');
			$this->load->library('ckfinder');
			$this->ckeditor->basePath = base_url().'assets/ckeditor/';
			
			$this->ckeditor->config['language'] = 'en';
			$this->ckeditor->config['width'] = '1000px';
			$this->ckeditor->config['height'] = '300px';            
			
			//Add Ckfinder to Ckeditor
			$this->ckfinder->SetupCKEditor($this->ckeditor,'../../assets/ckfinder/'); 

			if($this->input->post()){
				
				$this->load->helper(array('form', 'url'));
				$this->form_validation->set_error_delimiters('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>', '</div>');
				$this->form_validation->set_rules('title', 'Title', 'trim|required');
				$this->form_validation->set_rules('small_description', 'Short Description', 'trim');
				$this->form_validation->set_rules('classified_contact_no', 'Contact Number', 'trim');
				$this->form_validation->set_rules('classified_contact_name', 'Contact Name', 'trim');
				$this->form_validation->set_rules('classified_contact_email', 'Contact email', 'trim|email');
				$this->form_validation->set_rules('classified_land_mark', 'Land Mark', 'trim');
				$this->form_validation->set_rules('classified_address', 'Address', 'trim');
				/*if(isset($_FILES['classified_image']) && $_FILES['classified_image']['name']!=''){
					$this->form_validation->set_rules('classified_image', 'Image', 'trim|callback_upload_image');

				}else{
					$_POST['classified_image']	= (isset($_POST['uploaded_file']) && $_POST['uploaded_file'] != '') ? $_POST['uploaded_file'] : '';
				}*/

				if ($this->form_validation->run() == TRUE)
				{
					if($this->input->post('action') == 'Add') {					
						$_POST['classified_slug'] = $this->classified_model->generateClassifiedSlug($this->input->post('title'));
						$_POST['description'] = addslashes($_POST['description']);
                        $data = $this->input->post();
                        if (!empty($data['action'])) unset($data['action']);
						$insertedId = $this->classified_model->addDetails($data);
						
						if($insertedId){
							$this->session->set_flashdata('flash_success', 'Classified Details Added successfully');
							redirect(ADMIN_ROOT_URL.'classified');
						}
					}else{
						$_POST['description'] = addslashes($_POST['description']);
                        $data = $this->input->post();
                        if (!empty($data['action'])) unset($data['action']);
						$updateStatus = $this->classified_model->updateDetails($classifiedId, $data);
						if($updateStatus){
							$this->session->set_flashdata('flash_success', 'Classified Details Updated successfully');
							redirect(ADMIN_ROOT_URL.'classified');
						}
					}
				}else{
					$_SESSION = $_POST;	
					$classifiedCateId = $_POST['category_id'];
				}
				
			}
			//$contentSelectData = $this->category_model->getClassifiedCategoryList($classifiedCateId);
			$memberList = $this->member_model->getAllRecords('id, first_name,last_name','is_active = "1"');
			$contentSelectData = $this->category_model->getClassifiedCategoryList($classifiedCateId);
			$this->contentData['memberList'] = $memberList;


            $this->contentData['parentCategoriesList'] = $this->category_model->getAllRecords('id,title,category_slug,sub_title',"parent_id = '0' AND is_active = '1'",'ORDER BY category_order ASC');
            $this->contentData['fuelTypeList'] = $this->admin_model->getFuelTypeList();
            $this->contentData['transmissionLookUp'] = $this->admin_model->getTransmissionLookUp();
            $this->contentData['career_levelLookUp'] = $this->admin_model->prepareLookUpItems('tbl_career_level');
            $this->contentData['educationLookUp'] = $this->admin_model->prepareLookUpItems('tbl_education');
            $this->contentData['employment_typeLookUp'] = $this->admin_model->prepareLookUpItems('tbl_employment_type');
            $this->contentData['brandList'] = $this->brand_model->getAllRecords('id, title');

			$this->contentData['action'] = $action;
			$this->headerData['title']= $action.' Classified | Admin Module';
			$this->load->view('admin/templates/header', $this->headerData);
			$this->load->view('admin/add_classified', $this->contentData);
			$this->load->view('admin/templates/footer', $this->footerData);
			
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
	}
	function order(){
		
		$updateStatus = $this->classified_model->changeOrder($_REQUEST['id'],$_REQUEST['classified_order'],$_REQUEST['position']);
		$this->session->set_flashdata('flash_success', 'Classified Order Updated successfully');
		if(isset($_GET['parent']) && $_POST['parent'] != 0)
			redirect(ADMIN_ROOT_URL.'classified/index/'.$_GET['parent']);
		else
			redirect(ADMIN_ROOT_URL.'classified');
								
	}
	function upload_files(){
        //$fileName = uniqid('', true);// . "-" .  sprintf("%06d", rand());
        $fileName = date('Ymdhis_').sprintf("%06d", rand());
        $uploaded_path_parts = pathinfo($_FILES['classified_image']['name']);
        $i = 1;
        while (file_exists(DIR_UPLOAD_CLASSIFIED . $fileName)) {
            $target_file_name = $fileName . '-' . ($i++);
        }
        $config['file_name'] = $fileName. '.' . $uploaded_path_parts['extension'];

        //echo $config['file_name'] = date('dmYHis').'_'.$_FILES['classified_image']['name'];
		$_POST['classified_image'] = $config['file_name'];
		$config['upload_path'] = DIR_UPLOAD_CLASSIFIED;
		$config['allowed_types'] = FILE_ALLOWED_TYPES;
		$config['max_size']	= MAX_BANNER_IMAGE_SIZE;				
		$this->load->library('upload', $config);		
		if ($this->upload->do_upload('classified_image'))
		{
			return TRUE;
		}
		else
		{
			$this->form_validation->set_message('upload_files', $this->upload->display_errors());
			return FALSE;
		}
		
	}
	function upload_image(){
		$config['file_name'] = date('dmYHis').'_'.$_FILES['classified_image']['name'];
		$_POST['classified_image'] = $config['file_name'];
		$config['upload_path'] = DIR_UPLOAD_CLASSIFIED;
		$config['allowed_types'] = IMAGE_ALLOWED_TYPES;
		$config['max_size']	= MAX_BANNER_IMAGE_SIZE;
		$this->load->library('upload', $config);
		if ($this->upload->do_upload('classified_image'))
		{
			return TRUE;
		}
		else
		{
			$this->form_validation->set_message('upload_image', $this->upload->display_errors());
			return FALSE;
		}

	}
	function image_list(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
		
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
		if($this->session->userdata('admin_id')==''){
			redirect(ADMIN_ROOT_URL.'login');
		}else{
			$classified_id = $this->uri->segment(4);			
			if($classified_id != '' && $classified_id != 0){			
				
				$classifiedDetails = $this->classified_model->getDetails($classified_id);
				
				if(isset($classifiedDetails->id)) {
					$imageList = $this->image_model->getAllRecords('*','classified_id='.$classified_id,'');
					$this->contentData['classifiedDetails'] = $classifiedDetails;
					$this->contentData['imageList'] = $imageList;
					$this->headerData['title']= 'Classified Image | Admin Module';
					$this->load->view('admin/templates/header', $this->headerData);
					$this->load->view('admin/image_list', $this->contentData);
					$this->load->view('admin/templates/footer', $this->footerData);
				}else{
					redirect(ADMIN_ROOT_URL.'classified');
				}
			}else{
				redirect(ADMIN_ROOT_URL.'classified');
			}
		}
	}
	
	function inquiry_list(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
		
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
		if($this->session->userdata('admin_id')==''){
			redirect(ADMIN_ROOT_URL.'login');
		}else{
			$classified_id = $this->uri->segment(4);			
			if($classified_id != '' && $classified_id != 0){			
				
				$classifiedDetails = $this->classified_model->getDetails($classified_id);
				
				if(isset($classifiedDetails->id)) {
					$inquiryList = $this->inquiry_model->getAllRecords('*','classified_id='.$classified_id);
					$this->contentData['classifiedDetails'] = $classifiedDetails;
					$this->contentData['inquiryList'] = $inquiryList;
					$this->headerData['title']= 'Classified Inquiry | Admin Module';
					$this->load->view('admin/templates/header', $this->headerData);
					$this->load->view('admin/inquiry_list', $this->contentData);
					$this->load->view('admin/templates/footer', $this->footerData);
				}else{
					redirect(ADMIN_ROOT_URL.'classified');
				}
			}else{
				redirect(ADMIN_ROOT_URL.'classified');
			}
		}
	}
	function email_exist($email){
		$alreadyExist = $this->admin_model->checkEmailExist($email,$_POST['id']);
		if(count($alreadyExist) > 0){
			$this->form_validation->set_message('email_exist', 'The %s is already registered !!!');
			return FALSE;
		}else{
			
			return TRUE;
		}
	}
	function inquiry_reply(){
		if($this->input->post()){
			$inquiryReply = $this->inquiry_model->replyInquiry();
			$inquiryDetails = $this->inquiry_model->getDetails($this->input->post('inquiryId'));
			$memberDetails = $this->member_model->getDetails($this->input->post('replied_member_id'));
			$classifiedDetails = $this->classified_model->getDetails($this->input->post('classifiedId'));
			$subject = 'Reply of your inquiry on '.$classifiedDetails->title;
			$content = '';
			$content .= $this->admin_model->emailHeader();
			$content .= $this->input->post('reply_text');
			$content .= $this->admin_model->emailFooter();
			$email = $this->admin_model->sendEmail($inquiryDetails->inquiry_name, $memberDetails->first_name.' '.$memberDetails->last_name, $inquiryDetails->inquiry_email , $memberDetails->email, $subject, $content);
			if($email){
				echo 'replied';
			}else{
				echo 'error';
			}
		}else{
			echo 'error';
		}
		exit;
		
	}
	public function index()
	{
		$this->load->library('session');
		$pId = $this->uri->segment(4);
		$categoryId =  (isset($pId) && $pId != '') ? $this->uri->segment(4) : 0;
		$qWhere = '';
		if($categoryId != 0){
			$qWhere = 'category_id = '.$categoryId;
		}
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
		
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
		
		if($this->session->userdata('admin_id')==''){
			redirect(ADMIN_ROOT_URL.'login');
		}else{
			
			$this->contentData['classifiedList'] = $this->classified_model->getAllRecords('id, title, classified_slug,category_id,is_active,number_of_hits,created_date_time' ,$qWhere,' ORDER BY id DESC');
			$succ_msg = $this->session->flashdata('flash_success');
			$err_msg = $this->session->flashdata('flash_error');
			if(isset($succ_msg) && $succ_msg != ''){				
				$this->contentData['successMsg'] = $this->session->flashdata('flash_success');				
			}
			if(isset($err_msg) && $err_msg != ''){				
				$this->contentData['errMsg'] = $this->session->flashdata('flash_error');				
			}
			$this->headerData['title']= 'Classified List | Admin Module';
			$this->load->view('admin/templates/header', $this->headerData);
			$this->load->view('admin/classified_list', $this->contentData);
			$this->load->view('admin/templates/footer', $this->footerData);
		}
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH . 'libraries/Admin_controller.php';
class Dashboard extends Admin_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 public $headerData;
	 public $contentData;
	 public $footerData;
	 public function __construct()
	{
		parent::__construct();

		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->model('admin_model');
		$this->load->model('member_model');
		$this->load->library('form_validation');
		$this->load->model('classified_model');
		$this->load->model('inquiry_model');
		$this->load->library('session');
		$this->headerData['adminModuleList'] = $this->admin_model->getModuleList();
		$this->headerData['isAdminLogin'] = $this->admin_model->checkAdminLogin();
		$this->headerData['activeAdminDetails'] = $this->admin_model->activeAdminDetails();
		
	}
	public function index()
	{
		$this->load->library('session');
		
		if($this->session->userdata('admin_id')==''){
			redirect(ADMIN_ROOT_URL.'login');
		}else{	
			$this->headerData['title']= 'Dashboard';
			$succ_msg = $this->session->flashdata('flash_success');
			$err_msg = $this->session->flashdata('flash_error');
			if(isset($succ_msg) && $succ_msg != ''){				
				$this->contentData['successMsg'] = $this->session->flashdata('flash_success');				
			}
			if(isset($err_msg) && $err_msg != ''){				
				$this->contentData['errMsg'] = $this->session->flashdata('flash_error');				
			}
			
			$this->contentData['totalMembers'] = $this->member_model->getCount();
			$this->contentData['totalNewMembers'] = $this->member_model->getCount('0');
			$this->contentData['totalClassified'] = $this->classified_model->getCount();
			$this->contentData['totalNewClassified'] = $this->classified_model->getCount('0');
			$this->contentData['totalInquiry'] = $this->inquiry_model->getCount();
			$this->contentData['totalNewInquiry'] = $this->inquiry_model->getCount('0');
			$this->load->view('admin/templates/header', $this->headerData);
			$this->load->view('admin/dashboard', $this->contentData);
			$this->load->view('admin/templates/footer', $this->footerData);
		}
	}
	function admin_profile(){
		
		if($this->session->userdata('admin_id')==''){
			redirect(ADMIN_ROOT_URL.'login');
		}else{
			
			$adminId =  $this->session->userdata('admin_id');
			
			$action = 'Edit';
			$adminDetails = $this->admin_model->getAdminDetails($adminId);
			$this->contentData['adminDetails'] = $adminDetails;
			if($this->input->post()){
				$this->load->helper(array('form', 'url'));
				$this->form_validation->set_error_delimiters('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>', '</div>');
				$this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
				$this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_email_exist');
				
				if ($this->form_validation->run() == TRUE)
				{
					
					$updateStatus = $this->admin_model->updateCurrentDetails();
					if($updateStatus){
						$this->session->set_flashdata('flash_success', 'Admin Details Updated successfully');
						redirect(ADMIN_ROOT_URL);
					}
				}
				
			}
		
			$this->contentData['action'] = $action;
			$this->headerData['title']= 'View Profile Details | Admin Module';
			$this->load->view('admin/templates/header', $this->headerData);
			$this->load->view('admin/admin_profile', $this->contentData);
			$this->load->view('admin/templates/footer', $this->footerData);
			
		
		}
		
		
		
	
	}
	function forgot_password(){

		if($this->input->post('email')){
			$isLogin = $this->admin_model->getDetailsFromEmail($this->input->post('email'));
			if(!empty($isLogin)){
                $this->load->library('emailclass');
                $message = "<p>Dear ".$isLogin->first_name.",<br/><br/>Please use the below link to update your password</p>";
                $message .= '<p><a target="_blank" href="'.ADMIN_ROOT_URL."reset_password/".$this->admin_model->setPasswordToken($isLogin->id).'">'.ADMIN_ROOT_URL."reset_password/".base64_encode($isLogin->email)."</a></p>";

                $subject = 'Password recovery link for  '.SITE_NAME;
                $content = '';
                $content .= $this->emailclass->emailHeader();
                $content .= $message;
                $content .= $this->emailclass->emailFooter();
                $email = $this->emailclass->send_mail($isLogin->email, $subject, $content);
                if($email){
                    $this->session->set_flashdata('flash_success', 'Email sent successfully !! Please check your inbox.');
                    $this->contentData['succMsg'] = 'Email sent successfully !! Please check your inbox.';
                    redirect(ADMIN_ROOT_URL.'login', 'refresh');
                }else{
                    $this->contentData['errMsg'] = 'Email Error Please try again later !!!!';
                }
			}else{
				$this->contentData['errMsg'] = 'Invalid Email Address';
			}	
		}
		if($this->session->userdata('admin_id')==''){
			
			$this->headerData['title']= 'Forgot Password';
			$this->load->view('admin/templates/header', $this->headerData);
			$this->load->view('admin/forgot_password', $this->contentData);
			$this->load->view('admin/templates/footer', $this->footerData);
		}else{		
			redirect(ADMIN_ROOT_URL, 'refresh');
		}
		
	
	}
	public function login()
	{
		if($this->input->post()){
			$isLogin = $this->admin_model->adminLogin();

			if($isLogin != 0){
                $adminDetails = $this->admin_model->getAdminDetails($isLogin);
                $data = array(
                    'admin_name' => $adminDetails->first_name.' '.$adminDetails->last_name,
                    'admin_id' => $adminDetails->id,
                    'email' => $adminDetails->email,
                    'module_access' => $adminDetails->module_access,
                    'is_admin_logged_in' => true,
                    'is_admin' => true
                );
                $this->session->set_userdata($data);

                redirect(ADMIN_ROOT_URL);
			} else {
				$this->contentData['errMsg'] = 'Invalid Email or Password';
			}	
		}
		if($this->session->userdata('admin_id')==''){
			
			$succ_msg = $this->session->flashdata('flash_success');
			$err_msg = $this->session->flashdata('flash_error');
			if(isset($succ_msg) && $succ_msg != ''){				
				$this->contentData['succMsg'] = $this->session->flashdata('flash_success');				
			}
			if(isset($err_msg) && $err_msg != ''){				
				$this->contentData['errMsg'] = $this->session->flashdata('flash_error');				
			}
			
			$this->headerData['title']= 'Login';
			$this->load->view('admin/templates/header', $this->headerData);
			$this->load->view('admin/login', $this->contentData);
			$this->load->view('admin/templates/footer', $this->footerData);
		}else{		
			redirect(ADMIN_ROOT_URL, 'refresh');
		}
	}

    function logout(){
        $logout=$this->admin_model->adminLogout();
        if($logout == TRUE)	{
            $this->session->unset_userdata('admin_name');
            $this->session->unset_userdata('admin_id');
            $this->session->unset_userdata('email');
            $this->session->unset_userdata('module_access');
            $this->session->unset_userdata('is_admin_logged_in');
            $this->session->unset_userdata('is_admin');
        }

        redirect(ADMIN_ROOT_URL);
    }
	
	public function reset_password()
	{
		$password_token = $this->uri->segment(3);
		$this->contentData['password_token'] = $password_token;
		if($password_token && !empty($password_token)) {
			if($this->input->post()){
				$this->load->helper(array('form', 'url'));
					$this->form_validation->set_error_delimiters('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>', '</div>');
					$this->form_validation->set_rules('password', 'Password', 'trim|required');
					$this->form_validation->set_rules('retype_password', 'Password again', 'trim|required');
					
					if ($this->form_validation->run() == TRUE)
					{	
						$isLogin = $this->admin_model->updatePassword($password_token);
                        $this->session->set_flashdata('flash_success', 'Password updated successfully');
						redirect(ADMIN_ROOT_URL.'login', 'refresh');
                        exit(0);
					}
			}
			if($this->session->userdata('admin_id')==''){
                $this->contentData['validToken'] = $this->admin_model->isValidPasswordToken($password_token);
                if (!$this->contentData['validToken']) {
                    $this->contentData['errMsg'] = 'Invalid link. Please try again';
                }
				$this->headerData['title']= 'Reset Password';
				$this->load->view('admin/templates/header', $this->headerData);
				$this->load->view('admin/reset_password', $this->contentData);
				$this->load->view('admin/templates/footer', $this->footerData);
			}else{		
				redirect(ADMIN_ROOT_URL, 'refresh');
			}
		}else{
			redirect(ADMIN_ROOT_URL, 'refresh');
		}
	}
	
	public function no_access(){
		$this->headerData['title']= 'Access Denied';
		$this->load->view('admin/templates/header', $this->headerData);
		$this->load->view('admin/no_access', $this->contentData);
		$this->load->view('admin/templates/footer', $this->footerData);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
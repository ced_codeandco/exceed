<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH . 'libraries/Admin_controller.php';

class Fuel extends Admin_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 public $headerData;
	 public $contentData;
	 public $footerData;
	 public function __construct()
	 {
		parent::__construct();

		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->model('admin_model');
		$this->load->model('fuel_model');
		$this->load->model('model_model');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->headerData['adminModuleList'] = $this->admin_model->getModuleList();
		$this->headerData['isAdminLogin'] = $this->admin_model->checkAdminLogin();
		$this->headerData['activeAdminDetails'] = $this->admin_model->activeAdminDetails();
		$this->headerData['noneEditPage'] = array('home');
		
	}
	function status_inactive(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
			$fuelId =  $this->uri->segment(4);
			if($fuelId == ''){
				redirect(ADMIN_ROOT_URL.'fuel');
			}else{
				$this->fuel_model->changeStatus(0,$fuelId);
				$this->session->set_flashdata('flash_success', 'Fuel Status changed successfully');
				redirect(ADMIN_ROOT_URL.'fuel');
			}
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}		
	}
	function status_active(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
			$fuelId =  $this->uri->segment(4);
			if($fuelId == ''){
				redirect(ADMIN_ROOT_URL.'fuel');
			}else{
				$this->fuel_model->changeStatus(1,$fuelId);
				$this->session->set_flashdata('flash_success', 'Fuel Status changed successfully');
				redirect(ADMIN_ROOT_URL.'fuel');
			}
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
		
	}
	function delete(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
			$fuelId =  $this->uri->segment(4);			
			$this->fuel_model->deleteRecord($fuelId);
			$this->session->set_flashdata('flash_success', 'Fuel deleted successfully');
			redirect(ADMIN_ROOT_URL.'fuel');
			
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
	}
	
	function add(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
			$fuelId =  $this->uri->segment(4);
			$action = 'Add';
			if($fuelId == ''){
				$action = 'Add';
				$this->contentData['fuelDetails'] = array();				
			}else{
				$action = 'Edit';
				$fuelDetails = $this->fuel_model->getDetails($fuelId);
				$this->contentData['fuelDetails'] = $fuelDetails;
				
			}
			
			if($this->input->post()){
				
				$this->load->helper(array('form', 'url'));
				
				$this->form_validation->set_error_delimiters('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>', '</div>');
				$this->form_validation->set_rules('title', 'Title', 'trim|required|callback_fuel_exist');
					
				if ($this->form_validation->run() == TRUE)
				{
					if($this->input->post('action') == 'Add') {					
					
						$insertedId = $this->fuel_model->addDetails();
						
						if($insertedId){
							$this->session->set_flashdata('flash_success', 'Fuel Details Added successfully');
							
								redirect(ADMIN_ROOT_URL.'fuel');
						}
					}else{
						
						$updateStatus = $this->fuel_model->updateDetails();
						if($updateStatus){
							$this->session->set_flashdata('flash_success', 'Fuel Details Updated successfully');
							redirect(ADMIN_ROOT_URL.'fuel');
						}
					}
				}else{
					$_SESSION = $_POST;
					
				}
				
			}
			//$this->contentData['parentPageList'] = $this->fuel_model->getParentFuelLists('id, title' ,' parent_id=0',' ORDER BY fuel_order ASC');
			$this->contentData['action'] = $action;
			$this->headerData['title']= $action.' Fuel | Admin Module';
			$this->load->view('admin/templates/header', $this->headerData);
			$this->load->view('admin/add_fuel', $this->contentData);
			$this->load->view('admin/templates/footer', $this->footerData);
			
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
	}
	function get_parent(){
		$this->fuel_model->getParentFuelLists($_REQUEST['id'],$_REQUEST['current_parent_id']);
		exit;
		
	}
	function order(){
		
		$updateStatus = $this->fuel_model->changeOrder($_REQUEST['id'],$_REQUEST['fuel_order'],$_REQUEST['position']);
		$this->session->set_flashdata('flash_success', 'Fuel Order Updated successfully');
		if(isset($_GET['parent']) && $_POST['parent'] != 0)
			redirect(ADMIN_ROOT_URL.'fuel/index/'.$_GET['parent']);
		else
			redirect(ADMIN_ROOT_URL.'fuel');
								
	}
	function upload_image(){
		$config['file_name'] = date('dmYHis').'_'.$_FILES['fuel_image']['name'];
		$_POST['fuel_image'] = $config['file_name'];
		$config['upload_path'] = DIR_UPLOAD_BANNER;
		$config['allowed_types'] = IMAGE_ALLOWED_TYPES;
		$config['max_size']	= MAX_BANNER_IMAGE_SIZE;				
		$this->load->library('upload', $config);		
		if ($this->upload->do_upload('fuel_image'))
		{
			if($this->input->post('action') == 'Edit') {
					if(isset($_POST['uploaded_file']) && $_POST['uploaded_file']!='' && file_exists(DIR_UPLOAD_BANNER.$_POST['uploaded_file'])){
						unlink(DIR_UPLOAD_BANNER.$_POST['uploaded_file']);
					}
			}
			return TRUE;
		}
		else
		{
			$this->form_validation->set_message('upload_image', $this->upload->display_errors());
			return FALSE;
		}
		
	}
	
	public function index()
	{
		$this->load->library('session');
		$pId = $this->uri->segment(4);
		$parentId =  (isset($pId) && $pId != '') ? $this->uri->segment(4) : 0;
		
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
		
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
		
		if($this->session->userdata('admin_id')==''){
			redirect(ADMIN_ROOT_URL.'login');
		}else{
			
			$this->contentData['fuelList'] = $this->fuel_model->getAllRecords('*' ,'',' ORDER BY id ASC');
			$succ_msg = $this->session->flashdata('flash_success');
			$err_msg = $this->session->flashdata('flash_error');
			if(isset($succ_msg) && $succ_msg != ''){				
				$this->contentData['successMsg'] = $this->session->flashdata('flash_success');				
			}
			if(isset($err_msg) && $err_msg != ''){				
				$this->contentData['errMsg'] = $this->session->flashdata('flash_error');				
			}
			$this->headerData['title']= 'Fuel List | Admin Module';
			$this->load->view('admin/templates/header', $this->headerData);
			$this->load->view('admin/fuel_list', $this->contentData);
			$this->load->view('admin/templates/footer', $this->footerData);
		}
	}
	
	function fuel_exist(){
		$alreadyExist = $this->fuel_model->checkTitleExist($_POST['title'],$_POST['id']);
		if(count($alreadyExist) > 0){
			$this->form_validation->set_message('fuel_exist', 'The %s is already exist !!!');
			return FALSE;
		}else{
			
			return TRUE;
		}
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
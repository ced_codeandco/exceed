<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH . 'libraries/Admin_controller.php';

class Home_page_image extends Admin_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 public $headerData;
	 public $contentData;
	 public $footerData;
	 public function __construct()
	 {
		parent::__construct();

		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->model('admin_model');
		$this->load->model('home_page_image_model');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->headerData['adminModuleList'] = $this->admin_model->getModuleList();
		$this->headerData['isAdminLogin'] = $this->admin_model->checkAdminLogin();
		$this->headerData['activeAdminDetails'] = $this->admin_model->activeAdminDetails();
		$this->headerData['noneEditPage'] = array('home');
		
	}
	function status_inactive(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('6', $this->headerData['activeAdminDetails']->module_access)){
			$home_page_imageId =  $this->uri->segment(4);
			if($home_page_imageId == ''){
				redirect(ADMIN_ROOT_URL.'home_page_image');
			}else{
				$this->home_page_image_model->changeStatus(0,$home_page_imageId);
				$this->session->set_flashdata('flash_success', 'Image Status changed successfully');
				redirect(ADMIN_ROOT_URL.'home_page_image');
			}
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}		
	}
	function status_active(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('6', $this->headerData['activeAdminDetails']->module_access)){
			$home_page_imageId =  $this->uri->segment(4);
			if($home_page_imageId == ''){
				redirect(ADMIN_ROOT_URL.'home_page_image');
			}else{
				$this->home_page_image_model->changeStatus(1,$home_page_imageId);
				$this->session->set_flashdata('flash_success', 'Image Status changed successfully');
				redirect(ADMIN_ROOT_URL.'home_page_image');
			}
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
		
	}
	function delete(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('6', $this->headerData['activeAdminDetails']->module_access)){
			$home_page_imageId =  $this->uri->segment(4);
			
				$this->home_page_image_model->deleteRecord($home_page_imageId);
				$this->session->set_flashdata('flash_success', 'Admin deleted successfully');
				redirect(ADMIN_ROOT_URL.'home_page_image');
			
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
	}
	
	function add(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('6', $this->headerData['activeAdminDetails']->module_access)){
			$home_page_imageId =  $this->uri->segment(4);
			$action = 'Add';
			if($home_page_imageId == ''){
				$action = 'Add';
				$this->contentData['home_page_imageDetails'] = array();
			}else{
				$action = 'Edit';
				$imageDetails = $this->home_page_image_model->getDetails($home_page_imageId);
				$this->contentData['imageDetails'] = $imageDetails;
			}
			        
			if($this->input->post()){
				
				$this->load->helper(array('form', 'url'));
				
				$this->form_validation->set_error_delimiters('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>', '</div>');
				$this->form_validation->set_rules('image_title', 'Title', 'trim|required');
				if(isset($_FILES['image_path']) && $_FILES['image_path']['name']!=''){
					$this->form_validation->set_rules('image_path', 'Image', 'trim|callback_upload_image');
					
				}else{
					$_POST['image_path']	= (isset($_POST['uploaded_file']) && $_POST['uploaded_file'] != '') ? $_POST['uploaded_file'] : '';	
				}
					
				if ($this->form_validation->run() == TRUE)
				{
					if($this->input->post('action') == 'Add') {					
						$insertedId = $this->home_page_image_model->addDetails();
						
						if($insertedId){
							$this->session->set_flashdata('flash_success', 'Image Details Added successfully');
							redirect(ADMIN_ROOT_URL.'home_page_image');
						}
					}else{
						
						$updateStatus = $this->home_page_image_model->updateDetails();
						if($updateStatus){
							$this->session->set_flashdata('flash_success', 'Image Details Updated successfully');
							redirect(ADMIN_ROOT_URL.'home_page_image');
						}
					}
				}else{
					$_SESSION = $_POST;	
				}
				
			}
			$this->contentData['action'] = $action;
			$this->headerData['title']= $action.' Image | Admin Module';
			$this->load->view('admin/templates/header', $this->headerData);
			$this->load->view('admin/add_home_image', $this->contentData);
			$this->load->view('admin/templates/footer', $this->footerData);
			
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
	}
	function order(){
		
		$updateStatus = $this->home_page_image_model->changeOrder($_REQUEST['id'],$_REQUEST['home_page_image_order'],$_REQUEST['position']);
		$this->session->set_flashdata('flash_success', 'Image Order Updated successfully');
			redirect(ADMIN_ROOT_URL.'home_page_image');
								
	}
	function upload_image(){
		$config['file_name'] = date('dmYHis').'_'.$_FILES['image_path']['name'];
		$_POST['image_path'] = $config['file_name'];
		$config['upload_path'] = DIR_UPLOAD_BANNER;
		$config['allowed_types'] = IMAGE_ALLOWED_TYPES;
		$config['max_size']	= MAX_BANNER_IMAGE_SIZE;				
		$this->load->library('upload', $config);		
		if ($this->upload->do_upload('image_path'))
		{
			if($this->input->post('action') == 'Edit') {
					if(isset($_POST['uploaded_file']) && $_POST['uploaded_file']!='' && file_exists(DIR_UPLOAD_BANNER.$_POST['uploaded_file'])){
						unlink(DIR_UPLOAD_BANNER.$_POST['uploaded_file']);
					}
			}
			return TRUE;
		}
		else
		{
			$this->form_validation->set_message('upload_image', $this->upload->display_errors());
			return FALSE;
		}
		
	}
	
	public function index()
	{
		$this->load->library('session');
		$pId = $this->uri->segment(4);
		$parentId =  (isset($pId) && $pId != '') ? $this->uri->segment(4) : 0;
		
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('6', $this->headerData['activeAdminDetails']->module_access)){
		
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
		
		if($this->session->userdata('admin_id')==''){
			redirect(ADMIN_ROOT_URL.'login');
		}else{
			
			$this->contentData['imageList'] = $this->home_page_image_model->getAllRecords('*' ,'',' ORDER BY home_page_image_order ASC');
			$succ_msg = $this->session->flashdata('flash_success');
			$err_msg = $this->session->flashdata('flash_error');
			if(isset($succ_msg) && $succ_msg != ''){				
				$this->contentData['successMsg'] = $this->session->flashdata('flash_success');				
			}
			if(isset($err_msg) && $err_msg != ''){				
				$this->contentData['errMsg'] = $this->session->flashdata('flash_error');				
			}
			$this->headerData['title']= 'Image List | Admin Module';
			$this->load->view('admin/templates/header', $this->headerData);
			$this->load->view('admin/home_image_list', $this->contentData);
			$this->load->view('admin/templates/footer', $this->footerData);
		}
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
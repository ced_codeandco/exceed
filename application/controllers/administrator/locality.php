<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH . 'libraries/Admin_controller.php';

class Locality extends Admin_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 public $headerData;
	 public $contentData;
	 public $footerData;
	 public function __construct()
	 {
		parent::__construct();

		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->model('admin_model');
		$this->load->model('locality_model');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->headerData['adminModuleList'] = $this->admin_model->getModuleList();
		$this->headerData['isAdminLogin'] = $this->admin_model->checkAdminLogin();
		$this->headerData['activeAdminDetails'] = $this->admin_model->activeAdminDetails();
		$this->headerData['noneEditPage'] = array('home');
         if (!empty($_GET['cityId'])) {
             $this->contentData['cityId'] = !empty($_GET['cityId']) ? $_GET['cityId'] : '';
             $data = array('cityId' => $this->contentData['cityId']);
             $this->session->set_userdata($data);
         } else if (isset($_GET['cityId']) && $_GET['cityId'] == ''){
             $this->contentData['cityId'] = '';
         }else {
             $this->contentData['cityId'] = $this->session->userdata('cityId');
         }
	}
	function status_inactive(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
			$localityId =  $this->uri->segment(4);
			if($localityId == ''){
				redirect(ADMIN_ROOT_URL.'locality');
			}else{
				$this->locality_model->changeStatus(0,$localityId);
				$this->session->set_flashdata('flash_success', 'Locality Status changed successfully');
				redirect(ADMIN_ROOT_URL.'locality');
			}
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}		
	}
	function status_active(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
			$localityId =  $this->uri->segment(4);
			if($localityId == ''){
				redirect(ADMIN_ROOT_URL.'locality');
			}else{
				$this->locality_model->changeStatus(1,$localityId);
				$this->session->set_flashdata('flash_success', 'Locality Status changed successfully');
				redirect(ADMIN_ROOT_URL.'locality');
			}
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
		
	}
	function delete(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
			$localityId =  $this->uri->segment(4);
			$this->locality_model->deleteRecord($localityId);
			$this->session->set_flashdata('flash_success', 'Locality deleted successfully');
			redirect(ADMIN_ROOT_URL.'locality');

		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
	}
	
	function add(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
			$localityId =  $this->uri->segment(4);
			$action = 'Add';
			if($localityId == ''){
				$action = 'Add';
				$this->contentData['localityDetails'] = array();
				$classifiedParentId = 0;
				$classifiedId = 0;
			}else{
				$action = 'Edit';
				$localityDetails = $this->locality_model->getDetails($localityId);
				$this->contentData['localityDetails'] = $localityDetails;
				$classifiedParentId = $localityDetails->parent_id;
				$classifiedId = $localityDetails->id;
			}
			$this->load->library('ckeditor');
			$this->load->library('ckfinder');
			$this->ckeditor->basePath = base_url().'assets/ckeditor/';
			
			$this->ckeditor->config['language'] = 'en';
			$this->ckeditor->config['width'] = '1000px';
			$this->ckeditor->config['height'] = '300px';            
			
			//Add Ckfinder to Ckeditor
			$this->ckfinder->SetupCKEditor($this->ckeditor,'../../assets/ckfinder/'); 

			if($this->input->post()){
				
				$this->load->helper(array('form', 'url'));
                $this->form_validation->set_message('is_unique', '%s is already added.');
                $this->form_validation->set_error_delimiters('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>', '</div>');
                if($this->input->post('action') == 'Add') {
                    $this->form_validation->set_rules('title', 'Title', 'trim|required|is_unique[tbl_locality.title]');
                } else {
                    $this->form_validation->set_rules('title', 'Title', 'trim|required');
                }
                $this->form_validation->set_rules('parent_id', 'Parent Id', 'trim|required');
					
				if ($this->form_validation->run() == TRUE)
				{
					if($this->input->post('action') == 'Add') {
						$insertedId = $this->locality_model->addDetails();
						
						if($insertedId){
							$this->session->set_flashdata('flash_success', 'Locality Details Added successfully');
							redirect(ADMIN_ROOT_URL.'locality');
						}
					}else{
						$updateStatus = $this->locality_model->updateDetails();
						if($updateStatus){
							$this->session->set_flashdata('flash_success', 'Locality Details Updated successfully');
							
							if(isset($_POST['parent_id']) && $_POST['parent_id'] != 0)
								redirect(ADMIN_ROOT_URL.'locality/index/'.$_POST['parent_id']);
							else
								redirect(ADMIN_ROOT_URL.'locality');
						}
					}
				}else{
					$_SESSION = $_POST;
					$classifiedParentId = $_POST['parent_id'];
					$classifiedId =$_POST['id'];
				}
				
			}
			//$this->contentData['parentPageList'] = $this->locality_model->getParentLocalityLists('id, title' ,' parent_id=0',' ORDER BY locality_order ASC');
			$this->contentData['action'] = $action;
            $this->contentData['cityList'] = $this->admin_model->getCityList();
			$this->headerData['title']= $action.' Locality | Admin Module';
			$this->load->view('admin/templates/header', $this->headerData);
			$this->load->view('admin/add_locality', $this->contentData);
			$this->load->view('admin/templates/footer', $this->footerData);
			
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
	}
	function get_parent(){
		$this->locality_model->getParentLocalityLists($_REQUEST['id'],$_REQUEST['current_parent_id']);
		exit;
		
	}
	function order(){
		
		$updateStatus = $this->locality_model->changeOrder($_REQUEST['id'],$_REQUEST['locality_order'],$_REQUEST['position']);
		$this->session->set_flashdata('flash_success', 'Locality Order Updated successfully');
		if(isset($_GET['parent']) && $_GET['parent'] != 0)
			redirect(ADMIN_ROOT_URL.'locality/index/'.$_GET['parent']);
		else
			redirect(ADMIN_ROOT_URL.'locality');
								
	}

	function email_exist($email){
		$alreadyExist = $this->admin_model->checkEmailExist($email,$_POST['id']);
		if(count($alreadyExist) > 0){
			$this->form_validation->set_message('email_exist', 'The %s is already registered !!!');
			return FALSE;
		}else{
			
			return TRUE;
		}
	}
	public function index()
	{
		$this->load->library('session');
		$pId = $this->uri->segment(4);
		$parentId =  (isset($pId) && $pId != '') ? $this->uri->segment(4) : 0;
        $cityId = $this->contentData['cityId'];

		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
		
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
		
		if($this->session->userdata('admin_id')==''){
			redirect(ADMIN_ROOT_URL.'login');
		}else{
            $this->contentData['cityList'] = $this->admin_model->getCityList();

            $where = '';
            if (!empty($cityId)) {
                $where = " parent_id= $cityId ";
            }
			$this->contentData['localityList'] = $this->locality_model->getAllRecords('id, title, parent_id, is_active,created_date,locality_order' , $where,' ORDER BY locality_order ASC');

			$succ_msg = $this->session->flashdata('flash_success');
			$err_msg = $this->session->flashdata('flash_error');
			if(isset($succ_msg) && $succ_msg != ''){				
				$this->contentData['successMsg'] = $this->session->flashdata('flash_success');				
			}
			if(isset($err_msg) && $err_msg != ''){				
				$this->contentData['errMsg'] = $this->session->flashdata('flash_error');				
			}
			$this->headerData['title']= 'Locality List | Admin Module';
			$this->load->view('admin/templates/header', $this->headerData);
			$this->load->view('admin/locality_list', $this->contentData);
			$this->load->view('admin/templates/footer', $this->footerData);
		}
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH . 'libraries/Admin_controller.php';

class Members extends Admin_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 public $headerData;
	 public $contentData;
	 public $footerData;
	 public function __construct()
	 {
		parent::__construct();

		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->model('admin_model');
		$this->load->model('member_model');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->headerData['adminModuleList'] = $this->admin_model->getModuleList();
		$this->headerData['isAdminLogin'] = $this->admin_model->checkAdminLogin();
		$this->headerData['activeAdminDetails'] = $this->admin_model->activeAdminDetails();
		$this->headerData['noneEditPage'] = array('home');
		
	}
	function status_inactive(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('2', $this->headerData['activeAdminDetails']->module_access)){
			$MemberId =  $this->uri->segment(4);
			if($MemberId == ''){
				redirect(ADMIN_ROOT_URL.'members');
			}else{
				$this->member_model->changeStatus('0',$MemberId);
				$this->session->set_flashdata('flash_success', 'Member Status changed successfully');
				redirect(ADMIN_ROOT_URL.'members');
			}
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}		
	}
	function status_active(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('2', $this->headerData['activeAdminDetails']->module_access)){
			$MemberId =  $this->uri->segment(4);
			if($MemberId == ''){
				redirect(ADMIN_ROOT_URL.'members');
			}else{
				$this->member_model->changeStatus('1',$MemberId);
				$this->session->set_flashdata('flash_success', 'Member Status changed successfully');
				redirect(ADMIN_ROOT_URL.'members');
			}
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
		
	}
	function get_member_data(){
		
		$result = $this->member_model->getAllRecordsAjax();
		
	}
	function delete(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('2', $this->headerData['activeAdminDetails']->module_access)){
			$MemberId =  $this->uri->segment(4);
			$this->member_model->deleteRecord($MemberId);
			$this->session->set_flashdata('flash_success', 'Member deleted successfully');
			redirect(ADMIN_ROOT_URL.'members');
		
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
	}
	
	function add(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('2', $this->headerData['activeAdminDetails']->module_access)){
			$MemberId =  $this->uri->segment(4);
			$action = 'Add';
			if($MemberId == ''){
				$action = 'Add';
				$this->contentData['MemberDetails'] = array();
			}else{
				$action = 'Edit';
				$memberDetails = $this->member_model->getDetails($MemberId);
				$this->contentData['memberDetails'] = $memberDetails;
			}
			$this->load->library('ckeditor');
			$this->load->library('ckfinder');
			$this->ckeditor->basePath = base_url().'assets/ckeditor/';
			
			$this->ckeditor->config['language'] = 'en';
			$this->ckeditor->config['width'] = '1000px';
			$this->ckeditor->config['height'] = '300px';            
			
			//Add Ckfinder to Ckeditor
			$this->ckfinder->SetupCKEditor($this->ckeditor,'../../assets/ckfinder/'); 

			if($this->input->post()){
				
				$this->load->helper(array('form', 'url'));
				$this->form_validation->set_error_delimiters('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>', '</div>');
				$this->form_validation->set_rules('first_name', 'First Name', 'trim|required');
				$this->form_validation->set_rules('email', 'Email', 'required|valid_email|callback_email_exist');
				if($this->input->post('action') == 'Add') {
					$this->form_validation->set_rules('password', 'Password', 'trim|required');
					$this->form_validation->set_rules('retype_password', 'Password again', 'trim|required');
				}
				
					
				if ($this->form_validation->run() == TRUE)
				{
					
					if($this->input->post('action') == 'Add') {					
						$insertedId = $this->member_model->addDetails();
						
						if($insertedId){
							$this->session->set_flashdata('flash_success', 'Member Details Added successfully');
							redirect(ADMIN_ROOT_URL.'members');
						}
					}else{
						
						$updateStatus = $this->member_model->updateDetails();
						if($updateStatus){
							$this->session->set_flashdata('flash_success', 'Member Details Updated successfully');
							redirect(ADMIN_ROOT_URL.'members');
						}
					}
				}else{
					$_SESSION = $_POST;	
				}
				
			}
			$countryList = $this->admin_model->getCountryList();
			$cityList = $this->admin_model->getCityList();
			$businessList = $this->admin_model->getBusinessList();
			$this->contentData['countryList'] = $countryList;
			$this->contentData['cityList'] = $cityList;
			$this->contentData['businessList'] = $businessList;
			$this->contentData['action'] = $action;
			$this->headerData['title']= $action.' Member | Admin Module';
			$this->load->view('admin/templates/header', $this->headerData);
			$this->load->view('admin/add_Member', $this->contentData);
			$this->load->view('admin/templates/footer', $this->footerData);
			
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
	}
	
	function upload_image(){
		$config['file_name'] = date('dmYHis').'_'.$_FILES['Member_banner_image']['name'];
		$_POST['Member_banner_image'] = $config['file_name'];
		$config['upload_path'] = DIR_UPLOAD_BANNER;
		$config['allowed_types'] = IMAGE_ALLOWED_TYPES;
		$config['max_size']	= MAX_BANNER_IMAGE_SIZE;				
		$this->load->library('upload', $config);		
		if ($this->upload->do_upload('Member_banner_image'))
		{
			if($this->input->post('action') == 'Edit') {
					if(isset($_POST['uploaded_file']) && $_POST['uploaded_file']!='' && file_exists(DIR_UPLOAD_BANNER.$_POST['uploaded_file'])){
						unlink(DIR_UPLOAD_BANNER.$_POST['uploaded_file']);
					}
			}
			return TRUE;
		}
		else
		{
			$this->form_validation->set_message('upload_image', $this->upload->display_errors());
			return FALSE;
		}
		
	}
	function email_exist($email){
		$alreadyExist = $this->member_model->checkEmailExist($email,$_POST['id']);
		if(count($alreadyExist) > 0){
			$this->form_validation->set_message('email_exist', 'The %s is already registered !!!');
			return FALSE;
		}else{
			
			return TRUE;
		}
	}
	public function index()
	{
		$this->load->library('session');
		$pId = $this->uri->segment(4);
		$parentId =  (isset($pId) && $pId != '') ? $this->uri->segment(4) : 0;
		
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('2', $this->headerData['activeAdminDetails']->module_access)){
		
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
		
		if($this->session->userdata('admin_id')==''){
			redirect(ADMIN_ROOT_URL.'login');
		}else{
			
			$this->contentData['memberList'] = $this->member_model->getAllRecords('id,first_name,is_active,last_name,email,last_login_date,last_login_ip,contact_no','',' ORDER BY id DESC');
			$succ_msg = $this->session->flashdata('flash_success');
			$err_msg = $this->session->flashdata('flash_error');
			if(isset($succ_msg) && $succ_msg != ''){				
				$this->contentData['successMsg'] = $this->session->flashdata('flash_success');				
			}
			if(isset($err_msg) && $err_msg != ''){				
				$this->contentData['errMsg'] = $this->session->flashdata('flash_error');				
			}
			$this->headerData['title']= 'Member List | Admin Module';
			$this->load->view('admin/templates/header', $this->headerData);
			$this->load->view('admin/member_list', $this->contentData);
			$this->load->view('admin/templates/footer', $this->footerData);
		}
	}
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH . 'libraries/Admin_controller.php';

class Tips_and_tricks extends Admin_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 public $headerData;
	 public $contentData;
	 public $footerData;
	 public function __construct()
	 {
		parent::__construct();

		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->model('admin_model');
		$this->load->model('classified_model');
		$this->load->model('tips_and_tricks_model');
		$this->load->model('tips_and_tricks_category_model');
		$this->load->model('inquiry_model');
		$this->load->model('category_model');
		$this->load->model('brand_model');
		$this->load->model('member_model');
		$this->load->model('image_model');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->headerData['adminModuleList'] = $this->admin_model->getModuleList();
		$this->headerData['isAdminLogin'] = $this->admin_model->checkAdminLogin();
		$this->headerData['activeAdminDetails'] = $this->admin_model->activeAdminDetails();
		$this->headerData['noneEditPage'] = array('home');
		
	}

	public function index()
	{
		$this->load->library('session');
		$pId = $this->uri->segment(4);
		$categoryId =  (isset($pId) && $pId != '') ? $this->uri->segment(4) : 0;
		$qWhere = '';
		if($categoryId != 0){
			$qWhere = 'category_id = '.$categoryId;
		}
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
		
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
		
		if($this->session->userdata('admin_id')==''){
			redirect(ADMIN_ROOT_URL.'login');
		}else{

			$this->contentData['classifiedList'] = $this->tips_and_tricks_model->getAllRecords('id, title, author_name, url_slug,category_id,is_active,created_date_time' ,$qWhere,' ORDER BY id DESC');
			$succ_msg = $this->session->flashdata('flash_success');
			$err_msg = $this->session->flashdata('flash_error');
			if(isset($succ_msg) && $succ_msg != ''){				
				$this->contentData['successMsg'] = $this->session->flashdata('flash_success');				
			}
			if(isset($err_msg) && $err_msg != ''){				
				$this->contentData['errMsg'] = $this->session->flashdata('flash_error');				
			}
			$this->headerData['title']= 'Article List | Admin Module';
			$this->load->view('admin/templates/header', $this->headerData);
			$this->load->view('admin/tips_and_tricks_list', $this->contentData);
			$this->load->view('admin/templates/footer', $this->footerData);
		}
	}

    /**
     * Function to add tips and tricks
     */
    function add(){
        if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('6', $this->headerData['activeAdminDetails']->module_access)){
            $tipsAndTricksId =  $this->uri->segment(4);
            $action = 'Add';
            if($tipsAndTricksId == ''){
                $action = 'Add';
                $this->contentData['classifiedDetails'] = array();
                $classifiedCateId = 0;
                $classifiedSubCateId = 0;
            }else{
                $action = 'Edit';
                $tipsAndTricksDetails = $this->tips_and_tricks_model->getDetails($tipsAndTricksId);
                $this->contentData['tipsAndTricksDetails'] = $tipsAndTricksDetails;
                $classifiedCateId = $tipsAndTricksDetails->category_id;
                $classifiedSubCateId = $tipsAndTricksDetails->sub_category_id;
            }

            $this->load->library('ckeditor');
            $this->load->library('ckfinder');
            $this->ckeditor->basePath = base_url().'assets/ckeditor/';

            $this->ckeditor->config['language'] = 'en';
            $this->ckeditor->config['width'] = '1000px';
            $this->ckeditor->config['height'] = '300px';

            //Add Ckfinder to Ckeditor
            $this->ckfinder->SetupCKEditor($this->ckeditor,'../../assets/ckfinder/');

            if($this->input->post()){

                $this->load->helper(array('form', 'url'));
                $this->form_validation->set_error_delimiters('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>', '</div>');
                $this->form_validation->set_rules('category_id', 'Category', 'xss_clean|trim|required');
                $this->form_validation->set_rules('title', 'Title', 'xss_clean|trim|required');
                $this->form_validation->set_rules('description', 'Short Description', 'xss_clean|trim|required');
                $this->form_validation->set_rules('short_description', 'Description', 'xss_clean|trim|required');
                if($this->input->post('action') == 'Add') {
                    if(isset($_FILES['image']) && $_FILES['image']['name']!=''){
                        $this->form_validation->set_rules('image', 'Image', 'trim|callback_upload_image');
                    }
                } else {
                    if(isset($_FILES['image']) && $_FILES['image']['name'] !=''){
                        $this->form_validation->set_rules('image', 'Image', 'trim|callback_upload_image');
                    }else{
                        $_POST['image']	= (isset($_POST['uploaded_file']) && $_POST['uploaded_file'] != '') ? $_POST['uploaded_file'] : '';
                    }
                }

                if ($this->form_validation->run() == TRUE)
                {
                    if ($this->input->post('category_id')) {
                        $category_id = $this->tips_and_tricks_category_model->addNewTipsAndTricksCategory($this->input->post('category_id'));
                    }
                    if($this->input->post('action') == 'Add') {
                        $_POST['url_slug'] = $this->tips_and_tricks_model->generateTipsSlug($this->input->post('title'));
                        $_POST['description'] = addslashes($_POST['description']);
                        $data = $this->input->post();
                        $data['created_date_time'] = date('Y-m-d H:i:s');
                        if ($category_id != false){
                            $data['category_id'] = $category_id;
                        }
                        unset($data['action']);
                        unset($data['new_category']);
                        $insertedId = $this->tips_and_tricks_model->addDetails($data);

                        if($insertedId){
                            $this->session->set_flashdata('flash_success', 'Article Added successfully');
                            redirect(ADMIN_ROOT_URL.'tips_and_tricks');
                            exit();
                        }
                    }else{
                        $_POST['description'] = addslashes($_POST['description']);
                        $data = $this->input->post();
                        unset($data['action']);
                        unset($data['new_category']);
                        $updateStatus = $this->tips_and_tricks_model->updateDetails($tipsAndTricksId, $data);
                        if($updateStatus){
                            $this->session->set_flashdata('flash_success', 'Article Updated successfully');
                            redirect(ADMIN_ROOT_URL.'tips_and_tricks');
                            exit();
                        }
                    }
                }else{
                    $_SESSION = $_POST;
                    $classifiedCateId = $_POST['category_id'];
                }

            }
            $contentSelectData = $this->tips_and_tricks_category_model->getClassifiedCategoryList($classifiedCateId);

            $memberList = $this->member_model->getAllRecords('id, first_name,last_name','is_active = "1"');
            $this->contentData['contentSelectData'] = $contentSelectData;
            $contentSelectData = $this->category_model->getClassifiedCategoryList($classifiedCateId);
            $countryList = $this->admin_model->getCountryList();
            $brandList = $this->brand_model->getAllRecords('id,title'," is_active='1'");
            $this->contentData['countryList'] = $countryList;
            $this->contentData['memberList'] = $memberList;
            $this->contentData['brandList'] = $brandList;
            $this->contentData['action'] = $action;
            $this->headerData['title']= $action.' Blog | Admin Module';
            $this->load->view('admin/templates/header', $this->headerData);
            $this->load->view('admin/add_tips_and_tricks', $this->contentData);
            $this->load->view('admin/templates/footer', $this->footerData);

        }else{
            redirect(ADMIN_ROOT_URL.'no_access');
        }
    }

    /**
     * Function to add tips and tricks
     */
    function read_comments(){
        if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('6', $this->headerData['activeAdminDetails']->module_access)){
            $tipsAndTricksId =  $this->uri->segment(4);
            $action = 'Add';
            if($tipsAndTricksId == ''){
                $action = 'Add';
                $this->contentData['classifiedDetails'] = array();
            }else{
                $action = 'Edit';
                $tipsAndTricksDetails = $this->tips_and_tricks_model->getDetails($tipsAndTricksId);
                $this->contentData['tipsAndTricksDetails'] = $tipsAndTricksDetails;
            }

            $this->contentData['successMsg'] = $this->session->flashdata('flash_success');
            $this->contentData['errMsg'] = $this->session->flashdata('flash_error');

            $this->contentData['userComments'] = $this->tips_and_tricks_model->getComments($tipsAndTricksId);
            $this->contentData['action'] = $action;
            $this->headerData['title']= $action.' Blog | Admin Module';
            $this->load->view('admin/templates/header', $this->headerData);
            $this->load->view('admin/read_tips_and_tricks_comments', $this->contentData);
            $this->load->view('admin/templates/footer', $this->footerData);

        }else{
            redirect(ADMIN_ROOT_URL.'no_access');
        }
    }

    function delete_comments($commentId) {
        if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
            $commentId =  $this->uri->segment(4);

            $this->tips_and_tricks_model->deleteComment($commentId);
            $this->session->set_flashdata('flash_success', 'Comment deleted successfully');
            redirect($_SERVER['HTTP_REFERER']);

        }else{
            redirect(ADMIN_ROOT_URL.'no_access');
        }
    }

    function upload_image(){
        $config['file_name'] = date('dmYHis').'_'.$_FILES['image']['name'];
        $_POST['image'] = $config['file_name'];
        $config['upload_path'] = DIR_UPLOAD_BLOG;
        $config['allowed_types'] = IMAGE_ALLOWED_TYPES;
        $config['max_size']	= MAX_BANNER_IMAGE_SIZE;
        $this->load->library('upload', $config);
        if ($this->upload->do_upload('image'))
        {
            if($this->input->post('action') == 'Edit') {
                if(isset($_POST['uploaded_file']) && $_POST['uploaded_file']!='' && file_exists(DIR_UPLOAD_BLOG.$_POST['uploaded_file'])){
                    unlink(DIR_UPLOAD_BLOG.$_POST['uploaded_file']);
                }
            }
            return TRUE;
        }
        else
        {
            $this->form_validation->set_message('upload_image', $this->upload->display_errors());
            return FALSE;
        }

    }

    function delete(){
        if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
            $classifiedId =  $this->uri->segment(4);

            $this->tips_and_tricks_model->deleteRecord($classifiedId);
            $this->session->set_flashdata('flash_success', 'Article deleted successfully');
            redirect(ADMIN_ROOT_URL.'tips_and_tricks');

        }else{
            redirect(ADMIN_ROOT_URL.'no_access');
        }
    }

    public function _validateNewTipCategory($str)
    {
        if (!empty($_POST['category_id']) && $_POST['category_id'] != -1) {
            return true;
        } else if (!empty($str)) {
            if (!$this->tips_and_tricks_category_model->getAllRecords('id', 'title LIKE "'.$str.'"')) {
                return true;
            } else {
                $this->form_validation->set_message('_validateNewTipCategory', 'Category name already exists');
                return FALSE;
            }
        } else {
            $this->form_validation->set_message('_validateNewTipCategory', 'New category name could not be empty');
            return FALSE;
        }

        return true;
    }

    function status_inactive(){
        if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('7', $this->headerData['activeAdminDetails']->module_access)){
            $fun_with_mumId =  $this->uri->segment(4);
            if($fun_with_mumId == ''){
                redirect(ADMIN_ROOT_URL.'fun_with_mum');
            }else{
                $this->tips_and_tricks_model->changeStatus(0,$fun_with_mumId);
                $this->session->set_flashdata('flash_success', 'Article Status changed successfully');
                redirect(ADMIN_ROOT_URL.'fun_with_mum');
            }
        }else{
            redirect(ADMIN_ROOT_URL.'no_access');
        }
    }
    function status_active(){
        if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('7', $this->headerData['activeAdminDetails']->module_access)){
            $fun_with_mumId =  $this->uri->segment(4);
            if($fun_with_mumId == ''){
                redirect(ADMIN_ROOT_URL.'tips_and_tricks');
            }else{
                $this->tips_and_tricks_model->changeStatus(1,$fun_with_mumId);
                $this->session->set_flashdata('flash_success', 'Article Status changed successfully');
                redirect(ADMIN_ROOT_URL.'tips_and_tricks');
            }
        }else{
            redirect(ADMIN_ROOT_URL.'no_access');
        }

    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
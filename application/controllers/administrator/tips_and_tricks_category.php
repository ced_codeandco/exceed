<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH . 'libraries/Admin_controller.php';
class Tips_and_tricks_category extends Admin_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 public $headerData;
	 public $contentData;
	 public $footerData;
	 public function __construct()
	 {
		parent::__construct();

		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->model('admin_model');
		$this->load->model('tips_and_tricks_category_model');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->headerData['adminModuleList'] = $this->admin_model->getModuleList();
		$this->headerData['isAdminLogin'] = $this->admin_model->checkAdminLogin();
		$this->headerData['activeAdminDetails'] = $this->admin_model->activeAdminDetails();
		$this->headerData['noneEditPage'] = array('home');
		
	}
	function status_inactive(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('7', $this->headerData['activeAdminDetails']->module_access)){
			$tips_and_tricks_categoryId =  $this->uri->segment(4);
			if($tips_and_tricks_categoryId == ''){
				redirect(ADMIN_ROOT_URL.'tips_and_tricks_category');
			}else{
				$this->tips_and_tricks_category_model->changeStatus(0,$tips_and_tricks_categoryId);
				$this->session->set_flashdata('flash_success', 'Category Status changed successfully');
				redirect(ADMIN_ROOT_URL.'tips_and_tricks_category');
			}
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}		
	}
	function status_active(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('7', $this->headerData['activeAdminDetails']->module_access)){
			$tips_and_tricks_categoryId =  $this->uri->segment(4);
			if($tips_and_tricks_categoryId == ''){
				redirect(ADMIN_ROOT_URL.'tips_and_tricks_category');
			}else{
				$this->tips_and_tricks_category_model->changeStatus(1,$tips_and_tricks_categoryId);
				$this->session->set_flashdata('flash_success', 'Category Status changed successfully');
				redirect(ADMIN_ROOT_URL.'tips_and_tricks_category');
			}
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
		
	}
	function delete(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('7', $this->headerData['activeAdminDetails']->module_access)){
			$tips_and_tricks_categoryId =  $this->uri->segment(4);
			
				$this->tips_and_tricks_category_model->deleteRecord($tips_and_tricks_categoryId);
				$this->session->set_flashdata('flash_success', 'Category deleted successfully');
				redirect(ADMIN_ROOT_URL.'tips_and_tricks_category');
			
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
	}
	
	function add(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('7', $this->headerData['activeAdminDetails']->module_access)){
			$tips_and_tricks_categoryId =  $this->uri->segment(4);
			$action = 'Add';
			if($tips_and_tricks_categoryId == ''){
				$action = 'Add';
				$this->contentData['tips_and_tricks_categoryDetails'] = array();
			}else{
				$action = 'Edit';
				$tipsAndTricksCategoryDetails = $this->tips_and_tricks_category_model->getDetails($tips_and_tricks_categoryId);
				$this->contentData['tipsAndTricksCategoryDetails'] = $tipsAndTricksCategoryDetails;
			}
			        
			if($this->input->post()){
				
				$this->load->helper(array('form', 'url'));
				
				$this->form_validation->set_error_delimiters('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>', '</div>');
				$this->form_validation->set_rules('title', 'Title', 'trim|required');
				if(isset($_FILES['image_path']) && $_FILES['image_path']['name']!=''){
					$this->form_validation->set_rules('image_path', 'Image', 'trim|callback_upload_image');
					
				}else{
					$_POST['image_path']	= (isset($_POST['uploaded_file']) && $_POST['uploaded_file'] != '') ? $_POST['uploaded_file'] : '';	
				}
					
				if ($this->form_validation->run() == TRUE)
				{
                    if($this->input->post('action') == 'Add') {
                        $data = $this->input->post();
                        $categoryData['title'] = $data['title'];
                        $categoryData['is_active'] = $data['is_active'];
                        $categoryData['created_date'] = date('Y-m-d H:i:s');
                        $categoryData['created_by'] = $this->user_id;
                        $categoryId = $this->tips_and_tricks_category_model->addDetails($categoryData);
						if($categoryId){
							$this->session->set_flashdata('flash_success', 'Category Details Added successfully');
							redirect(ADMIN_ROOT_URL.'tips_and_tricks_category');
						}
					}else{
                        $data = $this->input->post();
                        $categoryData['title'] = $data['title'];
                        $categoryData['is_active'] = $data['is_active'];
                        $categoryData['updated_date'] = date('Y-m-d H:i:s');
                        $categoryData['updated_by'] = $this->user_id;
                        $updateStatus = $this->tips_and_tricks_category_model->updateDetails($categoryData, $tips_and_tricks_categoryId);

						if($updateStatus){
							$this->session->set_flashdata('flash_success', 'Category Details Updated successfully');
							redirect(ADMIN_ROOT_URL.'tips_and_tricks_category');
						}
					}
				}else{
                        $_SESSION = $_POST;
				}
				
			}
			$this->contentData['action'] = $action;
			$this->headerData['title']= $action.' Tips and Tricks Category | Admin Module';
			$this->load->view('admin/templates/header', $this->headerData);
			$this->load->view('admin/add_tips_and_tricks_category', $this->contentData);
			$this->load->view('admin/templates/footer', $this->footerData);
			
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
	}
	function order(){
		
		$updateStatus = $this->tips_and_tricks_category_model->changeOrder($_REQUEST['id'],$_REQUEST['tips_and_tricks_category_order'],$_REQUEST['position']);
		$this->session->set_flashdata('flash_success', 'Category Order Updated successfully');
			redirect(ADMIN_ROOT_URL.'tips_and_tricks_category');
								
	}
	
	public function index()
	{
		$this->load->library('session');
		$pId = $this->uri->segment(4);
		$parentId =  (isset($pId) && $pId != '') ? $this->uri->segment(4) : 0;
		
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('7', $this->headerData['activeAdminDetails']->module_access)){
		
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
		
		if($this->session->userdata('admin_id')==''){
			redirect(ADMIN_ROOT_URL.'login');
		}else{
			
			$this->contentData['imageList'] = $this->tips_and_tricks_category_model->getAllRecords('*' ,'',' ORDER BY category_order ASC');
			$succ_msg = $this->session->flashdata('flash_success');
			$err_msg = $this->session->flashdata('flash_error');
			if(isset($succ_msg) && $succ_msg != ''){				
				$this->contentData['successMsg'] = $this->session->flashdata('flash_success');				
			}
			if(isset($err_msg) && $err_msg != ''){				
				$this->contentData['errMsg'] = $this->session->flashdata('flash_error');				
			}
			$this->headerData['title']= ' Tips and Tricks Category List | Admin Module';
			$this->load->view('admin/templates/header', $this->headerData);
			$this->load->view('admin/tips_and_tricks_category_list', $this->contentData);
			$this->load->view('admin/templates/footer', $this->footerData);
		}
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
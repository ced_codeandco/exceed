<?php
/**
 * Created by PhpStorm.
 * User: Anas
 * Date: 5/6/2015
 * Time: 6:40 PM
 */

require APPPATH . 'libraries/Public_controller.php';

class Blog extends Public_controller {

    public $headerData;
    public $contentData;
    public $footerData;
    const TIPS_PER_PAGE = 5;
    const RECENT_TIPS_PER_PAGE = 3;
    const RECENT_COMMENTS_PER_PAGE = 5;
    public function __construct()
    {
        parent::__construct();
    }


    public function index()
    {
        $this->load->library('session');
        $this->load->model(array('tips_and_tricks_category_model', 'tips_and_tricks_model'));
        $this->headerData['title']= 'Homepage';
        $succ_msg = $this->session->flashdata('flash_success');
        $err_msg = $this->session->flashdata('flash_error');
        if(isset($succ_msg) && $succ_msg != ''){
            $this->contentData['successMsg'] = $this->session->flashdata('flash_success');
        }
        if(isset($err_msg) && $err_msg != ''){
            $this->contentData['errMsg'] = $this->session->flashdata('flash_error');
        }
        $searchCriteria = $this->input->get();
        $starts_with = !empty($searchCriteria['per_page']) ? $searchCriteria['per_page'] : 0;
        $searchString = $this->tips_and_tricks_model->build_search_string($searchCriteria);

        $searchResultTotal = $this->tips_and_tricks_model->getSearchResultTotal($searchString);
        $paginator = $this->tips_and_tricks_model->build_search_paginator($searchResultTotal, self::TIPS_PER_PAGE, $searchCriteria);
        $this->contentData['searchResultTotal'] = $searchResultTotal;
        $this->contentData['paginator'] = $paginator;

        $this->contentData['searchResult'] = $this->tips_and_tricks_model->getSearchResult(
            $searchString,self::TIPS_PER_PAGE,$starts_with, 'created_date_time DESC'
        );

        //$this->contentData['categoryQueryString'] = $this->tips_and_tricks_model->getCategoryQueryString($searchCriteria);

        $this->contentData['recentTips'] = $this->tips_and_tricks_model->getRecentTips(self::RECENT_TIPS_PER_PAGE);
        $this->contentData['recentComments'] = $this->tips_and_tricks_model->getRecentComments(self::RECENT_COMMENTS_PER_PAGE);

        $this->contentData['tipsCategory'] = $this->tips_and_tricks_category_model->getCategoryHierarchy();
        $this->contentData['searchCriteria'] = $searchCriteria;
        $this->load->view('templates/header', $this->headerData);
        $this->load->view('blog', $this->contentData);
        $this->load->view('templates/footer', $this->footerData);
    }

    public function read_blog($tip_slug) {
        $this->load->library('session');
        $this->load->model(array('tips_and_tricks_category_model', 'tips_and_tricks_model'));
        $this->headerData['title']= 'Homepage';
        $succ_msg = $this->session->flashdata('flash_success');
        $err_msg = $this->session->flashdata('flash_error');
        if(isset($succ_msg) && $succ_msg != ''){
            $this->contentData['successMsg'] = $this->session->flashdata('flash_success');
        }
        if(isset($err_msg) && $err_msg != ''){
            $this->contentData['errMsg'] = $this->session->flashdata('flash_error');
        }
        $searchCriteria = $this->input->get();
        $starts_with = !empty($searchCriteria['per_page']) ? $searchCriteria['per_page'] : 0;
        $searchString = " AND tt.url_slug='$tip_slug' ";
        $searchResultTotal = $this->tips_and_tricks_model->getSearchResultTotal($searchString);
        $paginator = '';

        $searchResult = $this->tips_and_tricks_model->getSearchResult(
            $searchString,self::TIPS_PER_PAGE,$starts_with
        );
        $searchResult = array_shift($searchResult);
        $this->contentData['searchResult'] = $searchResult;
        $tip_id = $searchResult->id;
        $this->contentData['categoryQueryString'] = $this->tips_and_tricks_model->getCategoryQueryString($searchCriteria);
        $this->contentData['paginator'] = $paginator;
        $this->contentData['tipsCategory'] = $this->tips_and_tricks_category_model->getCategoryHierarchy();
        $this->load->model('admin_model');
        //$this->contentData['cityList'] = $this->admin_model->getCityListByCountryId(DEFAULT_COUNTRY_ID);

        if ($this->input->post('comments') && $this->input->post('tip_id')) {
            $this->tips_and_tricks_model->postComments($this->input->post('comments'), $tip_id, $this->user_id);
            $this->session->set_flashdata('flash_success', 'Your comment submitted successfully !!');
            $redirectUrl = getCurrentUrl();

            redirect(ROOT_URL.'read-blog/'.$tip_slug);
        }

        $this->contentData['isMemberLogin'] = $this->headerData['isMemberLogin'];
        $this->contentData['userComments'] = $this->tips_and_tricks_model->getComments($tip_id);
        $this->contentData['tip_id'] = $tip_id;

        $this->contentData['succMsg'] = $this->session->flashdata('flash_success');
        $this->contentData['errMsg'] = $this->session->flashdata('flash_error');

        $this->contentData['searchCriteria'] = $searchCriteria;

        $this->contentData['recentTips'] = $this->tips_and_tricks_model->getRecentTips(self::RECENT_TIPS_PER_PAGE);
        $this->contentData['recentComments'] = $this->tips_and_tricks_model->getRecentComments(self::RECENT_COMMENTS_PER_PAGE);
        $this->headerData['ogTitle'] = $searchResult->title;

        if(!empty($searchResult->image) && file_exists(DIR_UPLOAD_BLOG.$searchResult->image)) {
            $this->headerData['ogImage'] = DIR_UPLOAD_BLOG_SHOW.$searchResult->image;
        } else {
            $this->headerData['ogImage'] = ROOT_URL_BASE.'images/Featured-Ads-01.jpg';
        }

        $this->load->view('templates/header', $this->headerData);
        $this->load->view('read_blog', $this->contentData);
        $this->load->view('templates/footer', $this->footerData);
    }

    public function rate_tip()
    {
        $this->load->model('tips_and_tricks_model');
        $averageRate = $this->tips_and_tricks_model->postTipRate($this->input->post('tipId'), $this->input->post('rate'), $this->user_id);
        echo json_encode(array('status' => 1, 'average' => $averageRate));
    }
}
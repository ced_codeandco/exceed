<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH . 'libraries/Public_controller.php';

class Cms extends Public_controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public $headerData;
    public $contentData;
    public $footerData;
    public function __construct()
    {
        parent::__construct();
    }
    public function index($page_slug)
    {
        $this->load->library('session');
        $wrapperType = $this->uri->segment(3);
        $this->headerData['title']= 'Homepage';
        $succ_msg = $this->session->flashdata('flash_success');
        $err_msg = $this->session->flashdata('flash_error');
        if(isset($succ_msg) && $succ_msg != ''){
            $this->contentData['successMsg'] = $this->session->flashdata('flash_success');
        }
        if(isset($err_msg) && $err_msg != ''){
            $this->contentData['errMsg'] = $this->session->flashdata('flash_error');
        }
        if ($cmsData = $this->cms_model->getPageDetails($page_slug)) {
//            print_r($cmsData);
            $this->contentData['cmsData'] = $cmsData;
            $this->contentData['show404'] = false;
        } else {
            $this->contentData['show404'] = true;
        }


            /*$this->contentData['bannerImages'] = $this->home_page_image_model->getAllRecords('*'," is_active = '1'",'ORDER BY home_page_image_order ASC');
            $this->contentData['featuredClassified'] = $this->classified_model->getAllRecords('id,title,classified_slug,small_description,amount,classified_city,category_id'," is_active = '1' AND is_futured = '1' ",'ORDER BY RAND()','LIMIT 0,4');
            $this->contentData['advertize'] = $this->advertize_model->getRandomAdvt();
            $this->contentData['brandList'] = $this->brand_model->getAllRecords('id,title'," is_active='1'");
            $contentSelectData = $this->category_model->getClassifiedCategoryList(0);
            $this->contentData['countryList'] = $this->admin_model->getCountryList();
            $this->contentData['contentSelectData'] = $contentSelectData;*/

        if (!empty($wrapperType) && $wrapperType =='ajax') {
            //$this->load->view('templates/header_ajax', $this->headerData);
            $this->load->view('ajax-cms', $this->contentData);
            //$this->load->view('templates/footer_ajax', $this->footerData);
        } else {
            $this->load->view('templates/header', $this->headerData);
            $this->load->view('cms', $this->contentData);
            $this->load->view('templates/footer', $this->footerData);
        }

        /*$this->load->view('templates/header', $this->headerData);
        $this->load->view('cms', $this->contentData);
        $this->load->view('templates/footer', $this->footerData);*/
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
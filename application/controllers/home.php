<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH . 'libraries/Public_controller.php';

class Home extends Public_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 public $headerData;
	 public $contentData;
	 public $footerData;
	 public function __construct()
	{
		parent::__construct();
	}
	public function index()
	{
		$this->load->library('session');
		$this->headerData['title']= 'Homepage';
		$succ_msg = $this->session->flashdata('flash_success');
		$err_msg = $this->session->flashdata('flash_error');
		if(isset($succ_msg) && $succ_msg != ''){
			$this->contentData['successMsg'] = $this->session->flashdata('flash_success');
		}
		if(isset($err_msg) && $err_msg != ''){
			$this->contentData['errMsg'] = $this->session->flashdata('flash_error');
		}

		$this->contentData['bannerImages'] = $this->home_page_image_model->getAllRecords('*'," is_active = '1'",'ORDER BY home_page_image_order ASC');
		$this->contentData['featuredClassified'] = $this->classified_model->getAllRecords('id,title,classified_slug,small_description,amount,classified_city,category_id'," is_active = '1' AND is_futured = '1' ",'ORDER BY RAND()','LIMIT 0,4');
		$this->contentData['latestClassified'] = $this->classified_model->getAllRecords('id,title,classified_slug,small_description,amount,classified_city,category_id'," is_active = '1' ",'ORDER BY  created_date_time DESC','LIMIT 0,12');
		$this->contentData['popularClassified'] = $this->classified_model->getAllRecords('id,title,classified_slug,small_description,amount,classified_city,category_id'," is_active = '1' ",'ORDER BY  number_of_hits DESC','LIMIT 0,12');
		$this->contentData['randomClassified'] = $this->classified_model->getAllRecords('id,title,classified_slug,small_description,amount,classified_city,category_id'," is_active = '1'  ",'ORDER BY RAND()','LIMIT 0,12');
		$this->contentData['advertize'] = $this->advertize_model->getRandomAdvt();
        $this->contentData['categoryList'] = $this->category_model->getSubOrCategoryHierarchy(true);

		$this->load->view('templates/header', $this->headerData);
		$this->load->view('home', $this->contentData);
		$this->load->view('templates/footer', $this->footerData);
	}
	function forgot_password(){

		if($this->input->post()){
			$isLogin = $this->member_model->getDetailsFromEmail($this->input->post('email'));
			if(count($isLogin) > 0){
			$message = "<p>Dear ".$isLogin->first_name.",<br/><br/>Please use the below link to update your password</p>";
            $message .= '<p><a target="_blank" href="'.ROOT_URL."reset_password/".base64_encode($isLogin->email).'">'.ROOT_URL."reset_password/".base64_encode($isLogin->email)."</a></p>";

			  $subject = 'Password recovery link for  '.SITE_NAME;
			$content = '';
			$content .= $this->admin_model->emailHeader();
			$content .= $message;
			$content .= $this->admin_model->emailFooter();
			$email = $this->admin_model->sendEmail($isLogin->first_name, SITE_NAME, $isLogin->email , DEFAULT_EMAIL, $subject, $content);
			if($email){
				$this->contentData['succMsg'] = 'Email sent successfully !! Please check your inbox.';
				redirect(ROOT_URL.'login', 'refresh');
			}else{
				$this->contentData['errMsg'] = 'Email Error Please try again later !!!!';
			}
			}else{
				$this->contentData['errMsg'] = 'Invalid Email Address';
			}
		}
		if($this->session->userdata('id')==''){

			$this->headerData['title']= 'Forgot Password';
			$this->load->view('templates/header', $this->headerData);
			$this->load->view('forgot_password', $this->contentData);
			$this->load->view('templates/footer', $this->footerData);
		}else{
			redirect(ROOT_URL, 'refresh');
		}


	}

    public function sellers(){

        $this->load->view('templates/header', $this->headerData);
        $this->load->view('sellers', $this->contentData);
        $this->load->view('templates/footer', $this->footerData);

    }

    public function categories_listing() {

        $this->contentData['categoryList'] = $this->category_model->getCategoryHierarchy();

        $this->load->view('templates/header', $this->headerData);
        $this->load->view('categories_listing', $this->contentData);
        $this->load->view('templates/footer', $this->footerData);
    }

    public function location_listing() {

        $this->load->model('locality_model');
        list($this->contentData['cityClassifiedsCount'], $this->contentData['localityClassifiedsCount']) = $this->locality_model->getLocalityHierarchy();
        $this->contentData['cityList'] = $this->headerData['cityList'];
        //print_r($this->contentData['categoryList']);

        $this->load->view('templates/header', $this->headerData);
        $this->load->view('location_listing', $this->contentData);
        $this->load->view('templates/footer', $this->footerData);

    }

	function getmodel(){

		$brandDetails = $this->brand_model->getDetailsFromTitle($_POST['brand_name']);
		$optionData = '<select name="model_id" id="model"><option value=""> Select Model</option>';
		if($brandDetails && count($brandDetails) > 0) {
			$modelDetails = $this->model_model->getAllRecords('id,title'," brand_id=".$brandDetails->id);
			foreach($modelDetails as $model){
				$optionData.= '<option value="'.$model->title.'">'.$model->title.'</option>';
			}

		}
		$optionData.="</select>";
		echo $optionData;
		exit;
	}

    function loadLookupDropDowns()
    {
        $lookupName = !empty($_REQUEST['lookupType']) ? $_REQUEST['lookupType'] : 'subCategory';
        $fieldName = !empty($_REQUEST['fieldName']) ? $_REQUEST['fieldName'] : $lookupName;
        $selected = !empty($_REQUEST['selected']) ? $_REQUEST['selected'] : '';
        $selectedText = !empty($_REQUEST['selectedText']) ? $_REQUEST['selectedText'] : '';
        $parentId = !empty($_REQUEST['parentId']) ? $_REQUEST['parentId'] : '';
        $extra = !empty($_REQUEST['extra']) ? $_REQUEST['extra'] : '';

        $inputName = '';
        $options = array( '' => !empty($selectedText) ? $selectedText : 'Select');
        switch ($lookupName) {
            case 'subCategory' :
                if (!empty($parentId)) {
                    $options = $options + $this->category_model->getSubCategoriesLookup($parentId, true);
                }
                break;
            case 'locality':
                if (!empty($parentId)) {
                    $this->load->model('locality_model');
                    $options = $options + $this->locality_model->getLocalityLookup($parentId, true);
                }
                break;
            case 'model_id':
                if (!empty($parentId)) {
                    $this->load->model('model_model');
                    $options = $options + $this->model_model->getModelLookup($parentId, true);
                }
                break;
        }

        echo form_dropdown($fieldName, $options, $selected, $extra);
    }

    public function subscribe()
    {
        $email = $this->input->post('email');
        if ($result = $this->admin_model->subscribeEmail($email)) {
            $response = array('status' => 1, 'msg' => "You've been subscribed to our news letter");
        } else {
            $response = array('status' => 0, 'msg' => "You've already subscribed to our news letter");
        }

        echo json_encode($response);
    }

	public function login()
	{

		if($this->input->post()){
			$isLogin = $this->member_model->memberLogin();
			if($isLogin != 0){
			$adminDetails = $this->member_model->getMemberDetails($isLogin);
			$data = array(
				'display_name' => $adminDetails->first_name.' '.$adminDetails->last_name,
               	'id' => $adminDetails->id,
				'email' => $adminDetails->email,
				'module_access' => $adminDetails->module_access,
				'is_logged_in' => true
			);
			$this->session->set_userdata($data);

			redirect(ROOT_URL);
			}else{
				$this->contentData['errorMessage'] = 'Invalid Email or Password';
			}
		}
		if($this->session->userdata('id')==''){

			$succ_msg = $this->session->flashdata('flash_success');
			$err_msg = $this->session->flashdata('flash_error');
			if(isset($succ_msg) && $succ_msg != ''){
				$this->contentData['succMsg'] = $this->session->flashdata('flash_success');
			}
			if(isset($err_msg) && $err_msg != ''){
				$this->contentData['errMsg'] = $this->session->flashdata('flash_error');
			}

			$this->headerData['title']= 'Login';
			$this->load->view('templates/header', $this->headerData);
			$this->load->view('login', $this->contentData);
			$this->load->view('templates/footer', $this->footerData);
		}else{
			redirect(ROOT_URL, 'refresh');
		}

	}

	public function reset_password()
	{
		$email = base64_decode($this->uri->segment(3));
		$this->contentData['email'] = $email;
		if($email && $email != '') {
			if($this->input->post()){
				$this->load->helper(array('form', 'url'));
					$this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
					$this->form_validation->set_rules('password', 'Password', 'trim|required');
					$this->form_validation->set_rules('retype_password', 'Password again', 'trim|required');

					if ($this->form_validation->run() == TRUE)
					{
						$isLogin = $this->member_model->updatePassword();
						$this->contentData['successMessage'] = 'Password updated successfully';
						redirect(ROOT_URL.'login', 'refresh');
					}else{
					}

			}
			if($this->session->userdata('id')==''){

				$this->headerData['title']= 'Reset Password';
				$this->load->view('templates/header', $this->headerData);
				$this->load->view('reset_password', $this->contentData);
				$this->load->view('templates/footer', $this->footerData);
			}else{
				redirect(ROOT_URL, 'refresh');
			}
		}else{
			redirect(ROOT_URL, 'refresh');
		}
	}

	function logout(){
		$logout=$this->member_model->adminLogout();
		if($logout == TRUE)	{
			session_start();
			session_destroy();

			redirect(ROOT_URL);
		}
	}
	public function no_access(){
		$this->headerData['title']= 'Access Denied';
		$this->load->view('templates/header', $this->headerData);
		$this->load->view('no_access', $this->contentData);
		$this->load->view('templates/footer', $this->footerData);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
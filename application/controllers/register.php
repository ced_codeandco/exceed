<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH . 'libraries/Public_controller.php';

class Register extends Public_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 public $headerData;
	 public $contentData;
	 public $footerData;
	 public function __construct()
	{
		parent::__construct();
	}
	public function index()
	{
		$this->load->library('session');
        $this->load->model('cms_model');
        if ($this->is_logged_in && !empty($this->is_member)) {
            redirect(ROOT_URL);
        }
		$this->headerData['title']= 'Member::Register';
		$succ_msg = $this->session->flashdata('flash_success');
		$err_msg = $this->session->flashdata('flash_error');
		if(isset($succ_msg) && $succ_msg != ''){				
			$this->contentData['successMsg'] = $this->session->flashdata('flash_success');				
		}
		if(isset($err_msg) && $err_msg != ''){				
			$this->contentData['errMsg'] = $this->session->flashdata('flash_error');				
		}

        if ($this->member_model->validate_registration()) {
            //$dob = DateTime::createFromFormat('d M Y', $this->input->post('date_of_birth'));
            //$_POST['date_of_birth'] = $dob->format("Y-m-d");
            $dateOfBirthField = strtotime($this->input->post('date_of_birth'));
            $dob = date('Y-m-d', $dateOfBirthField);
            $_POST['date_of_birth'] = $dob;
            //Register
            $emailVerificationToken = trim(strtolower(base64_encode(uniqid('', true) . "-" . date("YmdHis"))));
            $user_id = $this->member_model->addDetails($emailVerificationToken);
            $this->member_model->sendVerificationEmailOnRegistration($emailVerificationToken, $user_id);
            $this->session->set_flashdata('flash_success', 'Registration successfull.<br />Please check your inbox for verification email');

            redirect(ROOT_URL.'login');
        } else {
            $this->contentData['privacy_popup_content'] = $this->cms_model->getDetails(CMS_PRIVACY_POPUP_PAGE_ID);
            //var_dump(CMS_PRIVACY_POPUP_PAGE_ID);
            //var_dump($this->contentData['privacy_popup_content']);
            $this->contentData['terms_popup_content'] = $this->cms_model->getDetails(CMS_TERMS_POPUP_PAGE_ID);
            $this->contentData['formData'] = $this->input->post();
            $countryList = $this->admin_model->getCountryList();
            $currentCountry = !empty($countryList[0]) ? array_shift($countryList) : null;
            $this->contentData['currentCountry'] = $currentCountry;
            //$this->contentData['countryList'] = $this->admin_model->getCountryList();
            $this->contentData['cityList'] = $this->admin_model->getCityList();
            $this->contentData['questionList'] = $this->admin_model->getRandomSecurityQuestions();
            $this->headerData['active_tab']= 'register';
            $this->load->view('templates/header', $this->headerData);
            $this->load->view('register', $this->contentData);
            $this->load->view('templates/footer', $this->footerData);
        }
	}


    /**
     * function to check duplicate email id called from client registration page via AJAX
     *
     * @access public
     * @param string $email
     * @return status & message
     */
    public function check_duplicate_email()
    {
        $return = array();
        $this->form_validation->set_error_delimiters('', '');
        $this->form_validation->set_message('is_unique', '%s is already in use.');
        $this->form_validation->set_rules('email', 'Email Address', 'xss_clean|trim|required|valid_email|is_unique['.Member_model::$user_table.'.email]');
        if($this->form_validation->run() === FALSE)
        {
            $return = array('status' => 1, 'message' => form_error('email'));
        }
        else
        {
            $return = array('status' => 0, 'message' => '');
        }
        echo json_encode($return);
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH . 'libraries/Public_controller.php';

class Search extends Public_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 public $headerData;
	 public $contentData;
	 public $footerData;
	 public function __construct()
	{
		parent::__construct();
	}

    /**
     * Function to display search results
     * @param int $cur_page
     * @param string $urlId
     */
	public function index($urlId = '')
	{
		$this->load->library('session');
		$this->headerData['title']= 'Homepage';
		$succ_msg = $this->session->flashdata('flash_success');
		$err_msg = $this->session->flashdata('flash_error');

		if(isset($succ_msg) && $succ_msg != ''){				
			$this->contentData['successMsg'] = $this->session->flashdata('flash_success');				
		}
		if(isset($err_msg) && $err_msg != ''){				
			$this->contentData['errMsg'] = $this->session->flashdata('flash_error');				
		}
        $searchCriteria = $this->input->get();
        $starts_with = !empty($searchCriteria['per_page']) ? $searchCriteria['per_page'] : 0;//($cur_page - 1) * RECORD_PER_PAGE;
		$searchString = $this->classified_model->build_search_string($searchCriteria);
        $searchResultTotal = $this->classified_model->getSearchResultTotal($searchString);
        $paginator = $this->classified_model->build_search_paginator($searchResultTotal, RECORD_PER_PAGE, $searchCriteria);
        $result_order = $this->classified_model->build_search_order($searchCriteria);
        $order_by = $result_order['order_query'];
        $order_by_modifier = '';


        $this->contentData['searchCriteria'] = $searchCriteria;
        $searchedCategory = !empty($searchCriteria['category']) ? $this->category_model->getDetails($searchCriteria['category']) : false;
        $this->classified_model->saveCategoryBrandSearch($searchedCategory);
        $this->contentData['searchedCategoryString'] = (!$searchedCategory) ? 'Any' : $searchedCategory->title;
        list($this->contentData['searchPageHeader'], $this->contentData['breadCrumbsData']) = $this->classified_model->set_search_page_header($searchCriteria, $searchedCategory);
        //$this->contentData['searchPageBreadCrumbs'] = $this->classified_model->set_search_page_breadCrumbs($searchCriteria, $searchedCategory);
        $this->contentData['sortOption'] = $result_order['filters'];
        $this->contentData['sortUrl'] = $result_order['sortUrl'];
        $this->contentData['order'] = !empty($searchCriteria['order']) ? $searchCriteria['order'] : 1;
        $this->contentData['city_distance_filter'] = $this->classified_model->get_distance_from_city_options($searchCriteria);
        $this->contentData['keyword_search_url'] = $this->classified_model->get_keyword_search_url($searchCriteria);
        $this->contentData['save_search_query_string'] = $this->classified_model->get_save_search_query_string($searchCriteria);
        $this->contentData['back_url_query_string'] = $this->classified_model->get_back_url_query_string($searchCriteria);
        if ($this->is_logged_in) {
            $this->contentData['user_id'] = $this->user_id;
        }
        $this->contentData['searchResultTotal'] = $searchResultTotal;
        $this->contentData['paginator'] = $paginator;
		$this->contentData['searchResult'] = $this->classified_model->getSearchResult(
            'id,title,model_id, brand_id,classified_slug,classified_tags,category_id,description,small_description,amount,manufacture_year,fuel_type,created_date_time,classified_address,classified_land_mark,running_km,classified_city,classified_locality,career_level,experience_years,education,employment_type,bedrooms,bathrooms,area_sqft',
            $searchString,RECORD_PER_PAGE,$starts_with, $order_by, $order_by_modifier
        );

        $this->contentData['advertize'] = $this->advertize_model->getRandomAdvt();
		$this->contentData['cityLookup'] = $this->admin_model->getCityLookup();

		$this->contentData['modelLookup'] = $this->model_model->getModelHierarchy();
		$this->contentData['brandLookup'] = $this->brand_model->getBrandLookup(null, true);
        $this->contentData['fuelLookUp'] = $this->admin_model->getFuelLookUp();
        $this->contentData['transmissionLookUp'] = $this->admin_model->getTransmissionLookUp();

        $this->contentData['career_levelLookUp'] = $this->admin_model->prepareLookUpItems('tbl_career_level');
        $this->contentData['educationLookUp'] = $this->admin_model->prepareLookUpItems('tbl_education');
        $this->contentData['employment_typeLookUp'] = $this->admin_model->prepareLookUpItems('tbl_employment_type');


		$this->load->view('templates/header', $this->headerData);
		$this->load->view('search_page', $this->contentData);
		$this->load->view('templates/footer', $this->footerData);

	}

    public function details($classified_slug = '')
    {
        if (!empty($classified_slug)) {
            $back = $this->input->get('back');
            $this->classified_model->getBackUrlParams($back);
            $classifiedDetails = $this->classified_model->prepareClassifiedSearch($classified_slug, $back);
            $this->load->model('brand_model');
            $this->load->model('model_model');
            $this->load->model('inquiry_model');

            if ($this->inquiry_model->validate_inquiry_forms()) {
                $this->session->set_flashdata('flash_success', 'Your enquiry submitted successfully !!');

                $redirectUrl = getCurrentUrl();
                $redirectUrl = ROOT_URL.'details/'.$classified_slug.(!empty($back) ? '?back='.$back : '');

                redirect($redirectUrl);
            } else {
                if (!empty($classifiedDetails['classifiedDetails']->id) && (empty($this->user_id) OR $this->user_id != $classifiedDetails['classifiedDetails']->created_by)){
                    $this->classified_model->updateHitCount($classifiedDetails['classifiedDetails']->id);
                }

                $this->contentData['succMsg'] = $this->session->flashdata('flash_success');
                $this->contentData['errMsg'] = $this->session->flashdata('flash_error');
                //$back
                $searchCriteria = $this->classified_model->getSearchCriteriaFromBack($back);
                if (!empty($searchCriteria['brand'])) {
                    $brandDetails = $this->brand_model->getDetailsFromTitle($searchCriteria['brand']);
                    if ($brandDetails && count($brandDetails) > 0) {
                        $this->contentData['modelDetails'] = $this->model_model->getAllRecords('id,title', " brand_id=" . $brandDetails->id);
                    }
                }
                $this->contentData['inquiry_member_id'] = $this->user_id;
                $this->contentData['searchCriteria'] = $searchCriteria;
                $this->contentData['classifiedDetails'] = $classifiedDetails['classifiedDetails'];
                $this->contentData['city'] = $classifiedDetails['classifiedDetails']->city;
                $this->contentData['locality'] = $classifiedDetails['classifiedDetails']->locality;
                $this->contentData['classifiedImages'] = $classifiedDetails['classifiedImages'];
                $this->contentData['classifiedFiles'] = $classifiedDetails['classifiedFiles'];
                $this->contentData['userDetails'] = $this->headerData['isMemberLogin'];

                $this->contentData['cityList'] = $this->admin_model->getCityList();
                $this->contentData['brandList'] = $this->brand_model->getAllRecords('id,title', " is_active='1'");
                $this->contentData['brandList'] = $this->brand_model->getAllRecords('id,title', " is_active='1'");
                $this->contentData['transmissionLookUp'] = $this->admin_model->getTransmissionLookUp();

                $this->contentData['career_levelLookUp'] = $this->admin_model->prepareLookUpItems('tbl_career_level');
                $this->contentData['educationLookUp'] = $this->admin_model->prepareLookUpItems('tbl_education');
                $this->contentData['employment_typeLookUp'] = $this->admin_model->prepareLookUpItems('tbl_employment_type');

                //list($this->contentData['searchPageHeader'], $this->contentData['breadCrumbsData']) = $this->classified_model->set_search_page_header($searchCriteria, $searchedCategory);
                $this->load->view('templates/header', $this->headerData);
                $this->load->view('details', $this->contentData);
                $this->load->view('templates/footer', $this->footerData);
            }
        } else {
            $this->contentData['404'] = true;
            $this->load->view('templates/header', $this->headerData);
            $this->load->view('404', $this->contentData);
            $this->load->view('templates/footer', $this->footerData);
        }
    }


    function getmodel(){
		
		
		$brandDetails = $this->brand_model->getDetailsFromTitle($_POST['brand_name']);
		$optionData = '<select name="model" id="model"><option value=""> Select Model</option>';
		if($brandDetails && count($brandDetails) > 0) {
			$modelDetails = $this->model_model->getAllRecords('id,title'," brand_id=".$brandDetails->id);			
			foreach($modelDetails as $model){
				$optionData.= '<option value="'.$model->title.'">'.$model->title.'</option>';
			}
			
		}
		$optionData.="</select>";
		echo $optionData;
		exit;
	}
	
	public function no_access(){
		$this->headerData['title']= 'Access Denied';
		$this->load->view('templates/header', $this->headerData);
		$this->load->view('no_access', $this->contentData);
		$this->load->view('templates/footer', $this->footerData);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
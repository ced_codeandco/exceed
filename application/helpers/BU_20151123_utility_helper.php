<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Classifie Utility Helpers
 *
 * @package		Classifie
 * @subpackage	Helpers
 * @category	Helpers
 * @author		Anas
 */

// ------------------------------------------------------------------------
	/**
	 * Random String Generation
	 *
	 * @access	public
	 * @param	void
	 * @return	string	
	 */
	function gen_random_string($length = 10) {
		
		$characters = '0123456789abcdefghijklmnopqrstuvwxyz';
		$string = "";
		for ($p = 0; $p < $length; $p++) 
		{
			$string .= $characters[mt_rand(0, strlen($characters)-1)];
		}
		return $string;

	}

	/**
	 * Function to return proper error messages while uploading files.
	 *
	 * @param $code
	 * @author Anas
	 */
	function decode_upload_error( $code ) {
        switch ($code) {
            case UPLOAD_ERR_INI_SIZE:
                $message =  'The uploaded file exceeds the upload_max_filesize ('. ( (int)(ini_get('upload_max_filesize')) ."MB" ).') directive in php.ini';
                break;
            case UPLOAD_ERR_FORM_SIZE:
                $message = 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form';
                break;
            case UPLOAD_ERR_PARTIAL:
                $message = 'The uploaded file was only partially uploaded';
                break;
            case UPLOAD_ERR_NO_FILE:
                $message = 'No file was uploaded';
                break;
            case UPLOAD_ERR_NO_TMP_DIR:
                $message = 'Missing a temporary folder';
                break;
            case UPLOAD_ERR_CANT_WRITE:
                $message = 'Failed to write file to disk';
                break;
            case UPLOAD_ERR_EXTENSION:
                $message = 'File upload stopped by extension';
                break;
            default:
                $message = 'Unknown upload error';
                break;
        }
        return $message;
	}
	/**
     * Validate URL 
     *
     * @access public
     * @param  string
     * @return string
     */
    function valid_url($url)
    {
    	return ( ! preg_match('/^(http|https|ftp):\/\/([A-Z0-9][A-Z0-9_-]*(?:\.[A-Z0-9][A-Z0-9_-]*)+):?(\d+)?\/?/i', $url)) ? FALSE : TRUE;
    }

	/**
	 * Method is used to validate strings to allow alpha numeric spaces underscores and dashes ONLY.
	 *@param $str    String    The item to be validated.
	 *@return BOOLEAN   True if passed validation false if otherwise. 
	 */
	function alpha_numeric_dash_space($str = '')
	{
		if (preg_match('!^[\w .-]*$!', $str)) {
			return TRUE; 
		}
		return FALSE;
	}
	/**
	 * Method is used to validate strings to allow alphabets and spaces ONLY.
	 *@param $str    String    The item to be validated.
	 *@return BOOLEAN   True if passed validation false if otherwise. 
	 */
	function alpha_space($str_in = '')
	{
		
	    if (preg_match('/[^ A-Za-z]/', $str_in))
	    {
	        return FALSE;
	    }
	    else
	    {
	        return TRUE;
	    }
	}  
	/**
	 * Method is used to validate strings to allow alpha spaces underscores and dashes ONLY.
	 *@param $str    String    The item to be validated.
	 *@return BOOLEAN   True if passed validation false if otherwise. 
	 */
	function alpha_dash_space($str_in = '')
	{
	    if ( ! preg_match('/[^ A-Za-z]/', $str_in))
	    {
	        $this->form_validation->set_message('alpha_dash_space', 'The %s field may only contain alphabets, spaces, underscores, and dashes.');
	        return FALSE;
	    }
	    else
	    {
	        return TRUE;
	    }
	}  
	/**
	 * Truncate the input content with sentence breaks.
	 * 
	 */
	function content_truncate($string, $limit = 150 , $break = ".", $pad = "", $strict = FALSE) 
	{
		if(strlen($string) <= $limit) return $string;   
		if($strict === TRUE)
		{
			$string = substr($string, 0, $limit) . $pad;
		}
		else if(false !== ($breakpoint = strpos($string, $break, $limit))) 
		{ 
			if($breakpoint < strlen($string) - 1) 
			{ 
				$string = substr($string, 0, $breakpoint) . $pad; 
			} 
		} 
		return $string; 
	}
	/**
	 * Escapes content for insertion into the database using addslashes(), for security.
	 *
	 * Works on arrays.
	 *
	 * @param string|array $data to escape
	 * @return string|array escaped as query safe string
	 */
	function escape( $data ) {
		if ( is_array( $data ) ) {
			foreach ( (array) $data as $k => $v ) {
				if ( is_array( $v ) )
					$data[$k] = escape( $v );
				else
					$data[$k] = _weak_escape( $v );
			}
		} else {
			$data = _weak_escape( $data );
		}

		return $data;
	}

	/**
	 * Weak escape, using addslashes()
	 *
	 * @param string $string
	 * @return string
	 */
	function _weak_escape( $string ) {
		return addslashes( $string );
	}

    /**
	 * 
	 * 
	 * 
	 */
	function process_search($search_keyword = '', $fieldname = '', $operation_mode = '')
	{	
		
		$s = stripslashes($search_keyword);
		preg_match_all('/".*?("|$)|((?<=[\\s",+])|^)[^\\s",+]+/', $s, $matches);
		
		$search_terms = $matches[0];
		
		$searchand = $search = '';
		foreach ( (array) $search_terms as $term ) {
			$term = addslashes($term);
			$search .= "{$searchand} {$fieldname} LIKE '%{$term}%'";
			$searchand = ' OR ';
		}
		
		$term =  escape ( str_replace(array("%", "_"), array("\\%", "\\_"), $s) );
		if ( count($search_terms) > 1 && $search_terms[0] != $s )
			$search .= " OR {$fieldname} LIKE '%{$term}%'";
		
		$search_condition = (!empty($operation_mode)) ? $search.' '.$operation_mode : $search.' ';
		return $search_condition;
	} 
	/**
	 * Function to format csv strings to insert a space after comma.
	 * 
	 * @param string $str.
	 * @access public.
	 * @return string.
	 */
	function format_csv_string($str)
	{
		if($str == '')
		{
			return '--';
		}
		else
		{
			return str_replace('|', ', ', $str);	
		}
	}
	/**
	 * Function to re-arrange an array of items to be displayed in topdown columns. 
	 * Used to display price range, geographic area in top - down columns.
	 *  
	 * @access public. 
	 * @param Array $input_array.
	 * @param integer $column_count.
	 * @return Array.
	 *  
	 */
	function make_topdown_ordered_array($input_array, $column_count)
	{
		$input_array = array_values($input_array);
		$return_array = FALSE;
		if(is_array($input_array) && count($input_array) > 0)
		{
			$total_count = count($input_array);
			$col_reminder[0] = 0;
			for($i = 1; $i <= $column_count; $i++)
			{
				$col_reminder[$i] = 0;	
				if($total_count % $column_count >= $i){
					$col_reminder[$i] = 1;
				}
			}
			$count_rows = floor($total_count / $column_count);
			$ret_indx = 0;
			for($i = 0; $i <= $count_rows; $i++)
			{
				$array_index = $i;
				for($j = 0; $j < $column_count; $j++)
				{
					if(isset($input_array[$array_index])){
		            	$return_array[$ret_indx] = $input_array[$array_index];
		            	if(is_object($return_array[$ret_indx]))
		            	{
		            		$return_array[$ret_indx]->index = $array_index + 1;
		            	}
		            	else
		            	{
		            		$return_array[$ret_indx]['index'] = $array_index + 1;
		            	}
		            	unset($input_array[$array_index]);
					}
					$ret_indx ++;
					$array_index = $array_index + $count_rows + $col_reminder[$j+1];
				}
			}
		}
		return $return_array;
	}
	/**
	 * Function to get local time based upon timezone
	 * 
	 * @param integer $client_timezone client timezone
	 * @param date/time $current_date date/time value from DB
	 * @param string $format date format
	 * 
	 * @return timezone based local time
	 */
	function get_local_time($client_timezone, $current_date, $format)
	{
		$datetime = new DateTime($current_date);
		$la_time = new DateTimeZone($client_timezone);
		$datetime->setTimezone($la_time);
		
		return $datetime->format($format);
	}

    /*function make_difference_string($date)
    {
        $dStart = new DateTime($date);
        $dEnd  = new DateTime();
        $dDiff = $dStart->diff($dEnd);
        return $dDiff->format('%R%d') . ' days'; // use for point out relation: smaller/greater
        return $dDiff->days;
        return '20 minutes ago '.$date;
    }*/
    function pluralize( $count, $text )
    {
        return $count . ( ( $count == 1 ) ? ( " $text" ) : ( " ${text}s" ) );
    }

    function make_difference_string( $datetime )
    {
        if ($datetime instanceof DateTime) {
            $interval = date_create('now')->diff($datetime);
            $suffix = ($interval->invert ? ' ago' : '');
            if ($v = $interval->y >= 1) return pluralize($interval->y, 'year') . $suffix;
            if ($v = $interval->m >= 1) return pluralize($interval->m, 'month') . $suffix;
            if ($v = $interval->d >= 1) return pluralize($interval->d, 'day') . $suffix;
            if ($v = $interval->h >= 1) return pluralize($interval->h, 'hour') . $suffix;
            if ($v = $interval->i >= 1) return pluralize($interval->i, 'minute') . $suffix;
            return pluralize($interval->s, 'second') . $suffix;
            return '';
        }
    }

    /**
     * Function to crop a string and append a suffix
     *
     * @param $string
     * @param int $length
     * @param string $suffix
     *
     * @return string
     */
    function cropString($string, $length = 10, $suffix = '...')
    {
        return (strlen($string) > $length) ? substr($string, 0, $length) . $suffix : $string;
    }

    function parseJavascriptSafe($string){
        return str_replace("\n", '\n', str_replace('"', '\"', addcslashes(str_replace("\r", '', (string)$string), "\0..\37'\\")));
    }

    function build_filldata_javascript($fieldData) {

        $javascriptCode = '';
        foreach ( (array)$fieldData as $key => $val ) {
            $val = parseJavascriptSafe($val);
            $javascriptCode .= "\n";
            $javascriptCode .= "\t\tif($('input[name=\"$key\"]').length >0) {\n";
            $javascriptCode .= "\t\t\t$('input[name=\"$key\"]').val(\"$val\");";
            $javascriptCode .= "\n\t\t} else if($('select[name=\"$key\"]').length >0) {\n";
            $javascriptCode .= "\t\t\t$('select[name=\"$key\"]').val(\"$val\");";
            $javascriptCode .= "\n\t\t} else if($('textarea[name=\"$key\"]').length >0) {\n";
            $javascriptCode .= "\t\t\t$('textarea[name=\"$key\"]').val(\"$val\");";
            $javascriptCode .= "\n\t\t} \n";

        }

        return $javascriptCode;
    }

    function searchQueryToTags($searchUrl)
    {
        $searchUrl = !empty($searchUrl) ? urldecode($searchUrl) : '';
        $parsed_url = parse_url($searchUrl);
        $query = html_entity_decode($parsed_url['query']);
        if (!empty($query)) {
            parse_str($query, $queryParts);
        }
        $searchKeys = '';

        if (!empty($queryParts) && is_array($queryParts)) {
            $searchKeys .= '';
            if (isset($queryParts['country']) && $queryParts['country'] != '') {
                $countryName = getLookUpValues('tbl_countries', 'name', 'id', $queryParts['country']);
                $searchKeys .= ' <a href="javascript:void(0)">Country: ' . $countryName .'</a>';
            }
            if (isset($queryParts['category']) && $queryParts['category'] != '' && is_numeric($queryParts['category'])) {
                $categoryName = getLookUpValues('tbl_category', 'title', 'id', $queryParts['category']);
                $searchKeys .= ' <a href="javascript:void(0)">Category: ' . $categoryName . '</a>';
            }
            if (isset($queryParts['brand']) && $queryParts['brand'] != '')
                $searchKeys .= ' <a href="javascript:void(0)">Brand: '. $queryParts['brand'] . '</a>';

            if (isset($queryParts['model']) && $queryParts['model'] != '')
                $searchKeys .= ' <a href="javascript:void(0)">Model: '. $queryParts['brand'] . '</a>';

            if (isset($queryParts['manufacture_year']) && $queryParts['manufacture_year'] != '')
                $searchKeys .= ' <a href="javascript:void(0)">Manufacture year: '. $queryParts['manufacture_year'].'</a>';

            if (isset($queryParts['min_amount']) && $queryParts['min_amount'] != '')
                $searchKeys .= ' <a href="javascript:void(0)">Minimum price: '. $queryParts['min_amount'] .'</a>';

            if (isset($queryParts['max_amount']) && $queryParts['max_amount'] != '')
                $searchKeys .= ' <a href="javascript:void(0)">Maximum price: '.  $queryParts['max_amount'] .'</a>';

            if (isset($queryParts['search_city']) && $queryParts['search_city'] != '') {
                $searchKeys .= ' <a href="javascript:void(0)">City: '. $queryParts['search_city'] . '</a>';
            }

            if (isset($queryParts['search_city']) && $queryParts['search_city'] != '' &&
                isset($queryParts['search_city_range']) && $queryParts['search_city_range'] != ''
            ) {
                $searchKeys .= ' <a href="javascript:void(0)">Within the range of : '. $queryParts['search_city_range'] .' miles </a>';
            }

            if (isset($queryParts['search_keyword']) && $queryParts['search_keyword'] != '') {
                $searchKeys .= ' <a href="javascript:void(0)">Having keywords : '. $queryParts['search_keyword'] . '</a>';
            }
        }

        return  $searchKeys ;
    }

    function getLookUpValues($table, $selectField, $whereField, $whereValue)
    {
        $CI =& get_instance();
        $CI->load->database();
        $query = $CI->db->select($selectField)->from($table)->where($whereField, $whereValue)->limit(1)->get();

        $result = $query->row();

        return $result->$selectField;
    }

    function getCurrentUrl()
    {
        $path = ROOT_URL;
        $path .= (!empty($_SERVER['PATH_INFO']) ? ltrim($_SERVER['PATH_INFO'], "/") : '');
        $path .= (!empty($_SERVER['QUERY_STRING']) ? '?'.$_SERVER['QUERY_STRING'] : '');

        return $path;
    }

/*End of file utility_helper.php. Location application/helper*/
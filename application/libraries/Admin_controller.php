<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_controller extends CI_Controller {

    public $user_id;
    /**
     * Class constructor.
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->user_id = $this->session->userdata('admin_id');
        $is_admin = $this->session->userdata('is_admin');

        if($this->user_id == null || $this->user_id == '') {
            $safe_methods = array('login', 'logout', 'forgot_password', 'reset_password');
            $current_method = $this->router->fetch_method();
            if (!in_array($current_method, $safe_methods)) {

                redirect(ROOT_URL.'administrator/login');
            }
        }
    }
}
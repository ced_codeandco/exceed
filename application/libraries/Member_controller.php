<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Member_controller extends CI_Controller {

    /**
     * Holder for session values
     */
    public $user_id = false;
    public $is_admin = false;
    public $is_member = false;
    public $is_logged_in = false;
    public $headerData;
    public $contentData;
    public $footerData;

    /**
     * Class constructor.
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->helper(array('url','form'));
        $this->load->model('member_model');
        $this->load->library(array('form_validation', 'session'));
        $this->load->model(array('classified_model', 'inquiry_model', 'category_model', 'cms_model', 'setting_model', 'admin_model'));

        $this->headerData['isMemberLogin'] = $this->member_model->checkMemberLogin();
        $this->headerData['activeMemberDetails'] = $this->member_model->activeMemberDetails();
        $this->headerData['headerCMSList'] = $this->cms_model->getCMSPageList('id,title,cms_slug'," id != 1 AND is_active = '1' AND on_header='1' AND parent_id= 0",'ORDER BY cms_order ASC');
        $this->headerData['cityList'] = $this->admin_model->getCityList();
        $this->headerData['parentCategoriesList'] = $this->category_model->getAllRecords('id,title,category_slug,sub_title',"parent_id = '0' AND is_active = '1'",'ORDER BY category_order ASC');
        $this->footerData['footerCMSList'] = $this->cms_model->getCMSPageList('id,title,cms_slug'," id != 1 AND is_active = '1' AND on_footer='1' AND parent_id= 0",'ORDER BY cms_order ASC');
        $this->footerData['aboutUsDetails'] = $this->cms_model->getCMSPageList('small_description, cms_slug'," id = 16 ");

        $this->setting_model->defineAllSettingVariables();
        $this->user_id = $this->session->userdata('id');
        $this->is_member = $this->session->userdata('is_member');
        if (!empty($this->user_id) && $this->is_member == true && $this->is_admin == false) {
            $this->is_logged_in = true;
            $this->headerData['isLoggedIn'] = $this->is_logged_in;
        }

        if($this->user_id == null || $this->user_id == '' || $this->is_member != true) {
            $safe_methods = array('login', 'logout', 'forgot_password', 'reset_password', 'register', 'verify_email', 'facebookLogin');
            $current_method = $this->router->fetch_method();
            if (!in_array($current_method, $safe_methods)) {
                $curerntUrl = current_url();
                $backUrl = base64_encode(str_replace(ROOT_URL, '', $curerntUrl));
                redirect(ROOT_URL.'login'.(!empty($backUrl) ? '?back='.$backUrl : ''));
            }
        }
    }
}
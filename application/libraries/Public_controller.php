<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Public_controller extends CI_Controller {

    public $user_id = false;
    public $is_admin = false;
    public $is_member = false;
    public $is_logged_in = false;
    public $headerData;
    public $contentData;
    public $footerData;

    /**
     * Class constructor.
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->user_id = $this->session->userdata('id');
        $this->is_member = $this->session->userdata('is_member');
        $this->load->model('member_model');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model(
            array(
                'member_model', 'classified_model', 'cms_model', 'admin_model', 'model_model', 'brand_model',
                'advertize_model', 'inquiry_model', 'home_page_image_model', 'setting_model', 'category_model'
            )
        );
        $this->headerData['headerCMSList'] = $this->cms_model->getCMSPageList('id,title,cms_slug'," id != 1 AND is_active = '1' AND on_header='1' AND parent_id= 0",'ORDER BY cms_order ASC');
        $this->headerData['cityList'] = $this->admin_model->getCityList();
        $this->headerData['parentCategoriesList'] = $this->category_model->getAllRecords('id,title,category_slug,sub_title',"parent_id = '0' AND is_active = '1'",'ORDER BY category_order ASC');
        $this->footerData['footerCMSList'] = $this->cms_model->getCMSPageList('id,title,cms_slug'," id != 1 AND is_active = '1' AND on_footer='1' AND parent_id= 0",'ORDER BY cms_order ASC');
        $this->footerData['aboutUsDetails'] = $this->cms_model->getCMSPageList('small_description, cms_slug'," id = 16 ");

        $this->setting_model->defineAllSettingVariables();
        if (!empty($this->user_id) && $this->is_member == true) {
            $this->is_logged_in = true;
            $this->headerData['isLoggedIn'] = $this->is_logged_in;
            $this->headerData['isMemberLogin'] = $this->member_model->checkMemberLogin();
        } else {
            $this->headerData['isMemberLogin'] = false;
        }
    }
}
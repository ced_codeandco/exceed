<?php
class Advertize_model extends CI_Model {

    var $title   = '';
    var $content = '';
    var $date    = '';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }
    
    function addDetails(){
		
		$data = array(
			'title' => $this->input->post('title'),
			'advertize_url' => $this->input->post('advertize_url'),			
			'image_path' => $this->input->post('image_path'),
			'advertize_type' => $this->input->post('advertize_type'),			
			'is_active' => $this->input->post('is_active'),
			'created_date_time' =>date('Y-m-d H:i:s')			
		);
		
		$this->db->insert('tbl_advertize',$data) or die(mysql_error()); 	
		$id=mysql_insert_id();
		return $id;
		
	}
	
	function updateDetails(){
		
		$data = array(
			'title' => $this->input->post('title'),
			'advertize_url' => $this->input->post('advertize_url'),
			'image_path' => $this->input->post('image_path'),
			'advertize_type' => $this->input->post('advertize_type'),
			'is_active' => $this->input->post('is_active'),
			'created_date_time' =>date('Y-m-d H:i:s')
		);
		$this->db->where("id",$this->input->post('id'));
		$this->db->update('tbl_advertize',$data);
		
		
		return true;
		
	}
	function changeStatus($status,$id){
		
		mysql_query("UPDATE tbl_advertize SET is_active = '$status' WHERE id= ".$id."");
		return true;
	}
	function deleteRecord($id){
		mysql_query("DELETE FROM tbl_advertize WHERE id= ".$id."");
		return true;
	}
	function getDetails($id){
		
		$this->db->where('id', $id);		
		$query = $this->db->get('tbl_advertize') or die(mysql_error());
		if($query->num_rows >= 1)
			return $query->row();
		else
            return false;
	}
	function getRandomAdvt($type='Square'){
		
		$sql ="select * FROM tbl_advertize WHERE 1=1 AND is_active = '1' AND advertize_type = '$type' ORDER BY RAND() LIMIT 0,3";
		$query = $this->db->query($sql);
		$result = $query->result();
        //var_dump($result);
		return !empty($result) ? $result : false;
	}
	function getAllRecords($all='*',$where='',$orderby='',$limit=''){
		
		$sql ="select $all FROM tbl_advertize WHERE 1=1 ";
		if($where!=''){
			$sql .= " AND $where ";
		}
		
		if($orderby!=''){
			$sql .= " $orderby ";
		}
		if($limit!=''){
			$sql .= " $limit ";
		}
		$query = $this->db->query($sql);
		$query_data = $query->result();
		
		return $query_data;
	}
	
		
}
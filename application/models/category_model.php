<?php
class Category_model extends CI_Model {

    var $title   = '';
    var $content = '';
    var $date    = '';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }
    
    function addDetails(){
		
		
		$order = $this->getLastOrder();
		$category_order = $order + 1;
		
		$data = array(
			'title' => $this->input->post('title'),
			'sub_title' => $this->input->post('sub_title'),
			'category_slug' => $this->input->post('category_slug'),
			'parent_id' => $this->input->post('parent_id'),
			'description' => $this->input->post('description'),
			'category_image' => $this->input->post('category_image'),
			'category_icon_large' => $this->input->post('category_icon_large'),
			'category_order' => $category_order,
			'meta_title' => $this->input->post('meta_title'),
			'meta_desc' => $this->input->post('meta_desc'),
			'meta_keywords' => $this->input->post('meta_keywords'),
			'is_active' => $this->input->post('is_active'),
			'on_header' => $this->input->post('on_header'),
			'created_by' => $this->session->userdata('admin_id'),
			'updated_by' => $this->session->userdata('admin_id'),
			'created_date' =>date('Y-m-d H:i:s')			
		);
		
		$this->db->insert('tbl_category',$data) or die(mysql_error()); 	
		$id=mysql_insert_id();
		return $id;
		
	}
	
	function updateDetails(){
		
		$data = array(
			'title' => $this->input->post('title'),
			'sub_title' => $this->input->post('sub_title'),
			'parent_id' => $this->input->post('parent_id'),
			'description' => $this->input->post('description'),
			'category_image' => $this->input->post('category_image'),
			'category_icon_large' => $this->input->post('category_icon_large'),
			'meta_title' => $this->input->post('meta_title'),
			'on_header' => $this->input->post('on_header'),
			'meta_desc' => $this->input->post('meta_desc'),
			'meta_keywords' => $this->input->post('meta_keywords'),			
			'is_active' => $this->input->post('is_active'),
			'updated_by' => $this->session->userdata('admin_id')
		);
		$this->db->where("id",$this->input->post('id'));
		$this->db->update('tbl_category',$data);
		
		
		return true;
		
	}
	
	function changeStatus($status,$id){
		
		mysql_query("UPDATE tbl_category SET is_active = '$status' WHERE id= ".$id."");
		return true;
	}
	function deleteRecord($id){		
		mysql_query("UPDATE tbl_category SET is_deleted = '1', deleted_date_time = '".date('Y-m-d H:i:s')."' WHERE id= ".$id."");
		return true;
	}
	function getDetails($id){
		
		$this->db->where('id', $id);
		$this->db->where('is_deleted', '0');
		$query = $this->db->get('tbl_category') or die(mysql_error());
		if($query->num_rows >= 1)
			return $query->row();
		else
            return false;
	}
	function generateCategorySlug($title='category page'){
		$urltitle=preg_replace('/[^a-z0-9]/i',' ', ltrim(rtrim(strtolower($title))));
		$newurltitle=str_replace(" ","-",$urltitle);
		$queryCount = "SELECT category_slug from tbl_category WHERE category_slug LIKE '".$newurltitle."%'";
		$rqC = mysql_num_rows(mysql_query($queryCount));
		if($rqC != 0){
			$newurltitle = $newurltitle.'-'.$rqC; 
		}
		return $newurltitle;				
	}
	function getAllRecords($all='*',$where='',$orderby='',$limit=''){
		
		$sql ="select $all FROM tbl_category WHERE 1=1 ";
		if($where!=''){
			$sql .= " AND $where ";
		}
		$sql .= " AND is_deleted='0' ";
		if($orderby!=''){
			$sql .= " $orderby ";
		}
		if($limit!=''){
			$sql .= " $limit ";
		}
		
		$query = $this->db->query($sql);
		$query_data = $query->result();
		if(count($query_data) > 0 )
		{
			$i = 0;
			foreach ($query->result_array() as $value) 
			{
				
				$query_data[$i]->sub_page_count = $this->getSubPageCount($value['id']);
				$i++;
			}
		}

		return $query_data;
	}
	function getClassifiedCategoryList($category_id, $getActive = false){

		$sql ="select id,title FROM tbl_category WHERE 1=1 ";
		$sql .= " AND parent_id = 0 AND is_deleted='0' ";
		if ($getActive != false) {
            $sql .= " AND is_active = '1' ";
        }
		$query = $this->db->query($sql);
		$query_data = $query->result();
		
               
             
		$options = '';
		if(count($query_data) > 0 )
		{
			foreach ($query_data as $parent) 
			{
				$options .='<option value="'.$parent->id.'"';
				$options .= ($category_id != 0 && $category_id== $parent->id) ? 'selected="selected"' : "";				
				$options .= '>'.$parent->title.'</option>';
				$sqlChild ="select id,title FROM tbl_category WHERE 1=1 ";
				$sqlChild .= " AND parent_id = ".$parent->id." AND is_deleted='0' ";
                if ($getActive != false) {
                    $sqlChild .= " AND is_active = '1' ";
                }
				
				$queryChild = $this->db->query($sqlChild);
				$childData = $queryChild->result();
				if(count($childData) > 0 )
				{
					foreach ($childData as $child) 
					{
						$options .='<option value="'.$child->id.'"';
						$options .= ($category_id != 0 && $category_id== $child->id) ? 'selected="selected"' : "";						
						$options .= '>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$child->title.'</option>';
						$sqlChild1 ="select id,title FROM tbl_category WHERE 1=1 ";
						$sqlChild1 .= " AND parent_id = ".$child->id." AND is_deleted='0' ";
                        if ($getActive != false) {
                            $sqlChild1 .= " AND is_active = '1' ";
                        }
						
						$queryChild1 = $this->db->query($sqlChild1);
						$childData1 = $queryChild1->result();
						if(count($childData1) > 0 )
						{
							foreach ($childData1 as $child1) 
							{
								$options .='<option value="'.$child1->id.'"';
								$options .= ($category_id != 0 && $category_id== $child1->id) ? 'selected="selected"' : "";								
								$options .= '>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$child1->title.'</option>';
							}
						}
					}
				}
			}
		}
		$options .= '';
		return $options;
	}
	function getParentCategoryLists($id=0,$parent_id=0){
		$sql ="select id,title,parent_id FROM tbl_category WHERE 1=1 ";
		$sql .= " AND parent_id = 0 AND is_deleted='0' ";
		
		$query = $this->db->query($sql);
		$query_data = $query->result();
		
               
             
		$options = '<option value="0" selected="selected">Select Parent category</option>';
		if(count($query_data) > 0 )
		{
			foreach ($query_data as $parent) 
			{
				$options .='<option value="'.$parent->id.'"';
				$options .= ($parent_id != 0 && $parent_id== $parent->id) ? 'selected="selected"' : "";
				$options .= ($id != 0 && $id == $parent->id) ? "disabled='disabled'" : "";
				$options .= '>'.$parent->title.'</option>';
				$sqlChild ="select id,title,parent_id FROM tbl_category WHERE 1=1 ";
				$sqlChild .= " AND parent_id = ".$parent->id." AND is_deleted='0' ";
				
				$queryChild = $this->db->query($sqlChild);
				$childData = $queryChild->result();
				if(count($childData) > 0 )
				{
					foreach ($childData as $child) 
					{
						$options .='<option value="'.$child->id.'"';
						$options .= ($parent_id != 0 && $parent_id== $child->id) ? 'selected="selected"' : "";
						$options .= (($id != 0 && $id == $child->id) || ($id != 0 && $id == $child->parent_id)) ? "disabled='disabled'" : "";
						$options .= '>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$child->title.'</option>';
					}
				}
			}
		
		}
		$options .= '';
		return $options;
	}
	function getSubPageCount($parent_id){
		$sql ="select count(id) as sub_page_count FROM tbl_category WHERE is_deleted='0' AND parent_id = $parent_id";
		$query = $this->db->query($sql);
		$result = $query->result();
		return $result[0]->sub_page_count;		
	}
	function getLastOrder(){
		$sql ="select category_order FROM tbl_category WHERE is_deleted='0' ORDER BY category_order desc LIMIT 0,1 "; 
		$query = $this->db->query($sql);
		$result = $query->result();
		return $result[0]->category_order;	
		
	}
	
	function changeOrder($id,$category_order,$position){

		if($id!='' && $category_order!='' && $position!=''){
		$pageDetails = $this->getDetails($id);	
			if($position=='Dn'){
				$qr="select category_order,id from tbl_category where parent_id = $pageDetails->parent_id  AND category_order > '".$category_order."' AND is_deleted='0' order by category_order asc limit 0,1";
				$query = $this->db->query($qr);
				$result1 = $query->result();
				if($result1 && count($result1)>0){
					$NewId=$result1[0]->id;
					$NewOrder = $result1[0]->category_order;
					$qry = "UPDATE tbl_category SET `category_order`= $category_order WHERE id =".$NewId;
						mysql_query($qry);

					$qry1 = "UPDATE tbl_category SET `category_order`= $NewOrder WHERE id =".$id;
					mysql_query($qry1);
				}
			}
			
			if($position=='Up'){
				$qr="select category_order,id from tbl_category where parent_id = $pageDetails->parent_id  AND category_order < '".$category_order."' AND is_deleted='0' order by category_order desc limit 0,1";
				$query = $this->db->query($qr);
				$result1 = $query->result();
				
				if($result1 && count($result1)>0){
					$NewId=$result1[0]->id;
					$NewOrder = $result1[0]->category_order;
					$qry = "UPDATE tbl_category SET `category_order`= $category_order WHERE id =".$NewId;
					mysql_query($qry);

					$qry1 = "UPDATE tbl_category SET `category_order`= $NewOrder WHERE id =".$id;
					mysql_query($qry1);
				}
			}
		}		
	}

    function getSubOrCategoryHierarchy($getActive = false)
    {
        /*$this->db->select('categ.id, categ.title, categ.category_slug, categ.sub_title, categ.parent_id, categ.category_image, categ.category_icon_large, count(clasfds.id) AS classifiedsCount, categ.category_order', false);
        $this->db->from('tbl_category  categ');
        $this->db->join("tbl_classified AS clasfds ON ((clasfds.category_id = categ.id OR clasfds.sub_category_id = categ.id) AND clasfds.is_active='1')", null, 'LEFT', false);
        if ($getActive != false) {
            $this->db->where('categ.is_active', '1');
            $this->db->where('categ.is_deleted', '0');
        }
        $this->db->group_by('categ.id');
        $this->db->order_by('categ.category_order', 'ASC');*/

        $sql = "SELECT categ.id, categ.title, categ.category_slug, categ.sub_title, categ.parent_id, categ.category_image, categ.category_icon_large, count(clasfds.id) AS classifiedsCount, categ.category_order ";
        $sql .= "FROM (`tbl_category` categ) ";
        $sql .= "LEFT JOIN `tbl_classified` AS clasfds ON ((clasfds.category_id = categ.id OR clasfds.sub_category_id = categ.id) AND clasfds.is_active='1') ";
        $sql .= "WHERE ";
        if ($getActive != false) {
            $sql .= "`categ`.`is_active` = '1' ";
        }
        $sql .= "AND `categ`.`is_deleted` = '0' ";
        $sql .= "GROUP BY `categ`.`id` ";
        $sql .= "ORDER BY `categ`.`category_order` ASC";

        $query = $this->db->query($sql);
        $query_data = $query->result();
        $mainCategory = array();
        $subCategory = array();

        foreach ($query_data as $data) {
            if ($data->parent_id == 0) {
                if (!empty($mainCategory[$data->id]->allClassifiedsCount)) {
                    $data->allClassifiedsCount = $mainCategory[$data->id]->allClassifiedsCount + $data->classifiedsCount;
                } else {
                    $data->allClassifiedsCount = $data->classifiedsCount;
                }
                $mainCategory[$data->id] = $data;
            } else {
                if (empty($mainCategory[$data->parent_id])) {
                    $mainCategory[$data->parent_id]->allClassifiedsCount = $data->classifiedsCount;
                } else {
                    $mainCategory[$data->parent_id]->allClassifiedsCount += $data->classifiedsCount;
                }
                $subCategory[$data->parent_id][$data->id] = $data;
            }
        }

        return array(
            'mainCategory' => $mainCategory,
            'subCategory' => $subCategory
        );
    }

	function getCategoryHierarchy($getActive = false)
    {
        $this->db->select('categ.id, categ.title, categ.category_slug, categ.sub_title, categ.parent_id, categ.category_image, categ.category_icon_large, count(clasfds.id) AS classifiedsCount, categ.category_order', false);
        $this->db->from('tbl_category  categ');
        $this->db->join('tbl_classified AS clasfds', "clasfds.category_id = categ.id AND clasfds.is_active='1'", 'LEFT');
        if ($getActive != false) {
            $this->db->where('categ.is_active', '1');
        }
        $this->db->where('categ.is_deleted', '0');
        $this->db->group_by('categ.id');
        $this->db->order_by('categ.category_order', 'ASC');

        $query = $this->db->get();
        $query_data = $query->result();
        $mainCategory = array();
        $subCategory = array();

        foreach ($query_data as $data) {
            if ($data->parent_id == 0) {
                if (!empty($mainCategory[$data->id]->allClassifiedsCount)) {
                    $data->allClassifiedsCount = $mainCategory[$data->id]->allClassifiedsCount + $data->classifiedsCount;
                } else {
                    $data->allClassifiedsCount = $data->classifiedsCount;
                }
                $mainCategory[$data->id] = $data;
            } else {
                if (empty($mainCategory[$data->parent_id])) {
                    $mainCategory[$data->parent_id]->allClassifiedsCount = $data->classifiedsCount;
                } else {
                    $mainCategory[$data->parent_id]->allClassifiedsCount += $data->classifiedsCount;
                }
                $subCategory[$data->parent_id][$data->id] = $data;
            }
        }

        return array(
            'mainCategory' => $mainCategory,
            'subCategory' => $subCategory
        );
    }

    function getSubCategoriesLookup($category_id, $getActive = false){
        $sqlChild ="select id,title FROM tbl_category WHERE 1=1 ";
        $sqlChild .= " AND parent_id = ".$category_id." AND is_deleted='0' ";
        if ($getActive != false) {
            $sqlChild .= " AND is_active = '1' ";
        }

        $queryChild = $this->db->query($sqlChild);
        $childData = $queryChild->result();
        $lookUp = array();
        if(count($childData) > 0 ) {
            foreach ($childData as $child) {
                $lookUp[$child->id] = $child->title;
            }
        }

        return $lookUp;
    }

    /*function getCategoryClasifiedCount($searchString= '', $records_per_page = 10, $starts_with = 0, $order_by = '', $order_by_modifier = '')
    {
        $this->getAllRecords('*'," is_active = '1' AND parent_id =0 ",'ORDER BY category_order ASC');
        $sql = "";

        $query = "SELECT tc.*, COUNT(tcl.classified_id) AS classifiedsCount FROM tbl_category tc ";
        $query .= "LEFT JOIN tbl_classified tcl ON tc.id = tcl.tip_id ";

        $query .= "WHERE is_deleted = '0' AND is_active = '1' $searchString ";
        $query .= " GROUP BY (tc.id) ";
        $query .= !empty($order_by) ? " ORDER BY $order_by" : '';
        $query .= (!empty($order_by) && !empty($order_by_modifier)) ? " $order_by_modifier" : '';

        $query .= (!empty($starts_with) && !empty($records_per_page)) ? " LIMIT $starts_with, $records_per_page" : '';

        $query = $this->db->query($query);
        //echo $this->db->last_query();
        $result = $query->result();

        return $result;
    }*/

}
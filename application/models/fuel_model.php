<?php
class Fuel_model extends CI_Model {

    var $title   = '';
    var $content = '';
    var $date    = '';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }
    
    function addDetails(){
		
			
		$data = array(
			'title' => $this->input->post('title'),
			'is_active' => $this->input->post('is_active'),
			'created_date' =>date('Y-m-d H:i:s')			
		);
		
		$this->db->insert('tbl_fuel_type',$data) or die(mysql_error());
		$id=mysql_insert_id();
		return $id;

	}

	function updateDetails(){

		$data = array(
			'title' => $this->input->post('title'),
			'is_active' => $this->input->post('is_active')

		);
		$this->db->where("id",$this->input->post('id'));
		$this->db->update('tbl_fuel_type',$data);


		return true;

	}
	function changeStatus($status,$id){

		mysql_query("UPDATE tbl_fuel_type SET is_active = '$status' WHERE id= ".$id."");
		return true;
	}
	function deleteRecord($id){
		mysql_query("UPDATE tbl_fuel_type SET is_deleted = '1',is_active = '0' WHERE id= ".$id."");
		return true;
	}
	function getDetails($id){

		$this->db->where('id', $id);
		$this->db->where('is_deleted', '0');
		$query = $this->db->get('tbl_fuel_type') or die(mysql_error());
		if($query->num_rows >= 1)
			return $query->row();
		else
            return false;
	}
	function getDetailsFromTitle($title){

		$this->db->where('title', $title);
		$this->db->where('is_deleted', '0');
		$query = $this->db->get('tbl_fuel_type') or die(mysql_error());
		if($query->num_rows >= 1)
			return $query->row();
		else
            return false;
	}
	function checkTitleExist($title,$id=0){
		if($title!='' && $id != 0){
			$sql = "SELECT * FROM tbl_fuel_type WHERE title = '".$title."' AND id != $id AND is_deleted='0'";
		}else if($title!=''){
			$sql = "SELECT * FROM tbl_fuel_type WHERE title = '".$title."' AND is_deleted='0'";
		}
		$query = $this->db->query($sql);
		return $query->result();

	}
	function getAllRecords($all='*',$where='',$orderby='',$limit=''){

		$sql ="select $all FROM tbl_fuel_type WHERE 1=1 ";
		if($where!=''){
			$sql .= " AND $where ";
		}
		$sql .= " AND is_deleted='0' ";
		if($orderby!=''){
			$sql .= " $orderby ";
		}
		if($limit!=''){
			$sql .= " $limit ";
		}
		
		$query = $this->db->query($sql);
		$query_data = $query->result();
		
		
		return $query_data;
	}

}
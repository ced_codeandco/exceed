<?php
class Image_model extends CI_Model {

    var $title   = '';
    var $content = '';
    var $date    = '';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }
    
    function addDetails(){
		
		$data = array(
			'classified_id' => $this->input->post('classified_id'),
			'classified_image' => $this->input->post('classified_image'),			
			'is_image' => $this->input->post('is_image'),
			'is_active' => $this->input->post('is_active'),
			'created_date_time' =>date('Y-m-d H:i:s')			
		);
		
		$this->db->insert('tbl_classified_image',$data) or die(mysql_error());
		$id=mysql_insert_id();
		return $id;
		
	}
	
	

	function changeStatus($status,$id){
		
		mysql_query("UPDATE tbl_classified_image SET is_active = '$status' WHERE id= ".$id."");
		return true;
	}
	function deleteRecord($id){
		mysql_query("DELETE FROM tbl_classified_image WHERE id= ".$id."");
		return true;
	}
	function getDetails($id){
		$this->db->where('id', $id);		
		$query = $this->db->get('tbl_classified_image') or die(mysql_error());
		if($query->num_rows >= 1)
			return $query->row();
		else
            return false;
	}
	
	function getAllRecords($all='*',$where='',$orderby='',$limit=''){
		
		$sql ="select $all FROM tbl_classified_image WHERE 1=1 ";
		if($where!=''){
			$sql .= " AND $where ";
		}
		if($orderby!=''){
			$sql .= " $orderby ";
		}
		if($limit!=''){
			$sql .= " $limit ";
		}
		$query = $this->db->query($sql);
		$query_data = $query->result();
			
		return $query_data;
	}
}
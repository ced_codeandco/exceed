<?php
class Inquiry_model extends CI_Model {

    var $title   = '';
    var $content = '';
    var $date    = '';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }
    
    function addDetails(){
		$data = array(
			'classified_id' => $this->input->post('classified_id'),
			'inquiry_member_id' => $this->input->post('inquiry_member_id'),
			'inquiry_email' => $this->input->post('email'),
			'inquiry_name' => $this->input->post('name'),
			'inquiry_message' => $this->input->post('comment'),
			'inquiry_contact_no' => $this->input->post('phone'),
			'is_active' => '1',
			'inquiry_date_time' =>date('Y-m-d H:i:s')
		);
		
		$this->db->insert('tbl_classified_inquiry',$data) or die(mysql_error());

        return $this->db->insert_id();
	}
	
	function replyInquiry(){
		$data = array(
			'classified_id' => $this->input->post('classifiedId'),
			'inquiry_id' => $this->input->post('inquiryId'),
			'replied_member_id' => $this->input->post('replied_member_id'),			
			'replied_text' => $this->input->post('reply_text'),			
			'replied_date_time' =>date('Y-m-d H:i:s')			
		);
		
		$this->db->insert('tbl_classified_inquiry_reply',$data) or die(mysql_error()); 	
		$id=mysql_insert_id();
		if($id){
			$this->changeReplyStatus($this->input->post('inquiryId'));
		}
		return $id;
		
	}
	function getCount($status=''){
		if($status == ''){
			$sql ="select count(id) as total_inquiry FROM tbl_classified_inquiry WHERE is_active = '1' ";
		}else{
			$sql ="select count(id) as total_inquiry FROM tbl_classified_inquiry WHERE is_active = '$status'";
		}
		//echo $sql.'<br>';
		$query = $this->db->query($sql);
		$result = $query->result();
		return $result[0]->total_inquiry;
		
	}
	function changeReplyStatus($id){
		
		mysql_query("UPDATE tbl_classified_inquiry SET is_replied = '1' WHERE id= ".$id."");
		return true;
	}
	function changeStatus($status,$id){
		
		mysql_query("UPDATE tbl_classified_inquiry SET is_active = '$status' WHERE id= ".$id."");
		return true;
	}
	function deleteRecord($id){
		mysql_query("DELETE FROM tbl_classified_inquiry WHERE id= ".$id."");
		return true;
	}
	function getDetails($id){
		$this->db->where('id', $id);		
		$query = $this->db->get('tbl_classified_inquiry') or die(mysql_error());
		if($query->num_rows >= 1)
			return $query->row();
		else
            return false;
	}
	
	function getAllRecords($all='*',$where='',$orderby='',$limit=''){
		
		$sql ="select $all FROM tbl_classified_inquiry WHERE 1=1 ";
		if($where!=''){
			$sql .= " AND $where ";
		}
		if($orderby!=''){
			$sql .= " $orderby ";
		}
		if($limit!=''){
			$sql .= " $limit ";
		}
		$query = $this->db->query($sql);
		$query_data = $query->result();
			
		return $query_data;
	}

    function validate_inquiry_forms()
    {
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
        $this->form_validation->set_message('is_unique', '%s is already in use.');
        $this->form_validation->set_rules('name', 'Name', 'xss_clean|trim|required');
        $this->form_validation->set_rules('email', 'Email', 'xss_clean|trim|required');
        $this->form_validation->set_rules('comment', 'Comment', 'xss_clean|trim|required');
        $this->form_validation->set_rules('classified_contact_no', 'Telephone / Mobile number', 'xss_clean|trim');
        //registration_ip
        if ($this->form_validation->run()) {
            //Add enquiry
            $inquiryId = $this->addDetails();
            $this->sendInquiryEmail($this->input->post('classified_id'));
            $this->session->set_flashdata('flash_success', 'Your email changed successfully!!<br />Please check your email for verification link');

            return true;
        } else {

           return false;
        }
    }

    /**
     * Function to send an email alert for inquiries received
     *
     * @param $classified_id
     */
    public function sendInquiryEmail($classified_id)
    {
        $fields = ' id, title, classified_slug ';
        $where = " id= '$classified_id' ";
        $classifieds = $this->classified_model->getAllRecords($fields, $where);

        $classifieds = !empty($classifieds[0]) ? $classifieds[0] : array();
        $clientName = $this->input->post('name') ? $this->input->post('name') : 'A client';
        $email = $this->input->post('email') ? $this->input->post('email') : '';
        $phone = $this->input->post('phone') ? $this->input->post('phone') : '';
        $comment = $this->input->post('comment') ? $this->input->post('comment') : '';
        $this->load->library('emailclass');
        $message = "<p>Marhaba " . $classifieds->first_name . ",</p><br />";

        $message .= "<p>";
        $message .= "{$clientName} interested in speaking to you about your ";
        $message .= "<a href='".ROOT_URL."details/".$classifieds->classified_slug."'>".(!empty($classifieds->title) ? $classifieds->title : 'ad')."</a>.";
        $message .= "</p>";
        $message .= "<p>Name: {$clientName}</p>";
        $message .= "<p>Telephone: {$phone}</p>";
        $message .= "<p>Email: {$email}</p>";
        $message .= "<p>Message: {$comment}</p>";
        $message .= "<p><br /></p>";
        $message .= "<p>Already found someone? Congrats! Don't forget to delete the ad <a href='".ROOT_URL."member/my_adds'>here</a> so we don't bug you with more buyer notifications.</p>";

        $subject = SITE_NAME . ':: Someone is interested in your '.(!empty($classifieds->title) ? $classifieds->title : 'ad');

        $content = '';
        $content .= $this->emailclass->emailHeader();
        $content .= $message;
        $content .= $this->emailclass->emailFooter();

        $email = $this->emailclass->send_mail($classifieds->member_email, $subject, $content);
    }
}
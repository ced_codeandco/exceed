<?php
class Locality_model extends CI_Model {

    var $title   = '';
    var $content = '';
    var $date    = '';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }
    
    function addDetails(){

		$order = $this->getLastOrder();
		$locality_order = $order + 1;
		
		$data = array(
			'title' => $this->input->post('title'),
			'parent_id' => $this->input->post('parent_id'),
			'created_by' => $this->session->userdata('admin_id'),
			'updated_by' => $this->session->userdata('admin_id'),
            'is_active' => $this->input->post('is_active'),
			'created_date' =>date('Y-m-d H:i:s')			
		);
		
		$this->db->insert('tbl_locality',$data) or die(mysql_error());
		$id=mysql_insert_id();
		return $id;
		
	}
	
	function updateDetails(){

		$data = array(
            'title' => $this->input->post('title'),
            'parent_id' => $this->input->post('parent_id'),
            'created_by' => $this->session->userdata('admin_id'),
            'updated_by' => $this->session->userdata('admin_id'),
            'created_date' =>date('Y-m-d H:i:s'),
			'is_active' => $this->input->post('is_active'),
			'updated_by' => $this->session->userdata('admin_id')
		);

		$this->db->where("id",$this->input->post('id'));
		$this->db->update('tbl_locality',$data);
		
		return true;
		
	}
	
	function changeStatus($status,$id){
		
		mysql_query("UPDATE tbl_locality SET is_active = '$status' WHERE id= ".$id."");
		return true;
	}
	function deleteRecord($id){		
		mysql_query("UPDATE tbl_locality SET is_deleted = '1', deleted_date_time = '".date('Y-m-d H:i:s')."' WHERE id= ".$id."");
		return true;
	}
	function getDetails($id){
		
		$this->db->where('id', $id);
		$this->db->where('is_deleted', '0');
		$query = $this->db->get('tbl_locality') or die(mysql_error());
		if($query->num_rows >= 1)
			return $query->row();
		else
            return false;
	}

	function getAllRecords($all='*',$where='',$orderby='',$limit=''){
		
		$sql ="select $all FROM tbl_locality WHERE 1=1 ";
		if($where!=''){
			$sql .= " AND $where ";
		}
		$sql .= " AND is_deleted='0' ";
		if($orderby!=''){
			$sql .= " $orderby ";
		}
		if($limit!=''){
			$sql .= " $limit ";
		}
		
		$query = $this->db->query($sql);
		$query_data = $query->result();
		//echo $this->db->last_query();

		return $query_data;
	}
	function getClassifiedLocalityList($locality_id, $getActive = false){

		$sql ="select id,title FROM tbl_locality WHERE 1=1 ";
		$sql .= " AND parent_id = 0 AND is_deleted='0' ";
		if ($getActive != false) {
            $sql .= " AND is_active = '1' ";
        }
		$query = $this->db->query($sql);
		$query_data = $query->result();
		
               
             
		$options = '';
		if(count($query_data) > 0 )
		{
			foreach ($query_data as $parent) 
			{
				$options .='<option value="'.$parent->id.'"';
				$options .= ($locality_id != 0 && $locality_id== $parent->id) ? 'selected="selected"' : "";
				$options .= '>'.$parent->title.'</option>';
				$sqlChild ="select id,title FROM tbl_locality WHERE 1=1 ";
				$sqlChild .= " AND parent_id = ".$parent->id." AND is_deleted='0' ";
                if ($getActive != false) {
                    $sqlChild .= " AND is_active = '1' ";
                }
				
				$queryChild = $this->db->query($sqlChild);
				$childData = $queryChild->result();
				if(count($childData) > 0 )
				{
					foreach ($childData as $child) 
					{
						$options .='<option value="'.$child->id.'"';
						$options .= ($locality_id != 0 && $locality_id== $child->id) ? 'selected="selected"' : "";
						$options .= '>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$child->title.'</option>';
						$sqlChild1 ="select id,title FROM tbl_locality WHERE 1=1 ";
						$sqlChild1 .= " AND parent_id = ".$child->id." AND is_deleted='0' ";
                        if ($getActive != false) {
                            $sqlChild1 .= " AND is_active = '1' ";
                        }
						
						$queryChild1 = $this->db->query($sqlChild1);
						$childData1 = $queryChild1->result();
						if(count($childData1) > 0 )
						{
							foreach ($childData1 as $child1) 
							{
								$options .='<option value="'.$child1->id.'"';
								$options .= ($locality_id != 0 && $locality_id== $child1->id) ? 'selected="selected"' : "";
								$options .= '>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$child1->title.'</option>';
							}
						}
					}
				}
			}
		}
		$options .= '';
		return $options;
	}
	function getParentLocalityLists($id=0,$parent_id=0){
		$sql ="select id,title,parent_id FROM tbl_locality WHERE 1=1 ";
		$sql .= " AND parent_id = 0 AND is_deleted='0' ";
		
		$query = $this->db->query($sql);
		$query_data = $query->result();
		
               
             
		$options = '<option value="0" selected="selected">Select Parent locality</option>';
		if(count($query_data) > 0 )
		{
			foreach ($query_data as $parent) 
			{
				$options .='<option value="'.$parent->id.'"';
				$options .= ($parent_id != 0 && $parent_id== $parent->id) ? 'selected="selected"' : "";
				$options .= ($id != 0 && $id == $parent->id) ? "disabled='disabled'" : "";
				$options .= '>'.$parent->title.'</option>';
				$sqlChild ="select id,title,parent_id FROM tbl_locality WHERE 1=1 ";
				$sqlChild .= " AND parent_id = ".$parent->id." AND is_deleted='0' ";
				
				$queryChild = $this->db->query($sqlChild);
				$childData = $queryChild->result();
				if(count($childData) > 0 )
				{
					foreach ($childData as $child) 
					{
						$options .='<option value="'.$child->id.'"';
						$options .= ($parent_id != 0 && $parent_id== $child->id) ? 'selected="selected"' : "";
						$options .= (($id != 0 && $id == $child->id) || ($id != 0 && $id == $child->parent_id)) ? "disabled='disabled'" : "";
						$options .= '>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$child->title.'</option>';
					}
				}
			}
		
		}
		$options .= '';
		return $options;
	}
	function getSubPageCount($parent_id){
		$sql ="select count(id) as sub_page_count FROM tbl_locality WHERE is_deleted='0' AND parent_id = $parent_id";
		$query = $this->db->query($sql);
		$result = $query->result();
		return $result[0]->sub_page_count;		
	}
	function getLastOrder(){
		$sql ="select locality_order FROM tbl_locality WHERE is_deleted='0' ORDER BY locality_order desc LIMIT 0,1 ";
		$query = $this->db->query($sql);
		$result = $query->result();
		return $result[0]->locality_order;
		
	}
	
	function changeOrder($id,$locality_order,$position){

		if($id!='' && $locality_order!='' && $position!=''){
		$pageDetails = $this->getDetails($id);	
			if($position=='Dn'){
				$qr="select locality_order,id from tbl_locality where parent_id = $pageDetails->parent_id  AND locality_order > '".$locality_order."' AND is_deleted='0' order by locality_order asc limit 0,1";
				$query = $this->db->query($qr);
				$result1 = $query->result();
				if($result1 && count($result1)>0){
					$NewId=$result1[0]->id;
					$NewOrder = $result1[0]->locality_order;
					$qry = "UPDATE tbl_locality SET `locality_order`= $locality_order WHERE id =".$NewId;
						mysql_query($qry);

					$qry1 = "UPDATE tbl_locality SET `locality_order`= $NewOrder WHERE id =".$id;
					mysql_query($qry1);
				}
			}
			
			if($position=='Up'){
				$qr="select locality_order,id from tbl_locality where parent_id = $pageDetails->parent_id  AND locality_order < '".$locality_order."' AND is_deleted='0' order by locality_order desc limit 0,1";
				$query = $this->db->query($qr);
				$result1 = $query->result();
				
				if($result1 && count($result1)>0){
					$NewId=$result1[0]->id;
					$NewOrder = $result1[0]->locality_order;
					$qry = "UPDATE tbl_locality SET `locality_order`= $locality_order WHERE id =".$NewId;
					mysql_query($qry);

					$qry1 = "UPDATE tbl_locality SET `locality_order`= $NewOrder WHERE id =".$id;
					mysql_query($qry1);
				}
			}
		}		
	}

	function getLocalityHierarchy($getActive = false)
    {
        $this->db->select('loc.id, loc.title, loc.sub_title, loc.parent_id, loc.locality_image, count(clasfds.id) AS classifiedsCount', false);
        $this->db->from('tbl_locality  loc');
        $this->db->join('tbl_classified AS clasfds', 'clasfds.classified_locality = loc.id AND clasfds.is_active=\'1\'', 'LEFT');
        if ($getActive != false) {
            $this->db->where('loc.is_active', '1');
            $this->db->where('loc.is_deleted', '0');
        }
        $this->db->group_by('loc.id');
        $this->db->order_by('loc.locality_order', 'ASC');

        $query = $this->db->get();
        $query_data = $query->result();
        $mainLocality = array();
        $subLocality = array();

        foreach ($query_data as $data) {
            if ($data->parent_id == 0) {
                if (!empty($mainLocality[$data->id]->allClassifiedsCount)) {
                    $data->allClassifiedsCount = $mainLocality[$data->id]->allClassifiedsCount + $data->classifiedsCount;
                } else {
                    $data->allClassifiedsCount = $data->classifiedsCount;
                }
                $mainLocality[$data->id] = $data;
            } else {
                if (empty($mainLocality[$data->parent_id])) {
                    $mainLocality[$data->parent_id]->allClassifiedsCount = $data->classifiedsCount;
                } else {
                    $mainLocality[$data->parent_id]->allClassifiedsCount += $data->classifiedsCount;
                }
                $subLocality[$data->parent_id][$data->id] = $data;
            }
        }

        return array(
            0 => $mainLocality,
            1 => $subLocality
        );
    }

    function getLocalityLookup($country_id, $getActive = false){
        $sqlChild ="select id,title FROM tbl_locality WHERE 1=1 ";
        $sqlChild .= " AND parent_id = ".$country_id." AND is_deleted='0' ";
        if ($getActive != false) {
            $sqlChild .= " AND is_active = '1' ";
        }

        $queryChild = $this->db->query($sqlChild);
        $childData = $queryChild->result();
        $lookUp = array();
        if(count($childData) > 0 ) {
            foreach ($childData as $child) {
                $lookUp[$child->id] = $child->title;
            }
        }
        return $lookUp;
    }

}
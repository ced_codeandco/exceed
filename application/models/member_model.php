<?php
class Member_model extends CI_Model {

    static $user_table = 'tbl_member';
    var $title   = '';
    var $content = '';
    var $date    = '';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }
    function getCount($status=''){
		if($status == ''){
			$sql ="select count(id) as total_members FROM tbl_member WHERE is_active = '1' AND is_deleted ='0'";
		}else{
			$sql ="select count(id) as total_members FROM tbl_member WHERE is_active = '$status'";
		}

		$query = $this->db->query($sql);
		$result = $query->result();
		return $result[0]->total_members;
		
	}
    function addDetails($emailVerification = false, $data = array()){
        if (empty($data)) {
            $_POST['date_of_birth'] = date('Y-m-d', strtotime($_POST['date_of_birth']));
            $_POST['password'] = md5($_POST['password']);
            $data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'gender' => $this->input->post('gender'),
                'date_of_birth' => $this->input->post('date_of_birth'),
                'contact_no' => $this->input->post('contact_no'),
                'company' => $this->input->post('company'),
                'business_id' => $this->input->post('business_id'),
                'address_1' => $this->input->post('address_1'),
                'address_2' => $this->input->post('address_2'),
                'email' => $this->input->post('email'),
                'city' => $this->input->post('city'),
                'postal_code' => $this->input->post('postal_code'),
                'state' => $this->input->post('state'),
                'country' => $this->input->post('country'),
                'password' => $this->input->post('password'),
                'subscribe_intimation' => $this->input->post('subscribe_intimation'),
                'registration_ip' => $this->input->ip_address(),
                'is_active' => $this->input->post('is_active'),
                'created_date' => date('Y-m-d H:i:s')
            );
        }
        if ($emailVerification != false) {
            $data['emailToken'] = $emailVerification;
        }

		$this->db->insert('tbl_member',$data);
//echo "<br /><br />".$this->db->last_query()."<br /><br />"; die();
		return $this->db->insert_id();
	}
	function getAllRecordsAjax(){
		$sLimit = "";
	
		if ( isset( $_REQUEST['iDisplayStart'] ) )
		{
			$sLimit = "LIMIT ".mysql_real_escape_string( $_REQUEST['iDisplayStart'] ).", ".
				mysql_real_escape_string( $_REQUEST['iDisplayLength'] );
		}
		
		/* Ordering */
		if ( isset( $_REQUEST['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<mysql_real_escape_string( $_REQUEST['iSortingCols'] ) ; $i++ )
			{
				$sOrder .= $this->admin_model->fnColumnToField(mysql_real_escape_string( $_GET['iSortCol_'.$i] ))."
					".mysql_real_escape_string( $_REQUEST['sSortDir_'.$i] ) .", ";
			}
			$sOrder = substr_replace( $sOrder, "", -2 );
		}
		$sWhere = '';
		if ( $_GET['sSearch'] != "" )
		{
	
			$sWhere .= " and (first_name LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ".
				"last_name LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ".
				"company LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ".
				"email LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%' OR ".
				"country LIKE '%".mysql_real_escape_string( $_GET['sSearch'] )."%')";

	
		
		}
		$sQuery = "SELECT SQL_CALC_FOUND_ROWS id,first_name,is_active,last_name,email,last_login_date,last_login_ip,contact_no from tbl_member  
			$sWhere 
			$sOrder 		
			$sLimit	";
			
			$query = $this->db->query($sQuery);
			$query_data = $query->result();
			
			$sQuery2 = "SELECT FOUND_ROWS()";
			$rResultFilterTotal = $this->db->query($sQuery2);
			$iFilteredTotal = $rResultFilterTotal->num_rows;
		
			
		
			
			
			$sQuery1 = "SELECT COUNT(id) FROM tbl_member";
			$rResultTotal =$this->db->query($sQuery1);
			$iTotal = $rResultTotal->num_rows;
			
			echo '{';
			echo '"sEcho": '.$_GET['sEcho'].', ';
			echo '"iTotalRecords": '.$iTotal.', ';
			echo '"iTotalDisplayRecords": '.$iFilteredTotal.', ';
			echo '"aaData": [ ';
			$jsonData="";
			if($query_data && count($query_data) > 0 ){ 
		  		foreach ($query_data as $member){
					$lastLoginDate = ($member->last_login_date != '' && $member->last_login_date != '0000-00-00 00:00:00') ? date('d-m-Y H:i:s',strtotime($member->last_login_date)) : '';
					$is_active = '';
					if($member->is_active == 1) {
					  $is_active.= '<a class="label-success label label-default" href="'.ADMIN_ROOT_URL.'members/status_inactive/'.$member->id.'" >Active</a>';
					}else{
					   $is_active.= '<a class="label-default label label-danger" href="'.ADMIN_ROOT_URL.'members/status_active/'.$member->id.'">Inactive</a>';
					}
                 	 $is_active=mysql_real_escape_string($is_active);
					$action = '';
					$action .= '<a class=\'btn btn-success\' href="'.ADMIN_ROOT_URL.'members/add/'.$member->id.'"> <i class="glyphicon glyphicon-zoom-in icon-white"></i> View </a> <a class="btn btn-info" href="'.ADMIN_ROOT_URL.'members/add/'.$member->id.'"> <i class="glyphicon glyphicon-edit icon-white"></i> Edit </a>';
                 
                  $action .= '<a class=\'btn btn-danger\' href=\'#\' onclick=\'javascript:if(confirm("Are you sure to delete ? ")){location.href="'.ADMIN_ROOT_URL.'members/delete/'.$member->id.'\'}"> <i class="glyphicon glyphicon-trash icon-white"></i> Delete </a>';
                  $action=mysql_real_escape_string($action);
               
				  
					
					if($jsonData!="")
						$jsonData.=",";
					$jsonData.="['".$member->id."','".$member->first_name.' '.$member->last_name."','".$member->email."','".$lastLoginDate."','".$member->last_login_ip."','sdsa','sadsa']";

				}
			}
			echo $jsonData;
	echo '] }';
	exit;
	}
	function updateDetails($user_id = false, $data = array()){

        if (!empty($_POST['date_of_birth'])) {
            $_POST['date_of_birth'] = date('Y-m-d', strtotime($_POST['date_of_birth']));
        }
		if(LOG_ENTRY == TRUE){
			$id = $this->addMemberLog(($user_id != false) ? $user_id : $this->input->post('id'));
		}
        if (empty($data)) {
            $data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'gender' => $this->input->post('gender'),
                'date_of_birth' => $this->input->post('date_of_birth'),
                'contact_no' => $this->input->post('contact_no'),
                'company' => $this->input->post('company'),
                'business_id' => $this->input->post('business_id'),
                'address_1' => $this->input->post('address_1'),
                'address_2' => $this->input->post('address_2'),
                'email' => $this->input->post('email'),
                'city' => $this->input->post('city'),
                'postal_code' => $this->input->post('postal_code'),
                'state' => $this->input->post('state'),
                'country' => $this->input->post('country'),
                'updated_ip' => $this->input->ip_address(),
                'is_active' => $this->input->post('is_active'),
                'updated_date' => date('Y-m-d H:i:s')
            );
        }

        if ($user_id != false) {
            $this->db->where("id", $user_id);
        } else {
            $this->db->where("id", $this->input->post('id'));
        }
		$this->db->update('tbl_member',$data);

		return true;
	}
	
	function addMemberLog($id){
		$MemberDetails = $this->getDetails($id);
		if (!empty($MemberDetails)) {
            $data = array(
                'first_name' => $MemberDetails->first_name,
                'member_id' => $MemberDetails->id,
                'last_name' => $MemberDetails->last_name,
                'gender' => $MemberDetails->gender,
                'date_of_birth' => $MemberDetails->date_of_birth,
                'contact_no' => $MemberDetails->contact_no,
                'company' => $MemberDetails->company,
                'business_id' => $MemberDetails->business_id,
                'address_1' => $MemberDetails->address_1,
                'address_2' => $MemberDetails->address_2,
                'email' => $MemberDetails->email,
                'city' => $MemberDetails->city,
                'postal_code' => $MemberDetails->postal_code,
                'state' => $MemberDetails->state,
                'country' => $MemberDetails->country,
                'password' => $MemberDetails->password,
                'created_date' => date('Y-m-d H:i:s')
            );

            $this->db->insert('tbl_member_log', $data) or die(mysql_error());
            return mysql_insert_id();
        }
	}
	function changeStatus($status,$id){
		
		mysql_query("UPDATE tbl_member SET is_active = '$status' WHERE id= ".$id."");
		return true;
	}
	function deleteRecord($id){
		
		mysql_query("UPDATE tbl_member SET is_deleted = '1', deleted_date_time = '".date('Y-m-d H:i:s')."' WHERE id= ".$id."");
		return true;
	}
	function getDetails($id){
		$this->db->where('is_deleted', '0');
		$this->db->where('id', $id);		
		$query = $this->db->get('tbl_member') or die(mysql_error());
		if($query->num_rows >= 1)
			return $query->row();
		else
            return false;
	}
	function getAllRecords($all='*',$where='',$orderby='',$limit=''){
		
		$sql ="select $all FROM tbl_member WHERE 1=1 ";
		if($where!=''){

			$sql .= " AND $where ";
		}
		$sql .= " AND is_deleted='0' ";
		if($orderby!=''){
			$sql .= " $orderby ";
		}
		if($limit!=''){
			$sql .= " $limit ";
		}
		$query = $this->db->query($sql);
		$query_data = $query->result();
		return $query_data;
	}
	function checkMemberLogin()
	{
		$user = $this->session->userdata('id');
		if($user == null || $user == '')
		{
			return FALSE;
		}
		else
		{
            $this->db->select('tm.*, tc.combined as city_name');
            $this->db->from('tbl_member tm');
            $this->db->join('tbl_cities tc', 'tc.id = tm.city', 'left');

            $this->db->where('tm.id',$user);
			$this->db->where('is_deleted', '0');
			$query=$this->db->get();
			if($query->num_rows >0)
			{
                $user_data = $query->row();
				$user_id = $user_data->id;
				$screen_name = $user_data->first_name.' '.$user_data->last_name;
				$data = array(
					'display_name' => $user_data->first_name.' '.$user_data->last_name,
					'id' => $user_data->id,
					'email' => $user_data->email,
					'is_logged_in' => true,
                    'is_member'     => true
				);
				$this->session->set_userdata($data);

				return $user_data;
			} else {
				return FALSE;
			}
       }		
	
	}
	function memberLogout(){
        $this->session->unset_userdata('display_name');
        $this->session->unset_userdata('id');
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('module_access');
        $this->session->unset_userdata('is_logged_in');
        $this->session->unset_userdata('is_member');

		return TRUE;
	}
	function memberLogin(){
		$email =  $this->input->post('username');
		$password =  md5($this->input->post('password'));
		
		$this->db->where('email', $email);
		$this->db->where('password', $password);
        $this->db->where('is_active','1');
		$this->db->where('is_deleted', '0');
		$query = $this->db->get('tbl_member') or die(mysql_error());

		if($query->num_rows >= 1) {
			mysql_query("UPDATE tbl_member SET last_login_date = NOW(), last_login_ip='".$this->input->ip_address()."' WHERE id= ".$query->row()->id."");
			return $query->row()->id;
		}else{
            return '0';
		}
	}
	function getMemberDetails($memberId){
		$this->db->where('is_deleted', '0');
		$this->db->where('id', $memberId);		
		$query = $this->db->get('tbl_member') or die(mysql_error());
		if($query->num_rows >= 1)
			return $query->row();
		else
            return false;
	}
	function changePassword(){
		$_POST['password'] = md5($this->input->post('password'));
		$data = array(
			'password' => $this->input->post('password'),
		);
		$this->db->where("id",$this->input->post('id'));
		$this->db->update('tbl_member',$data);
		
	}
	function checkEmailExist($email,$id=0){
		if($email!='' && $id != 0){
			$sql = "SELECT * FROM tbl_member WHERE email = '".$email."' AND id != $id AND is_deleted='0'";
		}else if($email!=''){
			$sql = "SELECT * FROM tbl_member WHERE email = '".$email."' AND is_deleted='0'";
		}
		$query = $this->db->query($sql);
		return $query->result();

	}
	public function activeMemberDetails(){
		$memberId = $this->session->userdata('id');
		if(isset($memberId) && $memberId > 0){
			$this->db->where('id', $memberId);		
			$query = $this->db->get('tbl_member') or die(mysql_error());
			if($query->num_rows >= 1){
//				$query->row()->module_access = explode(',',$query->row()->module_access);
				return $query->row();
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

    /**
     * Function to get details of members from email
     *
     * @param string $email
     *
     * @return bool | array
     */
    function getDetailsFromEmail($email){

        $this->db->where('email', $email);
        $query = $this->db->get('tbl_member') or die(mysql_error());
        if($query->num_rows >= 1)
            return $query->row();
        else
            return false;
    }

    /**
     * Function to generate random string and update the token field of admin table
     *
     * @param $user_id
     *
     * @return string
     */
    function setPasswordToken($user_id)
    {
        $reset_pwd_string = strtolower(base64_encode($user_id . gen_random_string(15)));
        $this->db->where('id', $user_id);
        $this->db->update('tbl_member', array('password_token' => $reset_pwd_string));

        return $reset_pwd_string;
    }

    /**
     * Function to update password
     * If password token is passed, where condition checks password token and resets the token field aswell
     *
     * @return bool
     */
    function updatePassword($user_id, $password_token = null){
        $data['password'] = md5($this->input->post('password'));
        $data['is_active'] = '1';
        if (!empty($password_token)) {
            $data['password_token'] = '';
        }
        if (empty($password_token)) {
            $this->db->where("email", $this->input->post('email'));
        } else {
            $this->db->where("id", $user_id);
        }
        $this->db->update('tbl_member',$data);

        return true;
    }

    /**
     * Function to check if password token is valid
     *
     * @param $password_token
     *
     * @return boolean
     */
    function isValidPasswordToken($password_token)
    {
        $this->db->select('id');
        $this->db->where('password_token', $password_token);
        $query = $this->db->get('tbl_member');

        if ($query->num_rows > 0) {
            $result = $query->result();

            return $result[0]->id;
        } else {
            return false;
        }
    }

    /**
     * Function to check if email token is valid
     *
     * @param $password_token
     *
     * @return boolean
     */
    function isValidEmailToken($emailToken)
    {
        $this->db->select('id');
        $this->db->where('emailToken', $emailToken);
        $query = $this->db->get('tbl_member');

        return ($query->num_rows > 0);
    }

    /**
     * Function to build data for
     *
     * @param $current_page
     *
     * @return array
     */
    public function buildTabData($current_page)
    {
        $tabList = array(
            'my_profile' => array(
                'title' =>'My Profile',
                'is_active' => 0
            ),
            'my_adds' => array(
                'title' =>'My Ads',
                'is_active' => 0
            ),
            /*'' => array(
                'title' =>'My Watchlist',
                'is_active' => 0
            ),*/
            /*'my_searches' => array(
                'title' =>'My Searches',
                'is_active' => 0
            ),*/
        );

        $tabList[$current_page]['is_active'] = 1;

        return array(
            'tabList' => $tabList,
            'pageTitle' => $tabList[$current_page]['title']
        );
    }

    /**
     * Function to validate member registration form
     *
     * @return boolean
     */
    public function validate_registration($user_id = false)
    {
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
        $this->form_validation->set_message('is_unique', '%s is already in use.');
        if ($user_id == false) {
            $this->form_validation->set_rules('email', 'Email Address / Username', 'xss_clean|trim|required|valid_email|is_unique[' . self::$user_table . '.email]');
        } else {
            $this->form_validation->set_rules('email', 'Email Address / Username', 'xss_clean|trim|required|valid_email|callback__edit_unique[' . $user_id .']');
        }
        if ($user_id == false) {
            $this->form_validation->set_rules('password', 'Password', 'xss_clean|trim|required');
        }
        $this->form_validation->set_rules('first_name', 'First Name', 'xss_clean|trim|required');
        $this->form_validation->set_rules('last_name', 'Last Name', 'xss_clean|trim|required');
        if ($user_id == false) {
            $this->form_validation->set_rules('security_question', 'Security Question', 'xss_clean|trim|required');
            $this->form_validation->set_rules('security_answer', 'Answer to Security Question', 'xss_clean|trim|required|min_length[5]');
        }
        $this->form_validation->set_rules('date_of_birth', 'Date of Birth', 'xss_clean|trim|required');
        $this->form_validation->set_rules('city', 'City', 'xss_clean|trim');
        $this->form_validation->set_rules('subscribe_site_update', 'subscribe_site_update', 'xss_clean|trim');
        $this->form_validation->set_rules('subscribe_offers', 'subscribe_offers', 'xss_clean|trim');

        //registration_ip
        return $this->form_validation->run();
    }

    function save_search_criteria()
    {
        ;
    }

    function is_duplicate_email($email, $userId = false)
    {
        $isDuplicate = false;
        $this->db->select('id');
        $this->db->where('email', $email);
        if (!empty($userId)) {
            $this->db->where('id !=', $userId);
        }
        $query = $this->db->get('tbl_member');
        if ($query->num_rows > 0) {
            $isDuplicate = true;
        }

        return $isDuplicate;
    }

    function is_password_valid($password, $userId)
    {
        $isDuplicate = false;
        $this->db->select('id');
        $this->db->where('password', md5($password));
        $this->db->where('id', $userId);
        $query = $this->db->get('tbl_member');
        if ($query->num_rows > 0) {
            $isDuplicate = true;
        }

        return $isDuplicate;
    }

    function validate_account_forms($formAction, $user_id)
    {
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
        if ($formAction == 'deleteAccount') {
            $this->form_validation->set_rules('password', 'Password', 'trim|required|callback__validate_password['.$user_id.']');
            if ($this->form_validation->run()) {
                $fieldValueArray = array(
                    'is_active' => '0'
                );
                $this->updateProfileFields($user_id, $fieldValueArray);
                $this->session->sess_destroy();
                $this->session->sess_create();
                $this->session->set_flashdata('flash_success', 'Your account deleted successfully!!');

                redirect(ROOT_URL . 'login');
            }
        } else if ($formAction == 'changeEmail') {
            $this->form_validation->set_rules('email', 'Email Address', 'xss_clean|trim|required|valid_email|callback__edit_unique[' . $user_id .']|callback__check_email_changed[' . $user_id .']');
            if ($this->form_validation->run()){
                $emailToken = urlencode(strtolower(base64_encode($user_id . uniqid('', true) . "-" . date("YmdHis"))));
                $fieldValueArray = array(
                    'email' => $this->input->post('email'),
                    'emailToken' => $emailToken,
                    'emailVerified' => '0'
                );
                $this->updateProfileFields($user_id, $fieldValueArray);
                $this->sendEmailVerification($user_id, $emailToken);
                $this->session->set_flashdata('flash_success', 'Your email changed successfully!!<br />Please check your email for verification link');

                return true;
            }
        } else if ($formAction == 'changePassword') {
            $this->form_validation->set_rules('old_password', 'Old password', 'xss_clean|trim|required|callback__validate_password['.$user_id.']');
            $this->form_validation->set_rules('new_password', 'New password', 'xss_clean|trim|required|min_length[6]');
            $this->form_validation->set_rules('confirm_password', 'Confirm password', 'xss_clean|trim|required|matches[new_password]');
            if ($this->form_validation->run()){
                $fieldValueArray = array('password' => md5($this->input->post('new_password')));
                $this->updateProfileFields($user_id, $fieldValueArray);
                $this->session->set_flashdata('flash_success', 'Your password changed successfully!!');

                return true;
            }
        } else if ($formAction == 'changeName') {
            $this->form_validation->set_rules('first_name', 'First name', 'xss_clean|trim|required');
            $this->form_validation->set_rules('last_name', 'Last name', 'xss_clean|trim|required');
            if ($this->form_validation->run()){
                $fieldValueArray = array(
                    'first_name' => $this->input->post('first_name'),
                    'last_name' => $this->input->post('last_name')
                );
                $this->updateProfileFields($user_id, $fieldValueArray);
                $this->session->set_flashdata('flash_success', 'Your name changed successfully!!');

                return true;
            }
        }

        return false;
    }

    public function sendEmailVerification($user_id, $emailToken)
    {
        $userDetails = $this->getDetails($user_id);
        $this->load->library('emailclass');
        $message = "<p>Dear " . $userDetails->first_name . ",</p><br />";

        $message .= "<p>You have updated your email id successfully</p>";
        $message .= "<p>Please use the below link to verify your new email id</p>";
        $message .= '<p><a target="_blank" href="' . MEMBER_ROOT_URL . "verify_email/" . $emailToken . '">' . MEMBER_ROOT_URL . "verify_email/" . $emailToken . "</a></p>";
        $subject = 'Email verification link for  ' . SITE_NAME;
        $content = '';
        $content .= $this->emailclass->emailHeader();
        $content .= $message;
        $content .= $this->emailclass->emailFooter();

        $email = $this->emailclass->send_mail($userDetails->email, $subject, $content);
    }

    function sendVerificationEmailOnRegistration($emailVerificationToken, $user_id)
    {
        $this->load->library('emailclass');
        $message = "<p>Dear " . $this->input->post('first_name') . ",</p><br />";

        $message .= "<p><strong>Congratulations!!</strong> You successfully created an account with ".SITE_NAME." </p>";
        $message .= "<p>Please use the below link to verify your email id</p>";
        $message .= '<p><a target="_blank" href="' . MEMBER_ROOT_URL . "verify_email/" . $emailVerificationToken . '">' . MEMBER_ROOT_URL . "verify_email/" . $emailVerificationToken . "</a></p>";
        $subject = 'Email verification link for  ' . SITE_NAME;
        $content = '';
        $content .= $this->emailclass->emailHeader();
        $content .= $message;
        $content .= $this->emailclass->emailFooter();

        $email = $this->emailclass->send_mail($this->input->post('email'), $subject, $content);
    }

    function updateProfileFields($user_id, $fieldValueArray){
        $data['updated_date'] = date('Y-m-d H:i:s');
        if ($user_id != false) {
            $this->db->where("id", $user_id);
        } else {
            $this->db->where("id", $this->input->post('id'));
        }
        $this->db->update('tbl_member', $fieldValueArray);

        return true;
    }
    function verifyEmail($emailToken, $fieldValueArray){
        $data['updated_date'] = date('Y-m-d H:i:s');

        $this->db->where("emailToken", $emailToken);
        $this->db->update('tbl_member', $fieldValueArray);

        return true;
    }
    function getDashboardCounts($user_id)
    {
        $fieldList = ' count(*) as searchCount ';
        $where = " user_id='$user_id' ";
        $result = $this->saved_search_model->getAllRecords($fieldList, $where);
        $data['savedSearchCount'] = (is_array($result) && !empty($result)) ? $result[0]->searchCount : 0;
        $fieldList = ' count(*) as userProfileAdCount ';
        $where = " created_by='$user_id' ";
        $result = $this->classified_model->getAllRecords($fieldList, $where);
        $data['myAdCount'] = (is_array($result) && !empty($result)) ? $result[0]->userProfileAdCount : 0;

        return $data;
    }
}
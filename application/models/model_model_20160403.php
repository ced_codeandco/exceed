<?php
class Model_model extends CI_Model {

    var $title   = '';
    var $content = '';
    var $date    = '';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }
    
    function addDetails(){
		
		$data = array(
			'brand_id' => $this->input->post('brand_id'),
			'title' => $this->input->post('title'),			
			'is_active' => $this->input->post('is_active'),			
			'created_date_time' =>date('Y-m-d H:i:s')			
		);
		
		$this->db->insert('tbl_vehicle_model',$data) or die(mysql_error()); 	
		$id=mysql_insert_id();
		return $id;
		
	}
	function checkTitleExist($title,$id=0){
	if($title!='' && $id != 0){
			$sql = "SELECT * FROM tbl_vehicle_model WHERE title = '".$title."' AND id != $id";
		}else if($title!=''){
			$sql = "SELECT * FROM tbl_vehicle_model WHERE title = '".$title."'";
		}
		$query = $this->db->query($sql);
		return $query->result();
	}
	function updateDetails(){
		
		$data = array(
			'brand_id' => $this->input->post('brand_id'),
			'title' => $this->input->post('title'),			
			'is_active' => $this->input->post('is_active')	
		);
		$this->db->where("id",$this->input->post('id'));
		$this->db->update('tbl_vehicle_model',$data);
		
		return $this->input->post('id');
		
	}
	
	function changeStatus($status,$id){
		
		mysql_query("UPDATE tbl_vehicle_model SET is_active = '$status' WHERE id= ".$id."");
		return true;
	}
	function deleteRecord($id){
		mysql_query("DELETE FROM tbl_vehicle_model WHERE id= ".$id."");
		return true;
	}
	function getDetails($id){
		$this->db->where('id', $id);		
		$query = $this->db->get('tbl_vehicle_model') or die(mysql_error());
		if($query->num_rows >= 1)
			return $query->row();
		else
            return false;
	}
	
	function getAllRecords($all='*',$where='',$orderby='',$limit=''){
		
		$sql ="select $all FROM tbl_vehicle_model WHERE 1=1 ";
		if($where!=''){
			$sql .= " AND $where ";
		}
		if($orderby!=''){
			$sql .= " $orderby ";
		}
		if($limit!=''){
			$sql .= " $limit ";
		}
		$query = $this->db->query($sql);
		$query_data = $query->result();
			
		return $query_data;
	}

    function getModelLookup($brand_id = null, $getActive = false){
        $sqlChild ="select id,title FROM tbl_vehicle_model WHERE 1=1 ";
        if (!empty($brand_id)) {
            $sqlChild .= " AND brand_id = " . $brand_id . " ";
        }
        if ($getActive != false) {
            $sqlChild .= " AND is_active = '1' ";
        }

        $queryChild = $this->db->query($sqlChild);
        $childData = $queryChild->result();
        $lookUp = array();
        if(count($childData) > 0 ) {
            foreach ($childData as $child) {
                $lookUp[$child->id] = $child->title;
            }
        }

        return $lookUp;
    }

    function getModelHierarchy($getActive = false)
    {
        $this->db->select('model.id, model.title,  count(clasfds.id) AS classifiedsCount', false);
        $this->db->from('tbl_vehicle_model  model');
        $this->db->join('tbl_classified AS clasfds', 'clasfds.model_id = model.id AND clasfds.is_active=\'1\'', 'LEFT');
        if ($getActive != false) {
            $this->db->where('model.is_active', '1');
        }
        $this->db->group_by('model.id');
        $this->db->order_by('model.title', 'ASC');

        $query = $this->db->get();
        $query_data = $query->result();
        $return = false;
        foreach ($query_data as $data) {
            $return[$data->id] = $data;
        }
        return $return;
    }
}
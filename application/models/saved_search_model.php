<?php
class Saved_search_model extends CI_Model {

    var $title   = '';
    var $content = '';
    var $date    = '';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }
    
    function addDetails($user_id){
		
			
		$data = array(
            'user_id' => $user_id,
			'search_query' => urldecode($this->input->post('search_query')),
			'is_active' => $this->input->post('is_active'),
			'created_date' =>date('Y-m-d H:i:s')			
		);
		
		$this->db->insert('tbl_saved_search',$data) or die(mysql_error());
		$id=mysql_insert_id();
		return $id;
		
	}
	
	function updateDetails(){
		
		$data = array(  
			'search_query' => $this->input->post('search_query'),
			'is_active' => $this->input->post('is_active')
			
		);
		$this->db->where("id",$this->input->post('id'));
		$this->db->update('tbl_saved_search',$data);
		
		
		return true;
		
	}
	
	function changeStatus($status,$id){
		
		mysql_query("UPDATE tbl_saved_search SET is_active = '$status' WHERE id= ".$id."");
		return true;
	}
	function deleteRecord($id){		
		mysql_query("UPDATE tbl_saved_search SET is_deleted = '1',is_active = '0' WHERE id= ".$id."");
		return true;
	}
	function getDetails($id){
		
		$this->db->where('id', $id);
		$this->db->where('is_deleted', '0');
		$query = $this->db->get('tbl_saved_search') or die(mysql_error());
		if($query->num_rows >= 1)
			return $query->row();
		else
            return false;
	}
	function getDetailsFromSearchQuery($search_query){
		
		$this->db->where('search_query', $search_query);
		$this->db->where('is_deleted', '0');
		$query = $this->db->get('tbl_saved_search') or die(mysql_error());

		if($query->num_rows >= 1)
			return $query->row();
		else
            return false;
	}
	function checkSearchQueryExist($search_query,$id=0){
		if($search_query!='' && $id != 0){
			$sql = "SELECT * FROM tbl_saved_search WHERE search_query = '".$search_query."' AND id != $id AND is_deleted='0'";
		}else if($search_query!=''){
			$sql = "SELECT * FROM tbl_saved_search WHERE search_query = '".$search_query."' AND is_deleted='0'";
		}
		$query = $this->db->query($sql);
		return $query->result();

	}
	function getAllRecords($all='*',$where='',$orderby='',$limit='', $getTotalRecords = false){
		
		$sql ="select ".( ($getTotalRecords == true) ? 'SQL_CALC_FOUND_ROWS' : '' )." $all FROM tbl_saved_search WHERE 1=1 ";
		if($where!=''){
			$sql .= " AND $where ";
		}
		$sql .= " AND is_deleted='0' ";
		if($orderby!=''){
			$sql .= " $orderby ";
		}
		if($limit!=''){
			$sql .= " $limit ";
		}
		
		$query = $this->db->query($sql);
        if ($getTotalRecords == true) {
            $rc = $this->db->query("SELECT FOUND_ROWS()");
            $row = $rc->row_array();
            $total_row_count = $row['FOUND_ROWS()'];
        }
		$query_data = $query->result();
        if ($getTotalRecords == true) {

            return array(
                'total_row_count' => $total_row_count,
                'result' => $query_data,
            );
        }
		
		return $query_data;
	}
	function getSavedSearchCount($user_id)
    {
        $this->db->select('count(*) as searchCount');
        $this->db->where('user_id', $user_id);
        $this->db->where('is_deleted', '0');
        $query = $this->db->get('tbl_saved_search') or die(mysql_error());
        if($query->num_rows >= 1) {
            $result = $query->row();
            return $result->searchCount;
        }
    }
	
	
	
}
<?php
class Setting_model extends CI_Model {

    var $title   = '';
    var $content = '';
    var $date    = '';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }
    
   function getDetails($settingId){
		
		$this->db->where('id', $settingId);		
		$query = $this->db->get('tbl_setting') or die(mysql_error());
		if($query->num_rows >= 1)
			return $query->row();
		else
            return false;
	}
	
	function addDetails(){
		
		$_POST['default_title'] = $this->renameSettingTitle($this->input->post('default_title'));
		$data = array(
			'default_title' => $this->input->post('default_title'),
			'value' => $this->input->post('value'),
			'created_by' => $this->session->userdata('admin_id'),
			'updated_by' => $this->session->userdata('admin_id'),
			'is_active' => $this->input->post('is_active'),
			'created_date' =>date('Y-m-d H:i:s')			
		);
		
		$this->db->insert('tbl_setting',$data) or die(mysql_error()); 	
		$id=mysql_insert_id();
		return $id;
		
	}
	function defineAllSettingVariables(){
		$settingList = $this->getAllRecords('id,default_title,value',"is_active = '1'");
		if(count($settingList) > 0 )
		{
			foreach ($settingList as $setting) 
			{
				define($setting->default_title,$setting->value);
			}
		}
	}
	function updateDetails(){
		
		$_POST['default_title'] = $this->renameSettingTitle($this->input->post('default_title'));
		$data = array(
			'default_title' => $this->input->post('default_title'),
			'value' => $this->input->post('value'),			
			'updated_by' => $this->session->userdata('admin_id'),
			'is_active' => $this->input->post('is_active')
		);
		$this->db->where("id",$this->input->post('id'));
		$this->db->update('tbl_setting',$data);		
		return true;		
	}
	function renameSettingTitle($setting_title){
		$urltitle=preg_replace('/[^a-z0-9]/i',' ', ltrim(rtrim(strtoupper($setting_title))));
		$newurltitle=str_replace(" ","_",$urltitle);		
		return $newurltitle;
		
	}
	function checkSettingExist($setting_title,$id=0){
		$setting_title =  $this->renameSettingTitle($setting_title);
		if($setting_title!='' && $id != 0){
			$sql = "SELECT * FROM tbl_setting WHERE default_title = '".$setting_title."' AND id != $id";
		}else if($setting_title!=''){
			$sql = "SELECT * FROM tbl_setting WHERE default_title = '".$setting_title."' ";
		}
		$setting_title = $this->renameSettingTitle($setting_title);		
		$query = $this->db->query($sql);
		return $query->result();

	}
	
	function changeStatus($status,$id){
		
		mysql_query("UPDATE tbl_setting SET is_active = '$status' WHERE id= ".$id."");
		return true;
	}
	function deleteRecord($id){
		
		mysql_query("DELETE FROM tbl_setting WHERE id= ".$id);
		return true;
	}
	
	function getAllRecords($all='*',$where='',$orderby='',$limit=''){
		
		$sql ="select $all FROM tbl_setting WHERE 1=1 ";
		if($where!=''){
			$sql .= " AND $where ";
		}
		if($orderby!=''){
			$sql .= " $orderby ";
		}
		if($limit!=''){
			$sql .= " $limit ";
		}
		//echo $sql;
		//exit;
		$query = $this->db->query($sql);
		return $query->result();
	}
	
	
	
}
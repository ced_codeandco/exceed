<?php
class Tips_and_tricks_category_model extends CI_Model {

    var $title   = '';
    var $content = '';
    var $date    = '';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }
    
    function addDetails($data = array()){

        if (empty($data)) {

            $order = $this->getLastOrder();
            $category_order = $order + 1;

            $data = array(
                'title' => $this->input->post('title'),
                'url_slug' => $this->input->post('category_slug'),
                'description' => $this->input->post('description'),
                'category_order' => $category_order,
                'is_active' => $this->input->post('is_active'),
                'created_by' => $this->session->userdata('admin_id'),
                'updated_by' => $this->session->userdata('admin_id'),
                'created_date' => date('Y-m-d H:i:s')
            );
        }
		
		$this->db->insert('tbl_tips_and_tricks_category',$data) or die(mysql_error());
		$id=mysql_insert_id();
		return $id;
		
	}
	
	function updateDetails($data, $id){
		if (empty($data)) {
            $data = array(
                'title' => $this->input->post('title'),
                'description' => $this->input->post('description'),
                'is_active' => $this->input->post('is_active'),
                'updated_by' => $this->session->userdata('admin_id')
            );
        }
		$this->db->where("id",$id);
		$this->db->update('tbl_tips_and_tricks_category',$data);

		return true;
	}
	
	function changeStatus($status,$id){
		
		mysql_query("UPDATE tbl_tips_and_tricks_category SET is_active = '$status' WHERE id= ".$id."");
		return true;
	}
	function deleteRecord($id){		
		mysql_query("UPDATE tbl_tips_and_tricks_category SET is_deleted = '1', deleted_date_time = '".date('Y-m-d H:i:s')."' WHERE id= ".$id."");
		return true;
	}
	function getDetails($id){
		
		$this->db->where('id', $id);
		$this->db->where('is_deleted', '0');
		$query = $this->db->get('tbl_tips_and_tricks_category') or die(mysql_error());
		if($query->num_rows >= 1)
			return $query->row();
		else
            return false;
	}
	function generateCategorySlug($title='category page'){
		$urltitle=preg_replace('/[^a-z0-9]/i',' ', ltrim(rtrim(strtolower($title))));
		$newurltitle=str_replace(" ","-",$urltitle);
		$queryCount = "SELECT url_slug from tbl_tips_and_tricks_category WHERE url_slug LIKE '".$newurltitle."%'";
		$rqC = mysql_num_rows(mysql_query($queryCount));
		if($rqC != 0){
			$newurltitle = $newurltitle.'-'.$rqC; 
		}
		return $newurltitle;				
	}
	function getAllRecords($all='*',$where='',$orderby='',$limit=''){
		
		$sql ="select $all FROM tbl_tips_and_tricks_category WHERE 1=1 ";
		if($where!=''){
			$sql .= " AND $where ";
		}
		$sql .= " AND is_deleted='0' ";
		if($orderby!=''){
			$sql .= " $orderby ";
		}
		if($limit!=''){
			$sql .= " $limit ";
		}

		$query = $this->db->query($sql);
		$query_data = $query->result();

		return $query_data;
	}
	function getClassifiedCategoryList($category_id){

		$sql ="select id,title FROM tbl_tips_and_tricks_category WHERE 1=1 ";
		$sql .= " AND parent_id = 0 AND is_deleted='0' ";
		
		$query = $this->db->query($sql);
		$query_data = $query->result();
		
               
             
		$options = '';
		if(count($query_data) > 0 )
		{
			foreach ($query_data as $parent) 
			{
				$options .='<option value="'.$parent->id.'"';
				$options .= ($category_id != 0 && $category_id== $parent->id) ? 'selected="selected"' : "";				
				$options .= '>'.$parent->title.'</option>';
				$sqlChild ="select id,title FROM tbl_tips_and_tricks_category WHERE 1=1 ";
				$sqlChild .= " AND parent_id = ".$parent->id." AND is_deleted='0' ";
				
				$queryChild = $this->db->query($sqlChild);
				$childData = $queryChild->result();
				if(count($childData) > 0 )
				{
					foreach ($childData as $child) 
					{
						$options .='<option value="'.$child->id.'"';
						$options .= ($category_id != 0 && $category_id== $child->id) ? 'selected="selected"' : "";						
						$options .= '>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$child->title.'</option>';
						$sqlChild1 ="select id,title FROM tbl_tips_and_tricks_category WHERE 1=1 ";
						$sqlChild1 .= " AND parent_id = ".$child->id." AND is_deleted='0' ";
						
						$queryChild1 = $this->db->query($sqlChild1);
						$childData1 = $queryChild1->result();
						if(count($childData1) > 0 )
						{
							foreach ($childData1 as $child1) 
							{
								$options .='<option value="'.$child1->id.'"';
								$options .= ($category_id != 0 && $category_id== $child1->id) ? 'selected="selected"' : "";								
								$options .= '>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$child1->title.'</option>';
							}
						}
					}
				}
			}
		}
		$options .= '';
		return $options;
	}
	function getParentCategoryLists($id=0,$parent_id=0){
		$sql ="select id,title,parent_id FROM tbl_tips_and_tricks_category WHERE 1=1 ";
		$sql .= " AND parent_id = 0 AND is_deleted='0' ";
		
		$query = $this->db->query($sql);
		$query_data = $query->result();
		
               
             
		$options = '<option value="0" selected="selected">Select Parent category</option>';
		if(count($query_data) > 0 )
		{
			foreach ($query_data as $parent) 
			{
				$options .='<option value="'.$parent->id.'"';
				$options .= ($parent_id != 0 && $parent_id== $parent->id) ? 'selected="selected"' : "";
				$options .= ($id != 0 && $id == $parent->id) ? "disabled='disabled'" : "";
				$options .= '>'.$parent->title.'</option>';
				$sqlChild ="select id,title,parent_id FROM tbl_tips_and_tricks_category WHERE 1=1 ";
				$sqlChild .= " AND parent_id = ".$parent->id." AND is_deleted='0' ";
				
				$queryChild = $this->db->query($sqlChild);
				$childData = $queryChild->result();
				if(count($childData) > 0 )
				{
					foreach ($childData as $child) 
					{
						$options .='<option value="'.$child->id.'"';
						$options .= ($parent_id != 0 && $parent_id== $child->id) ? 'selected="selected"' : "";
						$options .= (($id != 0 && $id == $child->id) || ($id != 0 && $id == $child->parent_id)) ? "disabled='disabled'" : "";
						$options .= '>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$child->title.'</option>';
					}
				}
			}
		
		}
		$options .= '';
		return $options;
	}
	function getSubPageCount($parent_id){
		$sql ="select count(id) as sub_page_count FROM tbl_tips_and_tricks_category WHERE is_deleted='0' AND parent_id = $parent_id";
		$query = $this->db->query($sql);
		$result = $query->result();
		return $result[0]->sub_page_count;		
	}
	function getLastOrder(){
		$sql ="select category_order FROM tbl_tips_and_tricks_category WHERE is_deleted='0' ORDER BY category_order desc LIMIT 0,1 ";
		$query = $this->db->query($sql);
		$result = $query->result();
		return $result[0]->category_order;	
		
	}
	
	function changeOrder($id,$category_order,$position){

		if($id!='' && $category_order!='' && $position!=''){
		$pageDetails = $this->getDetails($id);	
			if($position=='Dn'){
				$qr="select category_order,id from tbl_tips_and_tricks_category where parent_id = $pageDetails->parent_id  AND category_order > '".$category_order."' AND is_deleted='0' order by category_order asc limit 0,1";
				$query = $this->db->query($qr);
				$result1 = $query->result();
				if($result1 && count($result1)>0){
					$NewId=$result1[0]->id;
					$NewOrder = $result1[0]->category_order;
					$qry = "UPDATE tbl_tips_and_tricks_category SET `category_order`= $category_order WHERE id =".$NewId;
						mysql_query($qry);

					$qry1 = "UPDATE tbl_tips_and_tricks_category SET `category_order`= $NewOrder WHERE id =".$id;
					mysql_query($qry1);
				}
			}
			
			if($position=='Up'){
				$qr="select category_order,id from tbl_tips_and_tricks_category where parent_id = $pageDetails->parent_id  AND category_order < '".$category_order."' AND is_deleted='0' order by category_order desc limit 0,1";
				$query = $this->db->query($qr);
				$result1 = $query->result();
				
				if($result1 && count($result1)>0){
					$NewId=$result1[0]->id;
					$NewOrder = $result1[0]->category_order;
					$qry = "UPDATE tbl_tips_and_tricks_category SET `category_order`= $category_order WHERE id =".$NewId;
					mysql_query($qry);

					$qry1 = "UPDATE tbl_tips_and_tricks_category SET `category_order`= $NewOrder WHERE id =".$id;
					mysql_query($qry1);
				}
			}
		}		
	}
	function getCategoryHierarchy()
    {
        $query = $this->db->query("SELECT tntc.id, tntc.`title` as category_title, count(tnt.id) AS itemsCount
                                    FROM `tbl_tips_and_tricks_category` tntc
                                    LEFT JOIN tbl_tips_and_tricks tnt ON (tnt.category_id = tntc.id AND tnt.is_active = '1' AND tnt.is_deleted = '0')
                WHERE tntc.is_active =  '1' AND tntc.is_deleted =  '0' GROUP BY tntc.id ORDER BY tntc.category_order ASC ");
//echo $this->db->last_query();
        $category = array();

        if ($query->num_rows >= 1) {
            $query_data = $query->result_array();
            foreach ($query_data as $data) {
                $category[$data['id']] = $data;
            }
        }

        return $category;
    }

    public function getCitiesByCategoryId($categoryId)
    {
        $sql = 'SELECT GROUP_CONCAT(city_id) AS city_id_list FROM `tbl_tips_and_tricks_city` WHERE 	tips_and_tricks_category_id = "'.$categoryId.'" ';
        $cities = array();
        $query = $this->db->query($sql);
        if ($query->num_rows >= 1) {
            $result = $query->result_array();
//            print_r($result);
            foreach ($result as $value) {
                $cities = $value['city_id_list'];
            }

            return $cities;
        }
    }

    function getCityDetailsByIdList($cityIdList)
    {

        $cityIdList = trim($cityIdList, ',');
        $sql = "SELECT `id`, `combined` FROM `tbl_cities` WHERE `id` IN($cityIdList)";
        $query = $this->db->query($sql);
        $return = array();
        if ($query->num_rows >= 1) {
            $result = $query->result_array();
            foreach ($result as $value) {
                $return[$value['id']] = $value;
            }
        }

        return $return;
    }

    function addNewTipsAndTricksCategory($category_id)
    {
        if ($category_id != -1) {
            return $category_id;
        }
        $new_category = $this->input->post('new_category');
        $order = $this->getLastOrder();
        $category_order = $order + 1;

        $data = array(
            'title' => $new_category,
            'url_slug' => $this->generateCategorySlug($new_category),
            'category_order' => $category_order,
            'is_active' => '1',
            'created_by' => $this->session->userdata('admin_id'),
            'updated_by' => $this->session->userdata('admin_id'),
            'created_date' => date('Y-m-d H:i:s')
        );

        $categoryId = $this->addDetails($data);
        $city_id = $this->input->post('city_id');
        if (!empty($city_id)) {
            $this->addCityCategoryCombination($categoryId, $city_id);
        }

        return $categoryId;
    }

    public function addCityCategoryCombination($categoryId, $city_id)
    {
        $this->db->select('id');
        $this->db->where('tips_and_tricks_category_id', $categoryId);
        $this->db->where('city_id', $city_id);
        $query = $this->db->get('tbl_tips_and_tricks_city');
        if ($query->num_rows() < 1) {
            $this->db->insert('tbl_tips_and_tricks_city', array('tips_and_tricks_category_id' => $categoryId, 'city_id' => $city_id));
        }
    }

    public function deleteCityCategoryCombination($categoryId)
    {
        $this->db->delete('tbl_tips_and_tricks_city', array('tips_and_tricks_category_id' => $categoryId));
    }

    public function getCityCategoryCombination($categoryId)
    {
        $this->db->select('tbl_cities.*');
        $this->db->from('tbl_tips_and_tricks_city');
        $this->db->join('tbl_cities', 'tbl_cities.id =tbl_tips_and_tricks_city.city_id');
        $this->db->where('tbl_tips_and_tricks_city.tips_and_tricks_category_id', $categoryId);
        $query = $this->db->get();
        $query_data = $query->result();
        $return = array();
        foreach ($query_data as $data) {
            $return[$data->id] = $data;
        }

        return $return;
    }
}
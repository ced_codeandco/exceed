<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 5/6/2015
 * Time: 4:46 PM
 */

class Tips_and_tricks_model extends CI_Model
{

    var $title = '';
    var $content = '';
    var $date = '';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
    }

    function getDetails($id){
        $this->db->where('is_deleted', '0');
        $this->db->where('id', $id);
        $query = $this->db->get('tbl_tips_and_tricks') or die(mysql_error());
        if($query->num_rows >= 1)
            return $query->row();
        else
            return false;
    }

    function updateDetails($classifiedId = false, $data = array()){

        if(LOG_ENTRY == TRUE){
//            $id = $this->addCMSLog($this->input->post('id'));
        }

        $this->db->where("id", empty($classifiedId) ? $this->input->post('id') : $classifiedId);
        $this->db->update('tbl_tips_and_tricks', $data);
//echo $this->db->last_query(); die();
        return true;

    }

    function changeStatus($status,$id){

        mysql_query("UPDATE tbl_tips_and_tricks SET is_active = '$status' WHERE id= ".$id."");
        return true;
    }

    /**
     * Function to get all tips and tricks
     */
    function getAllRecords($all='*',$where='',$orderby='',$limit='', $getTotalRecords = false){

        $sql ="select ".( ($getTotalRecords == true) ? 'SQL_CALC_FOUND_ROWS' : '' )." $all,created_by FROM tbl_tips_and_tricks WHERE 1=1 ";
        if($where!=''){
            $sql .= " AND $where ";
        }
        $sql .= " AND is_deleted='0' ";
        if($orderby!=''){
            $sql .= " $orderby ";
        }
        if($limit!=''){
            $sql .= " $limit ";
        }
        $query = $this->db->query($sql);
        if ($getTotalRecords == true) {
            $rc = $this->db->query("SELECT FOUND_ROWS()");
            $row = $rc->row_array();
            $total_row_count = $row['FOUND_ROWS()'];
        }
        $query_data = $query->result();
        if(count($query_data) > 0 )
        {
            $i = 0;
            foreach ($query->result_array() as $value)
            {
                if (!empty($value['category_id'])) {
                    $categoryData = $this->getCategoryDetails($value['category_id']);
                    $query_data[$i]->category_name = !empty($categoryData->title) ? $categoryData->title : '';
                    $query_data[$i]->category_id = !empty($categoryData->id) ? $categoryData->id : '';
                }
                if (!empty($value['userProfileAdCount'])) {
                    $query_data[$i]->adCount = $value['userProfileAdCount'];
                } else if (!empty($value['created_by'])) {
                    $memberData = $this->getMemberDetails($value['created_by']);
                    $query_data[$i]->member_name = $memberData->first_name . ' ' . $memberData->last_name;
                    $query_data[$i]->first_name = $memberData->first_name;
                    $query_data[$i]->last_name = $memberData->last_name;
                    $query_data[$i]->member_email = $memberData->email;
                }
                if (!empty($value['id'])) {
                    /*$query_data[$i]->new_inquiry_count = $this->getNewInquiryDetails($value['id']);
                    $query_data[$i]->total_inquiry_count = $this->getTotalInquiryDetails($value['id']);
                    $query_data[$i]->image_list = $this->getTotalImage($value['id']);*/
                }

                $i++;
            }

        }
        if ($getTotalRecords == true) {

            return array(
                'total_row_count' => $total_row_count,
                'result' => $query_data,
            );
        }
        return $query_data;
    }

    function getCategoryDetails($id){
        $this->db->where('id', $id);
        $query = $this->db->get('tbl_tips_and_tricks_category') ;
        if($query->num_rows >= 1)
            return $query->row();
        else
            return false;
    }
    function deleteRecord($id){
        $this->db->query("UPDATE tbl_tips_and_tricks SET is_deleted = '1', deleted_date_time = '".date('Y-m-d H:i:s')."' WHERE id= ".$id."");
        // echo $this->db->last_query();
        return true;
    }

    public function generateTipsSlug($title)
    {
        $urltitle=preg_replace('/[^a-z0-9]/i',' ', ltrim(rtrim(strtolower($title))));
        $newurltitle=str_replace(" ","-",$urltitle);
        $queryCount = "SELECT url_slug from tbl_tips_and_tricks WHERE url_slug LIKE '".$newurltitle."%'";
        $rqC = mysql_num_rows(mysql_query($queryCount));
        if($rqC != 0){
            $newurltitle = $newurltitle.'-'.$rqC;
        }
        return $newurltitle;
    }

    /**
     * Function to add details of tips and tricks to db
     *
     * @param array $data
     * @return mixed
     */
    function addDetails($data = array())
    {
        if (empty($data) OR !is_array($data)) {
            throw new Exception("Data cannot be null");
        }
        if (empty($data['url_slug'])) {
            $data['url_slug'] = $this->generateUrlSlug($this->input->post('title'));
        }
        $this->db->insert('tbl_tips_and_tricks', $data) or die(mysql_error());

        return $this->db->insert_id();
    }

    function generateUrlSlug($title='classified'){
        $urltitle=preg_replace('/[^a-z0-9]/i',' ', ltrim(rtrim(strtolower($title))));
        $newurltitle=str_replace(" ","-",$urltitle);
        $queryCount = "SELECT url_slug from tbl_tips_and_tricks WHERE url_slug LIKE '".$newurltitle."%'";
        $rqC = mysql_num_rows(mysql_query($queryCount));
        if($rqC != 0){
            $newurltitle = $newurltitle.'-'.$rqC;
        }
        return $newurltitle;
    }

    function getSearchResultTotal($searchString){
        $queryCount = "SELECT count(id) as total_rec FROM tbl_tips_and_tricks AS tt WHERE is_deleted = '0' AND is_active = '1'$searchString";
        $query = $this->db->query($queryCount);
        $result = $query->result();
        $totalRecords = $result[0]->total_rec;

        return $totalRecords;
    }
    /**
     * Function to build sql query string for searching
     *
     * @param $searchCriteria array
     *
     * @return string
     */
    public function build_search_string($searchCriteria)
    {
        $searchString  = '';
        $keywordSearchString = '';
        if (isset($searchCriteria['category_id']) && $searchCriteria['category_id'] != '')
            $searchString .= ' AND tt.category_id = ' . $searchCriteria['category_id'];

        if (isset($searchCriteria['city_id']) && $searchCriteria['city_id'] != '')
            $searchString .= ' AND tt.city_id = ' . $searchCriteria['city_id'];

        if (isset($searchCriteria['sub_category']) && $searchCriteria['sub_category'] != '')
            $searchString .= ' AND tt.sub_category_id = ' . $searchCriteria['sub_category'];

        if (isset($searchCriteria['keyword']) && $searchCriteria['keyword'] != '')
            $searchString .= 'AND ('.process_search($searchCriteria['keyword'], 'tt.title').' OR '
                .process_search($searchCriteria['keyword'], 'short_description').' OR '
                .process_search($searchCriteria['keyword'], 'tt.description').')';

        return $searchString;
    }

    /**
     * Function to build pagination links
     *
     * @param $searchResultTotal
     * @param $per_page
     * @param $searchCriteria
     *
     * @return string
     */
    function build_search_paginator($searchResultTotal, $per_page, $searchCriteria, $base_url = '')
    {
        $this->load->library('pagination');

        unset($searchCriteria['per_page']);
        //$config['uri_segment'] = 2;
        $config['page_query_string'] = TRUE;
        $config['enable_query_strings'] = TRUE;
        $config['base_url'] = !empty($base_url) ? $base_url : ROOT_URL.'/blog?';
        $config['base_url'] .= (empty($searchCriteria) ? '' : http_build_query($searchCriteria));
        $config['total_rows'] = $searchResultTotal;
        $config['per_page'] = $per_page;

        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['first_link'] = '<span aria-hidden="true">&laquo;&laquo;</span>';;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';

        $config['prev_link'] = '<span aria-hidden="true">&laquo;</span>';
        $config['prev_tag_open'] = '<li id="pagination-prev-link-bottom" >';
        $config['prev_tag_close'] = '</li>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $config['next_link'] = '<span aria-hidden="true">&raquo;</span>';
        $config['next_tag_open'] = '<li id="pagination-next-link-bottom">';
        $config['next_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li><a class="active">';
        $config['cur_tag_close'] = '</a></li>';

        $config['last_link'] = '<span aria-hidden="true">&raquo;&raquo;</span>';;
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);
        return $this->pagination->create_links();
    }

    function getSearchResult($searchString, $records_per_page = 10, $starts_with, $order_by = '', $order_by_modifier = ''){

        $query = "SELECT tt.*, COUNT(ttr.tip_id) AS commentCount, ttcateg.title AS categoryName FROM tbl_tips_and_tricks tt ";
        $query .= "LEFT JOIN tbl_tip_comment ttr ON tt.id = ttr.tip_id ";
        $query .= "LEFT JOIN tbl_tips_and_tricks_category ttcateg ON tt.category_id = ttcateg.id ";

        $query .= "WHERE tt.is_deleted = '0' AND tt.is_active = '1' $searchString ";
        $query .= " GROUP BY (tt.id) ";
        $query .= !empty($order_by) ? " ORDER BY $order_by" : '';
        $query .= (!empty($order_by) && !empty($order_by_modifier)) ? " $order_by_modifier" : '';
        $query .= " LIMIT $starts_with, $records_per_page";

        $query = $this->db->query($query);
        //echo $this->db->last_query();
        $result = $query->result();

        return $result;
    }

    /**
     * Function to build search query string to be saved
     *
     * @param $searchCriteria
     *
     * @return string
     */
    public function getCategoryQueryString($searchCriteria)
    {
        if(isset($searchCriteria['per_page'])) { unset($searchCriteria['per_page']); }
        if(isset($searchCriteria['order'])) { unset($searchCriteria['order']); }
        if(isset($searchCriteria['category'])) { unset($searchCriteria['category']); }

        return (empty($searchCriteria) ? '' : urlencode(http_build_query(array_filter($searchCriteria))));
    }

    public function getComments($tip_id)
    {
        $this->db->select('ttr.*, tm.first_name');
        $this->db->from('tbl_tip_comment as ttr');
        $this->db->join('tbl_member as tm', 'tm.id = ttr.reviewer_id');
        $this->db->where('ttr.tip_id', $tip_id);
        $this->db->order_by('created_date_time', 'DESC');
        $query = $this->db->get();

        if($query->num_rows >= 1)
            return $query->result();
        else
            return false;
    }

    public function postComments($comment, $tip_id, $user_id)
    {
        $data = array('reviewer_id' => $user_id, 'comment' => $comment, 'tip_id' => $tip_id, 'created_date_time' => date('Y-m-d H:i:s'));
        /*$query = $this->db->get_where('tbl_tip_comment', array('reviewer_id' => $user_id, 'tip_id' => $tip_id));
        if($query->num_rows >= 1) {
            $result = $query->row();
            $this->db->update('tbl_tip_comment', $data, array('id' => $result->id));
        } else {*/
            $this->db->insert('tbl_tip_comment', $data);
        /*}*/
    }

    public function postTipRate($tip_id, $rate, $user_id)
    {
        $query = $this->db->get_where('tbl_tips_review', array('reviewer_id' => $user_id, 'tip_id' => $tip_id));
        $data = array('reviewer_id' => $user_id, 'rate_value' => $rate, 'tip_id' => $tip_id);
        if($query->num_rows >= 1) {
            $result = $query->row();
            $this->db->update('tbl_tips_review', $data, array('id' => $result->id));
        } else {
            $this->db->insert('tbl_tips_review', $data);
        }

        $this->db->select_avg('rate_value');
        $this->db->from('tbl_tips_review');
        $this->db->where('tip_id', $tip_id);
        $query = $this->db->get();

        $result = $query->row();

        $rate = $result->rate_value;

        return roundAverage($rate);
    }

    function getMyReview($tip_id, $user_id)
    {
        $query = $this->db->get_where('tbl_tips_review', array('reviewer_id' => $user_id, 'tip_id' => $tip_id));
        if($query->num_rows >= 1) {
            return $query->row();
        }
    }

    function getRecentTips($count) {
        $query = "SELECT id, url_slug, title, short_description, image FROM tbl_tips_and_tricks ";

        $query .= "WHERE is_deleted = '0' AND is_active = '1' ";
        $query .= " ORDER BY created_date_time DESC ";
        $query .= " LIMIT $count";

        $query = $this->db->query($query);
        $result = $query->result();

        return $result;
    }

    function getRecentComments($count) {
        $this->db->select('t.id, t.url_slug, t.title, ttc.comment, image, tm.first_name');
        $this->db->from('tbl_tip_comment as ttc');
        $this->db->join('tbl_tips_and_tricks as t', 't.id = ttc.tip_id');
        $this->db->join('tbl_member as tm', 'tm.id = ttc.reviewer_id');
        $this->db->where('t.is_deleted', '0');
        $this->db->where('t.is_active', '1');
        if (!empty($count) && is_numeric($count)) {
            $this->db->limit($count);
        }
        $this->db->order_by('ttc.created_date_time', 'DESC');
        $query = $this->db->get();

        if($query->num_rows >= 1)
            return $query->result();
        else
            return false;
    }

    function deleteComment($commentId)
    {
        $this->db->delete('tbl_tip_comment', array('id' => $commentId));
    }
}
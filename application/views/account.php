<script type="text/javascript" src="<?php echo ROOT_URL_BASE?>js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo ROOT_URL_BASE?>js/jquery.accordion.js"></script>
<div class="container ">
    <ul class="bradcram">
        <li><a href="<?php echo ROOT_URL;?>">Home</a></li>
        <li><a href="#">Account Settings</a></li>
        <li>Account Settings</li>
    </ul>
    <h2 class="page-title">Account Settings</h2>
    <div class="row">
        <?php echo validation_errors(); ?>
        <?php
        if(isset($errMsg) && $errMsg != ''){
            echo '<div class="alert alert-danger">' . $errMsg. '</div>';
            unset($errMsg);
        }
        if(isset($succMsg) && $succMsg != ''){
            echo '<div class="alert alert-success">' . $succMsg . '</div>';
            unset($succMsg);
        }?>
        <div class="example2 accordionBoxes">
            <div class="panel panel-default"><span class="title-accoudin"><strong>Name:</strong> <?php echo $profileData->first_name .' '. $profileData->last_name;?></span>
                <div class="panel-heading" data-acc-link="demo5">Edit</div>
                <div class="panel-body" data-acc-content="demo5">
                    <div class="contact-form accountsetting-form">
                        <form id="nameChangeForm" method="post" action="<?php echo MEMBER_ROOT_URL?>account_settings">
                            <input type="hidden" name="formAction" value="changeName">
                            <div class="post-field-col">
                                <input type="text" name="first_name" required class="validate[required]"placeholder="First Name" value="<?php echo $profileData->first_name;?>" >
                            </div>
                            <div class="post-field-col left-margine-20px-rorm">
                                <input type="text" name="last_name" required  placeholder="Last Name" value="<?php echo $profileData->last_name;?>" >
                            </div>
                            <div class="post-field-col left-margine-20px-rorm">
                                <input name="submit" value="Change Name" type="submit" class="submit change-name">
                            </div>

                        </form>
                    </div>
                </div>
            </div>

            <div class="panel panel-default"><span class="title-accoudin"><strong>Password:</strong> *******</span>
                <div class="panel-heading" data-acc-link="demo6">Edit</div>
                <div class="panel-body" data-acc-content="demo6">
                    <div class="contact-form accountsetting-form">
                        <form id="passwordChangeForm" method="post" action="<?php echo MEMBER_ROOT_URL?>account_settings">
                            <input type="hidden" name="formAction" value="changePassword">
                            <div class="post-field-col col-md-4-form">
                                <input name="old_password" required type="password" placeholder="Old Password">
                            </div>
                            <div class="post-field-col  col-md-4-form left-margine-20px-rorm">
                                <input name="new_password" id="new_password_field" required type="password" placeholder="New Password">
                            </div>
                            <div class="post-field-col col-md-4-form left-margine-20px-rorm">
                                <input name="confirm_password" required type="password" placeholder="Confirm Password">
                            </div>
                            <div class="post-field-col col-md-4-form left-margine-20px-rorm">
                                <input name="submit" value="Change Password" type="submit" class="submit change-name">
                            </div>

                        </form>
                    </div>
                </div>
            </div>

            <div class="panel panel-default"><span class="title-accoudin"><strong>Email Address:</strong> <?php echo $profileData->email;?></span>
                <div class="panel-heading" data-acc-link="demo7">Edit</div>
                <div class="panel-body" data-acc-content="demo7">
                    <p class="font-16px text-left">"For security reasons, changing your email address requires confirmation"</p>
                    <div class="contact-form accountsetting-form">
                        <form id="emailChangeForm" method="post" action="<?php echo MEMBER_ROOT_URL?>account_settings">
                            <input type="hidden" name="formAction" value="changeEmail">
                            <div class="post-field-col ">
                                <input type="email" name="email"  placeholder="Email Address" value="<?php echo $profileData->email;?>">
                            </div>

                            <div class="post-field-col left-margine-20px-rorm">
                                <input name="submit" value="Change Email" type="submit" class="submit change-name">
                            </div>

                        </form>
                    </div>
                </div>
            </div>

            <div class="panel panel-default"><span class="title-accoudin"><strong>Dectivation:</strong> Delete my account</span>
                <div class="panel-heading" data-acc-link="demo8">Edit</div>
                <div class="panel-body" data-acc-content="demo8">
                    <div class="contact-form accountsetting-form">
                        <form id="deleteAccountForm" method="post" action="<?php echo MEMBER_ROOT_URL?>account_settings">
                            <input type="hidden" name="formAction" value="deleteAccount">
                            <div class="post-field-col ">
                                <p class="red-text-normal text-left" style="margin-top:10px;">Do you want delete your account?</p>
                            </div>

                            <div class="post-field-col left-margine-20px-rorm display-hidden confirmDeletePasswordFields">
                                <input name="password" required type="password" placeholder="Confirm your Password">
                            </div>
                            <div class="post-field-col left-margine-20px-rorm" id="confirmDeleteYesButton">
                                <button name="submit"  type="button" class="submit change-name">YES</button>
                            </div>
                            <div class="post-field-col left-margine-20px-rorm display-hidden confirmDeletePasswordFields">
                                <input name="submit" value="SUBMIT" type="submit" class="submit change-name">
                            </div>
                        </form>
                    </div>
                </div>
            </div>

        </div>


    </div>


    <div class="divider-futered"></div>
</div>


<script type="text/javascript">
    $(function() {
        $('.accordionBoxes').accordion();

        /*$('.example2').accordion();

        $('.example3').accordion();*/
    });
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#nameChangeForm').validate({
            rules: {
                first_name: { required: true },
                last_name: { required: true },
            },
            messages: {
                first_name: { required: 'Please enter your first name' },
                last_name: { required: 'Please enter your last name' },
            }
        })
        $('#passwordChangeForm').validate({
            rules: {
                old_password: { required: true },
                new_password: {
                    required: true,
                    minlength: 6
                },
                confirm_password: {
                    required: true,
                    equalTo: "#new_password_field"
                }
            },
            messages: {
                old_password: { required: 'Please enter your old password' },
                new_password: {
                    required: 'Please enter a new password',
                    minlength: 'Password should contain at least 6 characters'
                },
                confirm_password: {
                    required: 'Retype new password to confirm',
                    equalTo: 'New password and confirm password fields should match'
                }
            }
        })
        $('#emailChangeForm').validate({
            rules: {
                email: { required: true, email: true },
            },
            messages: {
                email: {
                    required: 'Please enter your email',
                    email: 'Please enter a valid email'
                },
            }
        })
        $('#deleteAccountForm').validate({
            rules: { password: { required: true } },
            messages: { email: { password: 'Please enter your password' } }
        })
    })
    $('#confirmDeleteYesButton').find('button').click(function(){
        if (confirm('Are you sure you want delete your account')) {
            $('#confirmDeleteYesButton').hide();
            $('.confirmDeletePasswordFields').show();
        }
    })
</script>
<script type="text/javascript" src="<?php echo ROOT_URL_BASE?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo ROOT_URL_BASE?>assets/ckfinder/ckfinder.js"></script>
<div id="content" class="col-lg-10 col-sm-10">
<!-- content starts -->
<div>
  <ul class="breadcrumb">
    <li> <a href="<?php echo ADMIN_ROOT_URL?>">Home</a> </li>
    <li> <a href="#"><?php echo $action;?> <?php echo (!empty($parentId)) ? 'Sub ' : '';?>Category</a> </li>
  </ul>
</div>
<div class="row">
  <div class="box-content">
    <div class="box col-md-12">
      <div class="box-inner">
        <div class="box-header well" data-original-title="">
          <h2><i class="glyphicon glyphicon-list-alt"></i> <?php echo $action;?> <?php echo (!empty($parentId)) ? 'Sub ' : '';?>Category</h2>
          <div class="box-icon"> <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a> <a href="#" class="btn btn-minimize btn-round btn-default"><i
                    class="glyphicon glyphicon-chevron-up"></i></a> <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a> </div>
        </div>
        <div class="box-content"> <?php echo validation_errors(); ?>
          <?php 
	$editUrl = '';
	if($action == 'Edit'){
		$editUrl = '/'.$categoryDetails->id;
	}
	
	$attributes = array('name' => 'categoryForm', 'id' => 'categoryForm', 'enctype' => 'multipart/form-data', 'role'=>'form', 'onsubmit'=>'return validate_category();');
				echo form_open(ADMIN_ROOT_URL.'category/add'.$editUrl,$attributes); ?>
          <input type="hidden" name="id" id="id" value="<?php echo (isset($categoryDetails->id)) ? $categoryDetails->id : 0;?>" />
          <input type="hidden" name="current_parent_id" id="current_parent_id" value="<?php if(isset($_SESSION['parent_id'])) { echo $_SESSION['parent_id']; unset($_SESSION['parent_id']); }else { echo (isset($categoryDetails->parent_id)) ? $categoryDetails->parent_id : 0; }?>" />
          <input type="hidden" name="action" id="action" value="<?php echo $action?>" />
          
          <div class="control-group">
            <label class="control-label" for="selectError">Parent Category</label>
            <div class="controls"> 
              <select id="parent_id" name="parent_id" data-rel="chosen">
              <?php echo $contentSelectData;?>
               </select>
            </div>
          </div>
          <div class="form-group input-group col-md-4" id="title_msg_error">
            <label class="control-label" for="title">Title<span class="required">*</span></label>
            <input type="text" class="form-control" maxlength="255" name="title" value="<?php if(isset($_SESSION['title']) && $_SESSION['title'] != '') { echo $_SESSION['title']; unset($_SESSION['title']);}else { echo (isset($categoryDetails->title)) ? $categoryDetails->title : ''; }?>" id="title" placeholder="Enter Title">
            <br />
            <label class="control-label" id="title_msg"></label>
          </div>
          <div class="form-group input-group col-md-4" id="sub_title_msg_error">
            <label class="control-label" for="title">Sub Title</label>
            <input type="text" class="form-control" maxlength="255" name="sub_title" value="<?php if(isset($_SESSION['sub_title']) && $_SESSION['sub_title'] != '') { echo $_SESSION['sub_title']; unset($_SESSION['sub_title']);}else { echo (isset($categoryDetails->sub_title)) ? $categoryDetails->sub_title : ''; }?>" id="sub_title" placeholder="Enter Sub Title">
            <br />
            <label class="control-label" id="title_msg"></label>
          </div>
          <div class="form-group input-group col-md-4" id="description_msg_error">
            <label for="description">Description</label><br />
            <?php if(isset($_SESSION['description']) && $_SESSION['description'] != '') { $description =  $_SESSION['description']; unset($_SESSION['description']);}else { $description =  (isset($categoryDetails->description)) ? $categoryDetails->description : ''; }?>
            <?php echo $this->ckeditor->editor("description",$description);?>
            
           
          </div>

        <div class="form-group input-group col-md-4" id="category_image_msg_error">
            <label for="category_image">Small Icon</label><br />
            <input type="file" name="category_image" id="category_image" class="input-text-02"   />
            <?php if(isset($categoryDetails->category_image) && $categoryDetails->category_image!='' && file_exists(DIR_UPLOAD_BANNER.$categoryDetails->category_image)) {?>
              <img src="<?php echo ROOT_URL_BASE?>assets/timthumb.php?src=<?php echo DIR_UPLOAD_BANNER_SHOW.$categoryDetails->category_image ?>&q=100&w=30"/>
              <input type="hidden" id="uploaded_file" name="uploaded_file" value="<?php echo $categoryDetails->category_image;  ?>" />
            <?php } ?>
        </div>

        <div class="form-group input-group col-md-4" id="category_icon_large_msg_error">
            <label for="category_icon_large">Large Icon</label><br />
            <input type="file" name="category_icon_large" id="category_icon_large" class="input-text-02"   />
            <?php if(isset($categoryDetails->category_icon_large) && $categoryDetails->category_icon_large!='' && file_exists(DIR_UPLOAD_BANNER.$categoryDetails->category_icon_large)) {?>
              <img src="<?php echo ROOT_URL_BASE?>assets/timthumb.php?src=<?php echo DIR_UPLOAD_BANNER_SHOW.$categoryDetails->category_icon_large ?>&q=100&w=100"/>
              <input type="hidden" id="uploaded_file" name="uploaded_file_large" value="<?php echo $categoryDetails->category_icon_large;  ?>" />
            <?php } ?>
        </div>


          <div class="control-group">
            <label class="control-label" for="selectError">On Header</label>
            <div class="controls">
              <select id="on_header" name="on_header" data-rel="chosen">
                <option value="1" selected="selected">Active</option>
                <option value="0" <?php if(isset($_SESSION['on_header']) && $_SESSION['on_header'] == 0) { echo 'selected="selected"'; unset($_SESSION['on_header']); }else { echo (isset($categoryDetails->on_header) && $categoryDetails->on_header == 0) ? 'selected="selected"' : ''; }?>  >In Active</option>
              </select>
             
            </div>
          </div>
          
          
          <div class="form-group input-group col-md-4" id="meta_title_msg_error">
            <label for="meta_title">Meta Title</label>
            <textarea class="form-control"  maxlength="255" name="meta_title"  id="meta_title" placeholder="Meta Title"><?php if(isset($_SESSION['meta_title']) && $_SESSION['meta_title'] != '') { echo $_SESSION['meta_title']; unset($_SESSION['meta_title']);}else { echo (isset($categoryDetails->small_description)) ? $categoryDetails->small_description : ''; }?></textarea>
            
          </div>
          
          <div class="form-group input-group col-md-4" id="meta_keywords_msg_error">
            <label for="meta_keywords">Meta Keywords</label>
            <textarea class="form-control"  maxlength="255" name="meta_keywords"  id="meta_keywords" placeholder="Meta Keywords"><?php if(isset($_SESSION['meta_keywords']) && $_SESSION['meta_keywords'] != '') { echo $_SESSION['meta_keywords']; unset($_SESSION['meta_keywords']);}else { echo (isset($categoryDetails->meta_keywords)) ? $categoryDetails->meta_keywords : ''; }?></textarea>
            
          </div>
          
          <div class="form-group input-group col-md-4" id="meta_desc_msg_error">
            <label for="meta_desc">Meta Description</label>
            <textarea class="form-control"  maxlength="255" name="meta_desc"  id="meta_desc" placeholder="Meta Description"><?php if(isset($_SESSION['meta_desc']) && $_SESSION['meta_desc'] != '') { echo $_SESSION['meta_desc']; unset($_SESSION['meta_desc']);}else { echo (isset($categoryDetails->meta_desc)) ? $categoryDetails->meta_desc : ''; }?></textarea>
            
          </div>
          <div class="control-group">
            <label class="control-label" for="selectError">Is Active</label>
            <div class="controls">
              <select id="is_active" name="is_active" data-rel="chosen">
                <option value="0" selected="selected">In Active</option>
                <option value="1" <?php if(isset($_SESSION['is_active']) && $_SESSION['is_active'] == 1) { echo 'selected="selected"'; unset($_SESSION['is_active']); }else { echo (isset($categoryDetails->is_active) && $categoryDetails->is_active == 1) ? 'selected="selected"' : ''; }?> >Active</option>
              </select>
            </div>
          </div>
         
          
          <br />
          <button type="submit" class="btn btn-success btn-sm">Submit</button>
          <?php echo form_close(); ?> </div>
      </div>
    </div>
  </div>
</div>
<script language="javascript" type="text/javascript">
$(document).ready(function() {
	/*$.ajax({
	   type: "POST",
	   url: "<?php echo ADMIN_ROOT_URL?>category/get_parent?id="+$("#id").val()+"&current_parent_id="+$("#current_parent_id").val(),
	   success: function(result){
		   $('#parent_id').val(result);
	   },
	   complete: function(){
	
	   },
	   error: function(){
		$('#parent_id').val('Error occured. Please try later');
	   }
	
	 });*/
});
function validate_category(){	
	if($("#title").val()==''){
		$("#title_msg").html('Please enter Category title');
		$("#title_msg_error").addClass('has-error');
		$("#title").focus();
		return false;
	}else{
		$("#title_msg").html('');
		$("#title_msg_error").removeClass('has-error');
	}
}
</script> 

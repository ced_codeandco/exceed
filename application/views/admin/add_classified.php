<script type="text/javascript" src="<?php echo ROOT_URL_BASE?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo ROOT_URL_BASE?>assets/ckfinder/ckfinder.js"></script>
<div id="content" class="col-lg-10 col-sm-10">
<!-- content starts -->
<div>
  <ul class="breadcrumb">
    <li> <a href="<?php echo ADMIN_ROOT_URL?>">Home</a> </li>
    <li> <a href="#"><?php echo $action;?> Classified</a> </li>
  </ul>
</div>
<div class="row">
  <div class="box-content">
    <div class="box col-md-12">
      <div class="box-inner">
        <div class="box-header well" data-original-title="">
          <h2><i class="glyphicon glyphicon-list-alt"></i> <?php echo $action;?> Classified</h2>
          <div class="box-icon"> <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a> <a href="#" class="btn btn-minimize btn-round btn-default"><i
                    class="glyphicon glyphicon-chevron-up"></i></a> <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a> </div>
        </div>
        <div class="box-content"> <?php echo validation_errors(); ?>
          <?php
          //print_r($classifiedDetails);
	$editUrl = '';
	if($action == 'Edit'){
		$editUrl = '/'.$classifiedDetails->id;
	}
	
	$attributes = array('name' => 'classifiedForm', 'id' => 'classifiedForm', 'enctype' => 'multipart/form-data', 'role'=>'form', 'onsubmit'=>'return validate_classified();');
				echo form_open(ADMIN_ROOT_URL.'classified/add'.$editUrl,$attributes); ?>
          <input type="hidden" name="id" id="id" value="<?php echo (isset($classifiedDetails->id)) ? $classifiedDetails->id : 0;?>" />
          <input type="hidden" name="action" id="action" value="<?php echo $action?>" />
          <div class="column_layout">
          <div class="control-group">
            <label class="control-label" for="selectError">Category<span class="required">*</span></label>
            <div class="controls">
                <select name="category_id" id="category_id" class="lookUpPreloader" onchange="create_loadCriteriaLookup(this, this.value, $('#sub_category_id'), 'subCategory', 'loadTertiaryOnCreate', $('#sub_category_wrap'));" data-rel="chosen">
                    <option value="">Select</option>
                    <?php if (!empty($parentCategoriesList) && is_array($parentCategoriesList)){
                        foreach ($parentCategoriesList as $item) {
                            $selected = '';
                            if (!empty($_REQUEST['category_id']) && $item->id == $_REQUEST['category_id']) {
                                $selected = 'selected="selected"';
                            } else if (!empty($classifiedDetails->category_id) && $item->id == $classifiedDetails->category_id) {
                                $selected = 'selected="selected"';
                            }
                            echo '<option '.$selected.' value="'.$item->id.'">'.$item->title.'</option>';
                        }
                    }?>
                </select>
            </div>
          </div>

            <div class="control-group">
                <label class="control-label" for="selectError">Subcategory<span class="required">*</span></label>
                <div class="controls" id="sub_category_wrap">
                    <input type="hidden" id="sub_category_id_preload" value="<?php if(isset($_SESSION['sub_category_id']) && $_SESSION['sub_category_id'] != '') { echo $_SESSION['sub_category_id']; unset($_SESSION['sub_category_id']);}else { echo (isset($classifiedDetails->sub_category_id)) ? $classifiedDetails->sub_category_id : ''; }?>">
                    <select name="sub_category_id" id="sub_category_id" onchange="loadLookup(this, this.value, $('#tertiary_category_id'), 'subCategory');" data-rel="chosen">
                        <option value="">Select</option>
                        <?php if (!empty($subCategory) && is_array($subCategory)){
                            foreach ($subCategory as $key => $val) {
                                $selected = (!empty($_REQUEST['sub_category_id']) && $key == $_REQUEST['sub_category_id']) ? 'selected="selected"' : '';
                                echo '<option '.$selected.' value="'.$key.'">'.$val.'</option>';
                            }
                        }?>
                    </select>
                </div>
            </div>
          </div>
            <div class="clearfix"></div>
          <div class="form-group input-group col-md-4" id="title_msg_error">
            <label class="control-label" for="title">Title<span class="required">*</span></label>
            <input type="text" class="form-control" maxlength="255" name="title" value="<?php if(isset($_SESSION['title']) && $_SESSION['title'] != '') { echo $_SESSION['title']; unset($_SESSION['title']);}else { echo (isset($classifiedDetails->title)) ? $classifiedDetails->title : ''; }?>" id="title" placeholder="Enter Title">
            <br />
            <label class="control-label" id="title_msg"></label>
          </div>

            <?php /*
          <div class="form-group input-group col-md-4" id="small_description_msg_error">
            <label for="small_description">Short Description<span class="required">*</span></label>
            <textarea class="form-control"  maxlength="255" name="small_description"  id="small_description" placeholder="Short Description"><?php if(isset($_SESSION['small_description']) && $_SESSION['small_description'] != '') { echo $_SESSION['small_description']; unset($_SESSION['small_description']);}else { echo (isset($classifiedDetails->small_description)) ? $classifiedDetails->small_description : ''; }?></textarea>

          </div>
          <div class="form-group input-group col-md-4" id="classified_tags_msg_error">
            <label for="classified_tags">Tags</label>
            <textarea class="form-control"  maxlength="255" name="classified_tags"  id="classified_tags" placeholder="Short Description"><?php if(isset($_SESSION['classified_tags']) && $_SESSION['classified_tags'] != '') { echo $_SESSION['classified_tags']; unset($_SESSION['classified_tags']);}else { echo (isset($classifiedDetails->classified_tags)) ? $classifiedDetails->classified_tags : ''; }?></textarea>
            
          </div>*/?>
          
          <div class="form-group input-group col-md-4" id="description_msg_error">
            <label for="description">Description</label><br />
            <?php if(isset($_SESSION['description']) && $_SESSION['description'] != '') { $description =  $_SESSION['description']; unset($_SESSION['description']);}else { $description =  (isset($classifiedDetails->description)) ? $classifiedDetails->description : ''; }?>
            <?php echo $this->ckeditor->editor("description",$description);?>
           </div>
          <div class="column_layout">
          <div class="control-group">
            <label class="control-label" for="selectError">Futured</label>
            <div class="controls">
              <select id="is_futured" name="is_futured" data-rel="chosen">
                <option value="0" selected="selected">In Active</option>
                <option value="1" <?php if(isset($_SESSION['is_futured']) && $_SESSION['is_futured'] == 1) { echo 'selected="selected"'; unset($_SESSION['is_futured']); }else { echo (isset($classifiedDetails->is_futured) && $classifiedDetails->is_futured == 1) ? 'selected="selected"' : ''; }?> >Active</option>
              </select>
            </div>
          </div>
          </div>
            <div class="clearfix"></div>
            <div class="column_layout" id="category_based-filter-wrap_common">
                <div class="form-group input-group col-md-4" id="amount_msg_error">
                    <label class="control-label" for="amount">Amount</label>
                    <input type="number" class="form-control" maxlength="255"  name="amount" value="<?php if(isset($_SESSION['amount']) && $_SESSION['amount'] != '') { echo $_SESSION['amount']; unset($_SESSION['amount']);}else { echo (isset($classifiedDetails->amount)) ? $classifiedDetails->amount : ''; }?>" id="classified_contact_no" placeholder="Enter Amount">
                    <br />
                    <label class="control-label" id="amount_msg"></label>
                </div>
            </div>

            <div class="column_layout" id="category_based-filter-wrap_<?php echo CATEG_JOB_ID;?>">
                <div class="form-group input-group col-md-4" id="amount_msg_error">
                    <label class="control-label" for="amount">Salary</label>
                    <input type="number" class="form-control" maxlength="255"  name="amount" value="<?php if(isset($_SESSION['amount']) && $_SESSION['amount'] != '') { echo $_SESSION['amount']; unset($_SESSION['amount']);}else { echo (isset($classifiedDetails->amount)) ? $classifiedDetails->amount : ''; }?>"  placeholder="Enter Amount">
                    <br />
                    <label class="control-label" id="amount_msg"></label>
                </div>
                <div class="form-group input-group col-md-4" id="experience_years_msg_error">
                    <label class="control-label" for="experience_years">Experience</label>
                    <input type="number" class="form-control" maxlength="255"  name="experience_years" value="<?php if(isset($_SESSION['experience_years']) && $_SESSION['experience_years'] != '') { echo $_SESSION['experience_years']; unset($_SESSION['experience_years']);}else { echo (isset($classifiedDetails->experience_years)) ? $classifiedDetails->experience_years : ''; }?>"  placeholder="Experience in years">
                    <br />
                    <label class="control-label" id="experience_years_msg"></label>
                </div>
                <div class="control-group">
                    <label class="control-label" for="selectError">Career Level</label>
                    <div class="controls">
                        <select name="career_level" data-rel="chosen">
                            <option value="">Select</option>
                            <?php if (!empty($career_levelLookUp) && is_array($career_levelLookUp)) {
                                foreach ($career_levelLookUp as $item) {
                                    $selected = '';
                                    if (!empty($_POST['career_level']) && $item->id == $_POST['career_level']) {
                                        $selected = 'selected="selected"';
                                    } else if (!empty($classifiedDetails->career_level) && $item->id == $classifiedDetails->career_level) {
                                        $selected = 'selected="selected"';
                                    }?>
                                    <option <?php echo $selected;?> value="<?php echo $item->id;?>"><?php echo $item->title;?></option>
                                <?php }
                            } ?>
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="selectError">Education</label>
                    <div class="controls">
                        <select name="education" data-rel="chosen">
                            <option value="">Select</option>
                            <?php if (!empty($educationLookUp) && is_array($educationLookUp)) {
                                foreach ($educationLookUp as $item) {
                                    $selected = '';
                                    if (!empty($_POST['education']) && $item->id == $_POST['education']) {
                                        $selected = 'selected="selected"';
                                    } else if (!empty($classifiedDetails->education) && $item->id == $classifiedDetails->education) {
                                        $selected = 'selected="selected"';
                                    }?>
                                    <option <?php echo $selected;?> value="<?php echo $item->id;?>"><?php echo $item->title;?></option>
                                <?php }
                            } ?>
                        </select>
                    </div>
                </div>
                <div class="control-group">
                    <label class="control-label" for="selectError">Employment Type</label>
                    <div class="controls">
                        <select name="employment_type" data-rel="chosen">
                            <option>Select</option>
                            <?php if (!empty($employment_typeLookUp) && is_array($employment_typeLookUp)) {
                                foreach ($employment_typeLookUp as $item) {
                                    $selected = '';
                                    if (!empty($_POST['employment_type']) && $item->id == $_POST['employment_type']) {
                                        $selected = 'selected="selected"';
                                    } else if (!empty($classifiedDetails->employment_type) && $item->id == $classifiedDetails->employment_type) {
                                        $selected = 'selected="selected"';
                                    }?>
                                    <option <?php echo $selected;?> value="<?php echo $item->id;?>"><?php echo $item->title;?></option>
                                <?php }
                            } ?>
                        </select>
                    </div>
                </div>

            </div>
            <div class="column_layout" id="category_based-filter-wrap_<?php echo CATEG_REAL_EST_ID;?>">
                <div class="form-group input-group col-md-4" id="amount_msg_error">
                    <label class="control-label" for="amount">Amount</label>
                    <input type="number" class="form-control" maxlength="255"  name="amount" value="<?php if(isset($_SESSION['amount']) && $_SESSION['amount'] != '') { echo $_SESSION['amount']; unset($_SESSION['amount']);}else { echo (isset($classifiedDetails->amount)) ? $classifiedDetails->amount : ''; }?>"  placeholder="Enter Amount">
                    <br />
                    <label class="control-label" id="amount_msg"></label>
                </div>

                <div class="form-group input-group col-md-4" id="bedrooms_msg_error">
                    <label class="control-label" for="bedrooms">Bed rooms</label>
                    <input type="number" class="form-control" min="1" maxlength="255"  name="bedrooms" value="<?php if(isset($_SESSION['bedrooms']) && $_SESSION['bedrooms'] != '') { echo $_SESSION['bedrooms']; unset($_SESSION['bedrooms']);}else { echo (isset($classifiedDetails->bedrooms)) ? $classifiedDetails->bedrooms : ''; }?>"  placeholder="Number of Bed rooms">
                    <br />
                    <label class="control-label" id="bedrooms_msg"></label>
                </div>

                <div class="form-group input-group col-md-4" id="bathrooms_msg_error">
                    <label class="control-label" for="bathrooms">Bath rooms</label>
                    <input type="number" class="form-control" min="1" maxlength="255"  name="bathrooms" value="<?php if(isset($_SESSION['bathrooms']) && $_SESSION['bathrooms'] != '') { echo $_SESSION['bathrooms']; unset($_SESSION['bathrooms']);}else { echo (isset($classifiedDetails->bathrooms)) ? $classifiedDetails->bathrooms : ''; }?>"  placeholder="Number of Bath rooms">
                    <br />
                    <label class="control-label" id="bathrooms_msg"></label>
                </div>


                <div class="form-group input-group col-md-4" id="area_sqft_msg_error">
                    <label class="control-label" for="area_sqft">Area</label>
                    <input type="number" class="form-control" min="1" maxlength="255"  name="area_sqft" value="<?php if(isset($_SESSION['area_sqft']) && $_SESSION['area_sqft'] != '') { echo $_SESSION['area_sqft']; unset($_SESSION['area_sqft']);}else { echo (isset($classifiedDetails->area_sqft)) ? $classifiedDetails->area_sqft : ''; }?>"  placeholder="Area in Square feet">
                    <br />
                    <label class="control-label" id="area_sqft_msg"></label>
                </div>
                <div class="clerafix"></div>
            </div>
            <div class="column_layout" id="category_based-filter-wrap_<?php echo CATEG_VEHICLES_ID;?>">
                <div class="form-group input-group col-md-4" id="amount_msg_error">
                    <label class="control-label" for="amount">Amount</label>
                    <input type="number" class="form-control" maxlength="255"  name="amount" value="<?php if(isset($_SESSION['amount']) && $_SESSION['amount'] != '') { echo $_SESSION['amount']; unset($_SESSION['amount']);}else { echo (isset($classifiedDetails->amount)) ? $classifiedDetails->amount : ''; }?>"  placeholder="Enter Amount">
                    <br />
                    <label class="control-label" id="amount_msg"></label>
                </div>

                <div class="control-group">
                    <label class="control-label" for="selectError">Brand</label>
                    <div class="controls">
                        <select name="brand_id" id="brand_id" class="lookUpPreloader" onchange="loadLookup(this, this.value, $('#model_id'), 'model_id');" data-rel="chosen">
                            <option value="">Select</option>
                            <?php if (!empty($brandList) && is_array($brandList)) {
                                foreach ($brandList as $brand) {
                                    $selected = '';
                                    if (!empty($_POST['brand_id']) && $brand->id == $_POST['brand_id']) {
                                        $selected = 'selected="selected"';
                                    } else if (!empty($classifiedDetails->brand_id) && $brand->id == $classifiedDetails->brand_id) {
                                        $selected = 'selected="selected"';
                                    }?>
                                    <option <?php echo $selected;?> value="<?php echo $brand->id;?>"><?php echo $brand->title;?></option>
                                <?php }
                            } ?>
                        </select>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="selectError">Model</label>
                    <div class="controls">
                        <input type="hidden" id="model_id_prelod" value="<?php if(isset($_SESSION['model_id']) && $_SESSION['model_id'] != '') { echo $_SESSION['model_id']; unset($_SESSION['model_id']);}else { echo (isset($classifiedDetails->model_id)) ? $classifiedDetails->model_id : ''; }?>" >
                        <select name="model_id" id="model_id" data-rel="chosen">
                            <option value="">Select</option>
                            <?php if (!empty($model) && is_array($model)){
                                foreach ($model as $key => $val) {
                                    $selected = (!empty($_POST['model_id']) && $key == $_POST['model_id']) ? 'selected="selected"' : '';
                                    echo '<option '.$selected.' value="'.$key.'">'.$val.'</option>';
                                }
                            }?>
                        </select>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="selectError">Year</label>
                    <div class="controls">
                        <select name="manufacture_year" data-rel="chosen">
                            <option value="">Select</option>
                            <?php for ($i = date('Y'); $i >= MANUFACTURE_YEAR_START; $i --){
                                $selected = '';
                                if (!empty($_POST['manufacture_year']) && $i == $_POST['manufacture_year']) {
                                    $selected = 'selected="selected"';
                                } else if (!empty($classifiedDetails->manufacture_year) && $i == $classifiedDetails->manufacture_year) {
                                    $selected = 'selected="selected"';
                                }
                                echo '<option '.$selected.'>'.$i.'</option>';
                            } ?>
                        </select>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="selectError">Fuel Type</label>
                    <div class="controls">
                        <select name="fuel_type" data-rel="chosen">
                            <option>Select</option>
                            <?php if (!empty($fuelTypeList) && is_array($fuelTypeList)) {
                                foreach ($fuelTypeList as $item) {?>
                                    <option value="<?php echo $item->id;?>"><?php echo $item->title;?></option>
                                <?php }
                            } ?>
                        </select>
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="selectError">Transmission</label>
                    <div class="controls">
                        <select name="transmission" data-rel="chosen">
                            <option>Select</option>
                            <?php if (!empty($transmissionLookUp) && is_array($transmissionLookUp)) {
                                foreach ($transmissionLookUp as $item) {?>
                                    <option value="<?php echo $item->id;?>"><?php echo $item->title;?></option>
                                <?php }
                            } ?>
                        </select>
                    </div>
                </div>

                <div class="form-group input-group col-md-4" id="running_km_msg_error">
                    <label class="control-label" for="running_km">KM's driven</label>
                    <input type="number" class="form-control" maxlength="255"  name="running_km" value="<?php if(isset($_SESSION['running_km']) && $_SESSION['running_km'] != '') { echo $_SESSION['running_km']; unset($_SESSION['running_km']);}else { echo (isset($classifiedDetails->running_km)) ? $classifiedDetails->running_km : ''; }?>"  placeholder="Enter Amount">
                    <br />
                    <label class="control-label" id="running_km_msg"></label>
                </div>
                <div class="clerafix"></div>
            </div>


















          <?php /*?>
          <div class="control-group" id="brand_msg_error">
            <label class="control-label" for="selectError">Brand</label>
            <div class="controls">
              <select id="brand" name="brand" data-rel="chosen">
                <option value="" selected="selected">Select Brand</option>
                <?php foreach($brandList as $brand){ ?>
				 <option value="<?php echo $brand->title?>" <?php if(isset($_SESSION['brand']) && $_SESSION['brand'] == $brand->id) { echo 'selected="selected"'; unset($_SESSION['brand']); }else { echo (isset($classifiedDetails->brand) && $classifiedDetails->brand == $brand->title) ? 'selected="selected"' : ''; }?> ><?php echo $brand->title; ?></option>
				<?php }?>
              </select>
            </div>
            <label class="control-label" id="brand_msg"></label>
          </div>
          <div class="form-group input-group col-md-4" id="model_msg_error">
            <label class="control-label" for="model">Model<span class="required">*</span></label>
            <input type="text" class="form-control" maxlength="255"  name="model" value="<?php if(isset($_SESSION['model']) && $_SESSION['model'] != '') { echo $_SESSION['model']; unset($_SESSION['model']);}else { echo (isset($classifiedDetails->model)) ? $classifiedDetails->model : ''; }?>" id="model" placeholder="Enter Model">
            <br />
            <label class="control-label" id="model_msg"></label>
          </div>
          <div class="form-group input-group col-md-4" id="running_km_msg_error">
            <label class="control-label" for="running_km">Total Kilometers</label>
            <input type="text" class="form-control" maxlength="255"  name="running_km" value="<?php if(isset($_SESSION['running_km']) && $_SESSION['running_km'] != '') { echo $_SESSION['running_km']; unset($_SESSION['running_km']);}else { echo (isset($classifiedDetails->running_km)) ? $classifiedDetails->running_km : ''; }?>" id="running_km" placeholder="Enter Kilometers">
            <br />
            <label class="control-label" id="model_msg"></label>
          </div>
          <div class="control-group">
            <label class="control-label" for="selectError">Manufacture Year</label>
            <div class="controls">
            <?php $maxYear = date('Y'); $minYear = $maxYear - 50; ?>
              <select id="manufacture_year" name="manufacture_year" data-rel="chosen"> 
			  	 <?php for($i = $maxYear; $i >= $minYear; $i--){ ?>
				 <option value="<?php echo $i?>" <?php if(isset($_SESSION['manufacture_year']) && $_SESSION['manufacture_year'] == $i) { echo 'selected="selected"'; unset($_SESSION['manufacture_year']); }else { echo (isset($classifiedDetails->manufacture_year) && $classifiedDetails->manufacture_year == $i) ? 'selected="selected"' : ''; }?> ><?php echo $i;?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
				<?php }?>                 
              </select>
            </div>
          </div>
          <div class="form-group input-group col-md-4" id="milage_msg_error">
            <label class="control-label" for="milage">Milage<span class="required">*</span></label>
            <input type="text" class="form-control" maxlength="255" name="milage" value="<?php if(isset($_SESSION['milage']) && $_SESSION['milage'] != '') { echo $_SESSION['milage']; unset($_SESSION['milage']);}else { echo (isset($classifiedDetails->milage)) ? $classifiedDetails->milage : ''; }?>" id="milage" placeholder="Milage">
            <br />
            <label class="control-label" id="milage_msg"></label>
          </div>

          <div class="control-group">
            <label class="control-label" for="selectError">Body Type</label>
            <div class="controls">
           
              <select id="body_type" name="body_type" data-rel="chosen"> 
			  	 <option selected="selected" value="">Body condition</option>
                    <option value="Perfect inside and out"  <?php if(isset($_SESSION['body_type']) && $_SESSION['body_type'] == 'Perfect inside and out') { echo 'selected="selected"'; unset($_SESSION['body_type']); }else { echo (isset($classifiedDetails->body_type) && $classifiedDetails->body_type == 'Perfect inside and out') ? 'selected="selected"' : ''; }?>>Perfect inside and out</option>
                    <option value="No accidents, very few faults"  <?php if(isset($_SESSION['body_type']) && $_SESSION['body_type'] == 'No accidents, very few faults') { echo 'selected="selected"'; unset($_SESSION['body_type']); }else { echo (isset($classifiedDetails->body_type) && $classifiedDetails->body_type == 'No accidents, very few faults') ? 'selected="selected"' : ''; }?>>No accidents, very few faults</option>
                    <option value="A bit of wear & tear, all repaired"  <?php if(isset($_SESSION['body_type']) && $_SESSION['body_type'] == 'A bit of wear & tear, all repaired') { echo 'selected="selected"'; unset($_SESSION['body_type']); }else { echo (isset($classifiedDetails->body_type) && $classifiedDetails->body_type == 'A bit of wear & tear, all repaired') ? 'selected="selected"' : ''; }?>>A bit of wear & tear, all repaired</option>
                    <option value="Normal wear & tear, a few issues"  <?php if(isset($_SESSION['body_type']) && $_SESSION['body_type'] == 'Normal wear & tear, a few issues') { echo 'selected="selected"'; unset($_SESSION['body_type']); }else { echo (isset($classifiedDetails->body_type) && $classifiedDetails->body_type == 'Normal wear & tear, a few issues') ? 'selected="selected"' : ''; }?>>Normal wear & tear, a few issues</option>
                    <option value="Lots of wear & tear to the body"  <?php if(isset($_SESSION['body_type']) && $_SESSION['body_type'] == 'Lots of wear & tear to the body') { echo 'selected="selected"'; unset($_SESSION['body_type']); }else { echo (isset($classifiedDetails->body_type) && $classifiedDetails->body_type == 'Lots of wear & tear to the body') ? 'selected="selected"' : ''; }?>>Lots of wear & tear to the body</option>               
              </select>
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" for="selectError">Doors</label>
            <div class="controls">
          
              <select id="no_of_doors" name="no_of_doors" data-rel="chosen"> 
			  	 <option selected="selected" value="">Select Doors</option>
                    <option value="2 door"  <?php if(isset($_SESSION['no_of_doors']) && $_SESSION['no_of_doors'] == '2 door') { echo 'selected="selected"'; unset($_SESSION['no_of_doors']); }else { echo (isset($classifiedDetails->no_of_doors) && $classifiedDetails->no_of_doors == '2 door') ? 'selected="selected"' : ''; }?>>2 door</option>
                    <option value="3 door"  <?php if(isset($_SESSION['no_of_doors']) && $_SESSION['no_of_doors'] == '3 door') { echo 'selected="selected"'; unset($_SESSION['no_of_doors']); }else { echo (isset($classifiedDetails->no_of_doors) && $classifiedDetails->no_of_doors == '3 door') ? 'selected="selected"' : ''; }?>>3 door</option>
                    <option value="4 door"  <?php if(isset($_SESSION['no_of_doors']) && $_SESSION['no_of_doors'] == '4 door') { echo 'selected="selected"'; unset($_SESSION['no_of_doors']); }else { echo (isset($classifiedDetails->no_of_doors) && $classifiedDetails->no_of_doors == '4 door') ? 'selected="selected"' : ''; }?>>4 door</option>
                    <option value="5+ doors"  <?php if(isset($_SESSION['no_of_doors']) && $_SESSION['no_of_doors'] == '5+ doors') { echo 'selected="selected"'; unset($_SESSION['no_of_doors']); }else { echo (isset($classifiedDetails->no_of_doors) && $classifiedDetails->no_of_doors == '5+ doors') ? 'selected="selected"' : ''; }?>>5+ doors</option>
                                   
              </select>
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" for="selectError">Transmission</label>
            <div class="controls">
            <select id="transmission" name="transmission" data-rel="chosen"> 
			  	 <option selected="selected" value="">Transmission</option>
                    <option value="Manual Transmission"  <?php if(isset($_SESSION['transmission']) && $_SESSION['transmission'] == 'Manual Transmission') { echo 'selected="selected"'; unset($_SESSION['transmission']); }else { echo (isset($classifiedDetails->transmission) && $classifiedDetails->transmission == 'Manual Transmission') ? 'selected="selected"' : ''; }?>>Manual Transmission</option>
                    <option value="Automatic Transmission"  <?php if(isset($_SESSION['transmission']) && $_SESSION['transmission'] == 'Automatic Transmission') { echo 'selected="selected"'; unset($_SESSION['transmission']); }else { echo (isset($classifiedDetails->transmission) && $classifiedDetails->transmission == 'Automatic Transmission') ? 'selected="selected"' : ''; }?>>Automatic Transmission</option>
                    
                                   
              </select>
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" for="selectError">Engine Type</label>
            <div class="controls">
            <select id="engine_size" name="engine_size" data-rel="chosen"> 
			  	 <option selected="selected" value="">Hores Power</option>
                 <option value="Less than 150 HP"  <?php if(isset($_SESSION['engine_size']) && $_SESSION['engine_size'] == 'Less than 150 HP') { echo 'selected="selected"'; unset($_SESSION['engine_size']); }else { echo (isset($classifiedDetails->engine_size) && $classifiedDetails->engine_size == 'Less than 150 HP') ? 'selected="selected"' : ''; }?>>Less than 150 HP</option>
                 
                    <option value="150 - 200 HP"  <?php if(isset($_SESSION['engine_size']) && $_SESSION['engine_size'] == '150 - 200 HP') { echo 'selected="selected"'; unset($_SESSION['engine_size']); }else { echo (isset($classifiedDetails->engine_size) && $classifiedDetails->engine_size == '150 - 200 HP') ? 'selected="selected"' : ''; }?>>150 - 200 HP</option>
                                      
                      <option value="200 - 300 HP"  <?php if(isset($_SESSION['engine_size']) && $_SESSION['engine_size'] == '200 - 300 HP') { echo 'selected="selected"'; unset($_SESSION['engine_size']); }else { echo (isset($classifiedDetails->engine_size) && $classifiedDetails->engine_size == '200 - 300 HP') ? 'selected="selected"' : ''; }?>>200 - 300 HP</option>     
                       <option value="300 - 400 HP"  <?php if(isset($_SESSION['engine_size']) && $_SESSION['engine_size'] == '300 - 400 HP') { echo 'selected="selected"'; unset($_SESSION['engine_size']); }else { echo (isset($classifiedDetails->engine_size) && $classifiedDetails->engine_size == '300 - 400 HP') ? 'selected="selected"' : ''; }?>>300 - 400 HP</option>
                       <option value="400 - 500 HP"  <?php if(isset($_SESSION['engine_size']) && $_SESSION['engine_size'] == '400 - 500 HP') { echo 'selected="selected"'; unset($_SESSION['engine_size']); }else { echo (isset($classifiedDetails->engine_size) && $classifiedDetails->engine_size == '400 - 500 HP') ? 'selected="selected"' : ''; }?>>400 - 500 HP</option>
                        <option value="500 - 600 HP"  <?php if(isset($_SESSION['engine_size']) && $_SESSION['engine_size'] == '500 - 600 HP') { echo 'selected="selected"'; unset($_SESSION['engine_size']); }else { echo (isset($classifiedDetails->engine_size) && $classifiedDetails->engine_size == '500 - 600 HP') ? 'selected="selected"' : ''; }?>>500 - 600 HP</option>
                         <option value="600-700 HP"  <?php if(isset($_SESSION['engine_size']) && $_SESSION['engine_size'] == '600-700 HP') { echo 'selected="selected"'; unset($_SESSION['engine_size']); }else { echo (isset($classifiedDetails->engine_size) && $classifiedDetails->engine_size == '600-700 HP') ? 'selected="selected"' : ''; }?>>600-700 HP</option> 
                         <option value="700-800 HP"  <?php if(isset($_SESSION['engine_size']) && $_SESSION['engine_size'] == '700-800 HP') { echo 'selected="selected"'; unset($_SESSION['engine_size']); }else { echo (isset($classifiedDetails->engine_size) && $classifiedDetails->engine_size == '700-800 HP') ? 'selected="selected"' : ''; }?>>700-800 HP</option>  
                         <option value="800 - 900 HP"  <?php if(isset($_SESSION['engine_size']) && $_SESSION['engine_size'] == '800 - 900 HP') { echo 'selected="selected"'; unset($_SESSION['engine_size']); }else { echo (isset($classifiedDetails->engine_size) && $classifiedDetails->engine_size == '800 - 900 HP') ? 'selected="selected"' : ''; }?>>800 - 900 HP</option>
                         <option value="900+ HP"  <?php if(isset($_SESSION['engine_size']) && $_SESSION['engine_size'] == '900+ HP') { echo 'selected="selected"'; unset($_SESSION['engine_size']); }else { echo (isset($classifiedDetails->engine_size) && $classifiedDetails->engine_size == '900+ HP') ? 'selected="selected"' : ''; }?>>900+ HP</option>
                         <option value="Unknown"  <?php if(isset($_SESSION['engine_size']) && $_SESSION['engine_size'] == 'Unknown') { echo 'selected="selected"'; unset($_SESSION['engine_size']); }else { echo (isset($classifiedDetails->engine_size) && $classifiedDetails->engine_size == 'Unknown') ? 'selected="selected"' : ''; }?>>Unknown</option>
              </select>
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" for="selectError">Are you ?</label>
            <div class="controls">
            <select id="entry_type" name="entry_type" data-rel="chosen"> 
			  	 
                 <option value="Individual"  <?php if(isset($_SESSION['entry_type']) && $_SESSION['entry_type'] == 'Individual') { echo 'selected="selected"'; unset($_SESSION['entry_type']); }else { echo (isset($classifiedDetails->entry_type) && $classifiedDetails->entry_type == 'Individual') ? 'selected="selected"' : ''; }?>>Individual</option>
                 
                    <option value="Dealer"  <?php if(isset($_SESSION['entry_type']) && $_SESSION['entry_type'] == 'Dealer') { echo 'selected="selected"'; unset($_SESSION['entry_type']); }else { echo (isset($classifiedDetails->entry_type) && $classifiedDetails->entry_type == 'Dealer') ? 'selected="selected"' : ''; }?>>Dealer</option>
                                      
                      
              </select>
            </div>
          </div><?php */?>


          <?php /*?>
          <div class="form-group input-group col-md-4" id="colour_msg_error">
            <label class="control-label" for="colour">Color<span class="required">*</span></label>
            <input type="text" class="form-control" maxlength="255" name="colour" value="<?php if(isset($_SESSION['colour']) && $_SESSION['colour'] != '') { echo $_SESSION['colour']; unset($_SESSION['colour']);}else { echo (isset($classifiedDetails->colour)) ? $classifiedDetails->colour : ''; }?>" id="colour" placeholder="Enter Color">
            <br />
            <label class="control-label" id="colour_msg"></label>
          </div>
          
          <div class="form-group input-group col-md-4" id="classified_contact_name_msg_error">
            <label class="control-label" for="classified_contact_name">Contact Name<span class="required">*</span></label>
            <input type="text" class="form-control" maxlength="255" name="classified_contact_name" value="<?php if(isset($_SESSION['classified_contact_name']) && $_SESSION['classified_contact_name'] != '') { echo $_SESSION['classified_contact_name']; unset($_SESSION['classified_contact_name']);}else { echo (isset($classifiedDetails->classified_contact_name)) ? $classifiedDetails->classified_contact_name : ''; }?>" id="classified_contact_name" placeholder="Enter Contact Name">
            <br />
            <label class="control-label" id="classified_contact_name_msg"></label>
          </div>
          <div class="form-group input-group col-md-4" id="classified_contact_email_msg_error">
            <label class="control-label" for="classified_contact_email">Contact Email<span class="required">*</span></label>
            <input type="email" class="form-control" maxlength="255" name="classified_contact_email" value="<?php if(isset($_SESSION['classified_contact_email']) && $_SESSION['classified_contact_email'] != '') { echo $_SESSION['classified_contact_email']; unset($_SESSION['classified_contact_email']);}else { echo (!empty($classifiedDetails->classified_contact_email)) ? $classifiedDetails->classified_contact_email : ''; }?>" id="classified_contact_email" placeholder="Enter Contact Email">
            <br />
            <label class="control-label" id="classified_contact_email_msg"></label>
          </div>
          
          <div class="form-group input-group col-md-4" id="classified_land_mark_msg_error">
            <label class="control-label" for="classified_land_mark">Landmark<span class="required">*</span></label>
            <input type="text" class="form-control" maxlength="255" name="classified_land_mark" value="<?php if(isset($_SESSION['classified_land_mark']) && $_SESSION['classified_land_mark'] != '') { echo $_SESSION['classified_land_mark']; unset($_SESSION['classified_land_mark']);}else { echo (isset($classifiedDetails->classified_land_mark)) ? $classifiedDetails->classified_land_mark : ''; }?>" id="classified_land_mark" placeholder="Enter Classified Landmark">
            <br />
            <label class="control-label" id="classified_land_mark_msg"></label>
          </div>
          <div class="form-group input-group col-md-4" id="classified_address_msg_error">
            <label for="classified_address">Address<span class="required">*</span></label>
            <textarea class="form-control"  maxlength="255" name="classified_address"  id="classified_address" placeholder="Short Description"><?php if(isset($_SESSION['classified_address']) && $_SESSION['classified_address'] != '') { echo $_SESSION['classified_address']; unset($_SESSION['classified_address']);}else { echo (isset($classifiedDetails->classified_address)) ? $classifiedDetails->classified_address : ''; }?></textarea>
            
          </div>
          <div class="control-group">
            <label class="control-label" for="selectError">Country</label>
            <div class="controls">
              <select id="classified_country" name="classified_country" data-rel="chosen">
                <option value="0" selected="selected">Select Country</option>
                <?php foreach($countryList as $country){ ?>
				 <option value="<?php echo $country->id?>" <?php if(isset($_SESSION['country']) && $_SESSION['country'] == $country->id) { echo 'selected="selected"'; unset($_SESSION['country']); }else { echo (isset($classifiedDetails->classified_country) && $classifiedDetails->classified_country == $country->id) ? 'selected="selected"' : ''; }?> ><?php echo $country->name; ?></option>
				<?php }?>
              </select>
            </div>
            <label class="control-label" id="country_msg"></label>
          </div>
                    
          
          <div class="form-group input-group col-md-4" id="state_msg_error">
            <label class="control-label" for="selectError">City</label>
              <div class="controls">
                  <select id="classified_city" name="classified_city" data-rel="chosen">
                      <option value="0" selected="selected">Select City</option>
                      <?php foreach($cityList as $country){ ?>
                          <option value="<?php echo $country->id?>" <?php if(isset($_POST['classified_city']) && $_SESSION['classified_city'] == $country->id) { echo 'selected="selected"'; unset($_SESSION['classified_city']); }else { echo (isset($classifiedDetails->classified_city) && $classifiedDetails->classified_city == $country->id) ? 'selected="selected"' : ''; }?> ><?php echo $country->combined; ?></option>
                      <?php }?>
                  </select>
              </div>
           <label class="control-label" id="city_msg"></label>
          </div>
            <div class="form-group input-group col-md-4" id="state_msg_error">
                <label class="control-label" for="selectError">Distance from City</label>
                <input type="number" class="form-control" maxlength="255" name="classified_city_distance" value="<?php if(isset($_SESSION['classified_city_distance']) && $_SESSION['classified_city_distance'] != '') { echo $_SESSION['classified_city_distance']; unset($_SESSION['classified_city_distance']);}else { echo (isset($classifiedDetails->classified_city_distance)) ? $classifiedDetails->classified_city_distance : ''; }?>" id="classified_city_distance" placeholder="Distance in miles">
                <br />
                <label class="control-label" id="city_msg"></label>
            </div>
            <?php */?>
            <div class="column_layout">
                <div class="control-group">
                    <label class="control-label" for="selectError">Classified By<span class="required">*</span></label>
                    <div class="controls">
                        <select id="created_by" name="created_by" data-rel="chosen">
                            <?php foreach($memberList as $member){ ?>
                                <option value="<?php echo $member->id?>" <?php echo (isset($classifiedDetails->created_by) && $classifiedDetails->created_by== $member->id) ? "selected='selected'" : "" ?> ><?php echo $member->first_name. ' ' .$member->last_name?></option>
                            <?php }?>
                        </select>
                    </div>
                </div>
                <div class="form-group input-group col-md-4" id="classified_contact_no_msg_error">
                    <label class="control-label" for="classified_contact_no">Contact No<span class="required">*</span></label>
                    <input type="text" class="form-control" maxlength="255" name="classified_contact_no" value="<?php if(isset($_SESSION['classified_contact_no']) && $_SESSION['classified_contact_no'] != '') { echo $_SESSION['classified_contact_no']; unset($_SESSION['classified_contact_no']);}else { echo (isset($classifiedDetails->classified_contact_no)) ? $classifiedDetails->classified_contact_no : ''; }?>" id="classified_contact_no" placeholder="Enter Contact Number">
                    <br />
                    <label class="control-label" id="classified_contact_no_msg"></label>
                </div>
				<div class="form-group input-group col-md-4" id="classified_contact_no_opt_msg_error">
                    <label class="control-label" for="classified_contact_no_opt">Contact No [2] (Optional)</label>
                    <input type="text" class="form-control" maxlength="255" name="classified_contact_no_opt" value="<?php if(isset($_SESSION['classified_contact_no_opt']) && $_SESSION['classified_contact_no_opt'] != '') { echo $_SESSION['classified_contact_no_opt']; unset($_SESSION['classified_contact_no_opt']);}else { echo (isset($classifiedDetails->classified_contact_no_opt)) ? $classifiedDetails->classified_contact_no_opt : ''; }?>" id="classified_contact_no_opt" placeholder="Enter Contact Number">
                    <br />
                    <label class="control-label" id="classified_contact_no_opt_msg"></label>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="column_layout">
          <div class="form-group input-group col-md-4" id="meta_title_msg_error">
            <label for="meta_title">Meta Title</label>
            <textarea class="form-control"  maxlength="255" name="meta_title"  id="meta_title" placeholder="Meta Title"><?php if(isset($_SESSION['meta_title']) && $_SESSION['meta_title'] != '') { echo $_SESSION['meta_title']; unset($_SESSION['meta_title']);}else { echo (!empty($classifiedDetails->meta_title)) ? $classifiedDetails->meta_title : ''; }?></textarea>
            
          </div>
          
          <div class="form-group input-group col-md-4" id="meta_keywords_msg_error">
            <label for="meta_keywords">Meta Keywords</label>
            <textarea class="form-control"  maxlength="255" name="meta_keywords"  id="meta_keywords" placeholder="Meta Keywords"><?php if(isset($_SESSION['meta_keywords']) && $_SESSION['meta_keywords'] != '') { echo $_SESSION['meta_keywords']; unset($_SESSION['meta_keywords']);}else { echo (!empty($classifiedDetails->meta_keywords)) ? $classifiedDetails->meta_keywords : ''; }?></textarea>
            
          </div>
          
          <div class="form-group input-group col-md-4" id="meta_desc_msg_error">
            <label for="meta_desc">Meta Description</label>
            <textarea class="form-control"  maxlength="255" name="meta_desc"  id="meta_desc" placeholder="Meta Description"><?php if(isset($_SESSION['meta_desc']) && $_SESSION['meta_desc'] != '') { echo $_SESSION['meta_desc']; unset($_SESSION['meta_desc']);}else { echo (!empty($classifiedDetails->meta_desc)) ? $classifiedDetails->meta_desc : ''; }?></textarea>
          </div>

          <div class="control-group">
            <label class="control-label" for="selectError">Is Active</label>
            <div class="controls">
              <select id="is_active" name="is_active" data-rel="chosen">
                <option value="0" selected="selected">In Active</option>
                <option value="1" <?php if(isset($_SESSION['is_active']) && $_SESSION['is_active'] == 1) { echo 'selected="selected"'; unset($_SESSION['is_active']); }else { echo (isset($classifiedDetails->is_active) && $classifiedDetails->is_active == 1) ? 'selected="selected"' : ''; }?> >Active</option>
              </select>
            </div>
          </div>
        </div>
            <div class="clearfix"></div>
          
          <br />
          <button type="submit" class="btn btn-success btn-sm">Submit</button>
          <?php echo form_close(); ?> </div>
      </div>
    </div>
  </div>
</div>
<script language="javascript" type="text/javascript">
$(document).ready(function(){
    var categ_id = $('#category_id').val();
    //create_manage_lookUpfields(categ_id);
    create_loadCriteriaLookup($('#category_id'), $('#category_id').val(), $('#sub_category_id'), 'subCategory', 'loadTertiaryOnCreate', $('#sub_category_wrap'), $('#sub_category_id_preload').val())
    var brand_id = $('#brand_id').val();
    loadLookup($('#brand_id'), $('#brand_id').val(), $('#model_id'), 'model_id', false, '', $('#model_id_prelod').val());
    //create_loadCriteriaLookup($('#brand_id'), $('#brand_id').val(), $('#model_id'), 'model_id');
})
function validate_classified(){	
	if($("#title").val()==''){
		$("#title_msg").html('Please enter Classified title');
		$("#title_msg_error").addClass('has-error');
		$("#title").focus();
		return false;
	}else{
		$("#title_msg").html('');
		$("#title_msg_error").removeClass('has-error');
	}
    /*if($("#small_description").val()==''){
		$("#small_description_msg").html('Please enter Classified Short Description');
		$("#small_description_msg_error").addClass('has-error');
		$("#small_description").focus();
		return false;
	}else{
		$("#small_description_msg").html('');
		$("#small_description_msg_error").removeClass('has-error');
	}
	if($("#brand").val()==''){
		$("#brand_msg").html('Please enter brand');
		$("#brand_msg_error").addClass('has-error');
		$("#brand").focus();
		return false;
	}else{
		$("#brand_msg").html('');
		$("#brand_msg_error").removeClass('has-error');
	}*/
	/*if($("#model").val()==''){
		$("#model_msg").html('Please enter model');
		$("#model_msg_error").addClass('has-error');
		$("#model").focus();
		return false;
	}else{
		$("#model_msg").html('');
		$("#model_msg_error").removeClass('has-error');
	}*/
	if($("#manufacture_year").val()==''){
		$("#manufacture_year_msg").html('Please enter Manufacture Year');
		$("#manufacture_year_msg_error").addClass('has-error');
		$("#manufacture_year").focus();
		return false;
	}else{
		$("#manufacture_year_msg").html('');
		$("#manufacture_year_msg_error").removeClass('has-error');
	}
	if($("#classified_contact_no:enabled").val()==''){
		$("#classified_contact_no_msg").html('Please enter Classified Contact Number');
		$("#classified_contact_no_msg_error").addClass('has-error');
		$("#classified_contact_no").focus();
		return false;
	}else{
		$("#classified_contact_no_msg").html('');
		$("#classified_contact_no_msg_error").removeClass('has-error');
	}
	/*if($("#classified_contact_name").val()==''){
		$("#classified_contact_name_msg").html('Please enter Classified Contact Name');
		$("#classified_contact_name_msg_error").addClass('has-error');
		$("#classified_contact_name").focus();
		return false;
	}else{
		$("#classified_contact_name_msg").html('');
		$("#classified_contact_name_msg_error").removeClass('has-error');
	}*/
	/*if($("#classified_contact_email").val()==''){
		*//*$("#classified_contact_email_msg").html('Please enter Classified Contact Email');
		$("#classified_contact_email_msg_error").addClass('has-error');
		$("#classified_contact_email").focus();
		return false*//*;
	}else{
		$("#classified_contact_email_msg").html('');
		$("#classified_contact_email_msg_error").removeClass('has-error');
	}*/
	/*if($("#classified_land_mark").val()==''){
		$("#classified_land_mark_msg").html('Please enter Classified Landmark');
		$("#classified_land_mark_msg_error").addClass('has-error');
		$("#classified_land_mark").focus();
		return false;
	}else{
		$("#classified_land_mark_msg").html('');
		$("#classified_land_mark_msg_error").removeClass('has-error');
	}
	if($("#classified_address").val()==''){
		$("#classified_address_msg").html('Please enter Classified Address');
		$("#classified_address_msg_error").addClass('has-error');
		$("#classified_address").focus();
		return false;
	}else{
		$("#classified_address_msg").html('');
		$("#classified_address_msg_error").removeClass('has-error');
	}*/
}
</script> 

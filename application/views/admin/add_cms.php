<script type="text/javascript" src="<?php echo ROOT_URL_BASE?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo ROOT_URL_BASE?>assets/ckfinder/ckfinder.js"></script>
<div id="content" class="col-lg-10 col-sm-10">
<!-- content starts -->
<div>
  <ul class="breadcrumb">
    <li> <a href="<?php echo ADMIN_ROOT_URL?>">Home</a> </li>
    <li> <a href="#"><?php echo $action;?> CMS</a> </li>
  </ul>
</div>
<div class="row">
  <div class="box-content">
    <div class="box col-md-12">
      <div class="box-inner">
        <div class="box-header well" data-original-title="">
          <h2><i class="glyphicon glyphicon-list-alt"></i> <?php echo $action;?> CMS</h2>
          <div class="box-icon"> <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a> <a href="#" class="btn btn-minimize btn-round btn-default"><i
                    class="glyphicon glyphicon-chevron-up"></i></a> <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a> </div>
        </div>
        <div class="box-content"> <?php echo validation_errors(); ?>
          <?php 
	$editUrl = '';
	if($action == 'Edit'){
		$editUrl = '/'.$cmsDetails->id;
	}
	
	$attributes = array('name' => 'cmsForm', 'id' => 'cmsForm', 'enctype' => 'multipart/form-data', 'role'=>'form', 'onsubmit'=>'return validate_cms();');
				echo form_open(ADMIN_ROOT_URL.'cms/add'.$editUrl,$attributes); ?>
          <input type="hidden" name="id" id="id" value="<?php echo (isset($cmsDetails->id)) ? $cmsDetails->id : 0;?>" />
          <input type="hidden" name="action" id="action" value="<?php echo $action?>" />
          <div class="control-group">
            <label class="control-label" for="selectError">Parent Page</label>
            <div class="controls">
              <select id="parent_id" name="parent_id" data-rel="chosen">
                <option value="0" selected="selected">Select Parent Page</option>
                <?php foreach($parentPageList as $parent){ ?>
				 <option value="<?php echo $parent->id?>" <?php echo (isset($cmsDetails->parent_id) && $cmsDetails->parent_id== $parent->id) ? "selected='selected'" : "" ?> <?php echo (isset($cmsDetails->id) && $cmsDetails->id == $parent->id) ? "disabled='disabled'" : "" ?> ><?php echo $parent->title?></option>
				<?php }?>
              </select>
            </div>
          </div>
          
          <div class="form-group input-group col-md-4" id="title_msg_error">
            <label class="control-label" for="title">Title<span class="required">*</span></label>
            <input type="text" class="form-control" maxlength="255" name="title" value="<?php if(isset($_SESSION['title']) && $_SESSION['title'] != '') { echo $_SESSION['title']; unset($_SESSION['title']);}else { echo (isset($cmsDetails->title)) ? $cmsDetails->title : ''; }?>" id="title" placeholder="Enter Title">
            <br />
            <label class="control-label" id="title_msg"></label>
          </div>
          <div class="form-group input-group col-md-4" id="small_description_msg_error">
            <label for="small_description">Short Description</label>
            <textarea class="form-control"  maxlength="255" name="small_description"  id="small_description" placeholder="Short Description"><?php if(isset($_SESSION['small_description']) && $_SESSION['small_description'] != '') { echo $_SESSION['small_description']; unset($_SESSION['small_description']);}else { echo (isset($cmsDetails->small_description)) ? $cmsDetails->small_description : ''; }?></textarea>
            
          </div>
          <div class="form-group input-group col-md-4" id="description_msg_error">
            <label for="description">Description</label><br />
            <?php if(isset($_SESSION['description']) && $_SESSION['description'] != '') { $description =  $_SESSION['description']; unset($_SESSION['description']);}else { $description =  (isset($cmsDetails->description)) ? $cmsDetails->description : ''; }?>
            <?php echo $this->ckeditor->editor("description", stripslashes($description));?>
            
           
          </div>
          <div class="form-group input-group col-md-4" id="cms_banner_image_msg_error">
            <label for="cms_banner_image">Banner Image</label><br />
                       
             <input type="file" name="cms_banner_image" id="cms_banner_image" class="input-text-02"   />
      <?php if(isset($cmsDetails->cms_banner_image) && $cmsDetails->cms_banner_image!='' && file_exists(DIR_UPLOAD_BANNER.$cmsDetails->cms_banner_image)) {?>
     
      <img src="<?php echo ROOT_URL_BASE?>assets/timthumb.php?src=<?php echo DIR_UPLOAD_BANNER_SHOW.$cmsDetails->cms_banner_image ?>&q=100&w=300"/>
      <input type="hidden" id="uploaded_file" name="uploaded_file" value="<?php echo $cmsDetails->cms_banner_image;  ?>" />
      <?php } ?>
        
          </div>
          <div class="control-group">
            <label class="control-label" for="selectError">On Header</label>
            <div class="controls">
              <select id="on_header" name="on_header" data-rel="chosen">
                <option value="1" selected="selected">Active</option>
                <option value="0" <?php if(isset($_SESSION['on_header']) && $_SESSION['on_header'] == 0) { echo 'selected="selected"'; unset($_SESSION['on_header']); }else { echo (isset($cmsDetails->on_header) && $cmsDetails->on_header == 0) ? 'selected="selected"' : ''; }?>  >In Active</option>
              </select>
             
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" for="selectError">On Footer</label>
            <div class="controls">
              <select id="on_footer" name="on_footer" data-rel="chosen">
                <option value="1" selected="selected">Active</option>
                <option value="0" <?php if(isset($_SESSION['on_footer']) && $_SESSION['on_footer'] == 0) { echo 'selected="selected"'; unset($_SESSION['on_footer']); }else { echo (isset($cmsDetails->on_footer) && $cmsDetails->on_footer == 0) ? 'selected="selected"' : ''; }?> >In Active</option>
              </select>
            </div>
          </div>
          
          <div class="form-group input-group col-md-4" id="meta_title_msg_error">
            <label for="meta_title">Meta Title</label>
            <textarea class="form-control"  maxlength="255" name="meta_title"  id="meta_title" placeholder="Meta Title"><?php if(isset($_SESSION['meta_title']) && $_SESSION['meta_title'] != '') { echo $_SESSION['meta_title']; unset($_SESSION['meta_title']);}else { echo (isset($cmsDetails->small_description)) ? $cmsDetails->small_description : ''; }?></textarea>
            
          </div>
          
          <div class="form-group input-group col-md-4" id="meta_keywords_msg_error">
            <label for="meta_keywords">Meta Keywords</label>
            <textarea class="form-control"  maxlength="255" name="meta_keywords"  id="meta_keywords" placeholder="Meta Keywords"><?php if(isset($_SESSION['meta_keywords']) && $_SESSION['meta_keywords'] != '') { echo $_SESSION['meta_keywords']; unset($_SESSION['meta_keywords']);}else { echo (isset($cmsDetails->meta_keywords)) ? $cmsDetails->meta_keywords : ''; }?></textarea>
            
          </div>
          
          <div class="form-group input-group col-md-4" id="meta_desc_msg_error">
            <label for="meta_desc">Meta Description</label>
            <textarea class="form-control"  maxlength="255" name="meta_desc"  id="meta_desc" placeholder="Meta Description"><?php if(isset($_SESSION['meta_desc']) && $_SESSION['meta_desc'] != '') { echo $_SESSION['meta_desc']; unset($_SESSION['meta_desc']);}else { echo (isset($cmsDetails->meta_desc)) ? $cmsDetails->meta_desc : ''; }?></textarea>
            
          </div>
          <div class="control-group">
            <label class="control-label" for="selectError">Is Active</label>
            <div class="controls">
              <select id="is_active" name="is_active" data-rel="chosen">
                <option value="0" selected="selected">In Active</option>
                <option value="1" <?php if(isset($_SESSION['is_active']) && $_SESSION['is_active'] == 1) { echo 'selected="selected"'; unset($_SESSION['is_active']); }else { echo (isset($cmsDetails->is_active) && $cmsDetails->is_active == 1) ? 'selected="selected"' : ''; }?> >Active</option>
              </select>
            </div>
          </div>
         
          
          <br />
          <button type="submit" class="btn btn-success btn-sm">Submit</button>
          <?php echo form_close(); ?> </div>
      </div>
    </div>
  </div>
</div>
<script language="javascript" type="text/javascript">
function validate_cms(){	
	if($("#title").val()==''){
		$("#title_msg").html('Please enter CMS title');
		$("#title_msg_error").addClass('has-error');
		$("#title").focus();
		return false;
	}else{
		$("#title_msg").html('');
		$("#title_msg_error").removeClass('has-error');
	}
}
</script> 

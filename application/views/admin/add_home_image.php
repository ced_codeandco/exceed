<script type="text/javascript" src="<?php echo ROOT_URL_BASE?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo ROOT_URL_BASE?>assets/ckfinder/ckfinder.js"></script>
<div id="content" class="col-lg-10 col-sm-10">
<!-- content starts -->
<div>
  <ul class="breadcrumb">
    <li> <a href="<?php echo ADMIN_ROOT_URL?>">Home</a> </li>
    <li> <a href="#"><?php echo $action;?> Home Page Banners</a> </li>
  </ul>
</div>
<div class="row">
  <div class="box-content">
    <div class="box col-md-12">
      <div class="box-inner">
        <div class="box-header well" data-original-image_title="">
          <h2><i class="glyphicon glyphicon-picture"></i> <?php echo $action;?> Home Page Banners</h2>
          
        </div>
        <div class="box-content"> <?php echo validation_errors(); ?>
          <?php 
	$editUrl = '';
	if($action == 'Edit'){
		$editUrl = '/'.$imageDetails->id;
	}
	
	$attributes = array('name' => 'cmsForm', 'id' => 'cmsForm', 'enctype' => 'multipart/form-data', 'role'=>'form', 'onsubmit'=>'return validate_cms();');
				echo form_open(ADMIN_ROOT_URL.'home_page_image/add'.$editUrl,$attributes); ?>
          <input type="hidden" name="id" id="id" value="<?php echo (isset($imageDetails->id)) ? $imageDetails->id : 0;?>" />
          <input type="hidden" name="action" id="action" value="<?php echo $action?>" />
          
          
          <div class="form-group input-group col-md-4" id="image_title_msg_error">
            <label class="control-label" for="image_title">Title<span class="required">*</span></label>
            <input type="text" class="form-control" maxlength="255" name="image_title" value="<?php if(isset($_SESSION['image_title']) && $_SESSION['image_title'] != '') { echo $_SESSION['image_title']; unset($_SESSION['image_title']);}else { echo (isset($imageDetails->image_title)) ? $imageDetails->image_title : ''; }?>" id="image_title" placeholder="Enter Title">
            <br />
            <label class="control-label" id="image_title_msg"></label>
          </div>
          
          <div class="form-group input-group col-md-4" id="image_title_2_msg_error">
            <label class="control-label" for="image_title_2">Title 2</label>
            <input type="text" class="form-control" maxlength="255" name="image_title_2" value="<?php if(isset($_SESSION['image_title_2']) && $_SESSION['image_title_2'] != '') { echo $_SESSION['image_title_2']; unset($_SESSION['image_title_2']);}else { echo (isset($imageDetails->image_title)) ? $imageDetails->image_title_2 : ''; }?>" id="image_title_2" placeholder="Enter Title 2">
            <br />
            <label class="control-label" id="image_title_2_msg"></label>
          </div>
          <div class="form-group input-group col-md-4" id="image_path_msg_error">
            <label for="image_path">Banner Image</label><br />
            <input type="file" name="image_path" id="image_path" class="input-text-02"   />
      <?php if(isset($imageDetails->image_path) && $imageDetails->image_path!='' && file_exists(DIR_UPLOAD_BANNER.$imageDetails->image_path)) {?>
     
      <img src="<?php echo ROOT_URL_BASE?>assets/timthumb.php?src=<?php echo DIR_UPLOAD_BANNER_SHOW.$imageDetails->image_path ?>&q=100&w=150"/>
      <input type="hidden" id="uploaded_file" name="uploaded_file" value="<?php echo $imageDetails->image_path;  ?>" />
      <?php } ?>
        
          </div>
          <div class="control-group">
            <label class="control-label" for="selectError">Is Active</label>
            <div class="controls">
              <select id="is_active" name="is_active" data-rel="chosen">
                <option value="0" selected="selected">In Active</option>
                <option value="1" <?php if(isset($_SESSION['is_active']) && $_SESSION['is_active'] == 1) { echo 'selected="selected"'; unset($_SESSION['is_active']); }else { echo (isset($imageDetails->is_active) && $imageDetails->is_active == 1) ? 'selected="selected"' : ''; }?> >Active</option>
              </select>
            </div>
          </div>
         
          
          <br />
          <button type="submit" class="btn btn-success btn-sm">Submit</button>
          <?php echo form_close(); ?> </div>
      </div>
    </div>
  </div>
</div>
<script language="javascript" type="text/javascript">
function validate_cms(){	
	if($("#image_title").val()==''){
		$("#image_title_msg").html('Please enter Home Page Banner image_title');
		$("#image_title_msg_error").addClass('has-error');
		$("#image_title").focus();
		return false;
	}else{
		$("#image_title_msg").html('');
		$("#image_title_msg_error").removeClass('has-error');
	}
}
</script> 

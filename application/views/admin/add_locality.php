<script type="text/javascript" src="<?php echo ROOT_URL_BASE?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo ROOT_URL_BASE?>assets/ckfinder/ckfinder.js"></script>
<div id="content" class="col-lg-10 col-sm-10">
<!-- content starts -->
<div>
  <ul class="breadcrumb">
    <li> <a href="<?php echo ADMIN_ROOT_URL?>">Home</a> </li>
    <li> <a href="#"><?php echo $action;?> Locality</a> </li>
  </ul>
</div>
<div class="row">
  <div class="box-content">
    <div class="box col-md-12">
      <div class="box-inner">
        <div class="box-header well" data-original-title="">
          <h2><i class="glyphicon glyphicon-list-alt"></i> <?php echo $action;?> Locality</h2>
          <div class="box-icon"> <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a> <a href="#" class="btn btn-minimize btn-round btn-default"><i
                    class="glyphicon glyphicon-chevron-up"></i></a> <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a> </div>
        </div>
        <div class="box-content"> <?php echo validation_errors(); ?>
          <?php 
	$editUrl = '';
	if($action == 'Edit'){
		$editUrl = '/'.$localityDetails->id;
	}
	
	$attributes = array('name' => 'localityForm', 'id' => 'localityForm', 'enctype' => 'multipart/form-data', 'role'=>'form', 'onsubmit'=>'return validate_locality();');
				echo form_open(ADMIN_ROOT_URL.'locality/add'.$editUrl,$attributes); ?>
          <input type="hidden" name="id" id="id" value="<?php echo (isset($localityDetails->id)) ? $localityDetails->id : 0;?>" />
          <input type="hidden" name="current_parent_id" id="current_parent_id" value="<?php if(isset($_SESSION['parent_id'])) { echo $_SESSION['parent_id']; unset($_SESSION['parent_id']); }else { echo (isset($localityDetails->parent_id)) ? $localityDetails->parent_id : 0; }?>" />
          <input type="hidden" name="action" id="action" value="<?php echo $action?>" />
          
          <div class="control-group">
            <label class="control-label" for="selectError">Select City</label>
            <div class="controls"> 
              <select id="parent_id" name="parent_id" data-rel="chosen"><?php print_r($localityDetails);?>
                  <option value="">Select City</option>
              <?php if(!empty($cityList) && is_array($cityList)) {
                  foreach ($cityList as $city) {
                      echo '<option'.((!empty($localityDetails->parent_id) && $localityDetails->parent_id == $city->id) ? ' selected="selected" ' : '' ).' value="'.$city->id.'">'.$city->combined.'</option>';
                  }
              }?>
               </select>
            </div>
          </div>
          <div class="form-group input-group col-md-4" id="title_msg_error">
            <label class="control-label" for="title">Title<span class="required">*</span></label>
            <input required type="text" class="form-control" maxlength="255" name="title" value="<?php if(isset($_SESSION['title']) && $_SESSION['title'] != '') { echo $_SESSION['title']; unset($_SESSION['title']);}else { echo (isset($localityDetails->title)) ? $localityDetails->title : ''; }?>" id="title" placeholder="Enter Title">
            <br />
            <label class="control-label" id="title_msg"></label>
          </div>

          <div class="control-group">
            <label class="control-label" for="selectError">Is Active</label>
            <div class="controls">
              <select id="is_active" name="is_active" data-rel="chosen">
                <option value="0" selected="selected">In Active</option>
                <option value="1" <?php if(isset($_SESSION['is_active']) && $_SESSION['is_active'] == 1) { echo 'selected="selected"'; unset($_SESSION['is_active']); }else { echo (isset($localityDetails->is_active) && $localityDetails->is_active == 1) ? 'selected="selected"' : ''; }?> >Active</option>
              </select>
            </div>
          </div>
         
          
          <br />
          <button type="submit" class="btn btn-success btn-sm">Submit</button>
          <?php echo form_close(); ?> </div>
      </div>
    </div>
  </div>
</div>
<script language="javascript" type="text/javascript">
$(document).ready(function() {
	/*$.ajax({
	   type: "POST",
	   url: "<?php echo ADMIN_ROOT_URL?>locality/get_parent?id="+$("#id").val()+"&current_parent_id="+$("#current_parent_id").val(),
	   success: function(result){
		   $('#parent_id').val(result);
	   },
	   complete: function(){
	
	   },
	   error: function(){
		$('#parent_id').val('Error occured. Please try later');
	   }
	
	 });*/
});
function validate_locality(){
	/*if($("#title").val()==''){
		$("#title_msg").html('Please enter Locality title');
		$("#title_msg_error").addClass('has-error');
		$("#title").focus();
		return false;
	}else{
		$("#title_msg").html('');
		$("#title_msg_error").removeClass('has-error');
	}*/
}
</script> 

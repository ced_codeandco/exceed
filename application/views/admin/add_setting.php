<script type="text/javascript" src="<?php echo ROOT_URL_BASE?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo ROOT_URL_BASE?>assets/ckfinder/ckfinder.js"></script>
<div id="content" class="col-lg-10 col-sm-10">
<!-- content starts -->
<div>
  <ul class="breadcrumb">
    <li> <a href="<?php echo ADMIN_ROOT_URL?>">Home</a> </li>
    <li> <a href="#"><?php echo $action;?> Settings</a> </li>
  </ul>
</div>
<div class="row">
  <div class="box-content">
    <div class="box col-md-12">
      <div class="box-inner">
        <div class="box-header well" data-original-title="">
          <h2><i class="glyphicon glyphicon-list-alt"></i> <?php echo $action;?> Setting</h2>
          <div class="box-icon"> <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a> <a href="#" class="btn btn-minimize btn-round btn-default"><i
                    class="glyphicon glyphicon-chevron-up"></i></a> <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a> </div>
        </div>
        <div class="box-content"> <?php echo validation_errors(); ?>
          <?php 
	$editUrl = '';
	if($action == 'Edit'){
		$editUrl = '/'.$settingDetails->id;
	}
	
	$attributes = array('name' => 'settingForm', 'id' => 'settingForm', 'enctype' => 'multipart/form-data', 'role'=>'form', 'onsubmit'=>'return validate_setting();');
				echo form_open(ADMIN_ROOT_URL.'setting/add'.$editUrl,$attributes); ?>
          <input type="hidden" name="id" id="id" value="<?php echo (isset($settingDetails->id)) ? $settingDetails->id : 0;?>" />
          <input type="hidden" name="action" id="action" value="<?php echo $action?>" />
          <div class="form-group input-group col-md-4" id="title_msg_error">
          <?php if($action == 'Edit'){ ?>
            <label class="control-label" for="title">Setting Title<span class="required">*</span> : <?php echo $settingDetails->default_title;?></label>
            
             <input type="hidden" class="form-control" maxlength="255" name="default_title" value="<?php if(isset($_SESSION['default_title']) && $_SESSION['default_title'] != '') { echo $_SESSION['default_title']; unset($_SESSION['default_title']);}else { echo (isset($settingDetails->default_title)) ? $settingDetails->default_title : ''; }?>" id="default_title" placeholder="Setting Title">
            <?php }else{ ?>
             <label class="control-label" for="title">Setting Title<span class="required">*</span></label>
            <input type="text" class="form-control" maxlength="255" name="default_title" value="<?php if(isset($_SESSION['default_title']) && $_SESSION['default_title'] != '') { echo $_SESSION['default_title']; unset($_SESSION['default_title']);}else { echo (isset($settingDetails->default_title)) ? $settingDetails->default_title : ''; }?>" id="default_title" placeholder="Setting Title">
			<?php } ?>
            <br />
            
            <label class="control-label" id="title_msg"></label>
          </div>
          <div class="form-group input-group col-md-4" id="value_msg_error">
            <label class="control-label"  for="value">Setting Value</label>
            <textarea class="form-control"  maxlength="255" name="value"  id="value" placeholder="Setting Value"><?php if(isset($_SESSION['value']) && $_SESSION['value'] != '') { echo $_SESSION['value']; unset($_SESSION['value']);}else { echo (isset($settingDetails->value)) ? $settingDetails->value : ''; }?></textarea>
            <br />
            <label class="control-label" id="value_msg"></label>
          </div>
          <input type="hidden" name="is_active" id="is_active"  value="1" />
         
         
          
          <br />
          <button type="submit" class="btn btn-success btn-sm">Submit</button>
          <?php echo form_close(); ?> </div>
      </div>
    </div>
  </div>
</div>
<script language="javascript" type="text/javascript">
function validate_setting(){	
	if($("#default_title").val()==''){
		$("#title_msg").html('Please enter Setting Title');
		$("#title_msg_error").addClass('has-error');
		$("#default_title").focus();
		return false;
	}else{
		$("#title_msg").html('');
		$("#title_msg_error").removeClass('has-error');
	}
	if($("#value").val()==''){
		$("#value_msg").html('Please enter Setting Value');
		$("#value_msg_error").addClass('has-error');
		$("#value").focus();
		return false;
	}else{
		$("#value_msg").html('');
		$("#value_msg_error").removeClass('has-error');
	}
}
</script> 

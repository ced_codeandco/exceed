<script type="text/javascript" src="<?php echo ROOT_URL_BASE?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo ROOT_URL_BASE?>assets/ckfinder/ckfinder.js"></script>
<div id="content" class="col-lg-10 col-sm-10">
<!-- content starts -->
<div>
  <ul class="breadcrumb">
    <li> <a href="<?php echo ADMIN_ROOT_URL?>">Home</a> </li>
    <li> <a href="#"><?php echo $action;?> Blog Category </a> </li>
  </ul>
</div>
<div class="row">
  <div class="box-content">
    <div class="box col-md-12">
      <div class="box-inner">
        <div class="box-header well" data-original-title="">
          <h2><i class=" glyphicon glyphicon-picture"></i> <?php echo $action;?> Category</h2>
          
        </div>
        <div class="box-content"> <?php echo validation_errors(); ?>
          <?php 
	$editUrl = '';
	if($action == 'Edit'){
		$editUrl = '/'.$tipsAndTricksCategoryDetails->id;
	}
	
	$attributes = array('name' => 'cmsForm', 'id' => 'cmsForm', 'enctype' => 'multipart/form-data', 'role'=>'form', 'onsubmit'=>'return validate_cms();');
				echo form_open(ADMIN_ROOT_URL.'tips_and_tricks_category/add'.$editUrl,$attributes); ?>
          <input type="hidden" name="id" id="id" value="<?php echo (isset($tipsAndTricksCategoryDetails->id)) ? $tipsAndTricksCategoryDetails->id : 0;?>" />
          <input type="hidden" name="action" id="action" value="<?php echo $action?>" />
          
          
          <div class="form-group input-group col-md-4" id="title_msg_error">
            <label class="control-label" for="title">Title<span class="required">*</span></label>
            <input type="text" class="form-control" maxlength="255" name="title" value="<?php if(isset($_SESSION['title']) && $_SESSION['title'] != '') { echo $_SESSION['title']; unset($_SESSION['title']);}else { echo (isset($tipsAndTricksCategoryDetails->title)) ? $tipsAndTricksCategoryDetails->title : ''; }?>" id="title" placeholder="Enter Title">
            <br />
            <label class="control-label" id="title_msg"></label>
          </div>
          
          <!--<div class="form-group input-group col-md-12" id="tips_and_tricks_category_url_msg_error">
            <label class="control-label" for="tips_and_tricks_category_url">City List</label><br>
            <br />
              <select class="city-select-list" multiple id="cityListContainer">
                  <?php
/*                  $addedCities = !empty($tipsAndTricksCategoryDetails->city_id) ? $tipsAndTricksCategoryDetails->city_id : array();
                  $addedCities = !empty($_POST['city_id']) ? $_POST['city_id'] : $addedCities;
                  print_r($addedCities);
                  $selectedCityList =array();
                  foreach($cityList as $listItem){
                      $selected = '';
                      if (!empty($addedCities) && is_array($addedCities) && isset($addedCities[$listItem->id])) {
                          $selectedCityList[] = $listItem;
                      } else { */?>
                          <option
                          value="<?php /*echo $listItem->id */?>" <?php /*echo $selected; */?> ><?php /*echo $listItem->combined; */?></option><?php
/*                      }
                  }*/?>
              </select>
              <select class="city-selected-list" multiple name="city_id[]" id="selectedCities"><?php
/*                  foreach($selectedCityList as $listItem){*/?>
                          <option
                          value="<?php /*echo $listItem->id */?>" selected="selected"><?php /*echo $listItem->combined; */?></option><?php
/*                  }*/?>
              </select>
            <label class="control-label" id="tips_and_tricks_category_url_msg"></label>
          </div>-->
          <div class="control-group">
            <label class="control-label" for="selectError">Is Active</label>
            <div class="controls">
              <select id="is_active" name="is_active" data-rel="chosen">
                <option value="0" selected="selected">In Active&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
                <option value="1" <?php if(isset($_SESSION['is_active']) && $_SESSION['is_active'] == 1) { echo 'selected="selected"'; unset($_SESSION['is_active']); }else { echo (isset($tipsAndTricksCategoryDetails->is_active) && $tipsAndTricksCategoryDetails->is_active == 1) ? 'selected="selected"' : ''; }?> >Active&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
              </select>
            </div>
          </div>

          
          <br />
          <button type="submit" class="btn btn-success btn-sm">Submit</button>
          <?php echo form_close(); ?> </div>
      </div>
    </div>
  </div>
</div>
<script language="javascript" type="text/javascript">
function validate_cms(){	
	if($("#title").val()==''){
		$("#title_msg").html('Please enter Category title');
		$("#title_msg_error").addClass('has-error');
		$("#title").focus();
		return false;
	}else{
		$("#title_msg").html('');
		$("#title_msg_error").removeClass('has-error');
	}
}
function SetMultiSelect(multiSltCtrl)
{
    //here, the 1th param multiSltCtrl is a html select control or its jquery object, and the 2th param values is an array
    var $sltObj = $(multiSltCtrl) || multiSltCtrl;
    var opts = $sltObj[0].options; //
    for (var i = 0; i < opts.length; i++)
    {
        opts[i].selected = true;
    }
    //$sltObj.multiselect("refresh");//don't forget to refresh!
}

function getSelectedOptions(sel, fn) {
    var opts = [], opt;

    // loop through options in select list
    for (var i=0, len=sel.options.length; i<len; i++) {
        opt = sel.options[i];

        // check if selected
        if ( opt.selected ) {
            // add to array of option elements to return from this function
            opts.push(opt);

            // invoke optional callback function if provided
            if (fn) {
                fn(opt);
            }
        }
    }

    // return array containing references to selected option elements
    return opts;
}

// example callback function (selected options passed one by one)
function callback(opt) {
    // can access properties of opt, such as...
    //alert( opt.value )
    //alert( opt.text )
    //alert( opt.form )

    // display in textarea for this example
    var display = document.getElementById('selectedCities');
    display.innerHTML += opt.value + ', ';
}
    $(document).ready(function(){
        $('#cityListContainer').click(function(){
            var selectedOptions = getSelectedOptions(this);
            console.log(selectedOptions);
            $('#selectedCities').append(selectedOptions);
        })
        $('#selectedCities').change(function(){
            var selectedOptions = getSelectedOptions(this);
            $(selectedOptions).each(function(){
                $(this).removeAttr('selected');
            });
            $('#cityListContainer').append(selectedOptions);
            el = document.getElementById("selectedCities");
            SetMultiSelect(el);
        })
    })
</script> 

<div id="content" class="col-lg-10 col-sm-10">
<div>
  <ul class="breadcrumb">
    <li> <a href="<?php echo ADMIN_ROOT_URL?>">Home</a> </li>
    <li> <a href="#">Brand List</a> </li>
  </ul>
</div>
<div class="row">
  <div class="box-content">
    <div class="box col-md-12">
      <div class="box-inner">
        <div class="box-header well" data-original-title="">
          <h2><i class="glyphicon glyphicon-user"></i> Brand  List</h2>
          <div class="box-icon"> <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a> <a href="#" class="btn btn-minimize btn-round btn-default"><i
                    class="glyphicon glyphicon-chevron-up"></i></a> <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a> </div>
        </div>
        <div class="box-content">
          <?php if(isset($successMsg) && $successMsg != ''){?>
          <div class="alert alert-success">
            <button data-dismiss="alert" class="close" type="button">×</button>
            <?php echo $successMsg; unset($successMsg);?></div>
          <?php } ?>
          <?php if(isset($errMsg) && $errMsg != ''){?>
          <div class="alert alert-danger">
            <button data-dismiss="alert" class="close" type="button">×</button>
            <?php echo $errMsg; unset($errMsg);?></div>
          <?php } ?>
          <table class="table table-striped table-bordered bootstrap-datatable datatable responsive" id="datatable_list">
            <thead>
              <tr>
          
          <th width="14%">Order</th>
          <th width="42%">Title</th>
        <th width="15%">Models</th>
          <th width="12%" style="text-align:center">Status</th>
          <th width="17%" style="text-align:center">Action</th>
        </tr>
            </thead>
            <tbody>
              <?php 
		$i = 0;
		if($brandList && count($brandList) > 0 ){
			$paOrder =1; 
			/*
			
			
		  	foreach ($brandList as $brand){ 
			$i++;
		?>
              <tr>
                <td><?php echo $i;?></td>
                <td><?php echo $brand->title?></td>
               
                
                
                <td class="center"><?php if($brand->id != 1) {?>
                  <?php if($brand->is_active == 1) {?>
                  <a class="label-success label label-default" href="<?php echo ADMIN_ROOT_URL?>brand/status_inactive/<?php echo $brand->id?>" >Active</a>
                  <?php }else{?>
                  <a class="label-default label label-danger" href="<?php echo ADMIN_ROOT_URL?>brand/status_active/<?php echo $brand->id?>">Inactive</a>
                  <?php } 
		  }else{
			  if($brand->is_active == 1) {?>
                  <span class="label-success label label-default">Active</span>
                  <?php }else{?>
                  <span class="label-default label label-danger" >Inactive</span>
                  <?php }	
		}
		?></td>
                <td class="center"><a class="btn btn-info" href="<?php echo ADMIN_ROOT_URL?>brand/add/<?php echo $brand->id?>"> <i class="glyphicon glyphicon-edit icon-white"></i> Edit </a>
                  <?php if($brand->id != 1) {?>
                  <a class="btn btn-danger" href="#" onclick="javascript:if(confirm('Are you sure to delete ? ')){location.href='<?php echo ADMIN_ROOT_URL?>brand/delete/<?php echo $brand->id?>'}"> <i class="glyphicon glyphicon-trash icon-white"></i> Delete </a>
                  <?php }?>                  
              </tr>
              <?php 	}
		*/
		foreach ($brandList as $brand){  ?>
        <tr>
          
          <td><?php echo $paOrder; ?> </td>
          <td>
            <a href="<?php echo ADMIN_ROOT_URL?>brand/add/<?php echo $brand->id?>" ><?php echo $brand->title;  ?></a>
                     </td>
          
           <td style="text-align:center"><a href="<?php echo ADMIN_ROOT_URL?>brand/model_list/<?php echo $brand->id?>" >
                   <img src="<?php echo ROOT_URL_BASE?>images/admin/plus.gif"  /></a></td>
          <td style="text-align:center" id="td_status_<?php echo $brand->id ?>">
          
           
            <?php if($brand->is_active=='1'){?>
            <a href="<?php echo ADMIN_ROOT_URL?>brand/status_inactive/<?php echo $brand->id?>" class="label-success label label-default" >Active</a>
            <?php }else{?>
            <a href="<?php echo ADMIN_ROOT_URL?>brand/status_active/<?php echo $brand->id?>" class="label-default label label-danger"  >In Active</a>
            <?php }?>
			      </td>
          <td class="t-center">
            <a href="<?php echo ADMIN_ROOT_URL?>brand/add/<?php echo $brand->id?>" class="btn btn-info"> <i class="glyphicon glyphicon-edit icon-white"></i> Edit</a>
           
            <a class="btn btn-danger" href="#" onclick="javascript:if(confirm('Are you sure to delete ? ')){location.href='<?php echo ADMIN_ROOT_URL?>brand/delete/<?php echo $brand->id?>'}"> <i class="glyphicon glyphicon-trash icon-white"></i> Delete </a>
            
                   </td>
        </tr>
       
        <?php $paOrder++; }
		
		} ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<div id="content" class="col-lg-10 col-sm-10">
<div>
  <ul class="breadcrumb">
    <li> <a href="<?php echo ADMIN_ROOT_URL?>">Home</a> </li>
    <li> <a href="#"><?php echo (!empty($parentId)) ? 'Sub ' : '';?>Category List</a> </li>
  </ul>
</div>
<div class="row">
  <div class="box-content">
    <div class="box col-md-12">
      <div class="box-inner">
        <div class="box-header well" data-original-title="">
          <h2><i class="glyphicon glyphicon-user"></i> <?php echo (!empty($parentId)) ? 'Sub ' : '';?>Category  List</h2>
          <div class="box-icon"> <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a> <a href="#" class="btn btn-minimize btn-round btn-default"><i
                    class="glyphicon glyphicon-chevron-up"></i></a> <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a> </div>
        </div>
        <div class="box-content">
          <?php if(isset($successMsg) && $successMsg != ''){?>
          <div class="alert alert-success">
            <button data-dismiss="alert" class="close" type="button">×</button>
            <?php echo $successMsg; unset($successMsg);?></div>
          <?php } ?>
          <?php if(isset($errMsg) && $errMsg != ''){?>
          <div class="alert alert-danger">
            <button data-dismiss="alert" class="close" type="button">×</button>
            <?php echo $errMsg; unset($errMsg);?></div>
          <?php } ?>
          <table class="table table-striped table-bordered bootstrap-datatable datatable responsive" id="datatable_list">
            <thead>
              <tr>
          
          <th width="10%">Order</th>
          <th width="22%">Title</th>
          <th width="20%">Slug</th>
          <th width="12%" style="text-align:center">Sub Categories</th>
          <th width="12%" style="text-align:center">Status</th>
          <th width="20%" style="text-align:center">Action</th>
        </tr>
            </thead>
            <tbody>
              <?php 
		$i = 0;
		if($categoryList && count($categoryList) > 0 ){
			$paOrder =1;
		foreach ($categoryList as $category){  ?>
        <tr>
          
          <td><?php echo $paOrder; ?>
         
            <?php if($paOrder!=1){?>
            <a href="javascript:changeOrderCategory('<?php echo $category->id ?>','<?php echo $category->category_order ?>','Up',<?php echo $category->parent_id ?>);"><img src="<?php echo ROOT_URL_BASE?>images/uparrow.png" alt="Up" width="16" height="16" border="0" /></a>
            <?php }?>
            <?php if($paOrder!=count($categoryList)){?>
            <a href="javascript:changeOrderCategory('<?php echo $category->id ?>','<?php echo $category->category_order ?>','Dn',<?php echo $category->parent_id ?>);"><img src="<?php echo ROOT_URL_BASE?>images/downarrow.png" alt="Down" width="16" height="16" border="0" /></a>
            <?php }?>
                 </td>
          <td>
            <a href="<?php echo ADMIN_ROOT_URL?>category/add/<?php echo $category->id?>" ><?php echo $category->title;  ?></a>
                     </td>
          <td><?php echo $category->category_slug;  ?></td>
          <td style="text-align:center"> <?php echo ($category->sub_page_count > 0) ? ' <a href="'.ADMIN_ROOT_URL.'category/index/'.$category->id.'" >Sub Categories</a>' : '';?></td>
          <td style="text-align:center" id="td_status_<?php echo $category->id ?>">
          
           <?php if(!in_array(trim($category->category_slug),$noneEditPage)) {?>
            <?php if($category->is_active=='1'){?>
            <a href="<?php echo ADMIN_ROOT_URL?>category/status_inactive/<?php echo $category->id?>" class="label-success label label-default" >Active</a>
            <?php }else{?>
            <a href="<?php echo ADMIN_ROOT_URL?>category/status_active/<?php echo $category->id?>" class="label-default label label-danger"  >In Active</a>
            <?php }?>
            <?php }else{ ?>
			 <?php if($category->is_active=='1'){?>
            <span class="label-success label label-default" >Active</span>
            <?php }else{?>
            <span class="label-default label label-danger" >In Active</span>
            <?php }?>
			<?php } ?>
			      </td>
          <td class="t-center">
            <a href="<?php echo ADMIN_ROOT_URL?>category/add/<?php echo $category->id?>" class="btn btn-info"> <i class="glyphicon glyphicon-edit icon-white"></i> Edit</a>
             <?php if(!in_array(trim($category->category_slug),$noneEditPage)) {?>
            <a class="btn btn-danger" href="#" onclick="javascript:if(confirm('Are you sure to delete ? ')){location.href='<?php echo ADMIN_ROOT_URL?>category/delete/<?php echo $category->id?>'}"> <i class="glyphicon glyphicon-trash icon-white"></i> Delete </a>
            <?php } ?>
                   </td>
        </tr>
       
        <?php $paOrder++; }
		
		} ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
function changeOrderCategory(id,category_order,position,parent)
{
	location.href ="<?php echo ADMIN_ROOT_URL?>category/order?id="+id+"&category_order="+category_order+"&position="+position+"&parent="+parent;
}


</script>
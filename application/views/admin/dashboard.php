

        <noscript>
            <div class="alert alert-block col-md-12">
                <h4 class="alert-heading">Warning!</h4>

                <p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a>
                    enabled to use this site.</p>
            </div>
        </noscript>

        <div id="content" class="col-lg-10 col-sm-10">
            <?php 
			if(isset($errMsg) && $errMsg != ''){ ?>
			<div class="alert alert-danger">
                <?php echo $errMsg;?>
            </div>
			<?php unset($errMsg);
			}
			if(isset($succMsg) && $succMsg != ''){ ?>
			<div class="alert alert-success">
                <?php echo $succMsg;?>
            </div>
			<?php unset($succMsg);
			} ?>
            <div>
    <ul class="breadcrumb">
        <li>
            <a href="#">Home</a>
        </li>
        <li>
            <a href="#">Dashboard</a>
        </li>
    </ul>
</div>
<div class=" row">
    <div class="col-md-3 col-sm-3 col-xs-6">
        <a data-toggle="tooltip" class="well top-block" href="<?php echo ADMIN_ROOT_URL?>members">
            <i class="glyphicon glyphicon-user blue"></i>

            <div>Total Members</div>
            <div><?php echo $totalMembers;?></div>
        </a>
    </div>

    <div class="col-md-3 col-sm-3 col-xs-6">
        <a data-toggle="tooltip" class="well top-block" href="<?php echo ADMIN_ROOT_URL?>classified">
            <i class="glyphicon glyphicon-list-alt"></i>

            <div>Classifieds</div>
            <div><?php echo $totalClassified;?></div>

        </a>
    </div>


</div>



<!--/row-->

<!--/row-->
    <!-- content ends -->
    </div><!--/#content.col-md-0-->
</div><!--/fluid-row-->

  

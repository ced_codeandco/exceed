<div id="content" class="col-lg-10 col-sm-10">
<div>
  <ul class="breadcrumb">
    <li> <a href="<?php echo ADMIN_ROOT_URL?>">Home</a> </li>
    <li> <a href="#">Home Page Banners List</a> </li>
  </ul>
</div>
<div class="row">
  <div class="box-content">
    <div class="box col-md-12">
      <div class="box-inner">
        <div class="box-header well" data-original-image_title="">
          <h2><i class="glyphicon glyphicon-picture"></i> Home Page Banners List</h2>
          
        </div>
        <div class="box-content">
          <?php if(isset($successMsg) && $successMsg != ''){?>
          <div class="alert alert-success">
            <button data-dismiss="alert" class="close" type="button">×</button>
            <?php echo $successMsg; unset($successMsg);?></div>
          <?php } ?>
          <?php if(isset($errMsg) && $errMsg != ''){?>
          <div class="alert alert-danger">
            <button data-dismiss="alert" class="close" type="button">×</button>
            <?php echo $errMsg; unset($errMsg);?></div>
          <?php } ?>
          <table class="table table-striped table-bordered bootstrap-datatable datatable responsive" id="datatable_list">
            <thead>
              <tr>
          
          <th width="10%">Order</th>
          <th width="22%">Title</th>
           <th width="22%">Image</th>
          <th width="12%" style="text-align:center">Status</th>
          <th width="20%" style="text-align:center">Action</th>
        </tr>
            </thead>
            <tbody>
              <?php 
		$i = 0;
		if($imageList && count($imageList) > 0 ){
			$paOrder =1; 
			/*
			
			
		  	foreach ($imageList as $home_page_image){ 
			$i++;
		?>
              <tr>
                <td><?php echo $i;?></td>
                <td><?php echo $home_page_image->image_title?></td>
               
                
                
                <td class="center"><?php if($home_page_image->id != 1) {?>
                  <?php if($home_page_image->is_active == 1) {?>
                  <a class="label-success label label-default" href="<?php echo ADMIN_ROOT_URL?>home_page_image/status_inactive/<?php echo $home_page_image->id?>" >Active</a>
                  <?php }else{?>
                  <a class="label-default label label-danger" href="<?php echo ADMIN_ROOT_URL?>home_page_image/status_active/<?php echo $home_page_image->id?>">Inactive</a>
                  <?php } 
		  }else{
			  if($home_page_image->is_active == 1) {?>
                  <span class="label-success label label-default">Active</span>
                  <?php }else{?>
                  <span class="label-default label label-danger" >Inactive</span>
                  <?php }	
		}
		?></td>
                <td class="center"><a class="btn btn-info" href="<?php echo ADMIN_ROOT_URL?>home_page_image/add/<?php echo $home_page_image->id?>"> <i class="glyphicon glyphicon-edit icon-white"></i> Edit </a>
                  <?php if($home_page_image->id != 1) {?>
                  <a class="btn btn-danger" href="#" onclick="javascript:if(confirm('Are you sure to delete ? ')){location.href='<?php echo ADMIN_ROOT_URL?>home_page_image/delete/<?php echo $home_page_image->id?>'}"> <i class="glyphicon glyphicon-trash icon-white"></i> Delete </a>
                  <?php }?>                  
              </tr>
              <?php 	}
		*/
		foreach ($imageList as $images){  ?>
        <tr>
          
          <td><?php echo $paOrder; ?>
         
            <?php if($paOrder!=1){?>
            <a href="javascript:changeOrderCMS('<?php echo $images->id ?>','<?php echo $images->home_page_image_order ?>','Up');"><img src="<?php echo ROOT_URL_BASE?>images/uparrow.png" alt="Up" width="16" height="16" border="0" /></a>
            <?php }?>
            <?php if($paOrder!=count($imageList)){?>
            <a href="javascript:changeOrderCMS('<?php echo $images->id ?>','<?php echo $images->home_page_image_order ?>','Dn');"><img src="<?php echo ROOT_URL_BASE?>images/downarrow.png" alt="Down" width="16" height="16" border="0" /></a>
            <?php }?>
                 </td>
          <td>
            <a href="<?php echo ADMIN_ROOT_URL?>home_page_image/add/<?php echo $images->id?>" ><?php echo $images->image_title;  ?></a>
          </td>
           <td>
                <?php if(isset($images->image_path) && $images->image_path!='' && file_exists(DIR_UPLOAD_BANNER.$images->image_path)) {?>
     
      <img src="<?php echo ROOT_URL_BASE?>assets/timthumb.php?src=<?php echo DIR_UPLOAD_BANNER_SHOW.$images->image_path ?>&q=100&w=150"/>
     
      <?php } ?>
          </td>
          <td style="text-align:center" id="td_status_<?php echo $images->id ?>">
            
            
            <?php if($images->is_active=='1'){?>
            <a href="<?php echo ADMIN_ROOT_URL?>home_page_image/status_inactive/<?php echo $images->id?>" class="label-success label label-default" >Active</a>
            <?php }else{?>
            <a href="<?php echo ADMIN_ROOT_URL?>home_page_image/status_active/<?php echo $images->id?>" class="label-default label label-danger"  >In Active</a>
            <?php }?>
            
          </td>
          <td class="t-center">
            <a href="<?php echo ADMIN_ROOT_URL?>home_page_image/add/<?php echo $images->id?>" class="btn btn-info"> <i class="glyphicon glyphicon-edit icon-white"></i> Edit</a>
         
            <a class="btn btn-danger" href="#" onclick="javascript:if(confirm('Are you sure to delete ? ')){location.href='<?php echo ADMIN_ROOT_URL?>home_page_image/delete/<?php echo $images->id?>'}"> <i class="glyphicon glyphicon-trash icon-white"></i> Delete </a>
            
                   </td>
        </tr>
       
        <?php $paOrder++; }
		
		} ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
function changeOrderCMS(id,home_page_image_order,position)
{
	location.href ="<?php echo ADMIN_ROOT_URL?>home_page_image/order?id="+id+"&home_page_image_order="+home_page_image_order+"&position="+position;
}

</script>
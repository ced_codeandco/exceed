<div id="content" class="col-lg-10 col-sm-10">
<div>
  <ul class="breadcrumb">
    <li> <a href="<?php echo ADMIN_ROOT_URL?>">Home</a> </li>
    <li> <a href="#">Inquiries for <?php echo $classifiedDetails->title?></a> </li>
   
  </ul>
  
</div>

<div class="row">
  <div class="box-content">
    <div class="box col-md-12">
    
      <div class="box-inner">
        <div class="box-header well" data-original-title="">
          <h2><i class="glyphicon glyphicon-user"></i> Inquiries for <?php echo $classifiedDetails->title?>  </h2> 
          
         </div>
         
        <div class="box-content">
        
          <?php 
		  
		  
		  if(isset($successMsg) && $successMsg != ''){?>
          <div class="alert alert-success">
            <button data-dismiss="alert" class="close" type="button">×</button>
            <?php echo $successMsg; unset($successMsg);?></div>
          <?php } ?>
          <?php if(isset($errMsg) && $errMsg != ''){?>
          <div class="alert alert-danger">
            <button data-dismiss="alert" class="close" type="button">×</button>
            <?php echo $errMsg; unset($errMsg);?></div>
          <?php } ?>
          <div class="alert alert-success" style="display:none;" id="emailSuccess">
            <button data-dismiss="alert" class="close" type="button">×</button>
            <span id="emailText"></span>
          </div>
          <table class="table table-striped table-bordered bootstrap-datatable datatable responsive" id="datatable_list">
            <thead>
              <tr>
          
          <th width="6%">No</th>
          <th width="13%">Inquiry By</th>
          <th width="13%">Inquiry Email</th>
          <th width="12%">Inquiry On</th>
           <th width="14%">Inquiry From</th>
            <th width="9%">Replied</th>
          <th width="11%" style="text-align:center">Status</th>
          <th width="22%" style="text-align:center">Action</th>
        </tr>
            </thead>
            <tbody>
              <?php 
		$i = 0;
		if($inquiryList && count($inquiryList) > 0 ){
			$paOrder =1; 
			
		foreach ($inquiryList as $inquiry){  ?>
        <tr>
          
          <td><?php echo $paOrder; ?> </td>
          <td>
            <?php echo $inquiry->inquiry_name ?>
          </td>
          <td>
            <?php echo $inquiry->inquiry_email ?>
          </td>
          <td>
            <?php echo date('d-m-Y H:i A', strtotime($inquiry->inquiry_date_time)); ?>
          </td>
          <td>
            <?php echo $inquiry->inquiry_ip ?>
          </td>
          <td id="td_replied_<?php echo $inquiry->id ?>">
            <?php echo ($inquiry->is_replied == '1') ? 'Yes': 'No'; ?>
          </td>
          <td style="text-align:center" id="td_status_<?php echo $inquiry->id ?>">
            
           
            <?php if($inquiry->is_active=='1'){?>
            <a href="<?php echo ADMIN_ROOT_URL?>classified/inquiry_status_inactive/<?php echo $inquiry->id?>" class="label-success label label-default" >Active</a>
            <?php }else{?>
            <a href="<?php echo ADMIN_ROOT_URL?>classified/inquiry_status_active/<?php echo $inquiry->id?>" class="label-default label label-danger"  >In Active</a>
            <?php }?>
            
          </td>
          <td class="t-center"><a href="#" onclick="addInquiryReply('<?php echo $inquiry->id?>','<?php echo $inquiry->inquiry_name ?>')" class="btn btn-setting btn-info"> <i class="glyphicon glyphicon-edit icon-white"></i> Reply</a>
          <a href="#" class="btn  btn-round btn-default"><i
                            class="glyphicon glyphicon-cog"></i></a>
           </td>
        </tr>
       
        <?php $paOrder++; }
		
		} ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
function changeOrderCMS(id,inquiry_order,position,parent)
{
	location.href ="<?php echo ADMIN_ROOT_URL?>inquiry/order?id="+id+"&inquiry_order="+inquiry_order+"&position="+position+"&parent="+parent;
}
function addInquiryReply(id,name){
	$('#inquiryId').val(id);
	$('#inquirerName').html(name);
}
function sendInquiryReply(){
	
var formdata = $( "#inquiryReply" ).serialize();

	$.ajax({
		url: '<?php echo ADMIN_ROOT_URL?>classified/inquiry_reply',  //server script to process data
		type: 'POST',
		//Ajax events
		success: function(data){ 
		
		 if(data == 'error'){
			  alert('Email Not sent!! Please try again later...');			 
			 	return false;
			  }else{
				  var id = $('#inquiryId').val();
				  $('#emailText').html('Reply email sent successfully');
				  $('#td_replied_'+id).html('Yes');
				  $('#emailSuccess').show();
				  $('.close_model_box').click();
			  }
		},
		error: function(data){$('#abhi').html(data);},
		// Form data
		data: formdata
	});

	return;

}
</script>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">

        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Reply to <span id="inquirerName"></span></h3>
                </div>
                
                 <form name="inquiryReply" method="post" id="inquiryReply">
                <div class="box-content">
                <div class="modal-body">
                  <input type="hidden" id="inquiryId" name="inquiryId" value="" />
                  <input type="hidden" id="classifiedId" name="classifiedId" value="<?php echo $classifiedDetails->id?>" />
                  <input type="hidden" id="replied_member_id" name="replied_member_id" value="<?php echo $classifiedDetails->created_by?>" />
                  
                  <div class="form-group input-group" id="small_description_msg_error">
            		<textarea class="form-control" rows="7" cols="79"  name="reply_text"  id="reply_text" placeholder="Please enter reply message here..."></textarea>
            
          		 </div>
                </div>
                </div>
                <div class="modal-footer">
                    <a href="#"  class="btn btn-default close_model_box" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary" onclick="return sendInquiryReply();">Reply</a>
                </div>
                </form>
            </div>
        </div>
    </div>
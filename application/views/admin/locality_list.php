<div id="content" class="col-lg-10 col-sm-10">
<div>
  <ul class="breadcrumb">
    <li> <a href="<?php echo ADMIN_ROOT_URL?>">Home</a> </li>
    <li> <a href="#">Category List</a> </li>
  </ul>
</div>
<div class="row">
  <div class="box-content">
    <div class="box col-md-12">
      <div class="box-inner">
        <div class="box-header well" data-original-title="">
          <h2><i class="glyphicon glyphicon-user"></i> Category  List</h2>
          <div class="box-icon"> <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a> <a href="#" class="btn btn-minimize btn-round btn-default"><i
                    class="glyphicon glyphicon-chevron-up"></i></a> <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a> </div>
        </div>
        <div class="box-content">
          <?php if(isset($successMsg) && $successMsg != ''){?>
          <div class="alert alert-success">
            <button data-dismiss="alert" class="close" type="button">×</button>
            <?php echo $successMsg; unset($successMsg);?></div>
          <?php } ?>
          <?php if(isset($errMsg) && $errMsg != ''){?>
          <div class="alert alert-danger">
            <button data-dismiss="alert" class="close" type="button">×</button>
            <?php echo $errMsg; unset($errMsg);?></div>
          <?php } ?>
            <div class="control-group">
                <label class="control-label" for="selectError">Filter By City:</label>
                <select data-rel="chosen" onchange="window.location='<?php echo ADMIN_ROOT_URL;?>locality?cityId='+this.value;">
                    <option value="">Select City</option>
                    <?php if (!empty($cityList) && is_array($cityList)){
                        foreach ($cityList as $city) {
                            echo '<option '.( (!empty($cityId) && $cityId == $city->id) ? 'selected="selected"' : '').' value="'.$city->id.'">'.$city->combined.'</option>';
                        }
                    }?>
                </select>
            </div>
          <table class="table table-striped table-bordered bootstrap-datatable datatable responsive" id="datatable_list">
            <thead>
              <tr>
          
          <th width="10%">Order</th>
          <th width="22%">Title</th>
          <th width="12%" style="text-align:center">Status</th>
          <th width="20%" style="text-align:center">Action</th>
        </tr>
            </thead>
            <tbody>
              <?php 
		$i = 0;
		if($localityList && count($localityList) > 0 ){
			$paOrder =1;
		foreach ($localityList as $locality){  ?>
        <tr>
          
          <td><?php echo $paOrder; ?>
         
            <?php if($paOrder!=1){?>
            <a href="javascript:changeOrderLocality('<?php echo $locality->id ?>','<?php echo $locality->locality_order ?>','Up',<?php echo $locality->parent_id ?>);"><img src="<?php echo ROOT_URL_BASE?>images/uparrow.png" alt="Up" width="16" height="16" border="0" /></a>
            <?php }?>
            <?php if($paOrder!=count($localityList)){?>
            <a href="javascript:changeOrderLocality('<?php echo $locality->id ?>','<?php echo $locality->locality_order ?>','Dn',<?php echo $locality->parent_id ?>);"><img src="<?php echo ROOT_URL_BASE?>images/downarrow.png" alt="Down" width="16" height="16" border="0" /></a>
            <?php }?>
                 </td>
          <td>
            <a href="<?php echo ADMIN_ROOT_URL?>locality/add/<?php echo $locality->id?>" ><?php echo $locality->title;  ?></a>
                     </td>

          <td style="text-align:center" id="td_status_<?php echo $locality->id ?>">
          
            <?php if($locality->is_active=='1'){?>
            <a href="<?php echo ADMIN_ROOT_URL?>locality/status_inactive/<?php echo $locality->id?>" class="label-success label label-default" >Active</a>
            <?php }else{?>
            <a href="<?php echo ADMIN_ROOT_URL?>locality/status_active/<?php echo $locality->id?>" class="label-default label label-danger"  >In Active</a>
            <?php }?>
			      </td>
          <td class="t-center">
            <a href="<?php echo ADMIN_ROOT_URL?>locality/add/<?php echo $locality->id?>" class="btn btn-info"> <i class="glyphicon glyphicon-edit icon-white"></i> Edit</a>

            <a class="btn btn-danger" href="#" onclick="javascript:if(confirm('Are you sure to delete ? ')){location.href='<?php echo ADMIN_ROOT_URL?>locality/delete/<?php echo $locality->id?>'}"> <i class="glyphicon glyphicon-trash icon-white"></i> Delete </a>

                   </td>
        </tr>
       
        <?php $paOrder++; }
		
		} ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
function changeOrderLocality(id,locality_order,position,parent)
{
	location.href ="<?php echo ADMIN_ROOT_URL?>locality/order?id="+id+"&locality_order="+locality_order+"&position="+position+"&parent="+parent;
}


</script>

<div id="content" class="col-lg-10 col-sm-10">
<div>
  <ul class="breadcrumb">
    <li> <a href="<?php echo ADMIN_ROOT_URL?>">Home</a> </li>
    <li> <a href="#">Member List</a> </li>
  </ul>
</div>
<div class="row">
  <div class="box-content">
    <div class="box col-md-12">
      <div class="box-inner">
        <div class="box-header well" data-original-title="">
          <h2><i class="glyphicon glyphicon-user"></i> Member List</h2>
          <div class="box-icon"> <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a> <a href="#" class="btn btn-minimize btn-round btn-default"><i
                    class="glyphicon glyphicon-chevron-up"></i></a> <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a> </div>
        </div>
        <div class="box-content">
          <?php if(isset($successMsg) && $successMsg != ''){?>
          <div class="alert alert-success">
            <button data-dismiss="alert" class="close" type="button">×</button>
            <?php echo $successMsg; unset($successMsg);?></div>
          <?php } ?>
          <?php if(isset($errMsg) && $errMsg != ''){?>
          <div class="alert alert-danger">
            <button data-dismiss="alert" class="close" type="button">×</button>
            <?php echo $errMsg; unset($errMsg);?></div>
          <?php } ?>
          <table class="table table-striped table-bordered bootstrap-datatable datatable responsive" id="datatable_list">
            <thead>
              <tr>
                <th>No</th>
                <th>Name</th>
                <th>Email</th>
                <th>Last Login</th>
                <th>Last Login IP</th>
                <th>Status</th>
                <th style="width: 360px !important;">Actions</th>
              </tr>
            </thead>
            <tbody>
              <?php 
		$i = 0;
		if($memberList && count($memberList) > 0 ){ 
		  	foreach ($memberList as $member){ 
			$i++;
		?>
              <tr>
                <td><?php echo $i;?></td>
                <td><?php echo $member->first_name.' '.$member->last_name?></td>
                <td><?php echo $member->email?></td>
                <td class="center"><?php if($member->last_login_date != '' && $member->last_login_date != '0000-00-00 00:00:00') { echo date('d-m-Y H:i A',strtotime($member->last_login_date));}?></td>
                <td class="center"><?php echo $member->last_login_ip?></td>
                <td class="center" >
                  <?php if($member->is_active == 1) {?>
                  <a class="label-success label label-default" href="<?php echo ADMIN_ROOT_URL?>members/status_inactive/<?php echo $member->id?>" >Active</a>
                  <?php }else{?>
                  <a class="label-default label label-danger" href="<?php echo ADMIN_ROOT_URL?>members/status_active/<?php echo $member->id?>">Inactive</a>
                  <?php } ?> </td>
                <td class="center"><a class="btn btn-success" href="<?php echo ADMIN_ROOT_URL?>members/add/<?php echo $member->id?>"> <i class="glyphicon glyphicon-zoom-in icon-white"></i> View </a> <a class="btn btn-info" href="<?php echo ADMIN_ROOT_URL?>members/add/<?php echo $member->id?>"> <i class="glyphicon glyphicon-edit icon-white"></i> Edit </a>
                 
                  <a class="btn btn-danger" href="#" onclick="javascript:if(confirm('Are you sure to delete ? ')){location.href='<?php echo ADMIN_ROOT_URL?>members/delete/<?php echo $member->id?>'}"> <i class="glyphicon glyphicon-trash icon-white"></i> Delete </a>
                 
                  <!--<a class="btn btn-info" href="<?php echo ADMIN_ROOT_URL?>members/change_password/<?php echo $member->id?>"> <i class="glyphicon glyphicon-edit icon-white"></i> Password </a>--></td>
              </tr>
              <?php 	}
		} ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript" charset="utf-8">			
				var oTable;
				$(document).ready(function() {
					
				oTable=$('#my_table').dataTable( {
					"bProcessing": true,					
					"bJQueryUI": true,
					"sPaginationType": "full_numbers",
					"iDisplayLength": 10,
					"bServerSide": true,					 
					"sAjaxSource": '<?php echo ADMIN_ROOT_URL?>members/get_member_data',
					"aaSorting": [[0,'desc']],
					"aoColumns": [						
						{ bSortable: true, sWidth: '40px' },
						{ bSortable: true, sWidth: '80px' },
						{ bSortable: true, sWidth: '20px' },						
						{ bSortable: true, sWidth: '80px' },
						{ bSortable: true, sWidth: '80px' },
						{ bSortable: true, sWidth: '80px' },
						{ bSortable: true, sWidth: '80px' }
					]
					
				} );
			} );			
			
		</script>
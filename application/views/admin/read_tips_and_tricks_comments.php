<script type="text/javascript" src="<?php echo ROOT_URL_BASE?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo ROOT_URL_BASE?>assets/ckfinder/ckfinder.js"></script>
<div id="content" class="col-lg-10 col-sm-10">
<!-- content starts -->
<div>
  <ul class="breadcrumb">
    <li> <a href="<?php echo ADMIN_ROOT_URL?>">Home</a> </li>
    <li> <a href="<?php echo ADMIN_ROOT_URL?>tips_and_tricks">Blog</a> </li>
    <li> <a href="#">Read comments</a> </li>
  </ul>
</div>
<div class="row">
  <div class="box-content">
    <div class="box col-md-12">
      <div class="box-inner">
        <div class="box-header well" data-original-title="">
          <h2><i class="glyphicon glyphicon-list-alt"></i> Read comments</h2>
          <div class="box-icon"> <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a> <a href="#" class="btn btn-minimize btn-round btn-default"><i
                    class="glyphicon glyphicon-chevron-up"></i></a> <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a> </div>
        </div>
        <div class="box-content"> <?php echo validation_errors(); ?>
            <?php if(isset($successMsg) && $successMsg != ''){?>
                <div class="alert alert-success">
                    <button data-dismiss="alert" class="close" type="button">×</button>
                    <?php echo $successMsg; unset($successMsg);?></div>
            <?php } ?>
            <?php if(isset($errMsg) && $errMsg != ''){?>
                <div class="alert alert-danger">
                    <button data-dismiss="alert" class="close" type="button">×</button>
                    <?php echo $errMsg; unset($errMsg);?></div>
            <?php } ?>
          <?php
          //print_r($tipsAndTricksDetails);
	$editUrl = '';
	if($action == 'Edit'){
		$editUrl = '/'.$tipsAndTricksDetails->id;
	}
	
	$attributes = array('name' => 'classifiedForm', 'id' => 'classifiedForm', 'enctype' => 'multipart/form-data', 'role'=>'form', 'onsubmit'=>'return validate_classified();');
				echo form_open('',$attributes); ?>



          <div class="form-group input-group col-md-4" id="title_msg_error">
            <label class="control-label" for="title">Title<span class="required">*</span></label><br />
            <?php echo (isset($tipsAndTricksDetails->title)) ? $tipsAndTricksDetails->title : '';?>
            <br />
            <label class="control-label" id="title_msg"></label>
          </div>

            <div class="form-group input-group col-md-4" id="short_description_msg_error">
                <label for="short_description">Short Description</label><br />
                <?php  echo (isset($tipsAndTricksDetails->short_description)) ? $tipsAndTricksDetails->short_description : ''; ?>
            </div>

            <div class="form-group input-group col-md-4" id="description_msg_error">
                <label for="description">Description</label><br />
                <?php echo (isset($tipsAndTricksDetails->description)) ? stripslashes($tipsAndTricksDetails->description) : '';?>
            </div>

            <div class="form-group input-group col-md-4" id="image_msg_error">
                <label for="image">Image</label><br />
                <?php if(isset($tipsAndTricksDetails->image) && $tipsAndTricksDetails->image!='' && file_exists(DIR_UPLOAD_BLOG.$tipsAndTricksDetails->image)) {?>

                    <img src="<?php echo ROOT_URL_BASE?>assets/timthumb.php?src=<?php echo DIR_UPLOAD_BLOG_SHOW.$tipsAndTricksDetails->image ?>&q=100&w=300"/>
                    <input type="hidden" id="uploaded_file" name="uploaded_file" value="<?php echo $tipsAndTricksDetails->image;  ?>" />
                <?php } ?>

            </div>

            <div class="form-group input-group col-md-10" id="description_msg_error">
                <label for="description">User comments</label><br />
                <?php if(!empty($userComments) && is_array($userComments)){
                    echo '<table class="table table-striped table-bordered bootstrap-datatable datatable responsive" >';
                    echo '<thead><tr><th>User Name</th><th>Comment</td><th>Date</th><th>Action</th></tr></thead><tbody>';
                    foreach ($userComments as $comments) {
                        echo '<tr><td>' . $comments->first_name . '</td>';
                        echo '<td>' . $comments->comment . '</td>';
                        echo '<td>' . $comments->created_date_time . '</td>';
                        echo '<td> <a onclick="return confirm(\'Areyou sure you want to delete this comment?\')" href="'.ADMIN_ROOT_URL.'tips_and_tricks/delete_comments/'.$comments->id.'">Delete</a></td></tr>';
                    }
                    echo '</tbody></table>';
                } else {
                    echo '<table class="table table-striped table-bordered bootstrap-datatable datatable responsive" >';
                    echo '<thead><tr><th>No Comments found</th></tr></thead><tbody></tbody></table>';
                }?>
            </div>
         
          
          <br />
          <?php echo form_close(); ?> </div>
      </div>
    </div>
  </div>
</div>


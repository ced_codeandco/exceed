
<div class="row">
  <div class="col-md-12 center login-header">
    <h2>Welcome to <?php echo SITE_NAME?></h2>
  </div>
  <!--/span--> 
</div>
<!--/row-->

<div class="row">
  <div class="well col-md-5 center login-box">
    <?php 
			if(isset($errMsg) && $errMsg != ''){ ?>
			<div class="alert alert-danger">
                <?php echo $errMsg;?>
            </div>
			<?php unset($errMsg);
			}
			if(isset($succMsg) && $succMsg != ''){ ?>
			<div class="alert alert-success">
                <?php echo $succMsg;?>
            </div>
			<?php unset($succMsg);
			}

            if (!empty($validToken)) {
                $attributes = array('name' => 'loginForm', 'id' => 'loginForm', 'enctype' => 'multipart/form-data', 'onsubmit' => 'return validate_admin();');
                echo form_open(ADMIN_ROOT_URL . 'reset_password/' . $password_token, $attributes); ?>
                <fieldset>
                    <div class="input-group input-group-lg" id="password_msg_error">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock red"></i></span>
                        <input type="password" required="required" class="form-control" name="password" id="password"
                               placeholder="Password">
                        <br/><label class="control-label" id="password_msg"></label>
                    </div>
                    <div class="input-group input-group-lg" id="retype_password_msg_error">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock red"></i></span>
                        <input type="password" required="required" class="form-control" name="retype_password"
                               id="retype_password" placeholder="Password Again">
                        <br/><label class="control-label" id="retype_password_msg"></label>
                    </div>

                    <p class="center col-md-5">
                        <button type="submit" name="submitLoginForm" id="submitLoginForm" class="btn btn-primary">Change
                            Password
                        </button>
                        &nbsp; </p>
                </fieldset>

                <?php echo form_close();
            } else {?>
                <a href="<?php echo ADMIN_ROOT_URL?>forgot_password">Forgot Password?</a> <br />
            <?php }?>
    </div>
  <!--/span--> 
</div>
<!--/row--> 

<script language="javascript" type="text/javascript">
function validate_admin(){
	
	
		var password = $("#password").val();
	
		var passed = validatePassword(password, {
	
			length:   [6, Infinity],
	
			numeric:  1,
	
			special:  1
	
		});
	
		
	
		if($("#password").val()==''){
	
			$("#password_msg").html('Please enter Password');
	
			$("#password_msg_error").addClass('has-error');
	
			$("#password").focus();
	
			return false;
	
			
	
		}else if(!passed){
	
			$("#password_msg").html('Password should have minimum 6 char and atleast one numeric or one special char.');
	
			$("#password_msg_error").addClass('has-error');
	
			$("#password").focus();
	
			return false;
	
		}else if($("#password").val()!='' && $("#password").val()!=$("#retype_password").val()){
	
			$("#retype_password_msg").html('Password and Again password do not matched');
	
			$("#retype_password_msg_error").addClass('has-error');
	
			$("#retype_password").focus();
	
			return false;
	
		}else{
	
			$("#password_msg").html('');
	
			$("#password_msg_error").removeClass('has-error');
			
			$("#retype_password_msg").html('');
	
			$("#retype_password_msg_error").removeClass('has-error');
	
		}
	
	
}
$(document).ready(function(){
	
	$("#password").blur(function (){
		$("#loading_pass").show();
		var password = $("#password").val();
		var passed = validatePassword(password, {
			length:   [6, Infinity],
			numeric:  1,
			special:  1
		});
		
		if($("#password").val()==''){
			$("#password_msg").html('Please enter Password');
			$("#password_msg_error").addClass('has-error');
			$("#loading_pass").hide();
			$("#password").focus();
			return false;
		}else if(!passed){
			$("#password_msg").html('Password should have minimum 6 char and atleast one numeric or one special char. ');
			$("#password_msg_error").addClass('has-error');
			$("#loading_pass").hide();
			$("#password").focus();
			return false;
		}else{
			$("#password_msg_error").removeClass('has-error');
			$("#loading_pass").hide();
			$("#password_msg").html('');
  		}
  		
 	});
	$("#retype_password").blur(function (){
		$("#loading_repass").show();
		if($("#retype_password").val()!==$("#password").val()){
			$("#retype_password_msg_error").addClass('has-error');
			$("#retype_password_msg").html('Password and Again password do not matched');
	  		$("#loading_repass").hide();
	  	}else{
			
			
			
			$("#retype_password_msg").html('');
	
			$("#retype_password_msg_error").removeClass('has-error');
	  		$("#loading_repass").hide();
  		}
	});
});
</script>
<div id="content" class="col-lg-10 col-sm-10">
<div>
  <ul class="breadcrumb">
    <li> <a href="<?php echo ADMIN_ROOT_URL?>">Home</a> </li>
    <li> <a href="#">Blog Category List</a> </li>
  </ul>
</div>
<div class="row">
  <div class="box-content">
    <div class="box col-md-12">
      <div class="box-inner">
        <div class="box-header well" data-original-title="">
          <h2><i class=" glyphicon glyphicon-picture"></i> Tips & Tricks Category List</h2>
          
        </div>
        <div class="box-content">
          <?php if(isset($successMsg) && $successMsg != ''){?>
          <div class="alert alert-success">
            <button data-dismiss="alert" class="close" type="button">×</button>
            <?php echo $successMsg; unset($successMsg);?></div>
          <?php } ?>
          <?php if(isset($errMsg) && $errMsg != ''){?>
          <div class="alert alert-danger">
            <button data-dismiss="alert" class="close" type="button">×</button>
            <?php echo $errMsg; unset($errMsg);?></div>
          <?php } ?>
          <table class="table table-striped table-bordered bootstrap-datatable datatable responsive" id="datatable_list">
            <thead>
              <tr>
          
          <th width="10%">Order</th>
          <th width="22%">Title</th>
          <th width="12%" style="text-align:center">Status</th>
          <th width="20%" style="text-align:center">Action</th>
        </tr>
            </thead>
            <tbody>
              <?php 
		$i = 0;
		if($imageList && count($imageList) > 0 ){
			$paOrder =1;
		foreach ($imageList as $images){  ?>
        <tr>
          
          <td><?php echo $paOrder; ?></td>
          <td>
            <a href="<?php echo ADMIN_ROOT_URL?>tips_and_tricks_category/add/<?php echo $images->id?>" ><?php echo $images->title;  ?></a>
          </td>
          <td style="text-align:center" id="td_status_<?php echo $images->id ?>">
            
            
            <?php if($images->is_active=='1'){?>
            <a href="<?php echo ADMIN_ROOT_URL?>tips_and_tricks_category/status_inactive/<?php echo $images->id?>" class="label-success label label-default" >Active</a>
            <?php }else{?>
            <a href="<?php echo ADMIN_ROOT_URL?>tips_and_tricks_category/status_active/<?php echo $images->id?>" class="label-default label label-danger"  >In Active</a>
            <?php }?>
            
          </td>
          <td class="t-center">
            <a href="<?php echo ADMIN_ROOT_URL?>tips_and_tricks_category/add/<?php echo $images->id?>" class="btn btn-info"> <i class="glyphicon glyphicon-edit icon-white"></i> Edit</a>
         
            <a class="btn btn-danger" href="#" onclick="javascript:if(confirm('Are you sure to delete ? ')){location.href='<?php echo ADMIN_ROOT_URL?>tips_and_tricks_category/delete/<?php echo $images->id?>'}"> <i class="glyphicon glyphicon-trash icon-white"></i> Delete </a>
            
                   </td>
        </tr>
       
        <?php $paOrder++; }
		
		} ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
function changeOrderCMS(id,tips_and_tricks_category_order,position)
{
	location.href ="<?php echo ADMIN_ROOT_URL?>tips_and_tricks_category/order?id="+id+"&tips_and_tricks_category_order="+tips_and_tricks_category_order+"&position="+position;
}

</script>
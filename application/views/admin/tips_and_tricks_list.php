<div id="content" class="col-lg-10 col-sm-10">
<div>
  <ul class="breadcrumb">
    <li> <a href="<?php echo ADMIN_ROOT_URL?>">Home</a> </li>
    <li> <a href="#">Blog List</a> </li>
  </ul>
</div>
<div class="row">
  <div class="box-content">
    <div class="box col-md-12">
      <div class="box-inner">
        <div class="box-header well" data-original-title="">
          <h2><i class="glyphicon glyphicon-user"></i> Blog List</h2>
          <div class="box-icon"> <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a> <a href="#" class="btn btn-minimize btn-round btn-default"><i
                    class="glyphicon glyphicon-chevron-up"></i></a> <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a> </div>
        </div>
        <div class="box-content">
          <?php if(isset($successMsg) && $successMsg != ''){?>
          <div class="alert alert-success">
            <button data-dismiss="alert" class="close" type="button">×</button>
            <?php echo $successMsg; unset($successMsg);?></div>
          <?php } ?>
          <?php if(isset($errMsg) && $errMsg != ''){?>
          <div class="alert alert-danger">
            <button data-dismiss="alert" class="close" type="button">×</button>
            <?php echo $errMsg; unset($errMsg);?></div>
          <?php } ?>
          <table class="table table-striped table-bordered bootstrap-datatable datatable responsive" id="datatable_list">
            <thead>
              <tr>
          
          <th width="5%">No</th>
          <th width="30%">Author Name/Title</th>
          <th width="10%" style="text-align:center">Category</th>
          <th width="10%" style="text-align:center">Comments</th>
          <th width="10%" style="text-align:center">Created Date</th>
          <th width="15%" style="text-align:center">Status</th>
          <th width="20%" style="text-align:center">Action</th>
        </tr>
            </thead>
            <tbody>
              <?php 
			 
		$i = 0;
		if($classifiedList && count($classifiedList) > 0 ){
			$paOrder =1; 

		foreach ($classifiedList as $classified){  ?>
        <tr>
          
          <td><?php echo $paOrder; ?></td>
          <!--<td>
            <a href="<?php /*echo ADMIN_ROOT_URL*/?>classified/add/<?php /*echo $classified->id*/?>" ><?php /*echo $classified->title;  */?></a>
                     </td>-->
            <td style="text-align:left">  <?php if(!empty($classified->author_name)) {echo $classified->author_name;} else if(!empty($classified->title)) {echo $classified->title;}?></td>
          <td style="text-align:center"><?php echo $classified->category_name;  ?></td>
          <td style="text-align:center"><a href="<?php echo ADMIN_ROOT_URL?>tips_and_tricks/read_comments/<?php echo $classified->id?>" ><?php echo 'Read Comments';  ?><a></td>
          <td style="text-align:center"><?php echo date('d-m-Y H:i A', strtotime($classified->created_date_time));  ?></td>
          <td style="text-align:center" id="td_status_<?php echo $classified->id ?>">
            <?php if($classified->is_active=='1'){?>
            <a href="<?php echo ADMIN_ROOT_URL?>tips_and_tricks/status_inactive/<?php echo $classified->id?>" class="label-success label label-default" >Active</a>
            <?php }else{?>
            <a href="<?php echo ADMIN_ROOT_URL?>tips_and_tricks/status_active/<?php echo $classified->id?>" class="label-default label label-danger"  >In Active</a>
            <?php }?>
           
			      </td>
                  
          <td class="t-center">
            <a href="<?php echo ADMIN_ROOT_URL?>tips_and_tricks/add/<?php echo $classified->id?>" class="btn btn-info"> <i class="glyphicon glyphicon-edit icon-white"></i> Edit</a>
            
            <a class="btn btn-danger" href="#" onclick="javascript:if(confirm('Are you sure to delete ? ')){location.href='<?php echo ADMIN_ROOT_URL?>tips_and_tricks/delete/<?php echo $classified->id?>'}"> <i class="glyphicon glyphicon-trash icon-white"></i> Delete </a>
          
                   </td>
        </tr>
       
        <?php $paOrder++; }
		
		} ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
function changeOrderClassified(id,classified_order,position,parent)
{
	location.href ="<?php echo ADMIN_ROOT_URL?>classified/order?id="+id+"&classified_order="+classified_order+"&position="+position+"&parent="+parent;
}

</script>
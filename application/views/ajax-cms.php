<?php
/**
 * Created by PhpStorm.
 * Author: Anas
 * Date: 4/19/2015
 * Time: 8:11 PM
 */

if ($show404 != true) {?>
    <div class="popup-wrap">
        <?php echo !empty($cmsData->description) ? stripslashes($cmsData->description) : '';?>
    </div>
<?php } else {?>
    <div class="banner" style="height: 480px">
        <div class="form-group">
            <div class="page-not-found">
                <p class="error-code">404</p>
                <p class="error-message">Page not found</p>
                <p class="error-message+"><a href="<?php echo ROOT_URL;?>">Click here to go to home page</a></p>
            </div>
        </div>
    </div>
<?php }?>
<link href="<?php echo ROOT_URL_BASE;?>css/styles.css" type="text/css" rel="stylesheet" title="<?php echo defined('EXCEED_TITLE') ? EXCEED_TITLE : SITE_NAME;?>" />
<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 6/8/2015
 * Time: 7:45 PM
 */?>
<div class="container ">
    <ul class="bradcram">
        <li><a href="<?php echo ROOT_URL;?>">Home</a></li>
        <li>Categories</li>
    </ul>
    <h2 class="page-title">Categories</h2>
    <?php
    //print_r($categoryList);
    if (!empty($categoryList['mainCategory']) && is_array($categoryList['mainCategory'])){?>
        <div class="row"><?php
        $j = 1;
        $subCategory = !empty($categoryList['subCategory']) ? $categoryList['subCategory'] : array();
        $totalCount = count($categoryList['mainCategory']);
        function sortByOption($a, $b) {
            return strcmp($a->category_order, $b->category_order);
        }
        usort($categoryList['mainCategory'], 'sortByOption');
        foreach ($categoryList['mainCategory'] as $category) {
            if (!empty($category->id)) {
                ?>
            <div class="main-categries-box <?php echo (($j % 4) == 1) ? 'margin-none' : '';?>">
                <a href="<?php echo ROOT_URL?>search?category=<?php echo $category->id;?>&source_catgories=1">
                    <div class="img_div">
                        <?php
                        if (!empty($category->category_image) && file_exists(DIR_UPLOAD_BANNER . $category->category_icon_large)) {
                            echo '<img src="' . DIR_UPLOAD_BANNER_SHOW . $category->category_icon_large . '">';
                        } else {
                            echo '<img src="' . ROOT_URL_BASE . 'images/real-estate.png">';
                        }
                        ?>
                    </div>
                    <p><?php echo $category->title;?></p>
                </a>
                </div><?php
                echo (($j % 4) == 0) ? '</div><div class="row">' : '';
                $j++;
            }
        }?>
        </div><?php
    }?>
    <div class="divider-futered"></div>
</div>
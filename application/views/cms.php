<?php
/**
 * Created by PhpStorm.
 * Author: Anas
 * Date: 4/19/2015
 * Time: 8:11 PM
 */?>

<div class="container ">
    <ul class="bradcram">
        <li><a href="<?php echo ROOT_URL;?>">Home</a></li>
        <li><?php echo (!empty($show404)) ? 'Page not found' :$cmsData->title;?></li>
    </ul>
    <h2 class="page-title"><?php echo (!empty($show404)) ? 'Page not found' :$cmsData->title;?></h2>
    <?php if ($show404 != true) {?>

    <div class="col-md-12">
        <?php echo stripslashes($cmsData->small_description); ?>
    </div>

    <div class="col-md-12">
        <?php echo stripslashes($cmsData->description); ?>
    </div>
    <?php } else {?>
        <div class="form-group">
            <div class="page-not-found">
                <p class="error-code">404</p>
                <p class="error-message">Page not found</p>
                <p class="error-message+"><a href="<?php echo ROOT_URL;?>">Click here to go to home page</a></p>
            </div>
        </div>
    <?php }?>


    <div class="divider-futered"></div>
</div>

<?php
/**
 * Created by PhpStorm.
 * User: Anas
 * Date: 4/24/2015
 * Time: 11:38 AM
 */?>
<div class="container ">
    <ul class="bradcram">
        <li><a href="<?php echo ROOT_URL;?>">Home</a></li>
        <!--<li><a href="#">Used Cars</a></li>-->
        <!--<li><a href="#">Aston Martin</a></li>
        <li>Details- AED <strong>2000</strong></li>-->
    </ul>
    <h2 class="page-title"><?php echo $classifiedDetails->title;?></h2>
    <div class="row item-details-wrap">
        <div class="contact-form" style="width:286px;">
            <span class="search-title margin-none">Advanced Search</span>
            <form method="get" action="<?php echo ROOT_URL.'search'?>" id="searchForm">

                <div class="post-field-col" style="margin-top:10px;">
                    <select name="search_city" id="search_city" onchange="loadLookup(this, this.value, $('#classified_locality'), 'locality');">
                        <option value="">Select City</option>
                        <?php foreach($cityList as $country){ ?>
                            <option value="<?php echo $country->id?>" <?php //echo (!empty($searchCriteria['search_city']) && $searchCriteria['search_city'] == $country->id) ? 'selected="selected"' : '';?>><?php echo $country->combined; ?></option>
                        <?php }?>
                    </select>
                </div>
                <div class="post-field-col" style="margin-top:10px;">
                    <select name="classified_locality" id="classified_locality">
                        <option value="">Select Locality</option>
                    </select>
                </div>
                <div class="post-field-col">
                    <select name="brand_id" id="brand_id" onchange="loadLookup(this, this.value, $('#model_id'), 'model_id');">
                        <option value="">Select Brand</option>
                        <?php foreach($brandList as $brand){ ?>
                            <option value="<?php echo $brand->id?>" <?php echo (!empty($searchCriteria['brand_id']) && $searchCriteria['brand_id'] == $brand->title) ? 'selected="selected"' : '';?> ><?php echo $brand->title; ?></option>
                        <?php }?>
                    </select>
                </div>
                <div class="post-field-col">
                    <select name="model_id" id="model_id">
                        <option value=""> Select Model</option>
                        <?php
                        if(!empty($modelDetails) && is_array($modelDetails) > 0) {
                            foreach($modelDetails as $model){?>
                                <option value="<?php echo $model->id;?>" <?php echo (!empty($searchCriteria['model_id']) && $searchCriteria['model_id'] == $model->title) ? 'selected="selected"' : '';?>><?php echo $model->title;?></option><?php
                            }
                        }?>
                    </select>
                </div>
                <div class="post-field-col">
                    <div class="date-col date-col-month margin-none" style="width:133px;">
                        <input class="validate[required] inputbg" placeholder="Price From" type="text" name="min_amount" id="min_amount">
                    </div>
                    <div class="date-col date-col-month margin-none pull-right" style="width:133px;">
                        <input class="validate[required] inputbg" placeholder="Price To" type="text" name="max_amount" id="max_amount">
                    </div>

                </div>

                <div class="post-field-col">
                    <input placeholder="Keywords" class="validate[required] inputbg" type="text" name="search_keyword" id="search_keyword">
                </div>



                <div class="clerafix"></div>
                <input name="submit" value="Update Search" type="submit" class="submit" style="margin-top:7px; margin-bottom:7px;">

            </form>
        </div>


        <div class="right-content-detils-page">
            <?php
            if(isset($errMsg) && $errMsg != ''){
                echo '<div class="alert alert-danger">' . $errMsg. '</div>';
                unset($errMsg);
            }
            if(isset($succMsg) && $succMsg != ''){
                echo '<div class="alert alert-success">' . $succMsg . '</div>';
                unset($succMsg);
            }?>

            <?php
            $fileExists = false;
            if (!empty($classifiedImages) && is_array($classifiedImages)) {?>
            <div class="fotorama" data-width="100%" data-height="333"><?php
                foreach ($classifiedImages as $image) {
                    if (file_exists(DIR_UPLOAD_CLASSIFIED.$image->classified_image)) {
                        $fileExists = true;?>
                    <a href="<?php echo DIR_UPLOAD_CLASSIFIED_SHOW . $image->classified_image; ?>">
                        <img src="<?php echo DIR_UPLOAD_CLASSIFIED_SHOW . $image->classified_image; ?>" />
                        </a><?php
                    }
                }?>
            </div><?php
            }
            if ($fileExists == false) {?>
                <div class="no-image-wrap">
                    <img src="<?php echo ROOT_URL_BASE.'images/Featured-Ads-01.jpg'; ?>" />
                </div><?php
            }?>

            <ul class="detisl-list">
                <p class="font-17px">Details</p>
                <?php
                if ($classifiedDetails->category_id == CATEG_VEHICLES_ID) {?>
                    <?php echo !empty($classifiedDetails->amount) ? '<li>Price:<span>'.'AED '.number_format($classifiedDetails->amount, 2).'</span></li>' : ''; ?>
                    <?php if (!empty($classifiedDetails->brand)) {?>
                        <li>Make:<span><?php echo $classifiedDetails->brand;?></span></li>
                    <?php }?>
                    <?php if (!empty($classifiedDetails->model)) {?>
                        <li>Model:<span><?php echo $classifiedDetails->model;?></span></li>
                    <?php }?>
                    <?php if (!empty($classifiedDetails->manufacture_year)) {?>
                        <li>Year:<span><?php echo $classifiedDetails->manufacture_year;?></span></li>
                    <?php }?>
                    <?php if (!empty($classifiedDetails->fuel)) {?>
                        <li>Fuel:<span><?php echo $classifiedDetails->fuel;?></span></li>
                    <?php }?>
                    <?php echo !empty($transmissionLookUp[$classifiedDetails->transmission]->title) ? '<li>Transmission:<span>'.$transmissionLookUp[$classifiedDetails->transmission]->title.'</span></li>' : ''; ?>
                    <?php if (!empty($classifiedDetails->running_km)) {?>
                        <li>Kilometers driven:<span><?php echo $classifiedDetails->running_km;?></span></li>
                    <?php }?>
                    <?php if (!empty($classifiedDetails->colour)) {?>
                        <li>Color:<span><?php echo $classifiedDetails->colour;?></span></li>
                    <?php }?>
                    <?php if (!empty($classifiedDetails->no_of_doors)) {?>
                        <li>Doors:<span><?php echo $classifiedDetails->no_of_doors;?></span></li>
                    <?php }?>
                    <?php if (!empty($classifiedDetails->body_type)) {?>
                        <li>Body Condition:<span><?php echo $classifiedDetails->body_type;?></span></li>
                    <?php }?>
                <?php } else if ($classifiedDetails->category_id == CATEG_REAL_EST_ID) {?>
                    <?php echo !empty($classifiedDetails->amount) ? '<li>Price:<span>'.'AED '.number_format($classifiedDetails->amount, 2).'</span></li>' : ''; ?>
                    <?php echo !empty($classifiedDetails->bedrooms) ? '<li>Bed Rooms:<span>'.$classifiedDetails->bedrooms.'</span></li>' : ''; ?>
                    <?php echo !empty($classifiedDetails->bathrooms) ? '<li>Bath Rooms:<span>'.$classifiedDetails->bathrooms.'</span></li>' : ''; ?>
                    <?php echo !empty($classifiedDetails->area_sqft) ? '<li>Area(Sq. Ft.):<span>'.$classifiedDetails->area_sqft.'</span></li>' : ''; ?>
                <?php } else if ($classifiedDetails->category_id == CATEG_JOB_ID) {?>

                    <?php echo !empty($classifiedDetails->amount) ? '<li>Salary:<span>'.'AED '.number_format($classifiedDetails->amount, 2).'</span></li>' : ''; ?>
                    <?php echo !empty($career_levelLookUp[$classifiedDetails->career_level]->title) ? '<li>Career Level:<span>'.$career_levelLookUp[$classifiedDetails->career_level]->title.'</span></li>' : ''; ?>
                    <?php echo !empty($employment_typeLookUp[$classifiedDetails->employment_type]->title) ? '<li>Employment Type:<span>'.$employment_typeLookUp[$classifiedDetails->employment_type]->title.'</span></li>' : ''; ?>
                    <?php echo !empty($classifiedDetails->experience_years) ? '<li>Experience:<span>'.$classifiedDetails->experience_years.' Year(s)</span></li>' : ''; ?>
                    <?php echo !empty($educationLookUp[$classifiedDetails->model_id]->title) ? '<li>Education:<span>'.$educationLookUp[$classifiedDetails->model_id]->title.'</span></li>' : ''; ?>

                <?php } else {?>
                    <?php echo !empty($classifiedDetails->amount) ? '<li>Price:<span>'.'AED '.number_format($classifiedDetails->amount, 2).'</span></li>' : ''; ?>
                <?php } ?>

                <?php echo !empty($classifiedDetails->city) ? ' <li>City:<span><a href="'.ROOT_URL.'search?search_city='.$classifiedDetails->classified_city.'">'.$classifiedDetails->city.'</a></span></li>' : ''; ?>
                <?php echo !empty($classifiedDetails->locality) ? ' <li>Locality:<span><a href="'.ROOT_URL.'search?classified_locality='.$classifiedDetails->classified_locality.'">'.$classifiedDetails->locality.'</a></span></li>' : ''; ?>
                <?php echo !empty($classifiedDetails->classified_land_mark) ? '   --   <a href="javascript:void(0)">'.$classifiedDetails->classified_land_mark.'</a>' : '' ?>


                <?php if (!empty($classifiedDetails->entry_type)) {?>
                    <!--<li>Seller Type:<span><?php /*echo $classifiedDetails->entry_type;*/?></span></li>-->
                <?php }?>

                <?php if (!empty($classifiedFiles) && is_array($classifiedFiles)) {?>
                    <li>Files uploaded:
                        <div class="file_list_wrap">
                            <?php if (!empty($classifiedFiles) && is_array($classifiedFiles)){
                                foreach ($classifiedFiles as $file) {
                                    if (!empty($file->classified_image) && file_exists(DIR_UPLOAD_CLASSIFIED.$file->classified_image)) {?>
                                        <span><?php echo printFileIcon($file->classified_image);?></span><?php
                                    }
                                }
                            }?>
                        </div>
                    </li>
                <?php }?>
            </ul>
            <?php if (!empty($classifiedDetails->description)) {?>
                <div class="inside-content bgnone">
                    <p class="font-17px">Description</p>
                    <p><?php echo $classifiedDetails->description;?></p>
                    </p>
                </div>
            <?php }?>
            <p>&nbsp;</p>

            <div class="contact-form" style="width:644px;">

                <form name="enquiry-form" method="post" action="" id="enquiryForm">
                    <p class="font-17px">REPLY TO THIS AD:</p>
                    <?php echo validation_errors(); ?>
                    <div class="post-field-col">
                        <input placeholder="Name" class="validate[required] inputbg" type="text" name="name" required value="<?php echo !empty($userDetails->first_name) ? $userDetails->first_name : '';?>">
                        <input type="hidden" name="classified_id" value="<?php echo $classifiedDetails->id;?>">
                        <input type="hidden" name="inquiry_member_id" value="<?php echo $inquiry_member_id;?>">
                    </div>

                    <div class="post-field-col pull-right">
                        <input placeholder="Email" class="validate[required] inputbg" type="email" name="email" required value="<?php echo !empty($userDetails->email) ? $userDetails->email : '';?>" >
                    </div>

                    <div class=" col-md-12">
                        <input placeholder="Phone No." class="validate[required] inputbg" type="text" name="phone" >
                    </div>

                    <div class=" col-md-12">
                        <textarea placeholder="Comment" required name="comment"></textarea>
                    </div>
                    <div class=" col-md-12">
                        <div class="g-recaptcha" id="rcaptcha" data-theme="light" data-sitekey="6LdvqQgTAAAAALZGKEdp_Y1xL4j26MN2Bg9Z4j-x"></div>
                        <input type="text" name="validateCaptcha" style="visibility: hidden; height: 0px;">
                    </div>
                    <div class="clerafix"></div>
                    <input name="submit" value="Send Reply" type="submit" class="submit" style="margin-top:7px; margin-bottom:7px;">

                </form>
            </div>
        </div>
    </div>

</div>
<div class="divider-futered"></div>
<link rel="stylesheet" href="<?php echo ROOT_URL_BASE;?>css/fotorama.css">
<script src="<?php echo ROOT_URL_BASE;?>js/fotorama.js"></script>
<script src='https://www.google.com/recaptcha/api.js?hl=en'></script>
<script type="text/javascript" src="<?php echo ROOT_URL_BASE?>js/jquery.validate.min.js"></script>
<script>

    $(document).ready(function(){
        $('#termsAndConditions').click(function(){
            $.colorbox({width:"700", height:"450", html:$('#termsAndConditions-content').html()});
            return false;
        })
        $('#privacyPolicy').click(function(){
            $.colorbox({width:"700", height:"450", html:$('#privacyPolicy-content').html()});
            return false;
        })

        jQuery.validator.addMethod("validPhoneNumber", function(value, element) {
            //$(element).val(value.trim());
            return this.optional(element) || validPhoneNumber(value);
        }, "Invalid phone number.");
        jQuery.validator.addMethod("validateCaptcha", function(value, element) {
            var v = grecaptcha.getResponse();
            if(v.length == 0)
            {
                return false;
            } else {
                return true;
            }
        }, "Please verify you are not a robot");
        $("#enquiryForm").validate({
            rules: {
                email: {
                    required: true,
                    email: true,
                    maxlength: 200
                },
                name:{
                    required: true,
                    maxlength: 100
                },
                phone:{
                    /*required: true,*/
                    maxlength: 15,
                    /*number: true,*/
                    validPhoneNumber: true,
                    minlength: 10
                },
                comment:{
                    required: true,
                    maxlength: 200
                },
                validateCaptcha:{
                    validateCaptcha: true
                }
            },
            messages: {
                email: {
                    required: 'Please select country of advertisement',
                    email: 'Invalid email id',
                    maxlength: 'Maximum length allowed is 200'
                },
                name:{
                    required: 'Please enter your name',
                    maxlength: 'Maximum length allowed is 100 characters'
                },
                phone:{
                    /*required: 'Please enter your phone number',*/
                    maxlength: 'Invalid phone number',
                    /*number: 'Invalid phone number',*/
                },
                comment:{
                    required: 'Please enter your comments',
                    maxlength: 200
                },
                validateCaptcha:{
                    validateCaptcha: 'Please verify you are not a robot'
                }
            }
        });

        $('#showPhoneNumber').click(function(){
            console.log($('#showPhoneNumber').parent().find('span').length);
            $(this).find('span').toggle();
        })
    })
</script>
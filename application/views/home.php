<div class="container ">
    <h1>FEATURED LISTINGS</h1>
    <div class="spreter-div top-margine-15px"></div>


    <!---->
    <div class="clerafix"></div>
    <div class="sub-portfolio">
        <ul id="mycarousel2" class="jcarousel-skin-tango">
            <?php
            if (!empty($featuredClassified) && is_array($featuredClassified)) {
                $i = 0;
                foreach($featuredClassified as $classified) {
                    $i++;?>
                    <li>
                    <a href="<?php echo !empty($classified->classified_slug) ? ROOT_URL.'details/'.$classified->classified_slug : '#';?>">
                        <span class="caption-flex"><?php echo $classified->title ?></span>
                        <?php if($classified->image_list && count($classified->image_list) > 0){?>
                            <?php if(!empty($classified->image_list[0]->classified_image) && $classified->image_list[0]->classified_image!='' && file_exists(DIR_UPLOAD_CLASSIFIED.$classified->image_list[0]->classified_image)) {?>
                                <img class="thumbNailImages" src="<?php echo DIR_UPLOAD_CLASSIFIED_SHOW.$classified->image_list[0]->classified_image; ?>">
                            <?php }else{ ?>
                                <img src="<?php echo ROOT_URL_BASE;?>images/Featured-Ads-01.jpg">
                            <?php }
                        }else{ ?>
                            <img src="<?php echo ROOT_URL_BASE;?>images/Featured-Ads-01.jpg">
                        <?php } ?>
                    </a>
                    </li><?php
                }
            }?>
        </ul>
        <h1>CATEGORIES</h1>
        <div class="spreter-div categories-widht top-margine-15px"></div>
        <?php if (!empty($categoryList['mainCategory']) && is_array($categoryList['mainCategory'])){?>
            <div class="row"><?php
            $j = 1;
            $subCategory = !empty($categoryList['subCategory']) ? $categoryList['subCategory'] : array();
            $totalCount = count($categoryList['mainCategory']);
            /*echo "<pre>";
            print_r($categoryList['mainCategory']);*/
            function sortByOption($a, $b) {
                return strcmp($a->category_order, $b->category_order);
            }
            usort($categoryList['mainCategory'], 'sortByOption');

            /*print_r($categoryList['mainCategory']);
            echo "</pre>";*/
            foreach ($categoryList['mainCategory'] as $category) {
                if (!empty($category->id)) { ?>
                <div class="col-md-4 categories-wrap <?php echo (($j % 2) == 0) ? 'left-right-margine-30px' : ''; ?>">
                    <div class="icon-box">
                        <a href="<?php echo ROOT_URL . 'search?category=' . $category->id; ?>">
                            <?php
                            if (!empty($category->category_image) && file_exists(DIR_UPLOAD_BANNER . $category->category_image)) {
                                echo '<img src="' . DIR_UPLOAD_BANNER_SHOW . $category->category_image . '">';
                            } else {
                                echo '<img src="' . ROOT_URL_BASE . 'images/real-estate.png">';
                            }
                            ?>
                        </a>
                    </div>
                    <div class="table-box1">
                        <a href="<?php echo ROOT_URL . 'search?category=' . $category->id; ?>">
                            <h2><?php echo $category->title; ?></h2></a>

                        <h2 class="pull-right"><?php echo !empty($category->classifiedsCount) ? $category->classifiedsCount : 0; ?></h2><?php
                        if (!empty($subCategory[$category->id]) && is_array($subCategory[$category->id])) { ?>
                            <ul><?php
                            $subCategCount = 1;
                            foreach ($subCategory[$category->id] as $subCateg) { ?>
                            <li class="<?php echo ($subCategCount > 5) ? 'hidden-categ-list' : ''; ?>"><a
                                    href="<?php echo ROOT_URL . 'search?category=' . $category->id . '&sub_category=' . $subCateg->id; ?>"><?php echo $subCateg->title; ?>
                                    <span><?php echo !empty($subCateg->classifiedsCount) ? $subCateg->classifiedsCount : 0; ?></span></a>
                                </li><?php
                                $subCategCount++;
                            }
                            if ($subCategCount > 6) {
                                echo '<li class="categ-read-more"><a href="javascript:void(0)">Read more &raquo;&raquo;</a></li>';
                            } else {
                                for ($subCategCount; $subCategCount < 6; $subCategCount++) {
                                    echo '<li class="nodata"><a href="javascript:void(0)">&nbsp;</a></li>';
                                }
                                echo '<li class="nodata categ-read-more"><a href="javascript:void(0)">&nbsp;</a></li>';
                            } ?>
                            </ul><?php
                        } else {
                            echo '<div class="home-categ-nodata"> No Sub categories found</div>';
                        } ?>
                    </div>
                    </div><?php
                    echo (($j % 2) == 0) ? '<div class="add-box"><a href="#"><img src="' . ROOT_URL_BASE . 'images/ajax-loader.gif" ></a></div></div><div class="row">' : '';
                    $j++;
                }
            }?>
            </div><?php
        }?>
    </div>
</div><!-- container -->

<div class="container-fuld">
    <div class="container">
        <div class="tabs-box1">
            <ul class="tabs">
                <li class="tabs1"><a href="#tabs1">LATEST ADS</a></li>
                <li><a href="#tabs2">MOST POPULAR ADS</a></li>
                <li><a href="#tabs3">RANDOM ADS</a></li>
            </ul>
        </div>

        <div class="row">
            <div id="tabs1" class="tab_content 111111111111111">
                <div class="col-md-12"><?php
                    //print_r($latestClassified);
                    if(!empty($latestClassified) && is_array($latestClassified)){
                        $k = 1;
                        foreach ($latestClassified as $classified) {
                            if($k%4 == 1){ $boxClass = 'first-marngin-none';}?>
                            <div class="product-box <?php echo ($k%4 == 1) ? 'first-marngin-none' : '';?> ">
                                <a href="<?php echo !empty($classified->classified_slug) ? ROOT_URL.'details/'.$classified->classified_slug : '#';?>">
                                    <span class="caption-flex"><?php echo $classified->title ?></span>
                                    <?php if($classified->image_list && count($classified->image_list) > 0){?>
                                        <?php if(!empty($classified->image_list[0]->classified_image) && $classified->image_list[0]->classified_image!='' && file_exists(DIR_UPLOAD_CLASSIFIED.$classified->image_list[0]->classified_image)) {?>
                                            <img class="thumbNailImages" src="<?php echo DIR_UPLOAD_CLASSIFIED_SHOW.$classified->image_list[0]->classified_image; ?>">
                                        <?php }else{ ?>
                                            <img src="<?php echo ROOT_URL_BASE;?>images/Featured-Ads-01.jpg">
                                        <?php }
                                    }else{ ?>
                                        <img src="<?php echo ROOT_URL_BASE;?>images/Featured-Ads-01.jpg">
                                    <?php } ?>
                                </a>
                            </div><?php
                            echo ( ($k%4) == 0 ) ? '</div><div class="col-md-12">' : '';
                            $k++;
                        }
                    }?>
                </div>
            </div>

            <div id="tabs2" class="tab_content 222222222222">
                <div class="col-md-12"><?php
                    //print_r($popularClassified);
                    if(!empty($popularClassified) && is_array($popularClassified)){
                        $k = 1;
                        foreach ($popularClassified as $classified) {
                            if($k%4 == 1){ $boxClass = 'first-marngin-none';}?>
                        <div class="product-box <?php echo ($k%4 == 1) ? 'first-marngin-none' : '';?> ">
                            <a href="<?php echo !empty($classified->classified_slug) ? ROOT_URL.'details/'.$classified->classified_slug : '#';?>">
                                <span class="caption-flex"><?php echo $classified->title ?></span>
                                <?php if($classified->image_list && count($classified->image_list) > 0){?>
                                    <?php if(!empty($classified->image_list[0]->classified_image) && $classified->image_list[0]->classified_image!='' && file_exists(DIR_UPLOAD_CLASSIFIED.$classified->image_list[0]->classified_image)) {?>
                                        <img class="thumbNailImages" src="<?php echo DIR_UPLOAD_CLASSIFIED_SHOW.$classified->image_list[0]->classified_image; ?>">
                                    <?php }else{ ?>
                                        <img src="<?php echo ROOT_URL_BASE;?>images/Featured-Ads-01.jpg">
                                    <?php }
                                }else{ ?>
                                    <img src="<?php echo ROOT_URL_BASE;?>images/Featured-Ads-01.jpg">
                                <?php } ?>
                            </a>
                            </div><?php
                            echo ( ($k%4) == 0 ) ? '</div><div class="col-md-12">' : '';
                            $k++;
                        }
                    }?>
                </div>
            </div>

            <div id="tabs3" class="tab_content 3333333333">
                <div class="col-md-12"><?php
                    //print_r($randomClassified);
                    if(!empty($randomClassified) && is_array($randomClassified)){
                        $k = 1;
                        foreach ($randomClassified as $classified) {
                            if($k%4 == 1){ $boxClass = 'first-marngin-none';}?>
                        <div class="product-box <?php echo ($k%4 == 1) ? 'first-marngin-none' : '';?> ">
                            <a href="<?php echo !empty($classified->classified_slug) ? ROOT_URL.'details/'.$classified->classified_slug : '#';?>">
                                <span class="caption-flex"><?php echo $classified->title ?></span>
                                <?php if($classified->image_list && count($classified->image_list) > 0){?>
                                    <?php if(!empty($classified->image_list[0]->classified_image) && $classified->image_list[0]->classified_image!='' && file_exists(DIR_UPLOAD_CLASSIFIED.$classified->image_list[0]->classified_image)) {?>
                                        <img class="thumbNailImages" src="<?php echo DIR_UPLOAD_CLASSIFIED_SHOW.$classified->image_list[0]->classified_image; ?>">
                                    <?php }else{ ?>
                                        <img src="<?php echo ROOT_URL_BASE;?>images/Featured-Ads-01.jpg">
                                    <?php }
                                }else{ ?>
                                    <img src="<?php echo ROOT_URL_BASE;?>images/Featured-Ads-01.jpg">
                                <?php } ?>
                            </a>
                            </div><?php
                            echo ( ($k%4) == 0 ) ? '</div><div class="col-md-12">' : '';
                            $k++;
                        }
                    }?>
                </div>

            </div>
        </div>
        <div class="clerafix"></div>
    </div>
</div> <!-- container-fuld -->
<div class="clerafix"></div>
<script language="javascript" type="text/javascript">
    $(document).ready(function(){
        <?php if(!empty($advertize)) {
            $i = 0;
            foreach ($advertize as $item) {
                if (file_exists(DIR_UPLOAD_ADVERTIZE.$item->image_path)) {
                echo "\n\t$('.add-box').eq(".$i.").html('<a href=\"#\"><img src=\"".DIR_UPLOAD_ADVERTIZE_SHOW.$item->image_path."\" width=\"300\" height=\"250\"></a>');\n";
                $i++;
                }
            }
        }?>
    })
</script>

   <!-- <script language="javascript" type="text/javascript">
        function getModelList(brand_name){
            $.ajax({
                type: "POST",
                url: "home/getmodel",
                data: "brand_name="+brand_name,
                success: function(result){
                    $('#model_select').html(result);
                },
                complete: function(){

                },
                error: function(){
                    $('#detail_inner').html('Error occured. Please try later');
                }
            });
        }
    </script>-->

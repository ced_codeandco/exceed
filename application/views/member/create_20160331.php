<div class="container ">
    <ul class="bradcram">
        <li><a href="<?php echo ROOT_URL;?>">Home</a></li>
        <li>Post an Ad</li>
    </ul>
    <h2 class="page-title">Post an Ad</h2>
    <form class="register-form" name="createClassified" method="post" id="createClassified" enctype="multipart/form-data">
        <input type="hidden" name="created_by" value="<?php echo $user_id;?>" />
    <div class="row">
        <div class="blog-box-right">
            <div class="right-box-post-ad">
                <p class="lead-right-para">Why must you post<br />
                    your ad here?</p>
                <p ><a href="#">Over 70,00,000+ users</a></p>
                <p><a href="#">Access via mobile also</a></p>
                <p><a href="#" class="border-none">Push your ads to top (FREE)</a></p>

            </div>
            <br />
            <br />
            <br />
        <!--</div>

        <div class="blog-box-right">-->
            <div class="right-box-post-ad">
                <p class="magine-top-60px">Please do not post duplicate ads.<br />
                    All duplicate and <br />
                    spam ads will be deleted.</p>
                <p>By posting your ad, you agree to<br />
                    <a href="#" class="righ-box-site-name"><?php echo SITE_LINK_TEXT;?></a> <br />
                    <a href="<?php echo ROOT_URL.'cms/'.$terms_popup_content->cms_slug.'/ajax';?>" class="terms-link ajax-popup">terms of use agreement.</a></p>

            </div>
        </div>



        <div class="post-left-box">
            <div class="contact-form form-titles-bold">
                <?php  echo validation_errors();?>
                    <div class="post-field-col">
                        <label>Category</label>
                        <select name="category_id" id="category" class="form-control lookUpPreloader" onchange="create_loadCriteriaLookup(this, this.value, $('#sub_category_id'), 'subCategory', 'loadTertiaryOnCreate');">
                            <option value="">Select</option>
                            <?php if (!empty($parentCategoriesList) && is_array($parentCategoriesList)){
                                foreach ($parentCategoriesList as $item) {
                                    $selected = (!empty($_REQUEST['category']) && $item->id == $_REQUEST['category']) ? 'selected="selected"' : '';
                                    echo '<option '.$selected.' value="'.$item->id.'">'.$item->title.'</option>';
                                }
                            }?>
                        </select>
                    </div>
                    <div class="post-field-col pull-right">
                        <label>Sub Category</label><?php //print_r($subCategory);?>
                        <select name="sub_category_id" id="sub_category_id" class="form-control" onchange="loadLookup(this, this.value, $('#tertiary_category_id'), 'subCategory');">
                            <option value="">Select</option>
                            <?php if (!empty($subCategory) && is_array($subCategory)){
                                foreach ($subCategory as $key => $val) {
                                    $selected = (!empty($_REQUEST['sub_category_id']) && $key == $_REQUEST['sub_category_id']) ? 'selected="selected"' : '';
                                    echo '<option '.$selected.' value="'.$key.'">'.$val.'</option>';
                                }
                            }?>
                        </select>
                    </div>
                    <div class="post-field-col">
                        <label>Sub Category</label><?php //print_r($subCategory);?>
                        <select name="tertiary_category_id" id="tertiary_category_id" class="form-control">
                            <option value="">Select</option>
                            <?php if (!empty($tertiaryCategory) && is_array($tertiaryCategory)){
                                foreach ($tertiaryCategory as $key => $val) {
                                    $selected = (!empty($_REQUEST['tertiary_category_id']) && $key == $_REQUEST['tertiary_category_id']) ? 'selected="selected"' : '';
                                    echo '<option '.$selected.' value="'.$key.'">'.$val.'</option>';
                                }
                            }?>
                        </select>
                    </div>
                    <div class="clerafix"></div>
                    <div class="post-field-col">
                        <label>City</label>
                        <select  name="classified_city" id="country" class="form-control lookUpPreloader" onchange="loadLookup(this, this.value, $('#locality'), 'locality');">
                            <option value="">Select</option>
                            <?php if (is_array($cityList)) {
                                foreach ($cityList as $city) {
                                    echo '<option value="' .$city->id. '" >' .$city->combined. '</option>';
                                }
                            }?>
                        </select>
                    </div>
                    <div class="post-field-col pull-right">
                        <label>Locality</label>
                        <select  name="classified_locality" id="locality" class="form-control">
                            <option value="">Select</option>
                            <?php if (!empty($locality) && is_array($locality)){
                                foreach ($locality as $key => $val) {
                                    $selected = (!empty($_REQUEST['classified_locality']) && $key == $_REQUEST['classified_locality']) ? 'selected="selected"' : '';
                                    echo '<option '.$selected.' value="'.$key.'">'.$val.'</option>';
                                }
                            }?>
                        </select>
                    </div>
                    <div class="spreter-hr"></div>
                <div id="category_based-filter-wrap_common">
                    <div class="post-field-col-4">
                        <label>Price</label>
                        <input value="" placeholder="Amount" class="validate[required] inputbg" type="text" name="amount" id="amount" />
                    </div>
                </div>
                <div id="category_based-filter-wrap_<?php echo CATEG_JOB_ID;?>">
                    <!--- JONS---->
                    <div class="post-field-col-4">
                        <label>Salary</label>
                        <input value="" placeholder="Amount" class="validate[required] inputbg" type="text" name="amount" id="amount" />
                    </div>
                    <div class="clerafix"></div>
                    <div class="post-field-col-4 ">
                        <label>Experience</label>
                        <input value="" class="validate[required] inputbg" type="number" name="experience_years" id="experience_years" placeholder="Experience in years" />
                    </div>

                    <div class="post-field-col-4 left-right-margine-post-field">
                        <label>Career Level  </label>
                        <select name="career_level">
                            <option value="">Select</option>
                            <?php if (!empty($career_levelLookUp) && is_array($career_levelLookUp)) {
                                foreach ($career_levelLookUp as $item) {?>
                                    <option value="<?php echo $item->id;?>"><?php echo $item->title;?></option>
                                <?php }
                            } ?>
                        </select>
                    </div>

                    <div class="post-field-col-4">
                        <label>Education  </label>
                        <select name="education">
                            <option value="">Select</option>
                            <?php if (!empty($educationLookUp) && is_array($educationLookUp)) {
                                foreach ($educationLookUp as $item) {?>
                                    <option value="<?php echo $item->id;?>"><?php echo $item->title;?></option>
                                <?php }
                            } ?>
                        </select>
                    </div>
                    <div class="clearfix"></div>
                    <div class="post-field-col-4">
                        <label>Employment Type</label>
                        <select name="employment_type">
                            <option>Select</option>
                            <?php if (!empty($employment_typeLookUp) && is_array($employment_typeLookUp)) {
                                foreach ($employment_typeLookUp as $item) {?>
                                    <option value="<?php echo $item->id;?>"><?php echo $item->title;?></option>
                                <?php }
                            } ?>
                        </select>
                    </div>


                </div>
                <div id="category_based-filter-wrap_<?php echo CATEG_REAL_EST_ID;?>">
                    <!--- JONS---->
                    <div class="post-field-col-4">
                        <label>Price</label>
                        <input value="" placeholder="Amount" class="validate[required] inputbg" type="text" name="amount" id="amount" />
                    </div>
                    <div class="clerafix"></div>
                    <div class="post-field-col-4 ">
                        <label>Bed rooms</label>
                        <input value="" class="validate[required] inputbg" type="number" name="bedrooms" id="bedrooms" placeholder="Number of Bed rooms" />
                    </div>

                    <div class="post-field-col-4 left-right-margine-post-field">
                        <label>Bath rooms</label>
                        <input value="" class="validate[required] inputbg" type="number" name="bathrooms" id="bathrooms" placeholder="Number of Bath rooms" />
                    </div>

                    <div class="post-field-col-4">
                        <label>Area</label>
                        <input value="" class="validate[required] inputbg" type="number" name="area_sqft" id="area_sqft" placeholder="Area in Square feet" />
                    </div>
                    <div class="clearfix"></div>


                </div>
                <div id="category_based-filter-wrap_<?php echo CATEG_VEHICLES_ID;?>">
                        <div class="post-field-col-4">
                            <label>Price</label>
                            <input value="" placeholder="Amount" class="validate[required] inputbg" type="text" name="amount" id="amount" />
                        </div>
                        <div class="clerafix"></div>
                        <div class="post-field-col-4">
                            <label>Brand</label>
                            <select name="brand_id" class="lookUpPreloader" onchange="loadLookup(this, this.value, $('#model_id'), 'model_id');">
                                <option value="">Select</option>
                                <?php if (!empty($brandList) && is_array($brandList)) {
                                    foreach ($brandList as $brand) {?>
                                        <option value="<?php echo $brand->id;?>"><?php echo $brand->title;?></option>
                                    <?php }
                                } ?>
                            </select>
                        </div>

                        <div class="post-field-col-4 left-right-margine-post-field">
                            <label>Model</label>
                            <select name="model_id" id="model_id">
                                <option value="">Select</option>
                                <?php if (!empty($model) && is_array($model)){
                                    foreach ($model as $key => $val) {
                                        $selected = (!empty($_REQUEST['model_id']) && $key == $_REQUEST['model_id']) ? 'selected="selected"' : '';
                                        echo '<option '.$selected.' value="'.$key.'">'.$val.'</option>';
                                    }
                                }?>
                            </select>
                        </div>
                        <div class="post-field-col-4">
                            <label>Year</label>
                            <select name="manufacture_year">
                                <option value="">Select</option>
                                <?php for ($i = date('Y'); $i >= MANUFACTURE_YEAR_START; $i --){
                                    echo '<option>'.$i.'</option>';
                                } ?>
                            </select>
                        </div>
                        <div class="post-field-col-4">
                            <label>Fuel Type</label>
                            <select name="fuel_type">
                                <option>Select</option>
                                <?php if (!empty($fuelTypeList) && is_array($fuelTypeList)) {
                                    foreach ($fuelTypeList as $item) {?>
                                        <option value="<?php echo $item->id;?>"><?php echo $item->title;?></option>
                                    <?php }
                                } ?>
                            </select>
                        </div>
                        <div class="post-field-col-4 left-right-margine-post-field">
                            <label>Transmission</label>
                            <select name="transmission">
                                <option>Select</option>
                                <?php if (!empty($transmissionLookUp) && is_array($transmissionLookUp)) {
                                    foreach ($transmissionLookUp as $item) {?>
                                        <option value="<?php echo $item->id;?>"><?php echo $item->title;?></option>
                                    <?php }
                                } ?>
                            </select>
                        </div>

                        <div class="post-field-col-4 ">
                            <label>KM's driven</label>
                            <input value="" class="validate[required] inputbg" type="text" name="running_km" id="running_km" placeholder="Kilometers driven" />
                        </div>
                    </div>
                    <div class="clerafix"></div>
                    <div class="post-field-col-12">
                        <label>Title</label>
                        <input value="" class="validate[required] inputbg" type="text" name="title" id="title" />
                    </div>
                    <div class="post-field-col-12">
                        <label>Description</label>
                        <div class="clerafix"></div>
                        <textarea style="width:100%" name="description"><?php echo !empty($classifiedDetails['description']) ? stripslashes($classifiedDetails['description']) : ''; unset($classifiedDetails['description']);?></textarea>
                    </div>


            </div>
        </div>
    </div>
        <div id="category_based-file-wrap_<?php echo CATEG_JOB_WANTED_ID;?>">
            <div class="col-md-12">
                <div class="preview-images-wrap">

                    <?php if (!empty($uploadedImages)) {
                        $counter = 1;
                        foreach($uploadedImages as $images) {

                            if (file_exists(DIR_UPLOAD_CLASSIFIED . $images->classified_image)) { ?>

                                <div class="preview-images file">

                                <button class="removeImages" imageId="<?php echo $images->id?>" title="Remove this file">&nbsp;</button>

                                <a href="<?php echo DIR_UPLOAD_CLASSIFIED_SHOW . $images->classified_image; ?>" target="_blank">Uploaded file <?php echo $counter;?></a>

                                </div><?php
                                $counter++;
                            }

                        }?>
                        <div class="clerafix"></div>
                    <?php }?>

                </div>
                <div class="post-left-box">

                    <div class="contact-form">
                        <label>&nbsp;</label>
                        <div class="file-uplopader-row">
                            <div class="file-selector-wrap">
                                <div class="post-field-col-4">
                                    <p class="form">
                                        <input type="text" id="path" />
                                        <label class="add-photo-btn">Select File
                                            <span>
                                               <input type="file" id="myfile" name="myfile[0]" />
                                            </span>
                                        </label>
                                    </p>
                                </div>
                                <div class="post-field-col-4"><input type="text" readonly id="pathInput"></div>

                                <div class="clerafix"></div>
                            </div>

                            <p class="font12px"> Max size: 3072 KB (Only doc, docx, pdf format allowed).</p>
                            <div class="clerafix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="category_based-file-wrap_common">
            <div class="col-md-12">
                <div class="preview-images-wrap">

                    <?php if (!empty($uploadedImages)) {

                        foreach($uploadedImages as $images) {

                            if (file_exists(DIR_UPLOAD_CLASSIFIED . $images->classified_image)) { ?>

                                <div class="preview-images">

                                <button class="removeImages" imageId="<?php echo $images->id?>" title="Remove this image">&nbsp;</button>

                                <img src="<?php echo ROOT_URL_BASE ?>assets/timthumb.php?src=<?php echo DIR_UPLOAD_CLASSIFIED_SHOW . $images->classified_image; ?>&q=100&w=100" />

                                </div><?php

                            }

                        }?>
                        <div class="clerafix"></div>
                    <?php }?>

                </div>
                <div class="post-left-box">

                    <div class="contact-form">
                        <label>&nbsp;</label>
                        <div class="file-uplopader-row">
                            <div class="file-selector-wrap">
                                <div class="post-field-col-4">
                                    <p class="form">
                                        <input type="text" id="path" />
                                        <label class="add-photo-btn">Select Photo
                                            <span>
                                               <input type="file" id="myfile" name="myfile[0]" />
                                            </span>
                                        </label>
                                    </p>
                                </div>
                                <div class="post-field-col-4"><input type="text" readonly id="pathInput"></div>
                                <div class="post-field-col-4"><input type="button" value="+" class="addFileSecetor" id="addFileSecetorid" onclick="addfileinputcloned(this)"></div>
                                <div class="clerafix"></div>
                            </div>


                            <p class="font12px"> Max size: 3072 KB per image (Only jpeg, jpg, png format and upto 5 images allowed).</p>
                            <div class="clerafix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

            <div class="col-md-12">
                <div class="post-left-box">
                    <div class="contact-form">
                        <div class="post-field-col">
                            <label>Mobile</label>
                            <input value="+1868" class="validate[required] inputbg" type="text" name="classified_contact_no" id="classified_contact_no" />
                        </div>
                        <div class="clerafix"></div>
                        <p class="font12px">10 digit mobile no. with +1 868 or 0</p>


                        <div class="post-field-col-12">
                            <label></label>
                        </div>
                        <input value="Post my ad" type="submit"  class="submit">

                    </div>
                </div>


        </div>
    </form>
</div>
<div class="divider-futered"></div>

<script type="text/javascript" src="<?php echo ROOT_URL_BASE?>js/jquery.validate.min.js"></script>

    <script language="javascript" type="text/javascript">
    function getModelList(brand_name){
        $.ajax({
            type: "POST",
            url: "home/getmodel",
            data: "brand_name="+brand_name,
            success: function(result){
                $('#model_select').html(result);
            },
            complete: function(){

            },
            error: function(){
                $('#detail_inner').html('Error occured. Please try later');
            }
        });
    }
    $(document).ready(function(){
        jQuery.validator.addMethod("validPhoneNumber", function(value, element) {
            //$(element).val(value.trim());
            return this.optional(element) || validDubaiPhoneNumber(value);
        }, "Invalid phone number");
        $("#createClassified").validate({
            rules: {
                category_id:{
                    required: true,
                },
                sub_category_id:{
                    required: true,
                },
                classified_city:{
                    required: true,
                    maxlength: 200
                },
                title:{
                    required: true,
                    maxlength: 200
                },
                description:{
                    minlength: 3
                },
                amount:{
                    required: true,
                    maxlength: 7,
                    number: true
                },
                manufacture_year:{
                    number: true
                },
                running_km:{
                    number: true
                },
                classified_address:{
                    maxlength: 200
                },
                classified_contact_no:{
                    required: true,
                    maxlength: 15,
                    validPhoneNumber: true,
                    minlength: 10
                }
            },
            messages: {
                classified_city:{
                    required: 'Please enter your city',
                    maxlength: 'Maximum length allowed is 100 characters'
                },
                category_id:{
                    required: 'Please select a category'
                },
                sub_category_id:{
                    required: 'Please select a sub category'
                },
                title:{
                    required: 'Please enter a title',
                    maxlength: 'Maximum length allowed is 200'
                },
                description:{
                    minlength: 'Description should be at least 3 characters long',
                },
                amount:{
                    required: 'Please enter an amount',
                    maxlength: 'Maximum digits allowed is 7',
                    number: 'Please enter a valid amount'
                },
                manufacture_year:{
                    number: 'Manufacture year should be numeric'
                },
                running_km:{
                    number: 'Invalid number'
                },
                classified_address:{
                    maxlength: 'Maximum length allowed is 200'
                },
                classified_contact_no:{
                    required: 'Contact number is required',
                    maxlength: 'Invalid phone number',
                    validPhoneNumber: 'Invalid phone number'
                }
            }
        });
    })
    <?php
    $classifiedDetails = (array) $classifiedDetails;
    if(!empty($classifiedDetails) && is_array($classifiedDetails)) {?>
    $(document).ready(function(){
        <?php echo build_filldata_javascript($classifiedDetails);?>
    })
    <?php }?>
    $('#myfile').live('change', function(){
        if(document.getElementById('myfile').files.length > 1) {
            $('#pathInput').val(document.getElementById('myfile').files.length + ' files selected');
        } else {
            $('#pathInput').val('1 file selected');
        }
    });
    $('#myfile1').live('change', function(){
        if(document.getElementById('myfile1').files.length > 1) {
            $('#pathInput1').val(document.getElementById('myfile1').files.length + ' files selected');
        } else {
            $('#pathInput1').val('1 file selected');
        }
    });
    $('#myfile2').live('change', function(){
        if(document.getElementById('myfile2').files.length > 1) {
            $('#pathInput2').val(document.getElementById('myfile2').files.length + ' files selected');
        } else {
            $('#pathInput2').val('1 file selected');
        }
    });
    $('#myfile3').live('change', function(){
        if(document.getElementById('myfile3').files.length > 1) {
            $('#pathInput3').val(document.getElementById('myfile3').files.length + ' files selected');
        } else {
            $('#pathInput3').val('1 file selected');
        }
    });
    $('#myfile4').live('change', function(){
        if(document.getElementById('myfile4').files.length > 1) {
            $('#pathInput4').val(document.getElementById('myfile4').files.length + ' files selected');
        } else {
            $('#pathInput4').val('1 file selected');
        }
    });
    </script>
    <script src="http://tinymce.cachefly.net/4.1/tinymce.min.js" language="javascript" type="text/javascript"></script>
    <script type="text/javascript">
        tinymce.init({
            selector: "textarea",
            plugins: [
                "advlist autolink lists link image charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
        });
    </script>
<link rel="stylesheet" href="<?php echo ROOT_URL_BASE?>css/post-add.js.css" />
<script>
    $(document).ready(function () {
        var maxFiles = 5;
        maxFiles =  maxFiles - $('.preview-images').length;
        var category = $('#category').val();
        create_manage_lookUpfields(category);
        var sub_category = $('#sub_category_id').val();
        create_manage_fileInputFields(sub_category);

        $('.removeImages').click(function(){
            var imageId = $(this).attr('imageId');
            if (confirm("Do you really want to remove this image ?")) {
                $('.preview-images-wrap').append('<input type="hidden" name="removeImageId[]" id="removeImageIdHidden" value="' + imageId + '">');
                $(this).parent().remove();
                if ($('.preview-images').length < 5) {
                    $('.file-uplopader-row').show();
                    $('#addFileSecetorid').show();
                }
            }
            return false;
        })
        if ($('.preview-images').length >= 5) {
            $('.file-uplopader-row').hide();
        }
    })
</script>
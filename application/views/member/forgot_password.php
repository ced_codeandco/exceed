<div class="container ">
    <ul class="bradcram">
        <li><a href="<?php echo ROOT_URL;?>">Home</a></li>
        <li>Forgot password</li>
    </ul>
    <h2 class="page-title">Welcome to Exceed </h2>
    <div class="row"><?php
        if(isset($errMsg) && $errMsg != ''){ ?>
            <div class="alert alert-danger">
                <?php echo $errMsg;?>
            </div>
            <?php unset($errMsg);
        }
        if(isset($succMsg) && $succMsg != ''){ ?>
            <div class="alert alert-success">
                <?php echo $succMsg;?>
            </div>
            <?php unset($succMsg);
        }
        echo validation_errors();
        $attributes = array('name' => 'loginForm', 'id' => 'loginForm', 'enctype' => 'multipart/form-data');
        echo form_open(MEMBER_ROOT_URL.'forgot_password',$attributes); ?>

        <div class="col-md-12">
            <div class="contact-form accountsetting-form">

                    <div class="post-field-col ">
                        <input value="" class="validate[required]" type="email" name="email" placeholder="Enter your Email Address">
                    </div>

                    <div class="post-field-col left-margine-20px-rorm">
                        <input name="submit" value="Reset Password" type="submit" class="submit change-name">
                    </div>


            </div>
        </div>
        </form>

    </div>


    <div class="divider-futered"></div>
</div>
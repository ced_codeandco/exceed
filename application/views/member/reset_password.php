<div class="container ">
    <ul class="bradcram">
        <li><a href="<?php echo ROOT_URL;?>">Home</a></li>
        <li>Forgot password</li>
    </ul>
    <h2 class="page-title">Welcome to Exceed </h2>
    <div class="row"><?php
        if(isset($errMsg) && $errMsg != ''){ ?>
            <div class="alert alert-danger">
                <?php echo $errMsg;?>
            </div>
            <?php unset($errMsg);
        }
        if(isset($succMsg) && $succMsg != ''){ ?>
            <div class="alert alert-success">
                <?php echo $succMsg;?>
            </div>
            <?php unset($succMsg);
        }

        if (!empty($validToken)) {
            $attributes = array('name' => 'loginForm', 'id' => 'loginForm', 'enctype' => 'multipart/form-data', 'onsubmit' => 'return validate_admin();');
            echo form_open(MEMBER_ROOT_URL . 'reset_password/' . $password_token, $attributes); ?>

            <div class="col-md-12">
                <div class="contact-form accountsetting-form">
                        <div class="post-field-col col-md-4-form">
                            <input type="password" required="required" name="password" id="password" placeholder="Enter New Password">
                        </div>
                        <div class="post-field-col  col-md-4-form left-margine-20px-rorm">
                            <input type="password" required="required" name="retype_password" id="retype_password" placeholder="Password Again">
                        </div>
                        <div class="post-field-col col-md-4-form left-margine-20px-rorm">
                            <input name="submit" value="Change Password" type="submit" class="submit change-name">
                        </div>
                </div>
            </div>
        </form><?php
        } else {?>
            <a href="<?php echo MEMBER_ROOT_URL?>forgot_password">Forgot Password?</a> <br />
        <?php }        ?>


    </div>


    <div class="divider-futered"></div>
</div>
<div class="container ">
    <ul class="bradcram">
        <li><a href="<?php echo ROOT_URL;?>">Home</a></li>
        <li><a href="#">My Profile</a></li>
    </ul>
    <h2 class="page-title">My Profile</h2>
    <div class="row">

        <div class="col-md-12">

            <?php $this->load->view('templates/member_tab', $tabData);?>
            <div class="clearfix"></div>
            <!--<p class="succesfull-para">Your ad placed successfully!!</p>-->
            <?php
            if(isset($errMsg) && $errMsg != ''){ ?>
                <div class="alert alert-danger">
                    <?php echo $errMsg;?>
                </div>
                <?php unset($errMsg);
            }
            if(isset($succMsg) && $succMsg != ''){ ?>
                <div class="alert alert-success">
                    <?php echo $succMsg;?>
                </div>
                <?php unset($succMsg);
            }?>



            <table class="bs-example">
                <?php
                if (empty($classifiedList) OR !is_array($classifiedList)) {?>
                    <div class="col-xs-12">
                    <div class="alert alert-danger">You have not placed any adds.</div>
                    <p>
                        <a href="<?php echo ROOT_URL?>create_classified"><button type="submit" class="place-add-btn">Place an ad for free</button></a>
                    </p>
                    </div><?php
                } else {
                    ?>
                    <tr>
                        <th class="border-none table-col-1">No</th>
                        <th class="table-col-2">Title</th>
                        <th class="table-col-3">Category</th>
                        <th class="table-col-4">Creted Date</th>
                        <th class="table-col-6">Number of hits</th>
                        <th class="table-col-5">Status</th>
                        <th class="table-col-6">Action</th>
                    </tr>
                    <?php

                    $i = 0;
                    $paOrder = !empty($recordCountStart) ? $recordCountStart : 1;
                    foreach ($classifiedList as $classified) {//classified_slug ?>
                        <tr>
                            <td class="border-none table-col-1"><?php echo $paOrder; ?></td>
                            <td class="table-col-2"> <a href="<?php echo ROOT_URL?>details/<?php echo $classified->classified_slug?>" ><?php echo $classified->title;  ?></a></td>
                            <td class="table-col-3"><?php echo $classified->category_name;?></td>
                            <td class="table-col-4"><?php echo date('d M Y H:i', strtotime($classified->created_date_time));?></td>
                            <td class="table-col-6"><?php echo !empty($classified->number_of_hits) ? $classified->number_of_hits : 0;  ?></td>
                            <td class="table-col-5">
                            <?php if($classified->is_active=='1'){?>
                                <span class="in-active">Active</span>
                            <?php }else{?>
                                <span class="in-active">Inactive</span>
                            <?php }?>
                                </span>
                            </td>
                            <td class="table-col-6">
                                <span class="in-active bg-none" >
                                    <a class="in-active" href="<?php echo ROOT_URL?>create_classified/<?php echo $classified->id?>">Edit</a>
                                    <a href="#" onclick="javascript:if(confirm('Are you sure to delete ? ')){location.href='<?php echo MEMBER_ROOT_URL?>dashboard/delete_classifieds/<?php echo $classified->id?>'}" class="active">Delete</a>
                                </span>
                            </td>
                        </tr>
                    <?php
                        $paOrder++;
                    }
                }?>

            </table>

            <div class="pagenation">
                <?php echo $paginator;?>
            </div>

        </div>


    </div>


    <div class="divider-futered"></div>
</div>
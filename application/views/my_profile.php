<div class="container ">
    <ul class="bradcram">
        <li><a href="<?php echo ROOT_URL;?>">Home</a></li>
        <li><a href="">My Profile</a></li>
    </ul>
    <h2 class="page-title">My Profile</h2>
    <div class="row">

        <div class="col-md-12">
            <?php $this->load->view('templates/member_tab', $tabData);?>
            <p class="font-16px"><span class="blue-text-semi-bold"><?php echo $profileData->first_name. ' ' .$profileData->last_name?></span>
                (not <?php echo $profileData->first_name;?>? <a href="<?php echo ROOT_URL?>logout" >Logout</a>)
            </p>
            <div class="post-left-box">

                <div class="contact-form profile-form"><?php
                    if(isset($errMsg) && $errMsg != ''){ ?>
                        <div class="alert alert-danger">
                            <?php echo $errMsg;?>
                        </div>
                        <?php unset($errMsg);
                    }
                    if(isset($succMsg) && $succMsg != ''){ ?>
                        <div class="alert alert-success">
                            <?php echo $succMsg;?>
                        </div>
                        <?php unset($succMsg);
                    }?>
                    <?php echo validation_errors(); ?>
                    <form name="" method="post" action="">
                        <div class="spapreter-line">
                            <p>Name:<span><?php echo $profileData->first_name. ' ' .$profileData->last_name?></span></p>
                            <div class="lable-profile"><label>First Name:</label></div>
                            <div class="account-field-col">
                                <input class="validate[required] inputbg" type="text" placeholder="First Name" required name="first_name" value="<?php echo !empty($formData['first_name']) ? $formData['first_name'] : ''?>" >
                            </div>
                        </div>

                        <div class="spapreter-line">
                            <div class="lable-profile"><label>Last Name:</label></div>
                            <div class="account-field-col">
                                <input  class="validate[required] inputbg" type="text" placeholder="Last Name" required name="last_name" value="<?php echo !empty($formData['last_name']) ? $formData['last_name'] : ''?>" >
                            </div>
                        </div>

                        <div class="spapreter-line">
                            <div class="lable-profile"><label>Email:</label></div>
                            <div class="account-field-col">
                                <input  class="validate[required] inputbg" type="email" placeholder="Email Address / Username" required name="email" value="<?php echo !empty($formData['email']) ? $formData['email'] : ''?>"
                                        onblur="validate_user_name(this, '<?php echo ROOT_URL.'register/check_duplicate_email';?>', 'p', 'alert-danger')"
                                    >
                            </div>
                        </div>

                        <div class="spapreter-line">
                            <div class="lable-profile"><label>Gender:</label></div>
                            <div class="account-field-col">
                                <div class="radio-btn-box">
                                    <input id="radio1" type="radio" required name="gender" value="Male" <?php echo (!empty($formData['gender']) && strtolower($formData['gender']) == strtolower('Male')) ? 'checked="checked"' : ''?>>
                                    <span class="radio-caption">Male</span>
                                </div>
                                <div class="radio-btn-box">
                                    <input id="radio2" type="radio" required name="gender" value="Female" <?php echo (!empty($formData['gender']) && strtolower($formData['gender']) == strtolower('Female')) ? 'checked="checked"' : ''?>>
                                    <span class="radio-caption">Female</span>
                                </div>
                            </div>
                        </div>

                        <div class="spapreter-line">
                            <div class="lable-profile"><label>Date of Birth:</label></div>
                            <div class="account-field-col">
                                <input  class="validate[required] inputbg" type="text" id="datePicker" placeholder="Date of Birth" required name="date_of_birth" value="<?php echo !empty($formData['date_of_birth']) ? date('d F Y', strtotime($formData['date_of_birth'])) : ''?>">
                            </div>
                        </div>

                        <div class="spapreter-line">
                            <div class="lable-profile"><label>Contact No.:</label></div>
                            <div class="account-field-col">
                                <input  class="validate[required] inputbg" type="text" placeholder="Contact No." required name="contact_no" value="<?php echo !empty($formData['contact_no']) ? $formData['contact_no'] : ''?>">
                            </div>
                        </div>

                        <div class="spapreter-line">
                            <div class="lable-profile"><label>Caompany:</label></div>
                            <div class="account-field-col">
                                <input  class="validate[required] inputbg" type="text" placeholder="Company" name="company" value="<?php echo !empty($formData['company']) ? $formData['company'] : ''?>">
                            </div>
                        </div>

                        <div class="spapreter-line">
                            <div class="lable-profile"><label>City:</label></div>
                            <div class="account-field-col">
                                <select name="city" id="city">
                                    <option value="">Select City</option>
                                    <?php foreach($cityList as $country){ ?>
                                        <option value="<?php echo $country->id?>" <?php echo (!empty($formData['city']) && $formData['city'] == $country->id) ? 'selected="selected"' : '';?>><?php echo $country->combined; ?></option>
                                    <?php }?>
                                </select>
                            </div>
                        </div>


                        <input name="submit" value="Update Profile" type="submit" class="submit"  style="margin-top:10px;">

                    </form>
                </div>

            </div>

            <div class="my-account-right-box">
                <!--<p class="border-none"><span>My Ads</span><br /><?php /*echo $dashboardNotification['myAdCount'];*/?></p>-->
                <!--<p><span>My Searches</span> <br />0 <br />saved <br />searches<br />0</p>-->
            </div>


        </div>


    </div>


    <div class="divider-futered"></div>
</div>
<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 6/9/2015
 * Time: 5:18 PM
 */?>
<div class="container ">
    <ul class="bradcram">
        <li><a href="<?php echo ROOT_URL;?>">Home</a></li>
        <li>Blog</li>
    </ul>
    <h2 class="page-title">Blog</h2>
    <div class="row">
        <div class="blog-box-right">
            <form method="get" action="<?php echo ROOT_URL;?>blog" id="bloagSearchForm">
            <input type="text" name="Name" placeholder="Search" id="Search" class="validate[required] blog-box-right-input " onclick="if(this.value != '') { $('#bloagSearchForm').submit(); }" />
            </form>
            <span class="search-title">FOLLOW US</span>
            <div class="social-link right-social">
                <ul>
                    <li><a target="_blank" href="<?php  echo defined('TWITTER_PAGE_LINK') ? TWITTER_PAGE_LINK : 'javascript:void(0)';?>"></a></li>
                    <li><a target="_blank" href="<?php echo defined('FACEBOOK_PAGE_LINK') ? FACEBOOK_PAGE_LINK : 'javascript:void(0)';?>" class="facebook"></a></li>
                    <li><a target="_blank" href="<?php echo defined('LINKEDIN_PAGE_LINK') ? LINKEDIN_PAGE_LINK : 'javascript:void(0)';?>" class="linkdin"></a></li>
                    <li><a target="_blank" href="<?php echo defined('INSTAGRAM_PAGE_LINK') ? INSTAGRAM_PAGE_LINK : 'javascript:void(0)'?>" class="instagram"></a></li>
                </ul>
            </div>
            <span class="search-title">BLOG CATEGORIES</span>
            <ul class="blog-right-list">
                <?php foreach($tipsCategory as $category) {?>
                    <li title="<?php echo $category['category_title'];?>">
                        <a href="<?php echo ROOT_URL . 'blog?category_id=' . $category['id'] ?>"><?php echo $category['category_title'];echo ' (' . $category['itemsCount'] . ')'; ?></a>
                    </li>
                    <?php
                }?>
            </ul>
            <span class="search-title">RECENT ARTICLES</span>
            <?php if(!empty($recentTips) && is_array($recentTips)) {
                foreach ($recentTips as $tip) {?>
                <div class="row blog-row-right">
                    <div class="img-box-blog-right">
                        <?php if(!empty($tip->image) && file_exists(DIR_UPLOAD_BLOG.$tip->image)) {?>
                        <img src="<?php echo DIR_UPLOAD_BLOG_SHOW.$tip->image?>" >
                        <?php } else {?>
                            <img  src="<?php echo ROOT_URL_BASE?>images/Featured-Ads-01.jpg" >
                        <?php }?>
                    </div>
                    <div class="blog-right-para-col-1">
                        <p><span><?php echo $tip->title;?></span><br />
                            <?php echo substr($tip->short_description, 0, 70);?><br />
                            <a href="<?php echo ROOT_URL.'read-blog/'.$tip->url_slug;?>">read more</a></p>
                    </div>
                </div><?php
                }
            }?>


            <span class="search-title">LATEST COMMENTS</span>
            <?php if(!empty($recentComments) && is_array($recentComments)){
                $i = 1;
                foreach ($recentComments as $comments) {?>
                <div class="comments-box <?php echo ($i%2==0) ? 'pull-right' : '';?>">
                    <p class="recent-comment-contents"><?php echo strlen($comments->comment)>80 ? substr($comments->comment, 0 , 80).'...' : $comments->comment;?></p>
                    <p class="right-caption">by <?php echo $comments->first_name;?> on <span><a href="<?php echo ROOT_URL.'read-blog/'.$comments->url_slug;?>"><?php echo (strlen($comments->title)>26) ? substr($comments->title, 0, 23).'...' : $comments->title;?></a></span></p>
                    </div><?php
                    $i ++;
                }
            }?>
        </div>

        <?php if (empty($searchResult)) { ?>

            <div class="blog-box-left">
                <div class="blog-content-box">
                    <span class="blog-title">Article not available</span>
                </div>
                <div class="social-link-box">
                </div>
            </div>
        <?php
        } else {

            $result = $searchResult;?>
            <div class="blog-box-left read-blog-wrap">
                <div class="blog-content-box wide">
                    <span class="datespan"><?php echo date('M. d.Y', strtotime($result->created_date_time));?> </span>
                    <span class="blog-title read-blog-title"><?php echo $result->title;?></span>
                    <div class="blog-img-box full">
                        <?php if(!empty($result->image) && file_exists(DIR_UPLOAD_BLOG.$result->image)) {?>
                        <img src="<?php echo DIR_UPLOAD_BLOG_SHOW.$result->image?>" >
                        <?php } else {?>
                            <img  src="<?php echo ROOT_URL_BASE?>images/Featured-Ads-01.jpg" >
                        <?php }?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="link-by-admin">
                        <p>
                            <a href="<?php echo ROOT_URL.'?category_id='.$result->category_id;?>">Written in <?php echo $result->categoryName;?></a>
                            <?php echo ' <span>|</span> <a href="javascript:void(0)"> '.$result->commentCount.' comments</a>';?>
                        </p>
                        <?php echo $result->description;?>
                    </div>
                </div>
                <div class="social-link-box">
                    <ul>
                        <!--<li><div class="fb-share-button" data-href="<?php /*echo ROOT_URL.'read-blog/'.$result->url_slug;*/?>" data-layout="button"></div></li>-->
                        <li><a href="javascript:void(0)" data-href="<?php echo ROOT_URL.'read-blog/'.$result->url_slug;?>" data-title="<?php echo $result->title;?>" class="facebook-social-share"></a> </li>
                        <li><a class="twitter-share-button" href="https://twitter.com/share?size=5">Tweet</a></li>
                        <li>
                            <a href="https://plus.google.com/share?url={<?php echo ROOT_URL.'read-blog/'.$result->url_slug;?>}" onclick="javascript:window.open(this.href,
'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
                                <img src="https://www.gstatic.com/images/icons/gplus-16.png" alt="Share on Google+"/>
                            </a>
                        </li>
                    </ul>
                </div>

            </div><?php
        }
        ?>
        <div class="contact-form">
            <?php
            $myReview = '';
            if( !empty($userComments)) {?>
                <h3 class="default-font underline-title">User Comments</h3><?php
                $i = 1;
                foreach ($userComments as $comment) {
                    if ($this->user_id == $comment->reviewer_id	){
                        $myReview = $comment;
                    }
                    if (!empty($comment->comment)) { ?>
                        <div class="blog-box-left default-font user-comments">
                            <label class="comment user-name-holder"><?php echo $comment->first_name; ?>:</label>

                            <div class="comment-text-box">
                                <p><?php echo $comment->comment; ?></p>
                            </div>
                        </div><?php
                    }
                }
            } else {?>
                <label class="comment user-name-holder">No comments found</label><?php
            }?>

            <?php
            if (!empty($isMemberLogin)) {?>

                <div class="comment-form">
                    <label class="comment">&nbsp;</label>

                    <form id="enquiryForm" name="formID" method="post" action="">

                        <label class="comment">Add your comment</label>
                                    <textarea name="comments" id="Comments" class="validate[required] textbg" rows="4"
                                              style="height:80px; font-size:13px;" required=""></textarea>
                        <input type="hidden" name="userId" value="<?php echo $this->user_id;?>">
                        <input type="hidden" name="tip_id" value="<?php echo $tip_id;?>">

                        <div class="clearfix"></div>
                        <div class="clearfix"></div>
                        <div class="col-md-12  pull-left">
                            <input name="submit" value="SUBMIT" type="submit" class="submit contact-form">
                        </div>
                    </form>
                </div>

            <?php } else {?>
                <a href="<?php echo ROOT_URL.'login?back='.base64_encode('read-blog/'.$result->url_slug);?>"><input name="button" value="Login to post comments" type="submit" class="submit" style="margin-top:10px;"></a>
            <?php }?>
        </div>
    </div>

</div>

<div id="fb-root"></div>
<script>
(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=1606664936257258";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
$(document).ready(function(){
    $('.facebook-social-share').click(function () {
        var shareUrl = $(this).attr('data-href');
        var shareCaption = $(this).attr('data-title');
        FB.ui({
            method: 'feed',
            link: shareUrl,
            caption: shareCaption,
        }, function(response){});
    })
})</script>
<script>
    window.twttr=(function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],t=window.twttr||{};if(d.getElementById(id))return t;js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);t._e=[];t.ready=function(f){t._e.push(f);};return t;}(document,"script","twitter-wjs"));
</script>
<!-- Please call pinit.js only once per page -->
<script type="text/javascript" async defer src="//assets.pinterest.com/js/pinit.js"></script>
<!-- Place this tag in your head or just before your close body tag. -->
<!--<script src="https://apis.google.com/js/platform.js" async defer></script>-->
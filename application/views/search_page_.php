
<div class="container ">
    <ul class="bradcram">
        <li><a href="<?php echo ROOT_URL;?>">Home</a></li>
        <?php if(!empty($_GET['source_catgories']) && $_GET['source_catgories'] == 1){?>
            <li><a href="<?php echo ROOT_URL;?>categories_listing">Categories</a></li><?php
        } else if(!empty($_GET['source_locations']) && $_GET['source_locations'] == 1){?>
            <li><a href="<?php echo ROOT_URL;?>location_listing">Locations</a></li><?php
        } else {?>

        <?php }?>
        <?php echo !empty($breadCrumbsData['city']) ? '<li><a href="'.ROOT_URL.'search?'.$breadCrumbsData['city']['url'].'">'.$breadCrumbsData['city']['title'].'</a></li>' : '';?>
        <?php echo !empty($breadCrumbsData['locality']) ? '<li><a href="'.ROOT_URL.'search?'.$breadCrumbsData['locality']['url'].'">'.$breadCrumbsData['locality']['title'].'</a></li>' : '';?>
        <?php echo !empty($breadCrumbsData['category']) ? '<li><a href="'.ROOT_URL.'search?'.$breadCrumbsData['category']['url'].'">'.$breadCrumbsData['category']['title'].'</a></li>' : '';?>
        <?php echo !empty($breadCrumbsData['sub_category']) ? '<li><a href="'.ROOT_URL.'search?'.$breadCrumbsData['sub_category']['url'].'">'.$breadCrumbsData['sub_category']['title'].'</a></li>' : '';?>
        <?php //echo !empty($_GET['sub_category']) ? '<li>'.$_GET['sub_category'].'</li>' : '';?>
    </ul>
    <h2 class="page-title border-none"><?php
        $showCount = false;
        if(!empty($searchPageHeader)) {
            echo $searchPageHeader;
            $showCount = true;
            echo " ($searchResultTotal)";
        } else {
            echo " ($searchResultTotal) Results found";
        }/*else if (!empty($_GET['category'])) {
            echo $searchPageHeader = $_GET['category'];
            $showCount = true;
        } else if (!empty($_GET['sub_category'])) {
            echo $searchPageHeader = $_GET['sub_category'];
            $showCount = true;
        } else {
            echo 'All';
        }*/
        //echo " ($searchResultTotal) - <span class='filter'>[Filter by city]</span>";

/*echo !empty($searchResultTotal) ? " ($searchResultTotal)" : '';*/?><!--Cars, Vans & Suvs in UAE (27151) - <span class="filter">[Filter by city]--></span></h2>
</div>
<div class="clerafix"></div>

<div class="blue-sub-header">
    <?php if (!empty($_GET['category']) && $_GET['category'] == CATEG_VEHICLES_ID ) {?>
    <div class="container ">
        <div class="table-box">
            <p class="font-15px">Browse results:</p>
            <table border="0" class="table-classifieds-box">
                <tbody><?php
                if(!empty($modelLookup) && is_array($modelLookup)){?>
                    <tr><?php
                    $i = 1;
                    foreach ($modelLookup as $model) {?>
                        <td>
                            <a href="<?php echo ROOT_URL.'search?model_id='.$model->model_id;?>">
                                <span><?php echo $model->title;?></span>   (<?php echo !empty($model->classifiedsCount) ? $model->classifiedsCount : 0;?>)
                            </a>
                        </td><?php
                        echo ($i%5==0) ? '</tr><tr>' : '';
                        $i++;
                    }?>
                    </tr><?php
                }?>
                </tbody>
            </table>
            <div class="contact-form" style="width:960px;">
                <form method="get" action="" id="searchForm">
                    <?php
                    $searchCriteria = $_GET;
                    foreach ($searchCriteria as $name => $value){
                        echo (!empty($name) && !empty($value)) ? '<input type="hidden" name="'.$name.'" value="'.$value.'" >' : '';
                    }?>
                    <div class="post-field-col" style="width:150px;">
                        <label>Brand Name</label>
                        <select name="brand_id" class="search-drop-down">
                            <option value="">Select</option>
                            <?php if(!empty($brandLookup) && is_array($brandLookup)){
                                foreach ($brandLookup as $brand) {
                                    echo '<option '.((!empty($_GET['brand_id']) && $_GET['brand_id'] == $brand->id) ? 'selected="selected"' : '').' value="'.$brand->id.'">'.$brand->title.'</option>';
                                }
                            }?>
                        </select>
                    </div>
                    <div class="post-field-col" style="width:90px; margin-left:20px;">
                        <label>Price Range</label>
                        <input value="<?php echo !empty($_GET['min_amount']) ? $_GET['min_amount'] : '';?>" placeholder="Price From" type="number" name="min_amount" id="min_amount">
                    </div>
                    <div class="post-field-col" style="width:90px; margin-left:10px;">
                        <label>&nbsp;</label>
                        <input value="<?php echo !empty($_GET['max_amount']) ? $_GET['max_amount'] : '';?>" placeholder="Price To" type="number" name="max_amount" id="max_amount">
                    </div>
                    <div class="post-field-col" style="width:120px; margin-left:20px;">
                        <label>Year</label>
                        <input value="<?php echo !empty($_GET['manufacture_year_from']) ? $_GET['manufacture_year_from'] : '';?>" placeholder="Year From" type="number" name="manufacture_year_from" id="manufacture_year_from1">
                    </div>
                    <div class="post-field-col" style="width:120px; margin-left:10px;">
                        <label>&nbsp;</label>
                        <input value="<?php echo !empty($_GET['manufacture_year_to']) ? $_GET['manufacture_year_to'] : '';?>" placeholder="Year To" type="number" maxlength="4" name="manufacture_year_to" id="manufacture_year_to">
                    </div>
                    <div class="post-field-col" style="width:85px; margin-left:20px;">
                        <label>Fuel Type</label>
                        <select name="fuel_type" class="search-drop-down">
                            <option value="">Select</option>
                            <?php if(!empty($fuelLookUp) && is_array($fuelLookUp)){
                                foreach ($fuelLookUp as $fuel) {
                                    echo '<option '.((!empty($_GET['fuel_type']) && $_GET['fuel_type'] == $fuel->id) ? 'selected="selected"' : '').' value="'.$fuel->id.'">'.$fuel->title.'</option>';
                                }
                            }?>
                        </select>
                    </div>
                    <div class="post-field-col" style="width:85px; margin-left:20px;">
                        <label>Transmission</label>
                        <select name="transmission" class="search-drop-down">
                            <option value="">Select</option>
                            <?php if(!empty($transmissionLookUp) && is_array($transmissionLookUp)){
                                foreach ($transmissionLookUp as $transmission) {
                                    echo '<option '.((!empty($_GET['transmission']) && $_GET['transmission'] == $transmission->id) ? 'selected="selected"' : '').' value="'.$transmission->id.'">'.$transmission->title.'</option>';
                                }
                            }?>
                        </select>
                    </div>
                    <div class="post-field-col" style="width:98px; margin-left:20px;">
                        <label>&nbsp;</label>
                        <input type="submit" value="Search">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <?php }?>

    <?php if (!empty($_GET['category']) && $_GET['category'] == CATEG_REAL_EST_ID ) {?>
        <div class="container ">
            <div class="table-box">
                <div class="contact-form" style="width:960px;">
                    <form method="get" action="" id="searchForm">
                        <?php
                        $searchCriteria = $_GET;
                        foreach ($searchCriteria as $name => $value){
                            echo (!empty($name) && !empty($value)) ? '<input type="hidden" name="'.$name.'" value="'.$value.'" >' : '';
                        }?>
                        <div class="post-field-col" style="width:90px; margin-left:20px;">
                            <label>Price Range</label>
                            <input value="<?php echo !empty($_GET['min_amount']) ? $_GET['min_amount'] : '';?>" placeholder="Price From" type="number" name="min_amount" id="min_amount">
                        </div>
                        <div class="post-field-col" style="width:90px; margin-left:10px;">
                            <label>&nbsp;</label>
                            <input value="<?php echo !empty($_GET['max_amount']) ? $_GET['max_amount'] : '';?>" placeholder="Price To" type="number" name="max_amount" id="max_amount">
                        </div>

                        <div class="post-field-col" style="width:75px; margin-left:20px;">
                            <label>Bed Rooms</label>
                            <input value="<?php echo !empty($_GET['min_bed_rooms']) ? $_GET['min_bed_rooms'] : '';?>" placeholder="Min." type="number" name="min_bed_rooms" id="min_bed_rooms1">
                        </div>
                        <div class="post-field-col" style="width:75px; margin-left:10px;">
                            <label>&nbsp;</label>
                            <input value="<?php echo !empty($_GET['max_bed_rooms']) ? $_GET['max_bed_rooms'] : '';?>" placeholder="Max." type="number" maxlength="4" name="max_bed_rooms" id="max_bed_rooms">
                        </div>

                        <div class="post-field-col" style="width:75px; margin-left:20px;">
                            <label>Bath Rooms</label>
                            <input value="<?php echo !empty($_GET['min_bath_rooms']) ? $_GET['min_bath_rooms'] : '';?>" placeholder="Min." type="number" name="min_bath_rooms" id="min_bath_rooms1">
                        </div>
                        <div class="post-field-col" style="width:75px; margin-left:10px;">
                            <label>&nbsp;</label>
                            <input value="<?php echo !empty($_GET['max_bath_rooms']) ? $_GET['max_bath_rooms'] : '';?>" placeholder="Max." type="number" maxlength="4" name="max_bath_rooms" id="max_bath_rooms">
                        </div>

                        <div class="post-field-col" style="width:75px; margin-left:20px;">
                            <label>Area (Sq. ft.)</label>
                            <input value="<?php echo !empty($_GET['min_area_sqft']) ? $_GET['min_area_sqft'] : '';?>" placeholder="Min." type="number" name="min_area_sqft" id="min_area_sqft1">
                        </div>
                        <div class="post-field-col" style="width:75px; margin-left:10px;">
                            <label>&nbsp;</label>
                            <input value="<?php echo !empty($_GET['max_area_sqft']) ? $_GET['max_area_sqft'] : '';?>" placeholder="Max." type="number" maxlength="4" name="max_area_sqft" id="max_area_sqft">
                        </div>


                        <div class="post-field-col" style="width:98px; margin-left:20px;">
                            <label>&nbsp;</label>
                            <input type="submit" value="Search">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    <?php }?>

</div>

<div class="container ">
    <div class="row">
        <div class="blog-box-right">
            <div class="add-box margin-none">
                <img src="<?php echo ROOT_URL_BASE;?>images/ajax-loader.gif" width="300" height="250">
            </div>

            <div class="add-box fbPageBox">
                <div class="fb-page" data-href="<?php echo FACEBOOK_PAGE_URL;?>" data-width="300" data-height="250" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="<?php echo FACEBOOK_PAGE_URL;?>"><a href="<?php echo FACEBOOK_PAGE_URL;?>">Facebook</a></blockquote></div></div>
            </div>
            <div class="add-box">
                <img src="<?php echo ROOT_URL_BASE;?>images/ajax-loader.gif" width="300" height="250">
            </div>

        </div>

        <div class="left-content-box">

            <?php
            if(isset($searchResult) && count($searchResult) > 0) {
                $i = 0;
                foreach($searchResult as $classified) { $i++;?>
                    <div class="list-box-product">
                        <div class="img-box-vic"><?php //print_r($classified);?>
                            <!--<img src="<?php echo ROOT_URL_BASE;?>images/cars01.jpg">-->
                            <a href="<?php echo ROOT_URL.'details/'.$classified->classified_slug.'?back='.$back_url_query_string;?>">
                                <?php if($classified->image_list && count($classified->image_list) > 0){
                                    if(isset($classified->image_list[0]->classified_image) && $classified->image_list[0]->classified_image!='' && file_exists(DIR_UPLOAD_CLASSIFIED.$classified->image_list[0]->classified_image)) {?>
                                        <img src="<?php echo DIR_UPLOAD_CLASSIFIED_SHOW.$classified->image_list[0]->classified_image; ?>" >
                                    <?php }else{?>
                                        <img src="<?php echo ROOT_URL_BASE;?>images/Featured-Ads-01.jpg" >
                                    <?php }
                                } else { ?>
                                    <img src="<?php echo ROOT_URL_BASE;?>images/Featured-Ads-01.jpg">
                                <?php } ?>
                            </a>
                        </div>
                        <div class="detils-main-col">

                            <p class="search-product-title">
                                <a href="<?php echo ROOT_URL.'details/'.$classified->classified_slug.'?back='.$back_url_query_string;?>"><span><?php echo !empty($classified->title) ? $classified->title : '';?></span></a>
                            </p>
                            <ul>
                                <li><?php echo make_difference_string(new DateTime($classified->created_date_time));?></li>
                                <li><?php echo !empty($cityLookup[$classified->classified_city]->combined) ? $cityLookup[$classified->classified_city]->combined : '';?></li>
                                <li class="last-product"><?php echo !empty($classified->model) ? $classified->model : '';?></li>
                            </ul>
                            <div class="clerafix"></div>
                            <p><?php echo !empty($classified->small_description) ? $classified->small_description : ''; ?></p>
                            <div id="content">
                                <?php if ($classified->category_id == CATEG_VEHICLES_ID) {?>
                                <blockquote class="bigtext">
                                    <div class="brand-box-expand">
                                        <p><span>Brand</span><br /><?php echo !empty($brandLookup[$classified->brand_id]->title) ? $brandLookup[$classified->brand_id]->title : ''; ?></p>
                                        <p><span>Fuel</span><br /><?php echo !empty($fuelLookUp[$classified->fuel_type]->title) ? $fuelLookUp[$classified->fuel_type]->title : '';?></p>
                                    </div>

                                    <div class="brand-box-expand">
                                        <p><span>Model</span><br /><?php echo !empty($modelLookup[$classified->model_id]->title) ? $modelLookup[$classified->model_id]->title : '';?></p>
                                        <p><span>KM's driven:</span><br /><?php echo !empty($classified->running_km) ? $classified->running_km : '';?></p>
                                    </div>

                                    <div class="brand-box-expand">
                                        <p><span>Year</span><br /><?php echo !empty($classified->manufacture_year) ? $classified->manufacture_year : '';?></p>
                                    </div>

                                    <div class="location-link">
                                        <p>
                                            <?php echo !empty($classified->city) ? ' <a href="'.ROOT_URL.'search?search_city='.$classified->classified_city.'">'.$classified->city.'</a>' : ''; ?>
                                            <?php echo !empty($classified->locality) ? ' <a href="'.ROOT_URL.'search?classified_locality='.$classified->classified_locality.'">'.$classified->locality.'</a>' : ''; ?>
                                            <?php echo !empty($classified->classified_land_mark) ? '   --   <a href="javascript:void(0)">'.$classified->classified_land_mark.'</a>' : '' ?>
                                            <?php //echo !empty($classified->classified_address) ? '  --   <a href="javascript:void(0)">'.$classified->classified_address.'</a>' : ''; ?>
                                        </p>
                                    </div>

                                </blockquote>
                                    <p class="expand backgorndnone"><i class="fa fa-arrow-down"></i>Show All<img src="<?php echo ROOT_URL_BASE;?>images/show-all-bg.png" class="right-margine-cion"><i class="fa fa-arrow-down"></i></p>
                                    <p class="contract hide backgorndnone"><i class="fa fa-arrow-up"></i>Hide<img src="<?php echo ROOT_URL_BASE;?>images/hide.png" class="right-margine-cion"><i class="fa fa-arrow-up"></i></p>
                                <?php } else if ($classified->category_id == CATEG_REAL_EST_ID) {?>
                                    <blockquote class="bigtext">
                                        <div class="brand-box-expand">
                                            <p><span>Bed Rooms</span><br /><?php echo !empty($classified->bedrooms) ? $classified->bedrooms : ''; ?></p>
                                        </div>

                                        <div class="brand-box-expand">
                                            <p><span>Bath Rooms</span><br /><?php echo !empty($classified->bathrooms) ? $classified->bathrooms : ''; ?></p>
                                        </div>

                                        <div class="brand-box-expand">
                                            <p><span>Area(Sq. Ft.)</span><br /><?php echo !empty($classified->area_sqft) ? $classified->area_sqft : ''; ?></p>
                                        </div>

                                        <div class="location-link">
                                            <p>
                                                <?php echo !empty($classified->city) ? ' <a href="'.ROOT_URL.'search?search_city='.$classified->classified_city.'">'.$classified->city.'</a>' : ''; ?>
                                                <?php echo !empty($classified->locality) ? ' <a href="'.ROOT_URL.'search?classified_locality='.$classified->classified_locality.'">'.$classified->locality.'</a>' : ''; ?>
                                                <?php echo !empty($classified->classified_land_mark) ? '   --   <a href="javascript:void(0)">'.$classified->classified_land_mark.'</a>' : '' ?>
                                                <?php //echo !empty($classified->classified_address) ? '  --   <a href="javascript:void(0)">'.$classified->classified_address.'</a>' : ''; ?>
                                            </p>
                                        </div>

                                    </blockquote>
                                    <p class="expand backgorndnone"><i class="fa fa-arrow-down"></i>Show All<img src="<?php echo ROOT_URL_BASE;?>images/show-all-bg.png" class="right-margine-cion"><i class="fa fa-arrow-down"></i></p>
                                    <p class="contract hide backgorndnone"><i class="fa fa-arrow-up"></i>Hide<img src="<?php echo ROOT_URL_BASE;?>images/hide.png" class="right-margine-cion"><i class="fa fa-arrow-up"></i></p>
                                <?php } else if ($classified->category_id == CATEG_JOB_ID) {?>
                                    <blockquote class="bigtext">
                                        <div class="brand-box-expand">
                                            <p><span>Career Level</span><br /><?php echo !empty($career_levelLookUp[$classified->career_level]->title) ? $career_levelLookUp[$classified->career_level]->title : ''; ?></p>
                                            <p><span>Employment Type</span><br /><?php echo !empty($employment_typeLookUp[$classified->employment_type]->title) ? $employment_typeLookUp[$classified->employment_type]->title : ''; ?></p>
                                        </div>

                                        <div class="brand-box-expand">
                                            <p><span>Experience</span><br /><?php echo !empty($classified->experience_years) ? $classified->experience_years.' Years' : '';?></p>
                                        </div>

                                        <div class="brand-box-expand">
                                            <p><span>Education</span><br /><?php echo !empty($educationLookUp[$classified->model_id]->title) ? $educationLookUp[$classified->model_id]->title : '';?></p>
                                        </div>

                                        <div class="location-link">
                                            <p>
                                                <?php echo !empty($classified->city) ? ' <a href="'.ROOT_URL.'search?search_city='.$classified->classified_city.'">'.$classified->city.'</a>' : ''; ?>
                                                <?php echo !empty($classified->locality) ? ' <a href="'.ROOT_URL.'search?classified_locality='.$classified->classified_locality.'">'.$classified->locality.'</a>' : ''; ?>
                                                <?php echo !empty($classified->classified_land_mark) ? '   --   <a href="javascript:void(0)">'.$classified->classified_land_mark.'</a>' : '' ?>
                                                <?php //echo !empty($classified->classified_address) ? '  --   <a href="javascript:void(0)">'.$classified->classified_address.'</a>' : ''; ?>
                                            </p>
                                        </div>

                                    </blockquote>
                                    <p class="expand backgorndnone"><i class="fa fa-arrow-down"></i>Show All<img src="<?php echo ROOT_URL_BASE;?>images/show-all-bg.png" class="right-margine-cion"><i class="fa fa-arrow-down"></i></p>
                                    <p class="contract hide backgorndnone"><i class="fa fa-arrow-up"></i>Hide<img src="<?php echo ROOT_URL_BASE;?>images/hide.png" class="right-margine-cion"><i class="fa fa-arrow-up"></i></p>
                                <?php }?>
                            </div>
                        </div>
                    </div>
                    <?php
                }?>
                <div class="clerafix"></div>
                <div class="pagenation">
                    <?php echo $paginator;?>
                </div><?php
            } else {?>
                <div class="list-box-product">

                    <div class="detils-main-col">

                        <div class="clerafix"></div>
                        <p class="margin-bottom-none">
                            <a href="javascript:void(0)"><span>No Results found</span></a>
                        </p>

                    </div>
                </div><?php
            }?>

        </div>
    </div>
</div>
<div class="divider-futered"></div>
<script type="text/javascript" src="<?php echo ROOT_URL_BASE?>js/search.js"></script>
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=1606664936257258";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

<script language="javascript" type="text/javascript">
    $(document).ready(function(){
        <?php if(!empty($advertize)) {
            $i = 0;
            foreach ($advertize as $item) {
                if (file_exists(DIR_UPLOAD_ADVERTIZE.$item->image_path)) {
                echo "\n\t$('.add-box').not('.fbPageBox').eq(".$i.").html('<a href=\"#\"><img src=\"".DIR_UPLOAD_ADVERTIZE_SHOW.$item->image_path."\" width=\"300\" height=\"250\"></a>');\n";
                $i++;
                }
            }
        }?>
    })
</script>
<script type="text/javascript" src="<?php echo ROOT_URL_BASE?>js/jquery.validate.min.js"></script>
<script>
    $(document).ready(function () {
        $('#searchForm').validate({
            rules: {
                min_amount: {
                    number: true,
                    maxlength: 10
                },
                max_amount: {
                    number: true,
                    maxlength: 10
                },
                manufacture_year_from: {
                    number: true,
                    maxlength: 4,
                    max:new Date().getFullYear()
                },
                manufacture_year_to: {
                    number: true,
                    maxlength: 4,
                    max:new Date().getFullYear()
                }
            },
            messages: {
                min_amount: {
                    number: 'Invalid amount',
                    maxlength: 'Exceed max limit'
                },
                max_amount: {
                    number: 'Invalid amount',
                    maxlength: 'Exceed max limit'
                },
                manufacture_year_from: {
                    number: 'Invalid year',
                    maxlength: 'Invalid year',
                    max: 'Invalid year',
                },
                manufacture_year_to: {
                    number: 'Invalid year',
                    maxlength: 'Invalid year',
                    max: 'Invalid year',
                }
            }
        })
    })
</script>
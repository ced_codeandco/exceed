<div class="clerafix"></div>
<div class="black-container">
    <div class="container">
        <div class="col-md-6">
            <div class="col-xs-6">
                <p class="font-13px"><span class="font-17px">Newsletter Signup</span><br />
                    Subscribe to our newsletter to get <br />
                    the latest scoop right to your inbox.</p>
                <form method="post" id="emailSubscriptionFprm" onsubmit="return subscribeme(this)">
                    <span>
                    <input type="email" required class="form-control footer-form" name="email" placeholder="Enter email">
                    </span>
                    <p class="lable-text">We don't spam, promise.</p>
                    <button type="submit" class="send-btn-default">Subscribe</button>
                </form>
            </div>
            <div class="col-xs-6 pull-right">
                <p class="font-13px"><span class="font-17px">Useful information</span><br />
                    <a href="<?php echo ROOT_URL;?>">Home</a><br />
                    <?php
                    if(!empty($footerCMSList)) {
                        foreach ($footerCMSList as $subPage) {?>
                        <a href="<?php echo ROOT_URL.'cms/'.$subPage->cms_slug;?>"><?php echo $subPage->title;?></a><br /><?php
                        }
                    }?>
                    <a href="<?php echo ROOT_URL;?>blog">Blog</a><br />
                </p>

            </div>

        </div>
        <div class="col-md-6" id="special_width">
            <p class="footer-para"><span class="font-17px">About Exceed</span><br />
                <?php
                $aboutUsDetails = !empty($aboutUsDetails[0]) ? $aboutUsDetails[0] : '';
                echo !empty($aboutUsDetails->small_description)  ? strip_tags($aboutUsDetails->small_description) : '';?>. <a href="<?php echo ROOT_URL.'cms/'.(!empty($aboutUsDetails->cms_slug) ? $aboutUsDetails->cms_slug : '');?>" class="readmore">read more</a>
            </p>
        </div>
        <div class="clerafix"></div>
        <p class="copy-right">&copy; 2015 <?php echo SITE_NAME;?>. All rights reserved</p>
        <div class="clerafix"></div></div>
    <div class="clerafix"></div>
</div>
<div class="addTOCartConfirmOverLay">
    <div class="addTOCartConfirmPopup showLoader"><img src="<?php echo ROOT_URL;?>images/ajax-loader.gif" alt="Loading..."/></div>
    <div class="addTOCartConfirmPopup showResult" >
        <h1></h1>
        <p id="cartMessage">This item is now in your Shopping Cart.</p>
        <div id="confirmButtons">
            <a href="javascript:void(0)" id="closeAddItemPopup" class="button blue">Continue Shopping<span></span></a>
            <a href="javascript:void(0)" id="closeAllPopupAndShowCart" class="button gray">View Shopping Cart<span></span></a>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo ROOT_URL_BASE?>js/global.js"></script>
<script type="text/javascript" src="<?php echo ROOT_URL_BASE?>js/jquery.lionbars.0.3.js"></script>
<script type="text/javascript" src="<?php echo ROOT_URL_BASE?>js/jquery.jcarousel.min.js"></script>
<script type="text/javascript" src="<?php echo ROOT_URL_BASE?>js/tabs.js"></script>
<link rel="stylesheet" href="<?php echo ROOT_URL_BASE?>css/colorbox/colorbox.css" />
<script src="<?php echo ROOT_URL_BASE?>js/jquery.colorbox.js"></script>
</body>
</html>
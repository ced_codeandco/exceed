<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title><?php echo defined('EXCEED_TITLE') ? EXCEED_TITLE : SITE_NAME; ?></title>
    <meta name="keywords" content="<?php echo DEFAULT_META_KEYWORDS?>" />
    <meta name="Description" content="<?php if (empty($pageMetaName)) { echo DEFAULT_META_DESCRIPTION; } else echo $pageMetaName;?>"/>

    <?php echo !empty($ogTitle) ? '<meta property="og:title" content="'.$ogTitle.'" />' : '';?>
    <meta property="og:site_name" content="<?php echo defined('EXCEED_TITLE') ? EXCEED_TITLE : SITE_NAME; ?>"/>
    <?php echo !empty($ogImage) ? '<meta property="og:image" content="'.$ogImage.'" />' : '';?>
    <link href="<?php echo ROOT_URL_BASE;?>css/style.css" type="text/css" rel="stylesheet" title="<?php echo defined('EXCEED_TITLE') ? EXCEED_TITLE : SITE_NAME;?>" />
    <link href="<?php echo ROOT_URL_BASE;?>fonts/fonts.css" type="text/css" rel="stylesheet" title="<?php echo defined('EXCEED_TITLE') ? EXCEED_TITLE : SITE_NAME;?>" />
    <link rel="shortcut icon" type="image/x-icon"  href="<?php echo ROOT_URL_BASE;?>images/facicon.png">
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <link rel="stylesheet" href="<?php echo ROOT_URL_BASE;?>css/bootstrap.min.css">
    <script type="text/javascript" src="<?php echo ROOT_URL_BASE?>js/jquery.1.7.1.min.js"></script>
    <script type="text/javascript" src="<?php echo ROOT_URL_BASE;?>js/fg.menu.js"></script>
    <script type="text/javascript" src="<?php echo ROOT_URL_BASE?>js/helper.js"></script>
    <link type="text/css" href="<?php echo ROOT_URL_BASE;?>css/fg.menu.css" media="screen" rel="stylesheet" />
    <link rel="stylesheet" href="<?php echo ROOT_URL_BASE;?>css/jquery-ui.css">
    <link rel="stylesheet" href="<?php echo ROOT_URL_BASE;?>css/dropdown.css">
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_BASE?>css/detailpage-skin.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_BASE?>css/skin.css" />
    <link rel="stylesheet" type="text/css"  href="<?php echo ROOT_URL_BASE?>css/style-accourdin.css"/>

    <!-- style exceptions for IE 6 -->
    <!--[if IE 6]>
    <style type="text/css">
        .fg-menu-ipod .fg-menu li { width: 95%; }
        .fg-menu-ipod .ui-widget-content { border:0; }
    </style>
    <![endif]-->

    <script type="text/javascript">
        $(function(){
            // MENUS
            $('#flat').menu({
                content: $('#flat').next().html(), // grab content from this page
                showSpeed: 400
            });

        });
    </script>
</head>

<body>
<input type="hidden" id="rootUrlLink" value="<?php echo ROOT_URL;?>" />
<div class="header">
    <div class="brand-Exceed"><a href="<?php echo ROOT_URL;?>" title="Exceed Trader"></a></div>
    <div class="social-link">
        <ul>
            <li><a target="_blank" href="<?php  echo defined('TWITTER_PAGE_LINK') ? TWITTER_PAGE_LINK : 'javascript:void(0)';?>"></a></li>
            <li><a target="_blank" href="<?php echo defined('FACEBOOK_PAGE_LINK') ? FACEBOOK_PAGE_LINK : 'javascript:void(0)';?>" class="facebook"></a></li>
            <li><a target="_blank" href="<?php echo defined('LINKEDIN_PAGE_LINK') ? LINKEDIN_PAGE_LINK : 'javascript:void(0)';?>" class="linkdin"></a></li>
            <li><a target="_blank" href="<?php echo defined('INSTAGRAM_PAGE_LINK') ? INSTAGRAM_PAGE_LINK : 'javascript:void(0)'?>" class="instagram"></a></li>
        </ul>
        <div class="clerafix"></div>
        <?php if( !empty($isMemberLogin) &&  $isMemberLogin != false) {?>
            <span class="welcome-title">
                Welcome
                <a tabindex="0" href="#search-engines" class="fg-button fg-button-icon-right ui-widget ui-state-default ui-corner-all" id="flat">
                    <span class="ui-icon ui-icon-triangle-1-s"></span><?php echo !empty($isMemberLogin->first_name) ? $isMemberLogin->first_name : '';?>
                </a>
                <div id="search-engines" class="hidden">
                    <ul>
                        <li><a href="<?php echo MEMBER_ROOT_URL;?>my_profile">My Profile</a></li>
                        <li><a href="<?php echo MEMBER_ROOT_URL;?>my_adds">My  Ads</a></li>
                        <!--<li><a href="#">My Searches</a></li>-->
                        <li><a href="<?php echo MEMBER_ROOT_URL;?>account_settings">Account Settings</a></li>
                        <li><a href="<?php echo ROOT_URL;?>logout">Sign Out</a></li>
                    </ul>
                </div>
            </span>
            <span class="icon">
                <a href="<?php echo ROOT_URL;?>"><img src="<?php echo ROOT_URL_BASE;?>images/iocn01.png"></a>&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="<?php echo ROOT_URL;?>logout"><img src="<?php echo ROOT_URL_BASE;?>images/iocn02.png"></a>
            </span>
        <?php } else {?>
                <a class="header-login-link default-font" href="<?php echo ROOT_URL;?>login">Register / Sign In</a>
        <?php }?>
    </div>
</div><!-- header -->
<div class="clerafix"></div>
<div class="red-box">
    <div class="container border-bottom">
        <ul class="nav-bar">
            <li home="1"><a href="<?php echo ROOT_URL?>">Home</a>/</li>
            <li><a href="<?php echo ROOT_URL?>categories_listing">Categories</a>/</li>
            <li><a href="<?php echo ROOT_URL?>location_listing">Locations</a>/</li>
            <!--<li><a href="<?php /*echo ROOT_URL*/?>sellers">How it works</a>/</li>--><?php
            if(!empty($headerCMSList)) {
                $total = is_array($headerCMSList) ? count($headerCMSList) : 0;
                $i = 1;
                foreach ($headerCMSList as $subPage) {?>
                    <li><a href="<?php echo ROOT_URL.'cms/'.$subPage->cms_slug;?>"><?php echo $subPage->title;?></a><?php echo ($i < $total ) ? '/' : '';?></li><?php
                    $i++;
                }
            }?>
            <?php /*<li child="/read-blog"><a href="<?php echo ROOT_URL?>blog">Blog</a></li>*/?>
        </ul>
        <span class="post-an-ad"><a href="<?php echo ROOT_URL.'create_classified'?>"><img src="<?php echo ROOT_URL_BASE;?>images/post-an-add.png"></a></span>
    </div>
</div> <!-- red-box -->
<div class="clerafix"></div>
<div class="red-box-bottom-part">
    <div class="container ">
        <div class="col-md-12 top-margine-15px">
            <form id="headerSearchForm" method="get" action="<?php echo ROOT_URL.'search'?>">
                <div class="col-md-2">
                    <div class="select_style"><?php //print_r($_GET['search_city']);?>
                        <select name="search_city">
                            <option value="">All Locations</option>
                            <?php if (!empty($cityList) && is_array($cityList)){
                                foreach ($cityList as $city) {
                                    $selected = (!empty($_GET['search_city']) && $city->id == $_GET['search_city']) ? 'selected="selected"' : '';
                                    echo '<option '.$selected.' value="'.$city->id.'">'.$city->combined.'</option>';
                                }
                            }?>
                        </select>
                    </div>
                </div>
                <div class="col-md-2 left-margine-20px-rorm">
                    <div class="select_style">
                        <select name="category">
                            <option value="">All Categories</option>
                            <?php if (!empty($parentCategoriesList) && is_array($parentCategoriesList)){
                                foreach ($parentCategoriesList as $item) {
                                    $selected = (!empty($_GET['category']) && $item->id == $_GET['category']) ? 'selected="selected"' : '';
                                    echo '<option '.$selected.' value="'.$item->id.'">'.$item->title.'</option>';
                                }
                            }?>
                        </select>
                    </div>
                </div>
                <div class="col-md-8 left-margine-20px-rorm">
                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Search..." name="search_keyword" value="<?php echo !empty($_GET['search_keyword']) ? $_GET['search_keyword'] : '';?>">
                </div>
                <span class="post-an-ad margin-none"><a href="#" onclick="$('#headerSearchForm').submit();"><img src="<?php echo ROOT_URL_BASE;?>images/search-icon.png" height="38"></a></span>
            </form>
        </div>
    </div>
</div>

<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 4/23/2015
 * Time: 10:37 AM
 */?>
<?php if(!empty($tabList) && is_array(($tabList))) {?>
    <ul class="tabs-link"><?php
    $count = 1;
    foreach ($tabList as $urlstring => $tab) {
        $tabId = 'memberTab'.$count;?>
        <li id="<?php echo $tabId;?>">
            <a class="right-border-none <?php echo ($tab['is_active'] == 1) ? 'active' : '';?>" href="<?php echo MEMBER_ROOT_URL.$urlstring;?>"><?php echo $tab['title'];?></a>
        </li><?php
        $count++;
    }?>
    </ul><?php
}?>
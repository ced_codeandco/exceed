-- phpMyAdmin SQL Dump
-- version 4.0.10.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 06, 2016 at 02:27 AM
-- Server version: 5.5.45-cll-lve
-- PHP Version: 5.4.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `exceed`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_admin`
--

CREATE TABLE IF NOT EXISTS `tbl_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `contact_no` varchar(50) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `module_access` varchar(255) DEFAULT NULL,
  `admin_role` enum('Super Admin','Member Admin') NOT NULL DEFAULT 'Member Admin',
  `created_date` datetime NOT NULL,
  `last_login_date` datetime NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_login_ip` varchar(255) NOT NULL,
  `is_active` enum('0','1') NOT NULL DEFAULT '0',
  `password_token` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_admin`
--

INSERT INTO `tbl_admin` (`id`, `first_name`, `last_name`, `contact_no`, `email`, `password`, `module_access`, `admin_role`, `created_date`, `last_login_date`, `updated_date`, `last_login_ip`, `is_active`, `password_token`) VALUES
(2, 'Girish', '', '989808980', 'test@codeandco.ae', 'cc03e747a6afbbcbf8be7668acfebee5', '1,2,3,4,5,6,7', 'Super Admin', '2015-02-26 12:32:28', '2016-04-04 04:55:51', '2016-04-04 11:55:51', '5.32.65.74', '1', ''),
(3, 'Anas', 'Here', NULL, 'anas2job@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', '1,5,6,7', 'Member Admin', '2015-04-17 22:47:49', '0000-00-00 00:00:00', '2015-04-21 15:28:55', '', '1', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_advertize`
--

CREATE TABLE IF NOT EXISTS `tbl_advertize` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `advertize_url` varchar(255) DEFAULT NULL,
  `image_path` varchar(255) DEFAULT NULL,
  `advertize_type` enum('Full Width','Square') NOT NULL DEFAULT 'Square',
  `created_date_time` datetime NOT NULL,
  `is_active` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `tbl_advertize`
--

INSERT INTO `tbl_advertize` (`id`, `title`, `advertize_url`, `image_path`, `advertize_type`, `created_date_time`, `is_active`) VALUES
(1, 'Advertize 1', 'www.google.com', '23062015133541_MPU-Valetines.gif', 'Square', '2015-06-23 13:35:41', '1'),
(2, 'Lexus', 'www.lexus.com/', '01032015074510_add-img-01.jpg', 'Square', '2015-03-02 07:26:50', '1'),
(3, 'Lexus', 'www.lexus.com/', '02032015072730_add-classifi.jpg', 'Full Width', '2015-03-02 07:27:30', '1'),
(4, 'Bing Add', '', '25072015104420_23062015163452_All-Out-Clearance-Event-300x250-Dealer-GIF.gif', 'Square', '2015-07-25 10:44:20', '1'),
(5, 'Bing Add', '', '25072015104436_23062015163500_300x2502.gif', 'Square', '2015-07-25 10:44:36', '1'),
(6, 'Bing Add', '', '25072015104453_23062015163515_20140902143527square_banner_autopartners_amimated_v2_4_.gif', 'Square', '2015-07-25 10:44:53', '1'),
(7, 'Bing Add', '', '25072015104510_23062015163521_brilliant-yoga-250.gif', 'Square', '2015-07-25 10:45:10', '1'),
(8, 'Bing Add', '', '25072015104516_23062015163529_MPU-Valetines.gif', 'Square', '2015-07-25 10:45:16', '1'),
(9, 'Bing Add', '', '25072015104522_23062015163535_Ford-2686.gif', 'Square', '2015-07-25 10:45:22', '1'),
(10, 'Bing Add', '', '25072015104527_23062015163551_banner.jpg', 'Square', '2015-07-25 10:45:27', '1'),
(11, 'Bing Add', '', '25072015104532_23062015163647_Corolla_Thumb.jpg', 'Square', '2015-07-25 10:45:32', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_business_type`
--

CREATE TABLE IF NOT EXISTS `tbl_business_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tbl_business_type`
--

INSERT INTO `tbl_business_type` (`id`, `name`, `created_date`, `updated_date`) VALUES
(1, 'Automotive Finance', '0000-00-00 00:00:00', '2015-04-21 05:32:20'),
(2, 'Business & Professional Services', '0000-00-00 00:00:00', '2015-04-21 05:32:20'),
(3, 'Clean Technology', '0000-00-00 00:00:00', '2015-04-21 05:32:20'),
(4, 'Energy', '0000-00-00 00:00:00', '2015-04-21 05:32:20'),
(5, 'Franchise', '0000-00-00 00:00:00', '2015-04-21 05:32:20');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_career_level`
--

CREATE TABLE IF NOT EXISTS `tbl_career_level` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `created_date` datetime NOT NULL,
  `is_active` enum('0','1') NOT NULL DEFAULT '1',
  `is_deleted` enum('0','1') NOT NULL DEFAULT '0',
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `tbl_career_level`
--

INSERT INTO `tbl_career_level` (`id`, `title`, `created_date`, `is_active`, `is_deleted`, `sort_order`) VALUES
(1, 'Student/Intern', '0000-00-00 00:00:00', '1', '0', 1),
(2, 'Junior', '0000-00-00 00:00:00', '1', '0', 2),
(3, 'Mid-level', '0000-00-00 00:00:00', '1', '0', 3),
(4, 'Senior', '0000-00-00 00:00:00', '1', '0', 4),
(5, 'Manager', '0000-00-00 00:00:00', '1', '0', 5),
(6, 'Executive/Director', '0000-00-00 00:00:00', '1', '0', 6);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category`
--

CREATE TABLE IF NOT EXISTS `tbl_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `sub_title` varchar(255) NOT NULL,
  `category_slug` varchar(255) NOT NULL,
  `description` text,
  `parent_id` int(11) NOT NULL,
  `has_sub_category` enum('0','1') NOT NULL DEFAULT '0',
  `category_image` varchar(255) NOT NULL,
  `category_icon_large` varchar(255) NOT NULL,
  `category_order` int(11) NOT NULL,
  `on_header` enum('0','1') NOT NULL DEFAULT '1',
  `meta_title` text,
  `meta_desc` text,
  `meta_keywords` text,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_active` enum('0','1') NOT NULL DEFAULT '0',
  `is_deleted` enum('0','1') NOT NULL DEFAULT '0',
  `deleted_date_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=73 ;

--
-- Dumping data for table `tbl_category`
--

INSERT INTO `tbl_category` (`id`, `title`, `sub_title`, `category_slug`, `description`, `parent_id`, `has_sub_category`, `category_image`, `category_icon_large`, `category_order`, `on_header`, `meta_title`, `meta_desc`, `meta_keywords`, `created_by`, `updated_by`, `created_date`, `updated_date`, `is_active`, `is_deleted`, `deleted_date_time`) VALUES
(1, 'Real Estate', 'Real Estate', 'real-estate', 'Real Estate', 0, '1', '07062015122758_real-estate.png', '08062015202403_Real-Estate-categ.png', 1, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-10-08 01:37:46', '1', '0', '0000-00-00 00:00:00'),
(2, 'Vehicles', 'Vehicles', 'vehicles', 'Vehicles', 0, '1', '07062015122845_Vehicles.png', '08062015202535_Vehicles-big.png', 2, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-10-08 01:37:54', '1', '0', '0000-00-00 00:00:00'),
(3, 'Buy & Sell', 'Buy & Sell', 'buy-sell', 'Buy & Sell', 0, '1', '07062015122925_Shopping.png', '08062015202604_Kids-and-Baby-and-Products.png', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-10-08 01:37:35', '1', '0', '0000-00-00 00:00:00'),
(49, 'Baby Items', '', '', NULL, 3, '0', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(4, 'Jobs', 'Jobs', 'jobs', NULL, 0, '1', '07062015122952_jobs.png', '08062015202619_Jobs-categ.png', 3, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-10-08 01:37:54', '1', '0', '0000-00-00 00:00:00'),
(6, 'Services', 'Services', 'services', 'Services', 0, '1', '07062015123116_jobs.png', '08062015202726_Services.png', 6, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-08-11 02:33:52', '1', '0', '0000-00-00 00:00:00'),
(7, 'Community', 'Community', 'community', '<p>\n	Community</p>\n', 0, '1', '08102015124450_iocn01.png', '07102015161335_Home141.png', 7, '1', '', '', '', 1, 2, '2015-07-30 18:00:00', '2015-10-08 12:44:50', '1', '0', '0000-00-00 00:00:00'),
(8, 'Property for Rent', '', '', NULL, 1, '1', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(9, 'Property for Sale ', '', '', NULL, 1, '1', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(10, 'Vacation Rentals ', '', '', NULL, 1, '1', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(11, 'Used Cars/Vans', '', '', NULL, 2, '0', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(12, 'Heavy Vehicles/Machinery - Forklifts, Trucks, Backhoe, Tractor, Bulldozer', '', '', NULL, 2, '0', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(13, 'Boats/Marine Equipment', '', '', NULL, 2, '0', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(14, 'Motorcycles - ATVs, Bikes', '', '', '', 2, '0', '', '', 0, '1', '', '', '', 1, 2, '2015-07-30 18:00:00', '2015-10-07 19:11:28', '1', '0', '0000-00-00 00:00:00'),
(15, 'Accessories/Parts', '', '', NULL, 2, '0', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(16, 'Jobs -  Vacancies', '', '', NULL, 4, '1', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(17, 'Jobs  - Wanted', '', '', NULL, 4, '1', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(18, 'Automotive Services', '', '', NULL, 6, '0', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(19, 'Career/HR Services', '', '', NULL, 6, '0', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(20, 'Child & Elderly Care', '', '', NULL, 6, '0', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(21, 'Computer/Tech Services', '', '', NULL, 6, '0', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(22, 'Construction/ Labour', '', '', NULL, 6, '0', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(23, 'Creative/Design Services', '', '', NULL, 6, '0', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(24, 'Event Services', '', '', NULL, 6, '0', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(25, 'Health/Beauty Services', '', '', NULL, 6, '0', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(26, 'Home Services', '', '', NULL, 6, '0', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(27, 'Insurance/Financial Services', '', '', NULL, 6, '0', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(28, 'Lawn/Garden Services', '', '', NULL, 6, '0', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(29, 'Legal Services', '', '', NULL, 6, '0', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(30, 'Moving/Storage Services', '', '', NULL, 6, '0', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(31, 'Office Services', '', '', NULL, 6, '0', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(32, 'Real Estate Services', '', '', NULL, 6, '0', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(33, 'Travel Services', '', '', NULL, 6, '0', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(34, 'Tutoring', '', '', NULL, 6, '0', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(35, 'Other Services', '', '', NULL, 6, '0', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(36, 'Activities  ', '', '', NULL, 7, '0', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(37, 'Charities ', '', '', NULL, 7, '0', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(38, 'Childcare  ', '', '', NULL, 7, '0', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(39, 'Classes ', '', '', NULL, 7, '0', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(40, 'Clubs  ', '', '', NULL, 7, '0', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(41, 'Domestic ', '', '', NULL, 7, '0', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(42, 'Education ', '', '', NULL, 7, '0', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(43, 'Freelancers ', '', '', NULL, 7, '0', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(44, 'Misc. ', '', '', NULL, 7, '0', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(45, 'Music ', '', '', NULL, 7, '0', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(46, 'News  ', '', '', NULL, 7, '0', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(47, 'Photography  ', '', '', NULL, 7, '0', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(48, 'Sports  ?', '', '', NULL, 7, '0', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(50, 'Books', '', '', NULL, 3, '0', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(51, 'Business & Industrial', '', '', NULL, 3, '0', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(52, 'Cameras & Imaging', '', '', NULL, 3, '0', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(53, 'Clothing & Accessories', '', '', NULL, 3, '0', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(54, 'Collectibles', '', '', NULL, 3, '0', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(55, 'Computers & Networking', '', '', NULL, 3, '0', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(56, 'DVDs & Movies', '', '', NULL, 3, '0', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(57, 'Electronics', '', '', NULL, 3, '0', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(58, 'Free Stuff', '', '', NULL, 3, '0', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(59, 'Furniture, Home & Garden', '', '', NULL, 3, '0', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(60, 'Gaming', '', '', NULL, 3, '0', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(61, 'Home Appliances', '', '', NULL, 3, '0', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(62, 'Jewelry & Watches', '', '', NULL, 3, '0', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(63, 'Lost/Found', '', '', NULL, 3, '0', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(64, 'Misc.', '', '', NULL, 3, '0', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(65, 'Mobile Phones & Tablets', '', '', NULL, 3, '0', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(66, 'Music', '', '', NULL, 3, '0', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(67, 'Musical Instruments', '', '', NULL, 3, '0', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(68, 'Pets', '', '', NULL, 3, '0', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(69, 'Sports Equipment', '', '', NULL, 3, '0', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(70, 'Stuff Wanted', '', '', NULL, 3, '0', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(71, 'Tickets & Vouchers', '', '', NULL, 3, '0', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00'),
(72, 'Toys', '', '', NULL, 3, '0', '', '', 0, '1', NULL, NULL, NULL, 1, 1, '2015-07-30 18:00:00', '2015-07-31 11:59:36', '1', '0', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category_brand_search`
--

CREATE TABLE IF NOT EXISTS `tbl_category_brand_search` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `brand_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `brand_title` varchar(255) NOT NULL,
  `search_string` varchar(100) NOT NULL,
  `search_count` int(11) NOT NULL,
  `created_date_time` datetime NOT NULL,
  `updated_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `tbl_category_brand_search`
--

INSERT INTO `tbl_category_brand_search` (`id`, `brand_id`, `category_id`, `brand_title`, `search_string`, `search_count`, `created_date_time`, `updated_date_time`) VALUES
(1, 1, 2, 'Honda', 'Used Honda', 10, '2015-04-24 17:18:25', '2015-04-30 13:25:47'),
(2, 3, 2, 'Audi', 'Used Audi', 5, '2015-04-24 17:19:47', '2015-04-25 10:12:29'),
(3, 4, 2, 'Toyota', 'Used Toyota', 3, '2015-04-24 17:28:13', '2015-04-25 09:57:36'),
(4, 5, 2, 'Hyundai', 'Used Hyundai', 1, '2015-04-24 17:28:35', '2015-04-25 09:57:47'),
(5, 6, 1, 'Lamborghini', 'New Lamborghini', 1, '2015-04-24 17:28:56', '2015-04-25 09:58:08'),
(6, 7, 4, 'Mercedes-Benz', 'For Export Mercedes-Benz', 18, '2015-04-24 17:29:16', '2015-04-25 10:12:34'),
(7, 6, 2, 'Lamborghini', 'Used Lamborghini', 1, '2015-04-24 19:10:13', '2015-04-25 11:39:25'),
(8, 4, 1, 'Toyota', 'New Toyota', 2, '2015-04-25 04:04:08', '2015-04-25 20:33:30'),
(9, 5, 5, 'Hyundai', ' Hyundai', 6, '2015-04-25 04:04:44', '2015-04-25 20:43:51'),
(10, 1, 3, 'Honda', ' Honda', 2, '2015-04-29 20:30:17', '2015-04-30 13:01:08'),
(11, 3, 3, 'Audi', ' Audi', 1, '2015-04-29 20:33:23', '2015-04-30 13:02:35');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_category_old`
--

CREATE TABLE IF NOT EXISTS `tbl_category_old` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `sub_title` varchar(255) NOT NULL,
  `category_slug` varchar(255) NOT NULL,
  `description` text,
  `parent_id` int(11) NOT NULL,
  `category_image` varchar(255) NOT NULL,
  `category_icon_large` varchar(255) NOT NULL,
  `category_order` int(11) NOT NULL,
  `on_header` enum('0','1') NOT NULL DEFAULT '1',
  `meta_title` text,
  `meta_desc` text,
  `meta_keywords` text,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_active` enum('0','1') NOT NULL DEFAULT '0',
  `is_deleted` enum('0','1') NOT NULL DEFAULT '0',
  `deleted_date_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `tbl_category_old`
--

INSERT INTO `tbl_category_old` (`id`, `title`, `sub_title`, `category_slug`, `description`, `parent_id`, `category_image`, `category_icon_large`, `category_order`, `on_header`, `meta_title`, `meta_desc`, `meta_keywords`, `created_by`, `updated_by`, `created_date`, `updated_date`, `is_active`, `is_deleted`, `deleted_date_time`) VALUES
(1, 'Vehicles', 'Vehicles', 'new-cars', '<p>\r\n	Vehicles</p>\r\n', 0, '07062015122845_Vehicles.png', '08062015202403_Real-Estate-categ.png', 3, '1', '', 'Vehicles', 'Vehicles', 2, 2, '2015-02-26 06:37:03', '2015-08-11 02:31:36', '1', '0', '0000-00-00 00:00:00'),
(2, 'Real Estate', 'Real Estate', 'used-cars', '<p>\r\n	Used Cars</p>\r\n', 0, '07062015122758_real-estate.png', '08062015202535_Vehicles-big.png', 1, '1', '', 'Real Estate', 'Real Estate', 2, 2, '2015-02-26 06:38:12', '2015-08-11 02:31:03', '1', '0', '0000-00-00 00:00:00'),
(20, 'Flat For Sale', 'Flat For Sale', 'condos-for-sale', '<p>\n	Flat For Sale</p>\n', 2, '', '', 20, '1', '', 'Condos For Sale ', 'Condos For Sale ', 2, 2, '2015-06-07 12:49:22', '2015-06-14 17:28:06', '1', '0', '0000-00-00 00:00:00'),
(22, 'Car Wash', '', 'car-wash', '', 18, '', '', 22, '1', '', '', '', 2, 2, '2015-06-17 15:25:02', '2015-06-18 02:24:38', '1', '0', '0000-00-00 00:00:00'),
(4, 'Shopping', 'Shopping', 'for-export', '<h2 style="margin: 0px; padding: 0px; border: 0px; font-family: open_sansregular; font-size: 20px; font-weight: inherit; font-stretch: inherit; vertical-align: baseline; color: rgb(70, 70, 70);">\r\n	Shopping</h2>\r\n', 0, '07062015122925_Shopping.png', '08062015202604_Kids-and-Baby-and-Products.png', 4, '1', '', 'For Export', 'For Export', 2, 2, '2015-02-26 06:40:59', '2015-06-09 14:25:16', '1', '0', '0000-00-00 00:00:00'),
(5, 'Jobs', 'Jobs', 'auto-parts-and-accessories', '<p>\r\n	Jobs</p>\r\n', 0, '07062015122952_jobs.png', '08062015202619_Jobs-categ.png', 5, '0', '', 'Auto Parts and Accessories', 'Auto Parts and Accessories', 2, 2, '2015-02-26 06:41:18', '2015-06-09 14:25:31', '1', '0', '0000-00-00 00:00:00'),
(6, 'Community', 'Community', 'imported-bikes', '<h2 style="margin: 0px; padding: 0px; border: 0px; font-family: open_sansregular; font-size: 20px; font-weight: inherit; font-stretch: inherit; vertical-align: baseline; color: rgb(70, 70, 70);">\n	Community</h2>\n', 0, '07062015123034_Community.png', '18062015054336_multy-user.png', 6, '1', '', 'Community', 'Community', 2, 2, '2015-04-17 23:10:39', '2015-06-18 16:43:12', '1', '0', '0000-00-00 00:00:00'),
(19, 'Commercial Real Estate', 'Commercial Real Estate', 'commercial-real-estate', '<p>\r\n	<strong>Commercial Real Estate</strong></p>\r\n', 2, '', '', 19, '1', 'Commercial Real Estate', 'Commercial Real Estate', 'Commercial Real Estate', 2, 2, '2015-06-07 12:48:37', '2015-06-08 06:47:49', '1', '0', '0000-00-00 00:00:00'),
(21, 'Farms & Ranches', 'Farms & Ranches', 'farms---ranches', '<p>\r\n	Farms &amp; Ranches</p>\r\n', 2, '', '', 21, '1', 'Farms & Ranches', 'Farms & Ranches', 'Farms & Ranches', 2, 2, '2015-06-07 12:49:55', '2015-06-08 06:49:07', '1', '0', '0000-00-00 00:00:00'),
(18, 'Services', 'Services', 'services', '<p>\r\n	Services</p>\r\n', 0, '07062015123116_jobs.png', '08062015202726_Services.png', 18, '1', '', 'Services', 'Services', 2, 2, '2015-06-07 12:31:16', '2015-06-09 14:26:38', '1', '0', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cities`
--

CREATE TABLE IF NOT EXISTS `tbl_cities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_code` varchar(10) DEFAULT NULL,
  `region` varchar(256) DEFAULT NULL,
  `population` int(10) unsigned DEFAULT NULL,
  `latitude` varchar(256) DEFAULT NULL,
  `longitude` varchar(256) DEFAULT NULL,
  `combined` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `combined` (`combined`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `tbl_cities`
--

INSERT INTO `tbl_cities` (`id`, `country_code`, `region`, `population`, `latitude`, `longitude`, `combined`) VALUES
(1, NULL, NULL, NULL, NULL, NULL, 'Central'),
(2, NULL, NULL, NULL, NULL, NULL, 'North-East'),
(3, NULL, NULL, NULL, NULL, NULL, 'North-West'),
(4, NULL, NULL, NULL, NULL, NULL, 'South-East'),
(5, NULL, NULL, NULL, NULL, NULL, 'South-West'),
(6, NULL, NULL, NULL, NULL, NULL, 'Tobago-East'),
(7, NULL, NULL, NULL, NULL, NULL, 'Tobago-West');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cities_old`
--

CREATE TABLE IF NOT EXISTS `tbl_cities_old` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_code` varchar(10) DEFAULT NULL,
  `region` varchar(256) DEFAULT NULL,
  `population` int(10) unsigned DEFAULT NULL,
  `latitude` varchar(256) DEFAULT NULL,
  `longitude` varchar(256) DEFAULT NULL,
  `combined` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `combined` (`combined`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=813 ;

--
-- Dumping data for table `tbl_cities_old`
--

INSERT INTO `tbl_cities_old` (`id`, `country_code`, `region`, `population`, `latitude`, `longitude`, `combined`) VALUES
(96, 'AE', '01', 603687, '24.4666667', '54.3666667', 'Abu Dhabi'),
(115, 'AE', '02', 0, '25.4061111', '55.4427778', 'Ajman'),
(116, 'AE', '', 0, '', '', 'Al AIn'),
(369, 'AE', '03', 1137376, '25.2522222', '55.28', 'Dubai'),
(404, 'AE', '04', 0, '25.1230556', '56.3375', 'Fujairah'),
(712, 'AE', '05', 0, '25.7911111', '55.9427778', 'Ras al Khaimah'),
(765, 'AE', '06', 543942, '25.3622222', '55.3911111', 'Sharjah'),
(812, 'AE', '07', 0, '25.5652778', '55.5533333', 'Umm Al Quwain');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_city`
--

CREATE TABLE IF NOT EXISTS `tbl_city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_id` int(11) NOT NULL,
  `city_title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_classified`
--

CREATE TABLE IF NOT EXISTS `tbl_classified` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `classified_slug` varchar(255) NOT NULL,
  `classified_tags` text,
  `description` text,
  `category_id` int(11) DEFAULT NULL,
  `sub_category_id` int(11) NOT NULL,
  `tertiary_category_id` int(11) NOT NULL,
  `classified_contact_no` varchar(255) DEFAULT NULL,
  `classified_contact_no_opt` varchar(255) DEFAULT NULL,
  `classified_contact_name` varchar(255) DEFAULT NULL,
  `classified_contact_email` varchar(255) DEFAULT NULL,
  `is_futured` enum('0','1') NOT NULL DEFAULT '0',
  `classifed_modal` varchar(255) NOT NULL,
  `amount` int(11) NOT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `model_id` int(11) DEFAULT NULL,
  `fuel_type` int(11) DEFAULT NULL,
  `manufacture_year` int(4) NOT NULL,
  `running_km` int(11) NOT NULL,
  `created_by` int(11) NOT NULL COMMENT 'Member ID',
  `created_date_time` datetime NOT NULL,
  `updated_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `classified_address` varchar(255) DEFAULT NULL,
  `classified_land_mark` varchar(255) DEFAULT NULL,
  `classified_country` int(11) DEFAULT NULL,
  `classified_city` int(11) DEFAULT NULL,
  `classified_city_distance` tinyint(4) NOT NULL,
  `classified_locality` int(11) NOT NULL,
  `number_of_hits` int(11) DEFAULT '0',
  `meta_title` text,
  `meta_desc` text,
  `meta_keywords` text,
  `classified_ip` varchar(255) DEFAULT NULL,
  `small_description` varchar(255) DEFAULT NULL,
  `is_featured` enum('0','1') NOT NULL DEFAULT '0',
  `is_deleted` enum('0','1') NOT NULL DEFAULT '0',
  `deleted_date_time` datetime DEFAULT NULL,
  `is_active` enum('0','1') NOT NULL DEFAULT '0',
  `milage` varchar(45) DEFAULT NULL,
  `body_type` varchar(45) DEFAULT NULL,
  `no_of_doors` varchar(255) NOT NULL,
  `colour` varchar(45) DEFAULT NULL,
  `transmission` int(11) DEFAULT NULL,
  `engine_size` varchar(45) DEFAULT NULL,
  `entry_type` enum('Individual','Dealer') NOT NULL DEFAULT 'Individual',
  `career_level` int(11) NOT NULL,
  `experience_years` int(11) NOT NULL,
  `education` int(11) NOT NULL,
  `employment_type` int(11) NOT NULL,
  `bedrooms` int(11) NOT NULL,
  `bathrooms` int(11) NOT NULL,
  `area_sqft` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=158 ;

--
-- Dumping data for table `tbl_classified`
--

INSERT INTO `tbl_classified` (`id`, `title`, `classified_slug`, `classified_tags`, `description`, `category_id`, `sub_category_id`, `tertiary_category_id`, `classified_contact_no`, `classified_contact_no_opt`, `classified_contact_name`, `classified_contact_email`, `is_futured`, `classifed_modal`, `amount`, `brand_id`, `model_id`, `fuel_type`, `manufacture_year`, `running_km`, `created_by`, `created_date_time`, `updated_date_time`, `classified_address`, `classified_land_mark`, `classified_country`, `classified_city`, `classified_city_distance`, `classified_locality`, `number_of_hits`, `meta_title`, `meta_desc`, `meta_keywords`, `classified_ip`, `small_description`, `is_featured`, `is_deleted`, `deleted_date_time`, `is_active`, `milage`, `body_type`, `no_of_doors`, `colour`, `transmission`, `engine_size`, `entry_type`, `career_level`, `experience_years`, `education`, `employment_type`, `bedrooms`, `bathrooms`, `area_sqft`) VALUES
(135, 'Villa - in Bon Accord', 'villa---in-bon-accord', NULL, '<p>Villa in Bon Accord - Avialble for Christmas and New Years.</p>', 1, 8, 0, '+18686304321', NULL, NULL, NULL, '0', '', 1500, NULL, NULL, NULL, 0, 0, 33, '2015-10-16 18:48:12', '2015-12-20 11:51:36', NULL, NULL, NULL, 7, 0, 122, 2, NULL, NULL, NULL, '94.207.225.101', NULL, '0', '0', NULL, '1', NULL, NULL, '', NULL, NULL, NULL, 'Individual', 0, 0, 0, 0, 4, 4, 3000),
(134, 'HP Printer', 'hp-printer', NULL, '<p>Like new HP printer, two sets of ink cartridges available for free with printer. Minimum usage. </p>', 3, 57, 0, '+18686441234', NULL, NULL, NULL, '0', '', 500, NULL, NULL, NULL, 0, 0, 33, '2015-10-16 18:45:41', '2016-01-12 08:43:40', NULL, NULL, NULL, 6, 0, 111, 4, NULL, NULL, NULL, '94.207.225.101', NULL, '0', '0', NULL, '1', NULL, NULL, '', NULL, NULL, NULL, 'Individual', 0, 0, 0, 0, 0, 0, 0),
(132, 'Dummy item under community', 'dummy-item-under-community', '', '<p>\n	Dummy item under community</p>\n', 7, 36, 0, '+1868343333', NULL, '', '', '1', '', 600, 0, 0, NULL, 2015, 0, 2, '2015-10-07 16:09:19', '2015-11-23 06:21:58', '', '', 0, 2, 0, 0, 2, '', '', '', '5.32.65.74', '', '0', '0', NULL, '1', '', '', '', '', 0, '', 'Individual', 0, 0, 0, 0, 0, 0, 0),
(133, 'Excellent Condition Civic, inlcuding custom modifications', 'excellent-condition-civic--inlcuding-custom-modifications', '0', '<p>Civic in immaculate condition, used mostly for car shows, with custom modification. Must See.</p>', 2, 11, 0, '+18686304321', NULL, '0', '0', '', '', 185000, 1, 0, 3, 2014, 75600, 33, '2015-10-16 18:32:29', '2015-12-20 11:50:49', '0', '0', 0, 4, 0, 80, 3, '0', '0', '0', '94.207.225.101', '0', '0', '0', NULL, '1', '0', '0', '0', '0', 1, '0', '', 0, 0, 0, 0, 0, 0, 0),
(131, 'Dummy item under services', 'dummy-item-under-services', '', '<p>\n	Dummy item under services</p>\n', 6, 27, 0, '+186832323', NULL, '', '', '1', '', 500, 0, 0, NULL, 2015, 0, 2, '2015-10-07 16:08:44', '2015-11-23 06:21:10', '', '', 0, 1, 0, 2, 2, '', '', '', '5.32.65.74', '', '0', '0', NULL, '1', '', '', '', '', 0, '', 'Individual', 0, 0, 0, 0, 0, 0, 0),
(130, 'Dummy item under Vehicles', 'dummy-item-under-vehicles', '', '<p>\n	Dummy item under Vehicles</p>\n', 2, 12, 0, '+18683443243', NULL, '', '', '1', '', 300, 0, 0, 1, 2015, 5000, 2, '2015-10-07 16:07:59', '2016-04-04 12:12:14', '', '', 0, 1, 0, 1, 3, '', '', '', '5.32.65.74', '', '0', '0', NULL, '1', '', '', '', '', 0, '', 'Individual', 0, 0, 0, 0, 0, 0, 0),
(128, 'Dummy item under buy and sell', 'dummy-item-under-buy-and-sell', '', '', 3, 60, 0, '+1868423434', NULL, '', '', '1', '', 200, 0, 0, NULL, 2015, 0, 2, '2015-10-07 16:06:10', '2015-11-23 06:18:32', '', '', 0, 1, 0, 1, 1, '', '', '', '5.32.65.74', '', '0', '0', NULL, '1', '', '', '', '', 0, '', 'Individual', 0, 0, 0, 0, 0, 0, 0),
(129, 'Dummy item under real estate', 'dummy-item-under-real-estate', '0', '<p>\n	Dummy item under real estate</p>\n', 1, 9, 0, '+1868324234', NULL, '0', '', '1', '', 250, 0, 0, NULL, 2015, 0, 2, '2015-10-07 16:07:06', '2015-11-23 06:19:27', '0', '0', 0, 1, 0, 2, 3, '0', '0', '0', '5.32.65.74', '0', '0', '0', NULL, '1', '0', '', '', '0', 0, '', 'Individual', 0, 0, 0, 0, 0, 0, 0),
(127, 'Dummy job title', 'property-for-rent---india', '0', '<p>\n	Please do not post duplicate ads. All duplicate and spam ads will be deleted</p>\n', 4, 0, 0, '+18688888888874', NULL, '0', '', '1', '', 100, 0, 0, NULL, 2015, 0, 2, '2015-10-07 19:32:17', '2015-10-08 12:43:40', '0', '0', 0, 1, 0, 1, 0, '0', '0', '0', '192.168.1.120', '0', '0', '0', NULL, '1', '0', '', '', '0', 0, '', 'Individual', 0, 0, 0, 0, 0, 0, 0),
(136, 'Used feeding chairs', 'used-feeding-chairs', NULL, '<p>Used feeding chairsUsed feeding chairsUsed feeding chairsUsed feeding chairs</p>', 3, 49, 0, '+1868 656565', NULL, NULL, NULL, '0', '', 200, NULL, NULL, NULL, 0, 0, 27, '2015-11-19 13:23:33', '2015-11-19 13:24:19', NULL, NULL, NULL, 2, 0, 20, 3, NULL, NULL, NULL, '5.32.65.74', NULL, '0', '0', NULL, '0', NULL, NULL, '', NULL, NULL, NULL, 'Individual', 0, 0, 0, 0, 0, 0, 0),
(137, 'Shared Bedspace', 'shared-bedspace', NULL, '', 1, 8, 0, '+1868 656565', NULL, NULL, NULL, '0', '', 2000, NULL, NULL, NULL, 0, 0, 27, '2015-11-19 13:26:36', '2015-11-23 07:37:25', NULL, NULL, NULL, 1, 0, 1, 1, NULL, NULL, NULL, '5.32.65.74', NULL, '0', '0', NULL, '0', NULL, NULL, '', NULL, NULL, NULL, 'Individual', 0, 0, 0, 0, 5, 2, 2000),
(138, 'Looking for job as Junior', 'looking-for-job-as-junior', NULL, '<p>\n	Looking for job as JuniorLooking for job as JuniorLooking for job as JuniorLooking for job as Junior</p>\n', 4, 17, 0, '+1868 656565', NULL, NULL, NULL, '0', '', 2000, NULL, 0, NULL, 0, 0, 2, '2015-11-23 07:41:21', '2015-11-23 15:06:10', NULL, NULL, NULL, 1, 0, 1, 1, '', '', '', '5.32.65.74', NULL, '0', '0', NULL, '1', NULL, NULL, '', NULL, NULL, NULL, 'Individual', 2, 5, 1, 1, 0, 0, 0),
(139, 'Beach Front Property - Mayaro', 'beach-front-property---mayaro', NULL, '<p>3 bedroom, beach front property, air condition. St Anns, Mayaro</p>', 1, 8, 0, '+18683011104', NULL, NULL, NULL, '0', '', 1000, NULL, NULL, NULL, 0, 0, 33, '2016-03-01 20:29:50', '2016-03-01 20:38:19', NULL, NULL, NULL, 4, 0, 79, 0, NULL, NULL, NULL, '94.207.225.101', NULL, '0', '0', NULL, '1', NULL, NULL, '', NULL, NULL, NULL, 'Individual', 0, 0, 0, 0, 3, 0, 0),
(140, 'Assistant HairStylists', 'assistant-hairstylists', NULL, '<p>Top hair dresser sallon, require fulltime / part time assistant Hair Stylists in Price Plaza. Please Call 671-4103</p>', 4, 16, 0, '+18686714103', NULL, NULL, NULL, '0', '', 3000, NULL, NULL, NULL, 0, 0, 33, '2016-03-01 20:33:47', '2016-04-03 09:29:18', NULL, NULL, NULL, 1, 0, 4, 2, NULL, NULL, NULL, '94.207.225.101', NULL, '0', '0', NULL, '1', NULL, NULL, '', NULL, NULL, NULL, 'Individual', 0, 0, 0, 2, 0, 0, 0),
(141, 'Sultan Wrecking', 'sultan-wrecking', NULL, '<p>Transport / Auto Rentals, 24hrs Nationwide. $500 per Wreck Special conditions apply. Call 384-8391, 730-8391</p>', 6, 18, 0, '+18683848391', NULL, NULL, NULL, '0', '', 500, NULL, NULL, NULL, 0, 0, 33, '2016-03-01 20:36:34', '2016-03-01 20:38:14', NULL, NULL, NULL, 1, 0, 4, 0, NULL, NULL, NULL, '94.207.225.101', NULL, '0', '0', NULL, '1', NULL, NULL, '', NULL, NULL, NULL, 'Individual', 0, 0, 0, 0, 0, 0, 0),
(142, '8ft Slate Pool Board', '8ft-slate-pool-board', NULL, '<p>8ft Slate pool Board - 3yrs old in great condition. </p>', 3, 60, 0, '+18686477566', NULL, NULL, NULL, '0', '', 4950, NULL, NULL, NULL, 0, 0, 33, '2016-03-30 05:13:09', '2016-03-30 05:40:16', NULL, NULL, NULL, 2, 0, 21, 0, NULL, NULL, NULL, '94.207.225.101', NULL, '0', '0', NULL, '1', NULL, NULL, '', NULL, NULL, NULL, 'Individual', 0, 0, 0, 0, 0, 0, 0),
(143, 'Barber Chair', 'barber-chair', NULL, '<p>Barber Chair in excellent condition. Imported and only used for 2 months. Priced for quick sale. A Bartolo brand. </p>', 3, 59, 0, '+18683635694', NULL, NULL, NULL, '0', '', 700, NULL, NULL, NULL, 0, 0, 33, '2016-03-30 05:18:40', '2016-04-04 11:55:12', NULL, NULL, NULL, 5, 0, 85, 1, NULL, NULL, NULL, '94.207.225.101', NULL, '0', '0', NULL, '1', NULL, NULL, '', NULL, NULL, NULL, 'Individual', 0, 0, 0, 0, 0, 0, 0),
(144, '1 bedroom - Fully furnished', '1-bedroom---fully-furnished', NULL, '<p>One bedroom, fully furnished, spacious, covered parking, air condition, internet, private, comfortable. </p>', 1, 8, 0, '+18683630445', NULL, NULL, NULL, '0', '', 3200, NULL, NULL, NULL, 0, 0, 33, '2016-03-30 05:23:59', '2016-03-30 05:40:06', NULL, NULL, NULL, 1, 0, 10, 0, NULL, NULL, NULL, '94.207.225.101', NULL, '0', '0', NULL, '1', NULL, NULL, '', NULL, NULL, NULL, 'Individual', 0, 0, 0, 0, 1, 1, 700),
(145, '1 bedroom - Fully furnished', '1-bedroom---fully-furnished-1', NULL, '<p>One bedroom, fully furnished, spacious, covered parking, air condition, internet, private, comfortable. </p>', 1, 8, 0, '+18683630445', NULL, NULL, NULL, '0', '', 3200, NULL, NULL, NULL, 0, 0, 33, '2016-03-30 05:24:09', '2016-03-30 05:24:09', NULL, NULL, NULL, 1, 0, 10, 0, NULL, NULL, NULL, '94.207.225.101', NULL, '0', '0', NULL, '0', NULL, NULL, '', NULL, NULL, NULL, 'Individual', 0, 0, 0, 0, 1, 1, 700),
(146, '1 bedroom - Fully furnished', '1-bedroom---fully-furnished-2', NULL, '<p>One bedroom, fully furnished, spacious, covered parking, air condition, internet, private, comfortable. </p>', 1, 8, 0, '+18683630445', NULL, NULL, NULL, '0', '', 3200, NULL, NULL, NULL, 0, 0, 33, '2016-03-30 05:25:11', '2016-03-30 05:25:11', NULL, NULL, NULL, 1, 0, 10, 0, NULL, NULL, NULL, '94.207.225.101', NULL, '0', '0', NULL, '0', NULL, NULL, '', NULL, NULL, NULL, 'Individual', 0, 0, 0, 0, 1, 1, 700),
(147, 'TEST3', 'test3', NULL, '<p>TEST3</p>', 3, 49, 0, '+1868 123456789', NULL, NULL, NULL, '0', '', 1500, NULL, NULL, NULL, 0, 0, 38, '2016-03-31 09:23:09', '2016-03-31 09:23:09', NULL, NULL, NULL, 1, 0, 1, 0, NULL, NULL, NULL, '5.32.65.74', NULL, '0', '0', NULL, '0', NULL, NULL, '', NULL, NULL, NULL, 'Individual', 0, 0, 0, 0, 0, 0, 0),
(148, 'TEST5', 'test5', NULL, '<p>TEST5</p>', 1, 8, 0, '+1868 123456789', NULL, NULL, NULL, '0', '', 1900, NULL, NULL, NULL, 0, 0, 38, '2016-03-31 09:45:28', '2016-03-31 09:45:28', NULL, NULL, NULL, 1, 0, 1, 0, NULL, NULL, NULL, '5.32.65.74', NULL, '0', '0', NULL, '0', NULL, NULL, '', NULL, NULL, NULL, 'Individual', 0, 0, 0, 0, 1, 1, 1),
(149, 'test ''t', 'test--t', NULL, '<p>test ''t</p>', 1, 8, 0, '0503560927', '0558086247', NULL, NULL, '0', '', 1000, NULL, NULL, NULL, 0, 0, 38, '2016-04-02 12:38:03', '2016-04-02 12:38:18', NULL, NULL, NULL, 2, 0, 31, 1, NULL, NULL, NULL, '5.32.65.74', NULL, '0', '0', NULL, '0', NULL, NULL, '', NULL, NULL, NULL, 'Individual', 0, 0, 0, 0, 1, 1, 12222),
(150, 'GE Black 4 Burner Stove', 'ge-black-4-burner-stove', NULL, '<p>Like new - GE Black - 4Burner Stove. Item in excellent condition. 2 year warranty. </p>', 3, 61, 0, '+18686899410', '+18686833733', NULL, NULL, '0', '', 2800, NULL, NULL, NULL, 0, 0, 33, '2016-04-02 22:30:12', '2016-04-02 22:30:12', NULL, NULL, NULL, 5, 0, 87, 0, NULL, NULL, NULL, '95.154.201.143', NULL, '0', '0', NULL, '0', NULL, NULL, '', NULL, NULL, NULL, 'Individual', 0, 0, 0, 0, 0, 0, 0),
(151, 'GE Black 4 Burner Stove', 'ge-black-4-burner-stove-1', NULL, '<p>Like new - GE Black - 4Burner Stove. Item in excellent condition. 2 year warranty. </p>', 3, 61, 0, '+18686899410', '', NULL, NULL, '0', '', 2800, NULL, NULL, NULL, 0, 0, 33, '2016-04-02 22:31:17', '2016-04-02 22:31:17', NULL, NULL, NULL, 5, 0, 87, 0, NULL, NULL, NULL, '95.154.201.143', NULL, '0', '0', NULL, '0', NULL, NULL, '', NULL, NULL, NULL, 'Individual', 0, 0, 0, 0, 0, 0, 0),
(152, 'GE Black 4 Burner Stove', 'ge-black-4-burner-stove-2', NULL, '<p>Like new - GE Black - 4Burner Stove. Item in excellent condition. 2 year warranty. </p>', 3, 61, 0, '18686899410', '', NULL, NULL, '0', '', 2800, NULL, NULL, NULL, 0, 0, 33, '2016-04-02 22:31:28', '2016-04-02 22:31:28', NULL, NULL, NULL, 5, 0, 87, 0, NULL, NULL, NULL, '95.154.201.143', NULL, '0', '0', NULL, '0', NULL, NULL, '', NULL, NULL, NULL, 'Individual', 0, 0, 0, 0, 0, 0, 0),
(153, 'GE Black 4 Burner Stove', 'ge-black-4-burner-stove-3', NULL, '<p>Like new - GE Black - 4Burner Stove. Item in excellent condition. 2 year warranty. </p>', 3, 61, 0, '18686899410', '', NULL, NULL, '0', '', 2800, NULL, NULL, NULL, 0, 0, 33, '2016-04-02 22:31:43', '2016-04-02 22:31:43', NULL, NULL, NULL, 5, 0, 87, 0, NULL, NULL, NULL, '95.154.201.143', NULL, '0', '0', NULL, '0', NULL, NULL, '', NULL, NULL, NULL, 'Individual', 0, 0, 0, 0, 0, 0, 0),
(154, 'Honda Sale', 'honda-sale', NULL, '<p>Honda Sale</p>', 2, 11, 0, '+18681234567890', '', NULL, NULL, '0', '', 100000, 1, 2, 1, 2016, 10, 38, '2016-04-03 07:49:37', '2016-04-03 07:52:40', NULL, NULL, NULL, 1, 0, 1, 0, NULL, NULL, NULL, '5.32.65.74', NULL, '0', '0', NULL, '1', NULL, NULL, '', NULL, 1, NULL, 'Individual', 0, 0, 0, 0, 0, 0, 0),
(155, 'test', 'test-3', NULL, '', 1, 8, 0, '+18681234567890', '', NULL, NULL, '0', '', 1, NULL, NULL, NULL, 0, 0, 38, '2016-04-03 09:20:09', '2016-04-03 09:47:03', NULL, NULL, NULL, 1, 0, 1, 1, NULL, NULL, NULL, '5.32.65.74', NULL, '0', '0', NULL, '1', NULL, NULL, '', NULL, NULL, NULL, 'Individual', 0, 0, 0, 0, 0, 0, 0),
(156, '1', '1-3', NULL, '', 1, 8, 0, '+18681234567890', '', NULL, NULL, '0', '', 1, NULL, NULL, NULL, 0, 0, 38, '2016-04-03 09:20:49', '2016-04-03 09:46:59', NULL, NULL, NULL, 1, 0, 0, 0, NULL, NULL, NULL, '5.32.65.74', NULL, '0', '0', NULL, '1', NULL, NULL, '', NULL, NULL, NULL, 'Individual', 0, 0, 0, 0, 0, 0, 0),
(157, '2000', '2000', NULL, '<p>\n	20000</p>\n', 1, 8, 0, '050', '', NULL, NULL, '0', '', 0, NULL, NULL, NULL, 0, 0, 2, '0000-00-00 00:00:00', '2016-04-04 11:57:31', NULL, NULL, NULL, NULL, 0, 0, 0, '', '', '', NULL, NULL, '0', '0', NULL, '0', NULL, NULL, '', NULL, NULL, NULL, 'Individual', 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_classified_image`
--

CREATE TABLE IF NOT EXISTS `tbl_classified_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `classified_id` int(11) NOT NULL,
  `classified_image` varchar(255) DEFAULT NULL,
  `is_image` enum('0','1') DEFAULT '1',
  `created_date_time` datetime NOT NULL,
  `is_active` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=245 ;

--
-- Dumping data for table `tbl_classified_image`
--

INSERT INTO `tbl_classified_image` (`id`, `classified_id`, `classified_image`, `is_image`, `created_date_time`, `is_active`) VALUES
(4, 1, '28022015101734_Featured-Ads-02.jpg', '1', '2015-02-28 10:17:34', '1'),
(5, 2, '04032015084702_audi.jpg', '1', '2015-03-04 08:47:02', '1'),
(6, 15, '1.jpg', '1', '0000-00-00 00:00:00', '1'),
(7, 15, '2.jpg', '1', '0000-00-00 00:00:00', '1'),
(8, 15, '3.jpg', '1', '0000-00-00 00:00:00', '1'),
(9, 15, '4.jpg', '1', '0000-00-00 00:00:00', '1'),
(10, 51, '515538121e5ab626.61160771-20150423025654.png', '1', '2015-04-23 02:56:54', '0'),
(11, 51, '515538121e5b1029.60989969-20150423025654.jpg', '1', '2015-04-23 02:56:54', '0'),
(12, 51, '515538121e5b5016.53736812-20150423025654.jpg', '1', '2015-04-23 02:56:54', '0'),
(13, 51, '515538121e5b9a74.55004385-20150423025654.jpg', '1', '2015-04-23 02:56:54', '0'),
(14, 51, '515538121e5be5e1.03546651-20150423025654.jpg', '1', '2015-04-23 02:56:54', '0'),
(15, 52, '52553812957420b8.82568951-20150423025853.png', '1', '2015-04-23 02:58:53', '0'),
(16, 52, '5255381295746ce8.63094202-20150423025853.jpg', '1', '2015-04-23 02:58:53', '0'),
(17, 52, '525538129574a669.93926493-20150423025853.jpg', '1', '2015-04-23 02:58:53', '0'),
(18, 52, '525538129574f587.60152064-20150423025853.jpg', '1', '2015-04-23 02:58:53', '0'),
(19, 52, '5255381295754615.09208982-20150423025853.jpg', '1', '2015-04-23 02:58:53', '0'),
(20, 53, '5355381425c6b008.02285007-20150423030533.png', '1', '2015-04-23 03:05:33', '0'),
(21, 53, '5355381425c70a18.33212829-20150423030533.jpg', '1', '2015-04-23 03:05:33', '0'),
(22, 53, '5355381425c74984.55519833-20150423030533.jpg', '1', '2015-04-23 03:05:33', '0'),
(23, 53, '5355381425c78909.76453851-20150423030533.jpg', '1', '2015-04-23 03:05:33', '0'),
(24, 53, '5355381425c7c857.44297517-20150423030533.jpg', '1', '2015-04-23 03:05:33', '0'),
(25, 54, '54553815adcc7844.40151812-20150423031205.jpg', '1', '2015-04-23 03:12:05', '0'),
(26, 54, '54553815adccd335.39370506-20150423031205.jpg', '1', '2015-04-23 03:12:05', '0'),
(27, 54, '54553815adcd1569.52387472-20150423031205.jpg', '1', '2015-04-23 03:12:05', '0'),
(28, 54, '54553815adcd5a26.52861178-20150423031205.jpg', '1', '2015-04-23 03:12:05', '0'),
(29, 55, '55553815d27119c4.73183115-20150423031242.jpg', '1', '2015-04-23 03:12:42', '1'),
(30, 55, '55553815d2717912.30886201-20150423031242.jpg', '1', '2015-04-23 03:12:42', '1'),
(31, 55, '55553815d271d8b7.03913128-20150423031242.jpg', '1', '2015-04-23 03:12:42', '1'),
(32, 55, '55553815d2723667.72174459-20150423031242.jpg', '1', '2015-04-23 03:12:42', '1'),
(33, 56, '56553815e6b25fe5.74082806-20150423031302.jpg', '1', '2015-04-23 03:13:02', '1'),
(34, 56, '56553815e6b2b346.92562985-20150423031302.jpg', '1', '2015-04-23 03:13:02', '1'),
(35, 56, '56553815e6b2ecf9.98350652-20150423031302.jpg', '1', '2015-04-23 03:13:02', '1'),
(36, 56, '56553815e6b32366.93321166-20150423031302.jpg', '1', '2015-04-23 03:13:02', '1'),
(37, 57, '575538c404768696.87070022-20150423153556.', '1', '2015-04-23 15:35:56', '1'),
(38, 58, '585538c4348a3c72.10338778-20150423153644.', '1', '2015-04-23 15:36:44', '1'),
(44, 9, '95538c843a74402.00162936-20150423155403.jpg', '1', '2015-04-23 15:54:03', '1'),
(43, 9, '95538c843a703c9.41352643-20150423155403.jpg', '1', '2015-04-23 15:54:03', '1'),
(45, 70, '70553a47929248d9.83694737-20150424190930.jpg', '1', '2015-04-24 19:09:30', '1'),
(46, 70, '70553a4792979200.06566905-20150424190930.jpg', '1', '2015-04-24 19:09:30', '1'),
(47, 75, '75556ac2f6121b26.62539024-20150531121446.jpg', '1', '2015-05-31 12:14:46', '1'),
(48, 75, '75556ac2f6144399.78522497-20150531121446.jpg', '1', '2015-05-31 12:14:46', '1'),
(49, 75, '75556ac2f6146566.06133557-20150531121446.jpg', '1', '2015-05-31 12:14:46', '1'),
(50, 75, '75556ac2f6148241.14156769-20150531121446.jpg', '1', '2015-05-31 12:14:46', '1'),
(51, 75, '75556ac2f6149d50.77306626-20150531121446.jpg', '1', '2015-05-31 12:14:46', '1'),
(52, 76, '76556ac41c9beee5.74171752-20150531121940.jpg', '1', '2015-05-31 12:19:40', '1'),
(53, 76, '76556ac41c9c1998.08237318-20150531121940.jpg', '1', '2015-05-31 12:19:40', '1'),
(54, 76, '76556ac41c9c2229.50490586-20150531121940.jpg', '1', '2015-05-31 12:19:40', '1'),
(55, 77, '77556ae4d1c285c1.44669108-20150531143913.jpg', '1', '2015-05-31 14:39:13', '1'),
(56, 77, '77556ae4d1c6a3f2.53773433-20150531143913.jpg', '1', '2015-05-31 14:39:13', '1'),
(57, 77, '77556ae4d1c6bd83.49674310-20150531143913.jpg', '1', '2015-05-31 14:39:13', '1'),
(58, 83, '1.jpg', '1', '0000-00-00 00:00:00', '1'),
(59, 83, '2.jpg', '1', '0000-00-00 00:00:00', '1'),
(60, 83, '3.jpg', '1', '0000-00-00 00:00:00', '1'),
(61, 83, '4.jpg', '1', '0000-00-00 00:00:00', '1'),
(62, 84, 'car-big01.jpg', '1', '2015-02-28 10:17:34', '1'),
(63, 98, '98557d2110c6f1c6.53660840-20150614063704.jpg', '1', '2015-06-14 06:37:04', '1'),
(64, 98, '98557d2110d68614.95180585-20150614063704.jpg', '1', '2015-06-14 06:37:04', '1'),
(65, 98, '98557d2110d6a349.73732410-20150614063704.jpg', '1', '2015-06-14 06:37:04', '1'),
(66, 98, '98557d2110d6bd17.59405124-20150614063704.jpg', '1', '2015-06-14 06:37:04', '1'),
(67, 98, '98557d2110d6cb80.08467520-20150614063704.jpg', '1', '2015-06-14 06:37:04', '1'),
(68, 99, '9955804c6aa0cad0.98999160-20150616161850.jpg', '1', '2015-06-16 16:18:50', '1'),
(69, 99, '9955804c6aa0deb6.53276464-20150616161850.jpg', '1', '2015-06-16 16:18:50', '1'),
(70, 99, '9955804c6aa0e989.26795384-20150616161850.jpg', '1', '2015-06-16 16:18:50', '1'),
(71, 100, '10055804c6b5742b0.57648162-20150616161851.jpg', '1', '2015-06-16 16:18:51', '1'),
(72, 100, '10055804c6b5752b4.63033561-20150616161851.jpg', '1', '2015-06-16 16:18:51', '1'),
(73, 100, '10055804c6b575cc3.15436444-20150616161851.jpg', '1', '2015-06-16 16:18:51', '1'),
(74, 101, '101558bab9a6ed8d1.75626223-20150625071954.jpg', '1', '2015-06-25 07:19:54', '1'),
(75, 101, '101558bab9a7a3d35.93194494-20150625071954.jpg', '1', '2015-06-25 07:19:54', '1'),
(76, 101, '101558bab9a7a5f49.15124035-20150625071954.jpg', '1', '2015-06-25 07:19:54', '1'),
(77, 101, '101558bab9a7a7391.59488335-20150625071954.jpg', '1', '2015-06-25 07:19:54', '1'),
(78, 102, '102558befd0775df6.61447089-20150625121056.jpeg', '1', '2015-06-25 12:10:56', '1'),
(79, 102, '102558befd0777e61.89769255-20150625121056.jpeg', '1', '2015-06-25 12:10:56', '1'),
(80, 103, '103559b2e8550f199.40929658-20150707014229.JPG', '1', '2015-07-07 01:42:29', '1'),
(81, 103, '103559b2e857ce4d3.80619275-20150707014229.png', '1', '2015-07-07 01:42:29', '1'),
(82, 104, '104559b302da2f479.89179448-20150707014933.jpg', '1', '2015-07-07 01:49:33', '1'),
(83, 105, '10555dae7afb682d9.23293138-20150824094519.jpg', '1', '2015-08-24 09:45:19', '1'),
(84, 106, '10656151aa9089cc7.08921527-20151007171417.jpeg', '1', '2015-10-07 17:14:17', '1'),
(85, 106, '10656151aa9135c52.49531835-20151007171417.jpg', '1', '2015-10-07 17:14:17', '1'),
(86, 106, '10656151aa91376c7.31633676-20151007171417.jpg', '1', '2015-10-07 17:14:17', '1'),
(87, 106, '10656151aa9138cc5.33581550-20151007171417.jpg', '1', '2015-10-07 17:14:17', '1'),
(88, 106, '10656151aa913a1d7.72113241-20151007171417.jpeg', '1', '2015-10-07 17:14:17', '1'),
(89, 107, '10756151ade8223a5.56824186-20151007171510.jpeg', '1', '2015-10-07 17:15:10', '1'),
(90, 107, '10756151ade823d39.05222016-20151007171510.jpg', '1', '2015-10-07 17:15:10', '1'),
(91, 107, '10756151ade824305.12019944-20151007171510.jpg', '1', '2015-10-07 17:15:10', '1'),
(92, 107, '10756151ade824810.07856289-20151007171510.jpg', '1', '2015-10-07 17:15:10', '1'),
(93, 107, '10756151ade824ce1.69969106-20151007171510.jpeg', '1', '2015-10-07 17:15:10', '1'),
(94, 108, '10856151b34c6ec47.63856195-20151007171636.jpeg', '1', '2015-10-07 17:16:36', '1'),
(95, 108, '10856151b34c71766.51183700-20151007171636.jpg', '1', '2015-10-07 17:16:36', '1'),
(96, 108, '10856151b34c720a1.91664228-20151007171636.jpg', '1', '2015-10-07 17:16:36', '1'),
(97, 108, '10856151b34c72a58.01007666-20151007171636.jpg', '1', '2015-10-07 17:16:36', '1'),
(98, 108, '10856151b34c72ef1.23970347-20151007171636.jpeg', '1', '2015-10-07 17:16:36', '1'),
(99, 109, '10956151b7e8abb74.87734706-20151007171750.jpeg', '1', '2015-10-07 17:17:50', '1'),
(100, 109, '10956151b7e8ad788.33089101-20151007171750.jpg', '1', '2015-10-07 17:17:50', '1'),
(101, 109, '10956151b7e8add07.35702255-20151007171750.jpg', '1', '2015-10-07 17:17:50', '1'),
(102, 109, '10956151b7e8ae1d8.00683793-20151007171750.jpg', '1', '2015-10-07 17:17:50', '1'),
(103, 109, '10956151b7e8ae670.92837124-20151007171750.jpeg', '1', '2015-10-07 17:17:50', '1'),
(104, 110, '11056151b96b92c18.93555193-20151007171814.jpeg', '1', '2015-10-07 17:18:14', '1'),
(105, 110, '11056151b96b93901.38599022-20151007171814.jpg', '1', '2015-10-07 17:18:14', '1'),
(106, 110, '11056151b96b94305.35967184-20151007171814.jpg', '1', '2015-10-07 17:18:14', '1'),
(107, 110, '11056151b96b94b75.55849732-20151007171814.jpg', '1', '2015-10-07 17:18:14', '1'),
(108, 110, '11056151b96b957d3.72284220-20151007171814.jpeg', '1', '2015-10-07 17:18:14', '1'),
(109, 111, '11156151bc16505a2.55573603-20151007171857.jpeg', '1', '2015-10-07 17:18:57', '1'),
(110, 111, '11156151bc1653d23.07938823-20151007171857.jpg', '1', '2015-10-07 17:18:57', '1'),
(111, 111, '11156151bc1654947.82150576-20151007171857.jpg', '1', '2015-10-07 17:18:57', '1'),
(112, 111, '11156151bc1655416.66901310-20151007171857.jpg', '1', '2015-10-07 17:18:57', '1'),
(113, 111, '11156151bc1655e77.80064750-20151007171857.jpeg', '1', '2015-10-07 17:18:57', '1'),
(114, 112, '11256151bf3016546.44751592-20151007171947.jpeg', '1', '2015-10-07 17:19:47', '1'),
(115, 112, '11256151bf30173c4.84283196-20151007171947.jpg', '1', '2015-10-07 17:19:47', '1'),
(116, 112, '11256151bf3017d28.11784013-20151007171947.jpg', '1', '2015-10-07 17:19:47', '1'),
(117, 112, '11256151bf30187f3.07968516-20151007171947.jpg', '1', '2015-10-07 17:19:47', '1'),
(118, 112, '11256151bf30190f4.11837238-20151007171947.jpeg', '1', '2015-10-07 17:19:47', '1'),
(119, 113, '11356151c593c8932.51287887-20151007172129.jpeg', '1', '2015-10-07 17:21:29', '1'),
(120, 113, '11356151c593c9995.44605566-20151007172129.jpg', '1', '2015-10-07 17:21:29', '1'),
(121, 113, '11356151c593ca490.55433666-20151007172129.jpg', '1', '2015-10-07 17:21:29', '1'),
(122, 113, '11356151c593caf12.30290591-20151007172129.jpg', '1', '2015-10-07 17:21:29', '1'),
(123, 113, '11356151c593cb977.70154649-20151007172129.jpeg', '1', '2015-10-07 17:21:29', '1'),
(124, 114, '11456151c755f1dd1.68892621-20151007172157.jpeg', '1', '2015-10-07 17:21:57', '1'),
(125, 114, '11456151c755f2fd5.54897442-20151007172157.jpg', '1', '2015-10-07 17:21:57', '1'),
(126, 114, '11456151c755f3b63.80330097-20151007172157.jpg', '1', '2015-10-07 17:21:57', '1'),
(127, 114, '11456151c755f44f5.05927824-20151007172157.jpg', '1', '2015-10-07 17:21:57', '1'),
(128, 114, '11456151c755f4e54.67748859-20151007172157.jpeg', '1', '2015-10-07 17:21:57', '1'),
(129, 115, '11556151c9234c982.48716292-20151007172226.jpeg', '1', '2015-10-07 17:22:26', '1'),
(130, 115, '11556151c9234da42.77106881-20151007172226.jpg', '1', '2015-10-07 17:22:26', '1'),
(131, 115, '11556151c9234e6f4.46608441-20151007172226.jpg', '1', '2015-10-07 17:22:26', '1'),
(132, 115, '11556151c9234f2d5.99733491-20151007172226.jpg', '1', '2015-10-07 17:22:26', '1'),
(133, 115, '11556151c9234fe72.67653130-20151007172226.jpeg', '1', '2015-10-07 17:22:26', '1'),
(134, 116, '11656151ca67090d8.19724991-20151007172246.jpeg', '1', '2015-10-07 17:22:46', '1'),
(135, 116, '11656151ca670b953.87545443-20151007172246.jpg', '1', '2015-10-07 17:22:46', '1'),
(136, 116, '11656151ca670c277.22235736-20151007172246.jpg', '1', '2015-10-07 17:22:46', '1'),
(137, 116, '11656151ca670ca93.56221110-20151007172246.jpg', '1', '2015-10-07 17:22:46', '1'),
(138, 116, '11656151ca670d259.03337848-20151007172246.jpeg', '1', '2015-10-07 17:22:46', '1'),
(139, 117, '11756151cbe3e9058.42605525-20151007172310.jpeg', '1', '2015-10-07 17:23:10', '1'),
(140, 117, '11756151cbe3eaad1.65104978-20151007172310.jpg', '1', '2015-10-07 17:23:10', '1'),
(141, 117, '11756151cbe3eb054.90564093-20151007172310.jpg', '1', '2015-10-07 17:23:10', '1'),
(142, 117, '11756151cbe3eb551.11383864-20151007172310.jpg', '1', '2015-10-07 17:23:10', '1'),
(143, 117, '11756151cbe3eba14.71139070-20151007172310.jpeg', '1', '2015-10-07 17:23:10', '1'),
(144, 118, '11856151cfe753e35.50105487-20151007172414.jpg', '1', '2015-10-07 17:24:14', '1'),
(145, 118, '11856151cfe7556f0.06091841-20151007172414.png', '1', '2015-10-07 17:24:14', '1'),
(146, 118, '11856151cfe755c84.76202091-20151007172414.jpeg', '1', '2015-10-07 17:24:14', '1'),
(147, 118, '11856151cfe756184.82828064-20151007172414.jpeg', '1', '2015-10-07 17:24:14', '1'),
(148, 118, '11856151cfe756641.25341627-20151007172414.jpg', '1', '2015-10-07 17:24:14', '1'),
(149, 119, '11956151e1aa82260.52943789-20151007172858.jpeg', '1', '2015-10-07 17:28:58', '1'),
(150, 119, '11956151e1aa83b12.50552438-20151007172858.jpg', '1', '2015-10-07 17:28:58', '1'),
(151, 119, '11956151e1aa84085.75040862-20151007172858.jpg', '1', '2015-10-07 17:28:58', '1'),
(152, 121, '12156151ea7b2da50.88832595-20151007173119.jpeg', '1', '2015-10-07 17:31:19', '1'),
(153, 121, '12156151ea7b31424.78634110-20151007173119.jpg', '1', '2015-10-07 17:31:19', '1'),
(154, 121, '12156151ea7b34041.97828767-20151007173119.jpg', '1', '2015-10-07 17:31:19', '1'),
(155, 122, '12256151ec0605159.70072294-20151007173144.jpeg', '1', '2015-10-07 17:31:44', '1'),
(156, 122, '12256151ec0607665.84241834-20151007173144.jpg', '1', '2015-10-07 17:31:44', '1'),
(157, 122, '12256151ec0609fd8.01622211-20151007173144.jpg', '1', '2015-10-07 17:31:44', '1'),
(158, 123, '12356151eddea37d9.64254223-20151007173213.jpeg', '1', '2015-10-07 17:32:13', '1'),
(159, 123, '12356151eddea6248.17010688-20151007173213.jpg', '1', '2015-10-07 17:32:13', '1'),
(160, 123, '12356151eddea8c89.87494715-20151007173213.jpg', '1', '2015-10-07 17:32:13', '1'),
(161, 124, '12456151f1a6ef294.58180536-20151007173314.jpeg', '1', '2015-10-07 17:33:14', '1'),
(162, 124, '12456151f1a6f1a68.06506832-20151007173314.jpg', '1', '2015-10-07 17:33:14', '1'),
(163, 124, '12456151f1a6f3f42.79953465-20151007173314.jpg', '1', '2015-10-07 17:33:14', '1'),
(164, 125, '12556151f558add90.44444541-20151007173413.jpeg', '1', '2015-10-07 17:34:13', '1'),
(165, 125, '12556151f558b0346.03722179-20151007173413.jpg', '1', '2015-10-07 17:34:13', '1'),
(166, 125, '12556151f558b2594.89187694-20151007173413.jpg', '1', '2015-10-07 17:34:13', '1'),
(176, 126, '126561527eb748b76.88374602-20151007181051.jpg', '1', '2015-10-07 18:10:51', '1'),
(175, 126, '126561527eb746688.76251165-20151007181051.jpeg', '1', '2015-10-07 18:10:51', '1'),
(174, 126, '126561526f15031d4.18985920-20151007180641.jpg', '1', '2015-10-07 18:06:41', '1'),
(173, 126, '126561526f1500279.73491932-20151007180641.jpeg', '1', '2015-10-07 18:06:41', '1'),
(177, 129, '1295615432a8c9e95.91728935-20151007160706.png', '1', '2015-10-07 16:07:06', '1'),
(178, 129, '1295615432a8e7f69.58198650-20151007160706.png', '1', '2015-10-07 16:07:06', '1'),
(179, 129, '1295615432a8f5725.30734945-20151007160706.jpg', '1', '2015-10-07 16:07:06', '1'),
(180, 130, '1305615435f1d7417.54706350-20151007160759.jpeg', '1', '2015-10-07 16:07:59', '1'),
(181, 130, '1305615435f373de4.17595558-20151007160759.jpeg', '1', '2015-10-07 16:07:59', '1'),
(182, 132, '132561543af7e2958.77887666-20151007160919.png', '1', '2015-10-07 16:09:19', '1'),
(183, 133, '133562142bd9ac816.33621013-20151016183229.jpg', '1', '2015-10-16 18:32:29', '1'),
(184, 133, '133562142bd9adde5.75059993-20151016183229.jpg', '1', '2015-10-16 18:32:29', '1'),
(185, 133, '133562142bd9aea08.21348111-20151016183229.jpg', '1', '2015-10-16 18:32:29', '1'),
(186, 133, '133562142bd9b80f1.35385653-20151016183229.jpg', '1', '2015-10-16 18:32:29', '1'),
(187, 133, '133562142bd9b9525.73655245-20151016183229.jpg', '1', '2015-10-16 18:32:29', '1'),
(188, 134, '134562145d5967a93.79627303-20151016184541.JPG', '1', '2015-10-16 18:45:43', '1'),
(189, 134, '134562145d795a355.04126727-20151016184543.JPG', '1', '2015-10-16 18:45:43', '1'),
(190, 134, '134562145d79637f6.03380656-20151016184543.JPG', '1', '2015-10-16 18:45:43', '1'),
(191, 135, '1355621466c408142.34566967-20151016184812.jpg', '1', '2015-10-16 18:48:12', '1'),
(192, 135, '1355621466c409307.00723493-20151016184812.jpg', '1', '2015-10-16 18:48:12', '1'),
(193, 135, '1355621466c409f31.05268281-20151016184812.jpg', '1', '2015-10-16 18:48:12', '1'),
(194, 135, '1355621466c40aa39.27537792-20151016184812.jpg', '1', '2015-10-16 18:48:12', '1'),
(195, 135, '1355621466c40b663.67621957-20151016184812.jpg', '1', '2015-10-16 18:48:12', '1'),
(196, 136, '136564dcd555565d2.10217043-20151119132333.png', '1', '2015-11-19 13:23:33', '1'),
(197, 136, '136564dcd55557a09.28838447-20151119132333.jpg', '1', '2015-11-19 13:23:33', '1'),
(198, 137, '137564dce0c8557d4.94625224-20151119132636.jpg', '1', '2015-11-19 13:26:36', '1'),
(199, 138, '1385652c321d89333.62400859-20151123074121.pdf', '0', '2015-11-23 07:41:21', '1'),
(200, 142, '14256fb60653a9437.52705726-20160330051309.jpeg', '1', '2016-03-30 05:13:09', '1'),
(201, 142, '14256fb60653c87e3.06352325-20160330051309.jpeg', '1', '2016-03-30 05:13:09', '1'),
(202, 143, '14356fb61b048bf23.47730684-20160330051840.jpg', '1', '2016-03-30 05:18:40', '1'),
(203, 143, '14356fb61b049d427.02751085-20160330051840.jpg', '1', '2016-03-30 05:18:40', '1'),
(204, 144, '14456fb62ef338a48.54399223-20160330052359.', '1', '2016-03-30 05:23:59', '1'),
(205, 144, '14456fb62ef339ed2.80928055-20160330052359.', '1', '2016-03-30 05:23:59', '1'),
(206, 144, '14456fb62ef354636.58946061-20160330052359.', '1', '2016-03-30 05:23:59', '1'),
(207, 144, '14456fb62ef3563e5.22129908-20160330052359.', '1', '2016-03-30 05:23:59', '1'),
(208, 145, '14556fb62f9327dd9.45229006-20160330052409.', '1', '2016-03-30 05:24:09', '1'),
(209, 145, '14556fb62f93290e8.06019035-20160330052409.', '1', '2016-03-30 05:24:09', '1'),
(210, 145, '14556fb62f932ba64.77489764-20160330052409.', '1', '2016-03-30 05:24:09', '1'),
(211, 145, '14556fb62f932dd58.90485146-20160330052409.', '1', '2016-03-30 05:24:09', '1'),
(212, 146, '14656fb63376c5353.91927053-20160330052511.', '1', '2016-03-30 05:25:11', '1'),
(213, 146, '14656fb63376c6865.78041948-20160330052511.', '1', '2016-03-30 05:25:11', '1'),
(214, 146, '14656fb63376c8848.66292982-20160330052511.', '1', '2016-03-30 05:25:11', '1'),
(215, 146, '14656fb63376cb4f7.63076142-20160330052511.', '1', '2016-03-30 05:25:11', '1'),
(216, 147, '14756fcec7db19021.49636661-20160331092309.jpg', '1', '2016-03-31 09:23:10', '1'),
(217, 147, '14756fcec7e91bad4.15969830-20160331092310.jpg', '1', '2016-03-31 09:23:10', '1'),
(218, 148, '14856fcf1b82eeec0.34084580-20160331094528.jpg', '1', '2016-03-31 09:45:28', '1'),
(219, 149, '14956ffbd2b9cbdb4.17352590-20160402123803.jpg', '1', '2016-04-02 12:38:04', '1'),
(220, 149, '14956ffbd2c8e5704.93510831-20160402123804.jpg', '1', '2016-04-02 12:38:04', '1'),
(221, 149, '14956ffbd2c8ef736.12580028-20160402123804.jpg', '1', '2016-04-02 12:38:04', '1'),
(222, 149, '14956ffbd2c8f5e90.20340713-20160402123804.jpg', '1', '2016-04-02 12:38:04', '1'),
(223, 149, '14956ffbd2c8fdad2.40612324-20160402123804.jpg', '1', '2016-04-02 12:38:04', '1'),
(224, 150, '150570047f4316e07.76410895-20160402223012.', '1', '2016-04-02 22:30:12', '1'),
(225, 150, '150570047f434fe70.37684092-20160402223012.', '1', '2016-04-02 22:30:12', '1'),
(226, 150, '150570047f436ae92.64733237-20160402223012.', '1', '2016-04-02 22:30:12', '1'),
(227, 150, '150570047f436cb38.45165388-20160402223012.', '1', '2016-04-02 22:30:12', '1'),
(228, 150, '150570047f436e660.47559133-20160402223012.', '1', '2016-04-02 22:30:12', '1'),
(229, 151, '15157004835ea3b76.35604169-20160402223117.', '1', '2016-04-02 22:31:17', '1'),
(230, 151, '15157004835ea5875.51752777-20160402223117.', '1', '2016-04-02 22:31:17', '1'),
(231, 151, '15157004835ea7ab3.94658422-20160402223117.', '1', '2016-04-02 22:31:17', '1'),
(232, 151, '15157004835ea95e8.94015937-20160402223117.', '1', '2016-04-02 22:31:17', '1'),
(233, 151, '15157004835eab216.89074418-20160402223117.', '1', '2016-04-02 22:31:17', '1'),
(234, 152, '152570048401fe3d6.03444413-20160402223128.', '1', '2016-04-02 22:31:28', '1'),
(235, 152, '152570048402001b7.90135022-20160402223128.', '1', '2016-04-02 22:31:28', '1'),
(236, 152, '15257004840202481.17869841-20160402223128.', '1', '2016-04-02 22:31:28', '1'),
(237, 152, '15257004840204cb2.51923038-20160402223128.', '1', '2016-04-02 22:31:28', '1'),
(238, 152, '152570048402060b9.61958198-20160402223128.', '1', '2016-04-02 22:31:28', '1'),
(239, 153, '1535700484fd98b32.39438329-20160402223143.', '1', '2016-04-02 22:31:43', '1'),
(240, 153, '1535700484fd99aa2.63362164-20160402223143.', '1', '2016-04-02 22:31:43', '1'),
(241, 153, '1535700484fd9b201.86311055-20160402223143.', '1', '2016-04-02 22:31:43', '1'),
(242, 153, '1535700484fd9c576.43670458-20160402223143.', '1', '2016-04-02 22:31:43', '1'),
(243, 153, '1535700484fd9e2f2.32122867-20160402223143.', '1', '2016-04-02 22:31:43', '1'),
(244, 154, '1545700cb11b7d7b3.30157207-20160403074937.jpg', '1', '2016-04-03 07:49:37', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_classified_inquiry`
--

CREATE TABLE IF NOT EXISTS `tbl_classified_inquiry` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `classified_id` int(11) NOT NULL,
  `inquiry_member_id` int(11) NOT NULL,
  `inquiry_email` varchar(255) NOT NULL,
  `inquiry_name` varchar(255) NOT NULL,
  `inquiry_message` text NOT NULL,
  `inquiry_ip` varchar(255) NOT NULL,
  `inquiry_contact_no` varchar(15) NOT NULL,
  `inquiry_date_time` datetime NOT NULL,
  `inquiry_updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_replied` enum('0','1') DEFAULT '0',
  `is_active` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `tbl_classified_inquiry`
--

INSERT INTO `tbl_classified_inquiry` (`id`, `classified_id`, `inquiry_member_id`, `inquiry_email`, `inquiry_name`, `inquiry_message`, `inquiry_ip`, `inquiry_contact_no`, `inquiry_date_time`, `inquiry_updated_time`, `is_replied`, `is_active`) VALUES
(1, 56, 17, '0', '0', '0', '', '0', '2015-04-29 21:38:05', '2015-04-30 14:07:17', '0', '1'),
(2, 56, 17, '0', '0', '0', '', '0', '2015-04-29 21:39:11', '2015-04-30 14:08:23', '0', '1'),
(3, 56, 17, '0', '0', '0', '', '0', '2015-04-29 21:39:34', '2015-04-30 14:08:46', '0', '1'),
(4, 56, 17, 'anas.muhammed+10@gmail.com', 'Anas', 'sadsadsadsadsad', '', '', '2015-04-29 21:40:37', '2015-04-30 14:09:49', '0', '1'),
(5, 56, 17, 'anas.muhammed+10@gmail.com', 'Anas', 'sadsadsadsadsad', '', '', '2015-04-29 21:43:41', '2015-04-30 14:12:53', '0', '1'),
(6, 56, 17, 'anas.muhammed+10@gmail.com', 'Anas', 'sadsadsadsadsad', '', '', '2015-04-29 21:46:14', '2015-04-30 14:15:26', '0', '1'),
(7, 56, 17, 'anas.muhammed+10@gmail.com', 'Anas', 'sadsadsadsadsad', '', '', '2015-04-29 21:49:59', '2015-04-30 14:19:11', '0', '1'),
(8, 56, 17, 'anas.muhammed+10@gmail.com', 'Anas', 'ssssssssssssssssssssssssssssssssssssssss', '', '7185538740', '2015-04-30 12:09:35', '2015-05-01 04:38:47', '0', '1'),
(9, 56, 17, 'anas.muhammed+10@gmail.com', 'Anas', 'ssssssssssssssssssssssssssssssssssssssss', '', '7185538740', '2015-04-30 12:10:11', '2015-05-01 04:39:23', '0', '1'),
(10, 56, 17, 'anas.muhammed+10@gmail.com', 'Anas', 'ssssssssssssssssssssssssssssssssssssssss', '', '7185538740', '2015-04-30 12:14:03', '2015-05-01 04:43:15', '0', '1'),
(11, 56, 17, 'anas.muhammed+10@gmail.com', 'Anas', 'ssssssssssssssssssssssssssssssssssssssss', '', '7185538740', '2015-04-30 12:15:40', '2015-05-01 04:44:52', '0', '1'),
(12, 56, 17, 'anas.muhammed+10@gmail.com', 'Anas', 'ssssssssssssssssssssssssssssssssssssssss', '', '7185538740', '2015-04-30 12:16:18', '2015-05-01 04:45:30', '0', '1'),
(13, 56, 17, 'anas.muhammed@gmail.com', 'Ansar', 'Is your Single owner toyota etiosSingle owner toyota etios is actually owned by you  or some of your friends who asked you to sell it for them? Hwo much is the commission?', '', '8999', '2015-04-30 12:53:53', '2015-05-01 05:23:05', '0', '1'),
(14, 0, 0, 'sss@sd.com', 'sss', 'ssssssssss', '', '999999', '2015-06-13 12:12:00', '2015-06-14 06:11:12', '0', '1'),
(15, 0, 0, 'sss@sd.com', 'sss', 'ssssssssss', '', '999999', '2015-06-13 12:12:56', '2015-06-14 06:12:08', '0', '1'),
(16, 83, 23, 'anas@codeandco.ae', 'Anas', 'sssssssssssssssssssssssssss', '', '', '2015-06-13 12:13:48', '2015-06-14 06:13:00', '0', '1'),
(17, 83, 23, 'anas@codeandco.ae', 'Anas', 'sssssssssssssssssssssssssss', '', '', '2015-06-13 12:14:11', '2015-06-14 06:13:23', '0', '1'),
(18, 83, 23, 'anas@codeandco.ae', 'Anas', 'sdasd sadasd sdsadsa sadsadsadasdsd asdasdsad', '', '', '2015-06-13 12:21:40', '2015-06-14 06:20:52', '0', '1'),
(19, 98, 27, 'anas@codeandco.ae', 'Anas', 'sadasdsad', '', '45465', '2015-06-14 06:40:44', '2015-06-14 17:40:20', '0', '1'),
(20, 98, 27, 'anas@codeandco.ae', 'Anas', 'sadasdsad', '', '45465', '2015-06-14 06:41:07', '2015-06-14 17:40:43', '0', '1'),
(21, 98, 27, 'anas@codeandco.ae', 'Anas', 'sadasdsad cddddddddddd', '', '45465', '2015-06-14 06:43:15', '2015-06-14 17:42:51', '0', '1'),
(22, 98, 27, 'anas@codeandco.ae', 'Anas', 'sadasdsad cddddddddddd', '', '45465', '2015-06-14 06:44:06', '2015-06-14 17:43:42', '0', '1'),
(23, 98, 27, 'anas@codeandco.ae', 'Anas', 'sadasdsad cddddddddddd', '', '45465', '2015-06-14 06:48:34', '2015-06-14 17:48:10', '0', '1'),
(24, 102, 33, 'snd2k6@gmail.com', 'Richard', 'i''m interested in viewing this car. Would you accept 98000. Is the transfer included.', '', '', '2015-06-25 12:34:24', '2015-06-25 23:34:00', '0', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_classified_inquiry_reply`
--

CREATE TABLE IF NOT EXISTS `tbl_classified_inquiry_reply` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `classified_id` int(11) NOT NULL,
  `inquiry_id` int(11) NOT NULL,
  `replied_member_id` int(11) NOT NULL,
  `replied_text` text,
  `replied_date_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_classified_log`
--

CREATE TABLE IF NOT EXISTS `tbl_classified_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `classified_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `classified_slug` varchar(255) NOT NULL,
  `classified_tags` text,
  `description` text,
  `category_id` int(11) DEFAULT NULL,
  `classified_contact_no` varchar(255) DEFAULT NULL,
  `classified_contact_name` varchar(255) DEFAULT NULL,
  `classified_contact_email` varchar(255) DEFAULT NULL,
  `is_futured` enum('0','1') NOT NULL DEFAULT '0',
  `classifed_modal` varchar(255) NOT NULL,
  `amount` int(11) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `model_id` int(11) NOT NULL,
  `manufacture_year` int(11) NOT NULL,
  `running_km` int(11) NOT NULL,
  `created_by` int(11) NOT NULL COMMENT 'Member ID',
  `created_date_time` datetime NOT NULL,
  `updated_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `classified_address` varchar(255) DEFAULT NULL,
  `classified_land_mark` varchar(255) DEFAULT NULL,
  `classified_country` int(11) DEFAULT NULL,
  `classified_city` varchar(255) DEFAULT NULL,
  `classified_city_distance` tinyint(4) NOT NULL,
  `number_of_hits` int(11) DEFAULT NULL,
  `meta_title` text,
  `meta_desc` text,
  `meta_keywords` text,
  `classified_ip` varchar(255) DEFAULT NULL,
  `small_description` varchar(255) DEFAULT NULL,
  `is_featured` enum('0','1') NOT NULL DEFAULT '0',
  `is_deleted` enum('0','1') NOT NULL DEFAULT '0',
  `deleted_date_time` datetime DEFAULT NULL,
  `is_active` enum('0','1') NOT NULL DEFAULT '0',
  `milage` varchar(45) DEFAULT NULL,
  `body_type` varchar(45) DEFAULT NULL,
  `no_of_doors` varchar(255) NOT NULL,
  `colour` varchar(45) DEFAULT NULL,
  `transmission` varchar(45) DEFAULT NULL,
  `engine_size` varchar(45) DEFAULT NULL,
  `entry_type` enum('Individual','Dealer') NOT NULL DEFAULT 'Individual',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `tbl_classified_log`
--

INSERT INTO `tbl_classified_log` (`id`, `classified_id`, `title`, `classified_slug`, `classified_tags`, `description`, `category_id`, `classified_contact_no`, `classified_contact_name`, `classified_contact_email`, `is_futured`, `classifed_modal`, `amount`, `brand_id`, `model_id`, `manufacture_year`, `running_km`, `created_by`, `created_date_time`, `updated_date_time`, `classified_address`, `classified_land_mark`, `classified_country`, `classified_city`, `classified_city_distance`, `number_of_hits`, `meta_title`, `meta_desc`, `meta_keywords`, `classified_ip`, `small_description`, `is_featured`, `is_deleted`, `deleted_date_time`, `is_active`, `milage`, `body_type`, `no_of_doors`, `colour`, `transmission`, `engine_size`, `entry_type`) VALUES
(1, 83, 'sdsd', 'sdsd1', NULL, 'ssss', 2, '+9710000000', NULL, NULL, '0', '', 27000, 3, 3, 2015, 0, 23, '0000-00-00 00:00:00', '2015-06-14 09:02:24', NULL, NULL, NULL, '96', 0, NULL, NULL, NULL, NULL, '192.168.1.104', NULL, '0', '0', NULL, '0', NULL, NULL, '', NULL, NULL, NULL, 'Individual'),
(2, 83, 'sdsd', 'sdsd1', '0', '<p>ssss</p>', 2, '+9710000000', '0', '0', '0', '', 27000, 3, 3, 2015, 0, 23, '0000-00-00 00:00:00', '2015-06-14 09:02:47', '0', '0', 0, '96', 0, NULL, '0', '0', '0', '192.168.1.104', '0', '0', '0', NULL, '0', NULL, NULL, '', NULL, NULL, NULL, 'Individual'),
(3, 92, 'asdadda', 'sxddadad', NULL, 'ssss', 2, '+9710000000', NULL, NULL, '0', '', 27000, 3, 3, 2015, 0, 23, '0000-00-00 00:00:00', '2015-06-14 09:03:02', NULL, NULL, NULL, '96', 0, NULL, NULL, NULL, NULL, '192.168.1.104', NULL, '0', '0', NULL, '0', NULL, NULL, '', NULL, NULL, NULL, 'Individual'),
(4, 92, 'asdadda', 'sxddadad', '0', '<p>ssss</p>', 2, '+9710000000', '0', '0', '0', '', 27000, 3, 3, 2015, 0, 23, '0000-00-00 00:00:00', '2015-06-14 09:03:56', '0', '0', 0, '96', 0, NULL, '0', '0', '0', '192.168.1.104', '0', '0', '0', NULL, '0', NULL, NULL, '', NULL, NULL, NULL, 'Individual'),
(5, 92, 'asdadda', 'sxddadad', '0', '<p>ssss</p>', 2, '+9710000000', '0', '0', '0', '', 27000, 3, 3, 2015, 0, 23, '0000-00-00 00:00:00', '2015-06-14 09:04:10', '0', '0', 0, '96', 0, NULL, '0', '0', '0', '192.168.1.104', '0', '0', '0', NULL, '0', NULL, NULL, '', NULL, NULL, NULL, 'Individual'),
(6, 83, 'sdsd', 'sdsd1', '0', '<p>ssss</p>', 2, '+9710000000', '0', '0', '0', '', 27000, 3, 3, 2015, 0, 23, '0000-00-00 00:00:00', '2015-06-14 09:04:55', '0', '0', 0, '96', 0, NULL, '0', '0', '0', '192.168.1.104', '0', '0', '0', NULL, '0', NULL, NULL, '', NULL, NULL, NULL, 'Individual');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_classified_log_bu`
--

CREATE TABLE IF NOT EXISTS `tbl_classified_log_bu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `classified_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `classified_slug` varchar(255) NOT NULL,
  `classified_tags` text NOT NULL,
  `description` text NOT NULL,
  `category_id` int(11) NOT NULL,
  `classified_contact_no` varchar(255) NOT NULL,
  `classified_contact_name` varchar(255) NOT NULL,
  `classified_contact_email` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL COMMENT 'Member ID',
  `updated_date_time` datetime NOT NULL,
  `classified_address` varchar(255) NOT NULL,
  `classified_land_mark` varchar(255) NOT NULL,
  `classified_country` int(11) NOT NULL,
  `classified_city` varchar(255) NOT NULL,
  `meta_title` text NOT NULL,
  `meta_desc` text NOT NULL,
  `meta_keywords` text NOT NULL,
  `classified_ip` varchar(255) NOT NULL,
  `small_description` varchar(255) NOT NULL,
  `brand` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cms`
--

CREATE TABLE IF NOT EXISTS `tbl_cms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `cms_slug` varchar(255) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `small_description` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `cms_banner_image` varchar(255) NOT NULL,
  `cms_order` int(11) NOT NULL,
  `meta_title` text NOT NULL,
  `meta_desc` text NOT NULL,
  `meta_keywords` text NOT NULL,
  `is_deleted` enum('0','1') NOT NULL DEFAULT '0',
  `on_header` enum('0','1') NOT NULL DEFAULT '1',
  `on_footer` enum('0','1') NOT NULL DEFAULT '1',
  `deleted_date_time` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_date_time` datetime NOT NULL,
  `updated_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_active` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `tbl_cms`
--

INSERT INTO `tbl_cms` (`id`, `title`, `cms_slug`, `parent_id`, `small_description`, `description`, `cms_banner_image`, `cms_order`, `meta_title`, `meta_desc`, `meta_keywords`, `is_deleted`, `on_header`, `on_footer`, `deleted_date_time`, `created_by`, `updated_by`, `created_date_time`, `updated_date_time`, `is_active`) VALUES
(1, 'Home', 'home', 0, 'Home Page', '<p>\r\n	Home Page</p>\r\n', '17042015232911_25022015143209_slider_01.jpg', 1, 'Home Page', 'Home', 'Home', '0', '0', '1', '0000-00-00 00:00:00', 1, 2, '2015-02-18 10:00:00', '2015-06-09 12:42:18', '1'),
(2, 'Test CMS', 'test-cms', 1, 'Test Desc', '<p>\r\n	test this is another parent</p>\r\n', '21022015071419_10906405_621387354633386_5453388469141055346_n.jpg', 2, 'Test Desc', 'Meta Description', 'Meta Keyworsa', '1', '1', '1', '2015-02-28 11:19:46', 1, 1, '2015-02-19 12:32:23', '2015-03-01 08:19:22', '1'),
(3, 'Useful Information', 'useful-information', 0, 'Useful Information', '<p>\r\n	Useful Information</p>\r\n', '', 3, 'Useful Information', 'Useful Information', 'Useful Information', '0', '0', '0', '0000-00-00 00:00:00', 2, 2, '2015-02-28 11:19:33', '2015-06-14 11:46:14', '1'),
(4, 'Classifie Features', 'classifie-features', 0, 'Classifie Features', '<p>\r\n	Classifie Features</p>\r\n', '', 4, 'Classifie Features', 'Classifie Features', 'Classifie Features', '1', '0', '1', '2015-06-13 17:47:52', 2, 2, '2015-02-28 11:20:48', '2015-06-14 11:47:04', '1'),
(5, 'FAQs', 'faqs', 3, 'FAQs', '<h2>\r\n	How does Google protect my privacy and keep my information secure?</h2>\r\n<p>\r\n	We know security and privacy are important to you &ndash; and they are important to us, too. We make it a priority to provide strong security and give you confidence that your information is safe and accessible when you need it.</p>\r\n<p>\r\n	We&rsquo;re constantly working to ensure strong security, protect your privacy, and make Google even more effective and efficient for you. We spend hundreds of millions of dollars every year on security, and employ world-renowned experts in data security to keep your information safe. We also built easy-to-use privacy and security tools like Google Dashboard, 2-step verification and Ads Settings. So when it comes to the information you share with Google, you&rsquo;re in control.</p>\r\n<p>\r\n	You can learn more about safety and security online, including how to protect yourself and your family online on our <a href="http://www.google.com/safetycenter/">Good to Know</a> site.</p>\r\n<h2>\r\n	How can I remove information about myself from Google&#39;s search results?</h2>\r\n<p>\r\n	Google search results are a reflection of the content publicly available on the web. Search engines can&rsquo;t remove content directly from websites, so removing search results from Google wouldn&rsquo;t remove the content from the web. If you want to remove something from the web, you should <a href="http://support.google.com/websearch/bin/answer.py?hl=en&amp;answer=9109">contact the webmaster</a> of the site the content is posted on and ask him or her to make a change. Once the content has been removed and Google has noted the update, the information will no longer appear in Google&rsquo;s search results. If you have an urgent removal request, you can also <a href="http://support.google.com/webmasters/bin/answer.py?hl=en&amp;answer=164734">visit our help page for more information</a>.</p>\r\n<h2>\r\n	Are my search queries sent to websites when I click on Google Search results?</h2>\r\n<p>\r\n	In some cases, yes. When you click on a search result in Google Search, your web browser also may send the Internet address, or URL, of the search results page to the destination webpage as the <a href="http://www.google.com/policies/privacy/key-terms/">HTTP Referrer</a>. The URL of the search results page may sometimes contain the search query you entered. If you are using SSL Search (Google&rsquo;s encrypted search functionality), under most circumstances, your search terms will not be sent as part of the URL in the HTTP Referrer. There are some exceptions to this behavior, such as if you are using some less popular browsers. More information on SSL Search can be found <a href="http://support.google.com/websearch/answer/173733?hl=en">here</a>. Search queries or information contained in the HTTP Referrer may be available via Google Analytics or an application programming interface (API). In addition, advertisers may receive information relating to the exact keywords that triggered an ad click.</p>\r\n', '22042015161107_FAQ.jpg', 5, 'FAQs', 'FAQs', 'FAQs', '1', '1', '1', '2015-06-13 17:47:14', 2, 2, '2015-02-28 11:25:00', '2015-06-14 11:46:26', '1'),
(6, 'Help', 'help', 3, 'Help', '', '', 6, 'Help', 'Help', 'Help', '1', '0', '1', '2015-06-13 17:47:18', 2, 2, '2015-02-28 11:25:38', '2015-06-14 11:46:30', '1'),
(7, 'About Classifie', 'about-classifie', 3, 'About Classifie', '<div class="row">\r\n	<div class="col-lg-12 top-spacing55px">\r\n		<center>\r\n			<!--<img class="img-responsive" src="images/about-pic.png" width="490" />--></center>\r\n		<div class="about-title">\r\n			<h4>\r\n				<span>WE ARE SIMPLIFYING TRADE!</span></h4>\r\n		</div>\r\n	</div>\r\n</div>\r\n<!-- /.row -->\r\n<div class="row featurette">\r\n	<div class="col-lg-6 left-right-spacing1">\r\n		<p>\r\n			Proin ut pretium ligula. Sed consequat, enim a finibus suscipit, neque purus condimentum velit, vitae varius augue urna in nulla. Aenean venenatis nec velit eu posuere. Donec velit turpis, suscipit mollis vehicula sed, convallis sit amet libero. Vivamus tempus eros felis, sit amet suscipit leo sodales at. Donec quis varius metus. Cras convallis gravida sapien, vel semper tortor egestas at. Vivamus lacinia a erat id gravida.</p>\r\n		<p class="lear-para">\r\n			Vivamus tempus eros felis, sit amet suscipit leo sodales at. Donec quis varius metus. Cras convallis gravida sapien, vel semper tortor egestas at. Vivamus lacinia a erat id gravida.</p>\r\n		<p>\r\n			Proin ut pretium ligula. Sed consequat, enim a finibus suscipit, neque purus condimentum velit, vitae varius augue urna in nulla.</p>\r\n	</div>\r\n	<div class="col-lg-6 left-right-spacing2">\r\n		<p>\r\n			Aenean venenatis nec velit eu posuere. Donec velit turpis, suscipit mollis vehicula sed, convallis sit amet libero. Vivamus tempus eros felis, sit amet suscipit leo sodales at. Donec quis varius metus. Cras convallis gravida sapien, vel semper tortor egestas at. Vivamus lacinia a erat id gravida.</p>\r\n		<p>\r\n			Donec quis varius metus. Cras convallis gravida sapien, vel semper tortor egestas at. Vivamus lacinia a erat id gravida.</p>\r\n	</div>\r\n</div>\r\n<p>\r\n	&nbsp;</p>\r\n', '22042015184613_about-pic.png', 7, 'About Classifie', 'About Classifie', 'About Classifie', '1', '0', '1', '2015-06-13 17:47:08', 2, 2, '2015-02-28 11:26:12', '2015-06-14 11:46:20', '1'),
(8, 'Terms and Conditions', 'terms-and-conditions', 3, 'Terms and Conditions', '', '', 8, 'Terms and Conditions', 'Terms and Conditions', 'Terms and Conditions', '1', '0', '1', '2015-06-13 17:47:22', 2, 2, '2015-02-28 11:26:42', '2015-06-14 11:46:34', '1'),
(9, 'Contact Us', 'contact-us', 3, 'Contact Us', '', '', 9, 'Contact Us', 'Contact Us', 'Contact Us', '1', '0', '1', '2015-06-13 17:47:25', 2, 2, '2015-02-28 11:27:17', '2015-06-14 11:46:37', '1'),
(10, 'Sponsored Links', 'sponsored-links', 4, 'Sponsored Links', '', '', 10, 'Sponsored Links', 'Sponsored Links', 'Sponsored Links', '1', '0', '1', '2015-06-13 17:47:29', 2, 2, '2015-02-28 11:31:01', '2015-06-14 11:46:41', '1'),
(11, 'Featured Ads', 'featured-ads', 4, 'Featured Ads', '', '', 11, 'Featured Ads', 'Featured Ads', 'Featured Ads', '1', '0', '1', '2015-06-13 17:47:31', 2, 2, '2015-02-28 11:31:34', '2015-06-14 11:46:43', '1'),
(12, 'How to Partner with Classifie', 'how-to-partner-with-classifie', 4, 'How to Partner with Classifie', '<p>\r\n	How to Partner with Classifie</p>\r\n', '', 12, 'How to Partner with Classifie', 'How to Partner with Classifie', 'How to Partner with Classifie', '1', '0', '1', '2015-06-13 17:47:35', 2, 2, '2015-02-28 11:32:22', '2015-06-14 11:46:47', '1'),
(13, 'Tools & Widgets', 'tools---widgets', 4, 'Tools & Widgets', '', '', 13, 'Tools & Widgets', 'Tools & Widgets', 'Tools & Widgets', '1', '0', '1', '2015-06-13 17:47:37', 2, 2, '2015-02-28 11:34:07', '2015-06-14 11:46:49', '1'),
(14, 'Post a Free Classified Ad', 'post-a-free-classified-ad', 4, 'Post a Free Classified Ad', '', '', 14, 'Post a Free Classified Ad', 'Post a Free Classified Ad', 'Post a Free Classified Ad', '1', '0', '1', '2015-06-13 17:47:40', 2, 2, '2015-02-28 11:35:38', '2015-06-14 11:46:52', '1'),
(15, 'FAQs', 'faqs-1', 3, 'Frequently asked questions', '<h1>\r\n	FAQ</h1>\r\n<h2>\r\n	How does Google protect my privacy and keep my information secure?</h2>\r\n<p>\r\n	We know security and privacy are important to you &ndash; and they are important to us, too. We make it a priority to provide strong security and give you confidence that your information is safe and accessible when you need it.</p>\r\n<p>\r\n	We&rsquo;re constantly working to ensure strong security, protect your privacy, and make Google even more effective and efficient for you. We spend hundreds of millions of dollars every year on security, and employ world-renowned experts in data security to keep your information safe. We also built easy-to-use privacy and security tools like Google Dashboard, 2-step verification and Ads Settings. So when it comes to the information you share with Google, you&rsquo;re in control.</p>\r\n<p>\r\n	You can learn more about safety and security online, including how to protect yourself and your family online on our <a href=\\"http://www.google.com/safetycenter/\\">Good to Know</a> site.</p>\r\n<h2>\r\n	How can I remove information about myself from Google&#39;s search results?</h2>\r\n<p>\r\n	Google search results are a reflection of the content publicly available on the web. Search engines can&rsquo;t remove content directly from websites, so removing search results from Google wouldn&rsquo;t remove the content from the web. If you want to remove something from the web, you should <a href=\\"http://support.google.com/websearch/bin/answer.py?hl=en&amp;answer=9109\\">contact the webmaster</a> of the site the content is posted on and ask him or her to make a change. Once the content has been removed and Google has noted the update, the information will no longer appear in Google&rsquo;s search results. If you have an urgent removal request, you can also <a href=\\"http://support.google.com/webmasters/bin/answer.py?hl=en&amp;answer=164734\\">visit our help page for more information</a>.</p>\r\n<h2>\r\n	Are my search queries sent to websites when I click on Google Search results?</h2>\r\n<p>\r\n	In some cases, yes. When you click on a search result in Google Search, your web browser also may send the Internet address, or URL, of the search results page to the destination webpage as the <a href=\\"http://www.google.com/policies/privacy/key-terms/\\">HTTP Referrer</a>. The URL of the search results page may sometimes contain the search query you entered. If you are using SSL Search (Google&rsquo;s encrypted search functionality), under most circumstances, your search terms will not be sent as part of the URL in the HTTP Referrer. There are some exceptions to this behavior, such as if you are using some less popular browsers. More information on SSL Search can be found <a href=\\"http://support.google.com/websearch/answer/173733?hl=en\\">here</a>. Search queries or information contained in the HTTP Referrer may be available via Google Analytics or an application programming interface (API). In addition, advertisers may receive information relating to the exact keywords that triggered an ad click.</p>\r\n', '22042015144708_add-classifi.jpg', 15, 'FAQ', '', '', '1', '0', '1', '2015-04-22 14:49:43', 2, 2, '2015-04-22 14:47:08', '2015-04-23 07:18:55', '1'),
(16, 'Advertise with Us', 'about-us', 0, '', '<div class="left-about-1">\n	<img src="../images/about-us-left01.jpg" style="margin-top:5px;" /></div>\n<div class="left-about-2">\n	<p>\n		Exceed Trader is a free classifieds website for users in Trinidad and Tobago whereby they can buy, sell, trade or find just about anything they are looking for within and outside their community. Its birth was a result of rapid changes in technological advancements and to make the buying, selling and trading of new and used items easy and accessible to all.</p>\n	<p>\n		We exist in a community where underused goods can be redistributed to fill a new need, and become wanted again and space; skills and money are exchanged and traded in new ways that don''t always require centralized institutions.</p>\n	<p>\n		Exceed Traders is not only a buying and selling platform but it also offer users the opportunity to advertise products and services via banners. With the digital landscape shifting rapidly towards mobile and video this is a very efficient and effective medium of advertising.</p>\n	<p>\n		We offer our users the ability to list and search classified ads in all parts of Trinidad and Tobago with ease and from any electronic device. To ensure the safety of our users Exceed Trader reviews all ads before posting. Exceed has its main operation in Trinidad with hopes of further expansion in the Caribbean in the near future.</p>\n	<div class="clerafix">\n		&nbsp;</div>\n</div>\n', '', 16, '', '', '', '0', '1', '1', '0000-00-00 00:00:00', 2, 2, '2015-06-08 18:42:57', '2016-04-03 09:41:42', '1'),
(20, 'How It Works', 'how-it-works', 0, '', '<div class="steps_boxes">\n	<div class="img_wrap">\n		<img alt="" src="http://demo.thecodeandco.net/uploads/userfiles/images/top_1bg.png" /></div>\n	<div class="steps_head">\n		Five simple steps to place your free ad in under 5 minutes</div>\n	<div class="steps_point">\n		1. Click on &quot;Post an Ad&quot; in the top right corner.</div>\n	<div class="steps_point">\n		2. Returning users sign in, new users fill in your sign up details.</div>\n	<div class="steps_point">\n		3. Follow the quick and easy steps to fill out the information about your ad.</div>\n	<div class="steps_point">\n		4. Preview your ad details and click &quot;Post Ad&quot; to finish publishing your ad.</div>\n	<div class="steps_point">\n		5. First time users will be asked to send an SMS to activate their ad/account.</div>\n</div>\n<div class="steps_boxes">\n	<div class="img_wrap">\n		<img alt="" src="http://demo.thecodeandco.net/uploads/userfiles/images/top_2bg.png" /></div>\n	<div class="steps_head">\n		Respond to an ad</div>\n	<div class="steps_point">\n		1. While viewing the ad you are interested in, scroll to the bottom of the page.</div>\n	<div class="steps_point">\n		2. In the &quot;Respond to the Ad&quot; box, fill in the information requested and write your message.</div>\n	<div class="steps_point">\n		3. Press the &quot;send reply&quot; button, and you&#39;re done!</div>\n	<div class="steps_point">\n		4. The advertiser will receive your message directly to his/her email inbox.</div>\n</div>\n<div class="steps_boxes">\n	<div class="img_wrap">\n		<img alt="" src="http://demo.thecodeandco.net/uploads/userfiles/images/top_3bg.png" /></div>\n	<div class="steps_head">\n		Register/Sign up</div>\n	<div class="steps_point">\n		1. Click on the &quot;Register&quot; link at the top of the page above &quot;Post an Ad&quot;.</div>\n	<div class="steps_point">\n		2. Fill in all of the required information, agree to the Terms, and click the &quot;sign up&quot; button.</div>\n	<div class="steps_point">\n		3. Exceed Trader will send you a verification message to the email you provided.</div>\n	<div class="steps_point">\n		4. Click on the verification link from the email (Check your spam folder if you don&#39;t find it).</div>\n	<div class="steps_point">\n		5. Your registration is now complete, you can now fill out your profile.</div>\n</div>\n<div class="steps_boxes">\n	<div class="img_wrap">\n		<img alt="" src="http://demo.thecodeandco.net/uploads/userfiles/images/top_4bg.png" /></div>\n	<div class="steps_head">\n		Edit/Delete your Ad</div>\n	<div class="steps_point">\n		1. Make sure you are logged into your account on Exceed Trader.</div>\n	<div class="steps_point">\n		2. Go to your account page by clicking on the &quot;Account&quot; link at the top of the page above &quot;Post an Ad&quot;.</div>\n	<div class="steps_point">\n		3. Click on the &quot;My Ads&quot; tab.</div>\n	<div class="steps_point">\n		4. From this page you can edit or delete any of your ads on Exceed Trader.</div>\n</div>\n<div class="steps_boxes">\n	<div class="img_wrap">\n		<img alt="" src="http://demo.thecodeandco.net/uploads/userfiles/images/top_5bg.png" /></div>\n	<div class="steps_head">\n		Edit account info:</div>\n	<div class="steps_point">\n		1. Make sure you are logged into your account on Exceed Trader.</div>\n	<div class="steps_point">\n		2. Go to your account page by clicking on the dropdown link next to your name at the top of the page above &quot;Post an Ad&quot;.</div>\n	<div class="steps_point">\n		3. Click on the &quot;My Profile&quot; tab.</div>\n	<div class="steps_point">\n		4. Edit the necessary you require and the click the &quot;Update Profile&quot; button.</div>\n	<div class="steps_point">\n		5. To edit account settings click on the &quot;Account Settings&quot; button from the dropdown link next to your name at the top of the page advise &quot;Post an Ad&quot;.</div>\n</div>\n', '', 20, '', '', '', '0', '1', '0', '0000-00-00 00:00:00', 2, 2, '2016-02-04 11:06:09', '2016-02-04 11:53:22', '1'),
(17, 'Contact Us', 'contact-us-1', 0, '<p class="lead">Hundreds of <span class="blute-text-smibold">great ads are now at your fingertips</span> with <br />\r\nCreativity''s new advertising gallery</p>', '<div class="left-about-1">\r\n	<img src="../images/about-us-left01.jpg" /></div>\r\n<div class="left-about-2">\r\n	<p>\r\n		Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>\r\n	<p>\r\n		Vestibulum auctor dapibus neque.<br />\r\n		Phasellus ultrices nulla quis nibh. Quisque a lectus. Donec consectetuer ligula vulputate sem tristique cursus. Nam nulla quam, gravida non, commodo a, sodales sit amet, nisi.</p>\r\n	<p>\r\n		Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.</p>\r\n	<p>\r\n		Phasellus ultrices nulla quis nibh. Quisque a lectus. Donec consectetuer ligula vulputate sem tristique cursus. Nam nulla quam, gravida non, commodo a, sodales sit amet, nisi.Pellentesque fermentum dolor. Aliquam quam lectus, facilisis auctor, ultrices ut, elementum vulputate, nunc.Sed adipiscing ornare risus. Morbi est est, blandit sit amet, sagittis vel, euismod vel, velit. Pellentesque egestas sem. Suspendisse commodo ullamcorper magna.</p>\r\n	<h2 class="out-team-heading">\r\n		OUT TEAM</h2>\r\n	<div class="clerafix">\r\n		&nbsp;</div>\r\n	<div class="add-box">\r\n		<img height="250" src="../images/BUTLER.jpg" width="300" />\r\n		<p class="about-bios text-upercase">\r\n			<span>J.C. BUTLER</span><br />\r\n			Managing Partner</p>\r\n		<p class="text-center">\r\n			Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci.</p>\r\n		<p class="text-center">\r\n			<a class="view-profile" href="#">view profile</a></p>\r\n	</div>\r\n	<div class="add-box pull-right">\r\n		<img height="250" src="../images/Whatley.jpg" width="300" />\r\n		<p class="about-bios text-upercase">\r\n			<span>Sim B Whatley II</span><br />\r\n			Co FOUNDER</p>\r\n		<p class="text-center">\r\n			Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra.</p>\r\n		<p class="text-center">\r\n			<a class="view-profile" href="#">view profile</a></p>\r\n	</div>\r\n</div>\r\n', '', 17, '<p class="lead">Hundreds of <span class="blute-text-smibold">great ads are now at your fingertips</span> with <br />\r\nCreativity''s new advertising gallery</p>', '', '', '0', '0', '1', '0000-00-00 00:00:00', 2, 2, '2015-06-13 17:48:37', '2015-06-14 11:50:17', '1'),
(18, 'Terms and conditions', 'terms-and-conditions-1', 0, '', '<div class=\\"privacy-popup\\">\n	<div class=\\"register-heading-popup\\">\n		<h2 class=\\"page-title-popup\\">\n			Terms &amp; Conditions</h2>\n		<p>\n			Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n		<p>\n			Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words.</p>\n		<p>\n			Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words.</p>\n		<p>\n			Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words.</p>\n		<p>\n			Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words.</p>\n		<p>\n			Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words.</p>\n		<p>\n			Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words.</p>\n		<p>\n			Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words.</p>\n		<p>\n			Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words.</p>\n		<p>\n			Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words.</p>\n		<p>\n			Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words.</p>\n		vv\n		<p>\n			Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words.</p>\n		<p>\n			Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words.</p>\n		<p>\n			Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words.</p>\n		<p>\n			Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words.</p>\n		<p>\n			Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words.</p>\n	</div>\n</div>\n', '', 18, '', '', '', '0', '0', '0', '0000-00-00 00:00:00', 2, 2, '2015-06-24 12:36:41', '2015-06-24 23:36:17', '1'),
(19, 'Privacy Policy', 'privacy-policy', 0, '', '<div class=\\"privacy-popup\\">\n	<div class=\\"register-heading-popup\\">\n		<h2 class=\\"page-title-popup\\">\n			Privacy Policy</h2>\n		<p>\n			Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\n		<p>\n			Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words.</p>\n		<p>\n			Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words.</p>\n		<p>\n			Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words.</p>\n		<p>\n			Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words.</p>\n		<p>\n			Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words.</p>\n		<p>\n			Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words.</p>\n		<p>\n			Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words.</p>\n		<p>\n			Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words.</p>\n		<p>\n			Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words.</p>\n		<p>\n			Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words.</p>\n		vv\n		<p>\n			Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words.</p>\n		<p>\n			Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words.</p>\n		<p>\n			Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words.</p>\n		<p>\n			Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words.</p>\n		<p>\n			Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words.</p>\n	</div>\n</div>\n', '', 19, '', '', '', '0', '0', '0', '0000-00-00 00:00:00', 2, 2, '2015-06-24 12:38:28', '2015-06-24 23:38:04', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_cms_log`
--

CREATE TABLE IF NOT EXISTS `tbl_cms_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `cms_id` int(11) NOT NULL,
  `cms_slug` varchar(255) NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `small_description` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `cms_banner_image` varchar(255) NOT NULL,
  `on_header` enum('0','1') NOT NULL DEFAULT '0',
  `on_footer` enum('0','1') NOT NULL DEFAULT '0',
  `meta_title` text NOT NULL,
  `meta_desc` text NOT NULL,
  `meta_keywords` text NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_date_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `tbl_cms_log`
--

INSERT INTO `tbl_cms_log` (`id`, `title`, `cms_id`, `cms_slug`, `parent_id`, `small_description`, `description`, `cms_banner_image`, `on_header`, `on_footer`, `meta_title`, `meta_desc`, `meta_keywords`, `updated_by`, `updated_date_time`) VALUES
(1, 'Home', 1, '', 0, 'Home Page', '<p>\r\n	Home Page</p>\r\n', '17042015232911_25022015143209_slider_01.jpg', '1', '1', 'Home Page', 'Home', 'Home', 2, '2015-06-08 18:43:06'),
(2, 'Useful Information', 3, '', 0, 'Useful Information', '<p>\r\n	Useful Information</p>\r\n', '', '0', '1', 'Useful Information', 'Useful Information', 'Useful Information', 2, '2015-06-08 18:43:15'),
(3, 'About Us', 16, '', 0, '<p class="lead">Hundreds of <span class="blute-text-smibold">great ads are now at your fingertips</span> with <br />\r\nCreativity''s new advertising gallery</p>', '\r\n            	<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>\r\n                <p>Vestibulum auctor dapibus neque.<br />\r\nPhasellus ultrices nulla quis nibh. Quisque a lectus. Donec consectetuer ligula vulputate sem tristique cursus. Nam nulla quam, gravida non, commodo a, sodales sit amet, nisi.</p>\r\n<p>Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.</p>\r\n<p>Phasellus ultrices nulla quis nibh. Quisque a lectus. Donec consectetuer ligula vulputate sem tristique cursus. Nam nulla quam, gravida non, commodo a, sodales sit amet, nisi.Pellentesque fermentum dolor. Aliquam quam lectus, facilisis auctor, ultrices ut, elementum vulputate, nunc.Sed adipiscing ornare risus. Morbi est est, blandit sit amet, sagittis vel, euismod vel, velit. Pellentesque egestas sem. Suspendisse commodo ullamcorper magna.</p>\r\n<h2 class=\\"out-team-heading\\">OUT TEAM</h2>\r\n<div class=\\"clerafix\\"></div>\r\n			<div class=\\"add-box\\">\r\n				<img src=\\"images/BUTLER.jpg\\" width=\\"300\\" height=\\"250\\">\r\n                <p class=\\"about-bios text-upercase\\"><span>J.C. BUTLER</span><br />\r\nManaging Partner</p>\r\n<p class=\\"text-center\\">Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci.</p>\r\n<p class=\\"text-center\\"><a href=\\"#\\" class=\\"view-profile\\">view profile</a></p>\r\n            </div>\r\n            <div class=\\"add-box pull-right\\">\r\n				<img src=\\"images/Whatley.jpg\\" width=\\"300\\" height=\\"250\\">\r\n                 <p class=\\"about-bios text-upercase\\"><span>Sim B Whatley II</span><br />\r\nCo FOUNDER</p>\r\n<p class=\\"text-center\\">Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra.</p>\r\n<p class=\\"text-center\\"><a href=\\"#\\" class=\\"view-profile\\">view profile</a></p>\r\n            </div>\r\n\r\n\r\n            ', '', '1', '1', '', '', '', 2, '2015-06-08 18:43:51'),
(4, 'About Us', 16, '', 0, '<p class="lead">Hundreds of <span class="blute-text-smibold">great ads are now at your fingertips</span> with <br />\r\nCreativity''s new advertising gallery</p>', '<p>\r\n	Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>\r\n<p>\r\n	Vestibulum auctor dapibus neque.<br />\r\n	Phasellus ultrices nulla quis nibh. Quisque a lectus. Donec consectetuer ligula vulputate sem tristique cursus. Nam nulla quam, gravida non, commodo a, sodales sit amet, nisi.</p>\r\n<p>\r\n	Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.</p>\r\n<p>\r\n	Phasellus ultrices nulla quis nibh. Quisque a lectus. Donec consectetuer ligula vulputate sem tristique cursus. Nam nulla quam, gravida non, commodo a, sodales sit amet, nisi.Pellentesque fermentum dolor. Aliquam quam lectus, facilisis auctor, ultrices ut, elementum vulputate, nunc.Sed adipiscing ornare risus. Morbi est est, blandit sit amet, sagittis vel, euismod vel, velit. Pellentesque egestas sem. Suspendisse commodo ullamcorper magna.</p>\r\n<h2 class="\\&quot;out-team-heading\\&quot;">\r\n	OUT TEAM</h2>\r\n<div class="\\&quot;clerafix\\&quot;">\r\n	&nbsp;</div>\r\n<div class="\\&quot;add-box\\&quot;">\r\n	<img height="\\&quot;250\\&quot;" src="\\" width="\\&quot;300\\&quot;" />\r\n	<p a="" aliquam="" amet="" class="\\&quot;add-box" div="" donec="" eget="" facilisis="" felis="" fermentum.="" href="\\" j.c.="" justo="" managing="" mauris="" nec="" p="" porttitor="" sit="" view="">\r\n		<img height="\\&quot;250\\&quot;" src="\\" width="\\&quot;300\\&quot;" /></p>\r\n	&lt;p class=&quot;\\&amp;quot;about-bios&quot; text-upercase\\&quot;=&quot;&quot;&gt;<span>Sim B Whatley II</span><br />\r\n	Co FOUNDER\r\n	<p class="\\&quot;text-center\\&quot;">\r\n		Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra.</p>\r\n	<p class="\\&quot;text-center\\&quot;">\r\n		<a class="\\&quot;view-profile\\&quot;" href="\\">view profile</a></p>\r\n</div>\r\n', '', '1', '1', '', '', '', 2, '2015-06-08 18:44:04'),
(5, 'About Us', 16, '', 0, '<p class="lead">Hundreds of <span class="blute-text-smibold">great ads are now at your fingertips</span> with <br />\r\nCreativity''s new advertising gallery</p>', '<p>\r\n	Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>\r\n<p>\r\n	Vestibulum auctor dapibus neque.<br />\r\n	Phasellus ultrices nulla quis nibh. Quisque a lectus. Donec consectetuer ligula vulputate sem tristique cursus. Nam nulla quam, gravida non, commodo a, sodales sit amet, nisi.</p>\r\n<p>\r\n	Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.</p>\r\n<p>\r\n	Phasellus ultrices nulla quis nibh. Quisque a lectus. Donec consectetuer ligula vulputate sem tristique cursus. Nam nulla quam, gravida non, commodo a, sodales sit amet, nisi.Pellentesque fermentum dolor. Aliquam quam lectus, facilisis auctor, ultrices ut, elementum vulputate, nunc.Sed adipiscing ornare risus. Morbi est est, blandit sit amet, sagittis vel, euismod vel, velit. Pellentesque egestas sem. Suspendisse commodo ullamcorper magna.</p>\r\n<h2 class="\\&quot;out-team-heading\\&quot;">\r\n	OUT TEAM</h2>\r\n<div class="\\&quot;clerafix\\&quot;">\r\n	&nbsp;</div>\r\n<div class="\\&quot;add-box\\&quot;">\r\n	<img height="\\&quot;250\\&quot;" src="\\" width="\\&quot;300\\&quot;" />\r\n	<p a="" aliquam="" amet="" class="\\&quot;add-box" div="" donec="" eget="" facilisis="" felis="" fermentum.="" href="\\" j.c.="" justo="" managing="" mauris="" nec="" p="" porttitor="" sit="" view="">\r\n		<img height="\\&quot;250\\&quot;" src="\\" width="\\&quot;300\\&quot;" /></p>\r\n	&lt;p class=&quot;\\&amp;quot;about-bios&quot; text-upercase\\&quot;=&quot;&quot;&gt;<span>Sim B Whatley II</span><br />\r\n	Co FOUNDER\r\n	<p class="\\&quot;text-center\\&quot;">\r\n		Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra.</p>\r\n	<p class="\\&quot;text-center\\&quot;">\r\n		<a class="\\&quot;view-profile\\&quot;" href="\\">view profile</a></p>\r\n</div>\r\n', '', '1', '1', '<p class="lead">Hundreds of <span class="blute-text-smibold">great ads are now at your fingertips</span> with <br />\r\nCreativity''s new advertising gallery</p>', '', '', 2, '2015-06-08 18:59:40'),
(6, 'About Us', 16, '', 0, '<p class="lead">Hundreds of <span class="blute-text-smibold">great ads are now at your fingertips</span> with <br />\r\nCreativity''s new advertising gallery</p>', '<div class="container ">\r\n    	<ul class="bradcram">\r\n        	<li><a href="index.html">Home</a></li>     \r\n            <li>About Us</li>\r\n        </ul>\r\n        <h2 class="page-title">About Us</h2>\r\n        <div class="col-md-12">\r\n        	<p class="lead">Hundreds of <span class="blute-text-smibold">great ads are now at your fingertips</span> with <br />\r\nCreativity''s new advertising gallery</p>\r\n        </div>\r\n        \r\n        <div class="col-md-12">\r\n        	<div class="left-about-1">\r\n            	<img src="images/about-us-left01.jpg">\r\n            </div>\r\n            <div class="left-about-2">\r\n            	<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>\r\n                <p>Vestibulum auctor dapibus neque.<br />\r\nPhasellus ultrices nulla quis nibh. Quisque a lectus. Donec consectetuer ligula vulputate sem tristique cursus. Nam nulla quam, gravida non, commodo a, sodales sit amet, nisi.</p>\r\n<p>Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.</p>\r\n<p>Phasellus ultrices nulla quis nibh. Quisque a lectus. Donec consectetuer ligula vulputate sem tristique cursus. Nam nulla quam, gravida non, commodo a, sodales sit amet, nisi.Pellentesque fermentum dolor. Aliquam quam lectus, facilisis auctor, ultrices ut, elementum vulputate, nunc.Sed adipiscing ornare risus. Morbi est est, blandit sit amet, sagittis vel, euismod vel, velit. Pellentesque egestas sem. Suspendisse commodo ullamcorper magna.</p>\r\n<h2 class="out-team-heading">OUT TEAM</h2>\r\n<div class="clerafix"></div>\r\n			<div class="add-box">\r\n				<img src="images/BUTLER.jpg" width="300" height="250">\r\n                <p class="about-bios text-upercase"><span>J.C. BUTLER</span><br />\r\nManaging Partner</p>\r\n<p class="text-center">Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci.</p>\r\n<p class="text-center"><a href="#" class="view-profile">view profile</a></p>\r\n            </div>\r\n            <div class="add-box pull-right">\r\n				<img src="images/Whatley.jpg" width="300" height="250">\r\n                 <p class="about-bios text-upercase"><span>Sim B Whatley II</span><br />\r\nCo FOUNDER</p>\r\n<p class="text-center">Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra.</p>\r\n<p class="text-center"><a href="#" class="view-profile">view profile</a></p>\r\n            </div>\r\n\r\n\r\n            </div>\r\n        </div>\r\n        \r\n        \r\n       \r\n    <div class="divider-futered"></div>    \r\n    </div>', '', '1', '1', '<p class="lead">Hundreds of <span class="blute-text-smibold">great ads are now at your fingertips</span> with <br />\r\nCreativity''s new advertising gallery</p>', '', '', 2, '2015-06-08 19:01:02'),
(7, 'Useful Information', 3, '', 0, 'Useful Information', '<p>\r\n	Useful Information</p>\r\n', '', '0', '1', 'Useful Information', 'Useful Information', 'Useful Information', 2, '2015-06-13 17:47:02'),
(8, 'Contact Us', 17, '', 0, '<p class="lead">Hundreds of <span class="blute-text-smibold">great ads are now at your fingertips</span> with <br />\r\nCreativity''s new advertising gallery</p>', '<div class=\\"left-about-1\\">\r\n	<img src=\\"../images/about-us-left01.jpg\\" /></div>\r\n<div class=\\"left-about-2\\">\r\n	<p>\r\n		Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>\r\n	<p>\r\n		Vestibulum auctor dapibus neque.<br />\r\n		Phasellus ultrices nulla quis nibh. Quisque a lectus. Donec consectetuer ligula vulputate sem tristique cursus. Nam nulla quam, gravida non, commodo a, sodales sit amet, nisi.</p>\r\n	<p>\r\n		Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.</p>\r\n	<p>\r\n		Phasellus ultrices nulla quis nibh. Quisque a lectus. Donec consectetuer ligula vulputate sem tristique cursus. Nam nulla quam, gravida non, commodo a, sodales sit amet, nisi.Pellentesque fermentum dolor. Aliquam quam lectus, facilisis auctor, ultrices ut, elementum vulputate, nunc.Sed adipiscing ornare risus. Morbi est est, blandit sit amet, sagittis vel, euismod vel, velit. Pellentesque egestas sem. Suspendisse commodo ullamcorper magna.</p>\r\n	<h2 class=\\"out-team-heading\\">\r\n		OUT TEAM</h2>\r\n	<div class=\\"clerafix\\">\r\n		&nbsp;</div>\r\n	<div class=\\"add-box\\">\r\n		<img height=\\"250\\" src=\\"../images/BUTLER.jpg\\" width=\\"300\\" />\r\n		<p class=\\"about-bios text-upercase\\">\r\n			<span>J.C. BUTLER</span><br />\r\n			Managing Partner</p>\r\n		<p class=\\"text-center\\">\r\n			Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci.</p>\r\n		<p class=\\"text-center\\">\r\n			<a class=\\"view-profile\\" href=\\"#\\">view profile</a></p>\r\n	</div>\r\n	<div class=\\"add-box pull-right\\">\r\n		<img height=\\"250\\" src=\\"../images/Whatley.jpg\\" width=\\"300\\" />\r\n		<p class=\\"about-bios text-upercase\\">\r\n			<span>Sim B Whatley II</span><br />\r\n			Co FOUNDER</p>\r\n		<p class=\\"text-center\\">\r\n			Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra.</p>\r\n		<p class=\\"text-center\\">\r\n			<a class=\\"view-profile\\" href=\\"#\\">view profile</a></p>\r\n	</div>\r\n</div>\r\n', '', '1', '0', '', '', '', 2, '2015-06-13 17:49:38'),
(9, 'Contact Us', 17, '', 0, '<p class="lead">Hundreds of <span class="blute-text-smibold">great ads are now at your fingertips</span> with <br />\r\nCreativity''s new advertising gallery</p>', '<div class="\\&quot;left-about-1\\&quot;">\r\n	<img src="\\" /></div>\r\n<div class="\\&quot;left-about-2\\&quot;">\r\n	<p>\r\n		Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>\r\n	<p>\r\n		Vestibulum auctor dapibus neque.<br />\r\n		Phasellus ultrices nulla quis nibh. Quisque a lectus. Donec consectetuer ligula vulputate sem tristique cursus. Nam nulla quam, gravida non, commodo a, sodales sit amet, nisi.</p>\r\n	<p>\r\n		Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.</p>\r\n	<p>\r\n		Phasellus ultrices nulla quis nibh. Quisque a lectus. Donec consectetuer ligula vulputate sem tristique cursus. Nam nulla quam, gravida non, commodo a, sodales sit amet, nisi.Pellentesque fermentum dolor. Aliquam quam lectus, facilisis auctor, ultrices ut, elementum vulputate, nunc.Sed adipiscing ornare risus. Morbi est est, blandit sit amet, sagittis vel, euismod vel, velit. Pellentesque egestas sem. Suspendisse commodo ullamcorper magna.</p>\r\n	<h2 class="\\&quot;out-team-heading\\&quot;">\r\n		OUT TEAM</h2>\r\n	<div class="\\&quot;clerafix\\&quot;">\r\n		&nbsp;</div>\r\n	<div class="\\&quot;add-box\\&quot;">\r\n		<img height="\\&quot;250\\&quot;" src="\\" width="\\&quot;300\\&quot;" />\r\n		<p a="" aliquam="" amet="" class="\\&quot;add-box" div="" donec="" eget="" facilisis="" felis="" fermentum.="" href="\\" j.c.="" justo="" managing="" mauris="" nec="" p="" porttitor="" sit="" view="">\r\n			<img height="\\&quot;250\\&quot;" src="\\" width="\\&quot;300\\&quot;" /></p>\r\n		&lt;p class=&quot;\\&amp;quot;about-bios&quot; text-upercase\\&quot;=&quot;&quot;&gt; <span>Sim B Whatley II</span><br />\r\n		Co FOUNDER\r\n		<p class="\\&quot;text-center\\&quot;">\r\n			Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra.</p>\r\n		<p class="\\&quot;text-center\\&quot;">\r\n			<a class="\\&quot;view-profile\\&quot;" href="\\">view profile</a></p>\r\n	</div>\r\n</div>\r\n', '', '0', '1', '<p class="lead">Hundreds of <span class="blute-text-smibold">great ads are now at your fingertips</span> with <br />\r\nCreativity''s new advertising gallery</p>', '', '', 2, '2015-06-13 17:51:05'),
(10, 'About Us', 16, '', 0, '<p class="lead">Hundreds of <span class="blute-text-smibold">great ads are now at your fingertips</span> with <br />\r\nCreativity''s new advertising gallery</p>', '\r\n        	<div class="left-about-1">\r\n            	<img src="../images/about-us-left01.jpg">\r\n            </div>\r\n            <div class="left-about-2">\r\n            	<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>\r\n                <p>Vestibulum auctor dapibus neque.<br />\r\nPhasellus ultrices nulla quis nibh. Quisque a lectus. Donec consectetuer ligula vulputate sem tristique cursus. Nam nulla quam, gravida non, commodo a, sodales sit amet, nisi.</p>\r\n<p>Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.</p>\r\n<p>Phasellus ultrices nulla quis nibh. Quisque a lectus. Donec consectetuer ligula vulputate sem tristique cursus. Nam nulla quam, gravida non, commodo a, sodales sit amet, nisi.Pellentesque fermentum dolor. Aliquam quam lectus, facilisis auctor, ultrices ut, elementum vulputate, nunc.Sed adipiscing ornare risus. Morbi est est, blandit sit amet, sagittis vel, euismod vel, velit. Pellentesque egestas sem. Suspendisse commodo ullamcorper magna.</p>\r\n<h2 class="out-team-heading">OUT TEAM</h2>\r\n<div class="clerafix"></div>\r\n			<div class="add-box">\r\n				<img src="../images/BUTLER.jpg" width="300" height="250">\r\n                <p class="about-bios text-upercase"><span>J.C. BUTLER</span><br />\r\nManaging Partner</p>\r\n<p class="text-center">Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci.</p>\r\n<p class="text-center"><a href="#" class="view-profile">view profile</a></p>\r\n            </div>\r\n            <div class="add-box pull-right">\r\n				<img src="../images/Whatley.jpg" width="300" height="250">\r\n                 <p class="about-bios text-upercase"><span>Sim B Whatley II</span><br />\r\nCo FOUNDER</p>\r\n<p class="text-center">Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra.</p>\r\n<p class="text-center"><a href="#" class="view-profile">view profile</a></p>\r\n            </div>\r\n\r\n\r\n            </div>\r\n        ', '', '1', '1', '<p class="lead">Hundreds of <span class="blute-text-smibold">great ads are now at your fingertips</span> with <br />\r\nCreativity''s new advertising gallery</p>', '', '', 2, '2016-02-04 10:48:49'),
(11, 'About Us', 16, '', 0, '', '<div class="left-about-1">\n	<img src="../images/about-us-left01.jpg" /></div>\n<div class="left-about-2">\n	<p>\n		Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, viverra non, semper suscipit, posuere a, pede.</p>\n	<p>\n		Vestibulum auctor dapibus neque.<br />\n		Phasellus ultrices nulla quis nibh. Quisque a lectus. Donec consectetuer ligula vulputate sem tristique cursus. Nam nulla quam, gravida non, commodo a, sodales sit amet, nisi.</p>\n	<p>\n		Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.</p>\n	<p>\n		Phasellus ultrices nulla quis nibh. Quisque a lectus. Donec consectetuer ligula vulputate sem tristique cursus. Nam nulla quam, gravida non, commodo a, sodales sit amet, nisi.Pellentesque fermentum dolor. Aliquam quam lectus, facilisis auctor, ultrices ut, elementum vulputate, nunc.Sed adipiscing ornare risus. Morbi est est, blandit sit amet, sagittis vel, euismod vel, velit. Pellentesque egestas sem. Suspendisse commodo ullamcorper magna.</p>\n	<h2 class="out-team-heading">\n		OUT TEAM</h2>\n	<div class="clerafix">\n		&nbsp;</div>\n	<div class="add-box">\n		<img height="250" src="../images/BUTLER.jpg" width="300" />\n		<p class="about-bios text-upercase">\n			<span>J.C. BUTLER</span><br />\n			Managing Partner</p>\n		<p class="text-center">\n			Donec nec justo eget felis facilisis fermentum. Aliquam porttitor mauris sit amet orci.</p>\n		<p class="text-center">\n			<a class="view-profile" href="#">view profile</a></p>\n	</div>\n	<div class="add-box pull-right">\n		<img height="250" src="../images/Whatley.jpg" width="300" />\n		<p class="about-bios text-upercase">\n			<span>Sim B Whatley II</span><br />\n			Co FOUNDER</p>\n		<p class="text-center">\n			Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra.</p>\n		<p class="text-center">\n			<a class="view-profile" href="#">view profile</a></p>\n	</div>\n</div>\n', '', '1', '1', '<p class="lead">Hundreds of <span class="blute-text-smibold">great ads are now at your fingertips</span> with <br />\nCreativity''s new advertising gallery</p>', '', '', 2, '2016-02-04 10:49:11'),
(12, 'About Us', 16, '', 0, '', '<div class="left-about-1">\n	<img src="../images/about-us-left01.jpg" /></div>\n<div class="left-about-2">\n	<p>\n		Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse urna nibh, \n\nviverra non, semper suscipit, posuere a, pede.</p>\n	<p>\n		Vestibulum auctor dapibus neque.<br />\n		Phasellus ultrices nulla quis nibh. Quisque a lectus. Donec consectetuer ligula vulputate sem tristique cursus. Nam nulla quam, gravida non, commodo a, sodales \n\nsit amet, nisi.</p>\n	<p>\n		Morbi in sem quis dui placerat ornare. Pellentesque odio nisi, euismod in, pharetra a, ultricies in, diam. Sed arcu. Cras consequat.</p>\n	<p>\n		Phasellus ultrices nulla quis nibh. Quisque a lectus. Donec consectetuer ligula vulputate sem tristique cursus. Nam nulla quam, gravida non, commodo a, sodales \n\nsit amet, nisi.Pellentesque fermentum dolor. Aliquam quam lectus, facilisis auctor, ultrices ut, elementum vulputate, nunc.Sed adipiscing ornare risus. Morbi est est, blandit sit \n\namet, sagittis vel, euismod vel, velit. Pellentesque egestas sem. Suspendisse commodo ullamcorper magna.</p>\n	<h2 class="out-team-heading">\n		OUT TEAM</h2>\n	<div class="clerafix">\n		&nbsp;</div>\n</div>\n', '', '1', '1', '', '', '', 2, '2016-02-04 10:50:39'),
(13, 'About Us', 16, '', 0, '', '<div class="left-about-1">\n	<img src="../images/about-us-left01.jpg" /></div>\n<div class="left-about-2">\n	<p></p>\n	\n	\n\n	\n	<div class="clerafix">\n		&nbsp;</div>\n</div>\n', '', '1', '1', '', '', '', 2, '2016-02-04 10:51:46'),
(14, 'How It Works', 20, '', 0, '', '<p>\n	<img alt=\\"\\" src=\\"http://demo.thecodeandco.net/uploads/userfiles/images/top_1bg.png\\" style=\\"width: 124px; height: 121px;\\" /></p>\n<p>\n	<img alt=\\"\\" src=\\"http://demo.thecodeandco.net/uploads/userfiles/images/top_2bg.png\\" style=\\"width: 124px; height: 121px;\\" /></p>\n<p>\n	<img alt=\\"\\" src=\\"http://demo.thecodeandco.net/uploads/userfiles/images/top_3bg.png\\" style=\\"width: 124px; height: 121px;\\" /></p>\n<p>\n	<img alt=\\"\\" src=\\"http://demo.thecodeandco.net/uploads/userfiles/images/top_4bg.png\\" style=\\"width: 124px; height: 121px;\\" /></p>\n<p>\n	<img alt=\\"\\" src=\\"http://demo.thecodeandco.net/uploads/userfiles/images/top_5bg.png\\" /></p>\n', '', '1', '0', '', '', '', 2, '2016-02-04 11:23:33'),
(15, 'How It Works', 20, '', 0, '', '<div class="steps_box">\n	<img alt="" src="http://demo.thecodeandco.net/uploads/userfiles/images/top_1bg.png" />\n<div class="steps_point">1.	Click on “Post an Ad” in the top right corner. </div>\n<div class="steps_point">2.	Returning users sign in, new users fill in your sign up details. </div>\n<div class="steps_point">3.	Follow the quick and easy steps to fill out the information about your ad. </div>\n<div class="steps_point">4.	Preview your ad details and click “Post Ad” to finish publishing your ad. </div>\n<div class="steps_point">5.	First time users will be asked to send an SMS to activate their ad/account. </div>\n</div>\n<div class="steps_box">\n\n	<img alt="" src="http://demo.thecodeandco.net/uploads/userfiles/images/top_2bg.png" />\n</div>\n<div class="steps_box">\n\n	<img alt="" src="http://demo.thecodeandco.net/uploads/userfiles/images/top_3bg.png" />\n</div>\n<div class="steps_box">\n\n	<img alt="" src="http://demo.thecodeandco.net/uploads/userfiles/images/top_4bg.png" />\n</div>\n<div class="steps_box">\n\n	<img alt="" src="http://demo.thecodeandco.net/uploads/userfiles/images/top_5bg.png" />\n</div>\n', '', '1', '0', '', '', '', 2, '2016-02-04 11:25:18'),
(16, 'How It Works', 20, '', 0, '', '<div class="steps_box">\n	<img alt="" src="http://demo.thecodeandco.net/uploads/userfiles/images/top_1bg.png" />\n	<div class="steps_head">\n                 Five simple steps to place your free ad in under 5 minutes</div>\n	<div class="steps_point">\n		1. Click on "Post an Ad" in the top right corner.</div>\n	<div class="steps_point">\n		2. Returning users sign in, new users fill in your sign up details.</div>\n	<div class="steps_point">\n		3. Follow the quick and easy steps to fill out the information about your ad.</div>\n	<div class="steps_point">\n		4. Preview your ad details and click "Post Ad" to finish publishing your ad.</div>\n	<div class="steps_point">\n		5. First time users will be asked to send an SMS to activate their ad/account.</div>\n</div>\n<div class="steps_box">\n	<img alt="" src="http://demo.thecodeandco.net/uploads/userfiles/images/top_2bg.png" /></div>\n<div class="steps_box">\n	<img alt="" src="http://demo.thecodeandco.net/uploads/userfiles/images/top_3bg.png" /></div>\n<div class="steps_box">\n	<img alt="" src="http://demo.thecodeandco.net/uploads/userfiles/images/top_4bg.png" /></div>\n<div class="steps_box">\n	<img alt="" src="http://demo.thecodeandco.net/uploads/userfiles/images/top_5bg.png" /></div>\n', '', '1', '0', '', '', '', 2, '2016-02-04 11:27:24'),
(17, 'How It Works', 20, '', 0, '', '<div class="steps_boxes">\n	<img alt="" src="http://demo.thecodeandco.net/uploads/userfiles/images/top_1bg.png" />\n	<div class="steps_head">\n		Five simple steps to place your free ad in under 5 minutes</div>\n	<div class="steps_point">\n		1. Click on &quot;Post an Ad&quot; in the top right corner.</div>\n	<div class="steps_point">\n		2. Returning users sign in, new users fill in your sign up details.</div>\n	<div class="steps_point">\n		3. Follow the quick and easy steps to fill out the information about your ad.</div>\n	<div class="steps_point">\n		4. Preview your ad details and click &quot;Post Ad&quot; to finish publishing your ad.</div>\n	<div class="steps_point">\n		5. First time users will be asked to send an SMS to activate their ad/account.</div>\n</div>\n<div class="steps_boxes">\n	<img alt="" src="http://demo.thecodeandco.net/uploads/userfiles/images/top_2bg.png" /></div>\n<div class="steps_boxes">\n	<img alt="" src="http://demo.thecodeandco.net/uploads/userfiles/images/top_3bg.png" /></div>\n<div class="steps_boxes">\n	<img alt="" src="http://demo.thecodeandco.net/uploads/userfiles/images/top_4bg.png" /></div>\n<div class="steps_boxes">\n	<img alt="" src="http://demo.thecodeandco.net/uploads/userfiles/images/top_5bg.png" /></div>\n', '', '1', '0', '', '', '', 2, '2016-02-04 11:50:22'),
(18, 'How It Works', 20, '', 0, '', '<div class="steps_boxes">\n	<img alt="" src="http://demo.thecodeandco.net/uploads/userfiles/images/top_1bg.png" />\n	<div class="steps_head">\n		Five simple steps to place your free ad in under 5 minutes</div>\n	<div class="steps_point">\n		1. Click on &quot;Post an Ad&quot; in the top right corner.</div>\n	<div class="steps_point">\n		2. Returning users sign in, new users fill in your sign up details.</div>\n	<div class="steps_point">\n		3. Follow the quick and easy steps to fill out the information about your ad.</div>\n	<div class="steps_point">\n		4. Preview your ad details and click &quot;Post Ad&quot; to finish publishing your ad.</div>\n	<div class="steps_point">\n		5. First time users will be asked to send an SMS to activate their ad/account.</div>\n</div>\n<div class="steps_boxes">\n	<img alt="" src="http://demo.thecodeandco.net/uploads/userfiles/images/top_2bg.png" />\n	<div class="steps_head">\n		Respond to an ad</div>\n	<div class="steps_point">\n		1. While viewing the ad you are interested in, scroll to the bottom of the page.</div>\n	<div class="steps_point">\n		2. In the &quot;Respond to the Ad&quot; box, fill in the information requested and write your message.</div>\n	<div class="steps_point">\n		3. Press the &quot;send reply&quot; button, and you&#39;re done!</div>\n	<div class="steps_point">\n		4. The advertiser will receive your message directly to his/her email inbox.</div>\n</div>\n<div class="steps_boxes">\n	<img alt="" src="http://demo.thecodeandco.net/uploads/userfiles/images/top_3bg.png" />\n	<div class="steps_head">\n		Register/Sign up</div>\n	<div class="steps_point">\n		1. Click on the &quot;Register&quot; link at the top of the page above &quot;Post an Ad&quot;.</div>\n	<div class="steps_point">\n		2. Fill in all of the required information, agree to the Terms, and click the &quot;sign up&quot; button.</div>\n	<div class="steps_point">\n		3. Exceed Trader will send you a verification message to the email you provided.</div>\n	<div class="steps_point">\n		4. Click on the verification link from the email (Check your spam folder if you don&#39;t find it).</div>\n	<div class="steps_point">\n		5. Your registration is now complete, you can now fill out your profile.</div>\n</div>\n<div class="steps_boxes">\n	<img alt="" src="http://demo.thecodeandco.net/uploads/userfiles/images/top_4bg.png" />\n	<div class="steps_head">\n		Edit/Delete your Ad</div>\n	<div class="steps_point">\n		1. Make sure you are logged into your account on Exceed Trader.</div>\n	<div class="steps_point">\n		2. Go to your account page by clicking on the &quot;Account&quot; link at the top of the page above &quot;Post an Ad&quot;.</div>\n	<div class="steps_point">\n		3. Click on the &quot;My Ads&quot; tab.</div>\n	<div class="steps_point">\n		4. From this page you can edit or delete any of your ads on Exceed Trader.</div>\n</div>\n<div class="steps_boxes">\n	<img alt="" src="http://demo.thecodeandco.net/uploads/userfiles/images/top_5bg.png" />\n	<div class="steps_head">\n		Edit account info:</div>\n	<div class="steps_point">\n		1. Make sure you are logged into your account on Exceed Trader.</div>\n	<div class="steps_point">\n		2. Go to your account page by clicking on the dropdown link next to your name at the top of the page above &quot;Post an Ad&quot;.</div>\n	<div class="steps_point">\n		3. Click on the &quot;My Profile&quot; tab.</div>\n	<div class="steps_point">\n		4. Edit the necessary you require and the click the &quot;Update Profile&quot; button.</div>\n	<div class="steps_point">\n		5. To edit account settings click on the &quot;Account Settings&quot; button from the dropdown link next to your name at the top of the page advise &quot;Post an Ad&quot;.</div>\n</div>\n', '', '1', '0', '', '', '', 2, '2016-02-04 11:53:22'),
(19, 'About Us', 16, '', 0, '', '<div class="left-about-1">\n	<img src="../images/about-us-left01.jpg" /></div>\n<div class="left-about-2">\n	<p>Exceed Trader is a free classifieds website for users in Trinidad and Tobago whereby they can buy, sell, trade or find just about anything they are looking for within and outside their community. Its birth was a result of rapid changes in technological advancements and to make the buying, selling and trading of new and used items easy and accessible to all.</p>\n\n<p>We exist in a community where underused goods can be redistributed to fill a new need, and become wanted again and space; skills and money are exchanged and traded in new ways that don’t always require centralized institutions.</p>\n\n<p>Exceed Traders is not only a buying and selling platform but it also offer users the opportunity to advertise products and services via banners. With the digital landscape shifting rapidly towards mobile and video this is a very efficient and effective medium of advertising. </p>\n\n<p>We offer our users the ability to list and search classified ads in all parts of Trinidad and Tobago with ease and from any electronic device. To ensure the safety of our users Exceed Trader reviews all ads before posting.  Exceed has its main operation in Trinidad with hopes of further expansion in the Caribbean in the near future. </p>\n\n	<div class="clerafix">\n		&nbsp;</div>\n</div>\n', '', '1', '1', '', '', '', 2, '2016-02-04 12:05:45'),
(20, 'About Us', 16, '', 0, '', '<div class="left-about-1">\n	<img src="../images/about-us-left01.jpg" style="margin-top:5px;" /></div>\n<div class="left-about-2">\n	<p>\n		Exceed Trader is a free classifieds website for users in Trinidad and Tobago whereby they can buy, sell, trade or find just about anything they are looking for within and outside their community. Its birth was a result of rapid changes in technological advancements and to make the buying, selling and trading of new and used items easy and accessible to all.</p>\n	<p>\n		We exist in a community where underused goods can be redistributed to fill a new need, and become wanted again and space; skills and money are exchanged and traded in new ways that don&rsquo;t always require centralized institutions.</p>\n	<p>\n		Exceed Traders is not only a buying and selling platform but it also offer users the opportunity to advertise products and services via banners. With the digital landscape shifting rapidly towards mobile and video this is a very efficient and effective medium of advertising.</p>\n	<p>\n		We offer our users the ability to list and search classified ads in all parts of Trinidad and Tobago with ease and from any electronic device. To ensure the safety of our users Exceed Trader reviews all ads before posting. Exceed has its main operation in Trinidad with hopes of further expansion in the Caribbean in the near future.</p>\n	<div class="clerafix">\n		&nbsp;</div>\n</div>\n', '', '1', '1', '', '', '', 2, '2016-02-04 12:06:31');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_countries`
--

CREATE TABLE IF NOT EXISTS `tbl_countries` (
  `id` int(3) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `iso_alpha2` varchar(2) DEFAULT NULL,
  `iso_alpha3` varchar(3) DEFAULT NULL,
  `iso_numeric` int(11) DEFAULT NULL,
  `currency_code` char(3) DEFAULT NULL,
  `currency_name` varchar(32) DEFAULT NULL,
  `currrency_symbol` varchar(3) DEFAULT NULL,
  `flag` varchar(200) DEFAULT NULL,
  `small_flag` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=241 ;

--
-- Dumping data for table `tbl_countries`
--

INSERT INTO `tbl_countries` (`id`, `name`, `iso_alpha2`, `iso_alpha3`, `iso_numeric`, `currency_code`, `currency_name`, `currrency_symbol`, `flag`, `small_flag`) VALUES
(225, 'United Arab Emirates', 'AE', 'ARE', 784, 'AED', 'Dirham', NULL, 'FW_0016_United Arab Emirates.jpg', 'FW_0016_United Arab Emirates.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_education`
--

CREATE TABLE IF NOT EXISTS `tbl_education` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `created_date` datetime NOT NULL,
  `is_active` enum('0','1') NOT NULL DEFAULT '1',
  `is_deleted` enum('0','1') NOT NULL DEFAULT '0',
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `tbl_education`
--

INSERT INTO `tbl_education` (`id`, `title`, `created_date`, `is_active`, `is_deleted`, `sort_order`) VALUES
(1, 'Bachelors Degree', '0000-00-00 00:00:00', '1', '0', 1),
(2, 'Masters Degree', '0000-00-00 00:00:00', '1', '0', 2),
(3, 'PhD', '0000-00-00 00:00:00', '1', '0', 3);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_employment_type`
--

CREATE TABLE IF NOT EXISTS `tbl_employment_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `created_date` datetime NOT NULL,
  `is_active` enum('0','1') NOT NULL DEFAULT '1',
  `is_deleted` enum('0','1') NOT NULL DEFAULT '0',
  `sort_order` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tbl_employment_type`
--

INSERT INTO `tbl_employment_type` (`id`, `title`, `created_date`, `is_active`, `is_deleted`, `sort_order`) VALUES
(1, 'Full Time', '0000-00-00 00:00:00', '1', '0', 1),
(2, 'Part Time', '0000-00-00 00:00:00', '1', '0', 2),
(3, 'Contract', '0000-00-00 00:00:00', '1', '0', 3),
(4, 'Temporary', '0000-00-00 00:00:00', '1', '0', 4),
(5, 'Other', '0000-00-00 00:00:00', '1', '0', 5);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_fuel_type`
--

CREATE TABLE IF NOT EXISTS `tbl_fuel_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `created_date` datetime NOT NULL,
  `is_active` enum('0','1') NOT NULL DEFAULT '0',
  `is_deleted` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tbl_fuel_type`
--

INSERT INTO `tbl_fuel_type` (`id`, `title`, `created_date`, `is_active`, `is_deleted`) VALUES
(1, 'Petrol', '2015-03-01 13:02:10', '1', '0'),
(2, 'Diesel', '2015-03-01 13:04:20', '0', '1'),
(3, 'Gasolin', '2015-03-04 09:06:43', '1', '0'),
(4, 'Solar', '2015-04-17 23:27:11', '1', '0');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_homepage_image`
--

CREATE TABLE IF NOT EXISTS `tbl_homepage_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_title` varchar(255) NOT NULL,
  `image_title_2` varchar(255) DEFAULT NULL,
  `image_path` varchar(255) DEFAULT NULL,
  `home_page_image_order` int(11) NOT NULL,
  `created_date_time` datetime NOT NULL,
  `is_active` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `tbl_homepage_image`
--

INSERT INTO `tbl_homepage_image` (`id`, `image_title`, `image_title_2`, `image_path`, `home_page_image_order`, `created_date_time`, `is_active`) VALUES
(2, 'Sell your car fast on Classifie.com', ' Create your advert in minutes', '25022015143821_slider_01.jpg', 1, '2015-02-25 14:38:21', '1'),
(3, 'Sell your car fast on Classifie.com', 'Create your advert in minutes', '25022015144055_slider_02.jpg', 2, '2015-02-25 14:40:55', '1'),
(4, 'Best car best price', 'Exchange your car ', '18042015005807_Jellyfish2.png', 3, '2015-04-18 00:58:08', '1'),
(6, 'Test banner images', 'Testing banner image', '18042015005827_Jellyfish2.png', 5, '2015-04-18 00:58:27', '0'),
(7, 'Test banner images', 'Testing banner image', '18042015005830_Jellyfish2.png', 6, '2015-04-18 00:58:30', '0'),
(8, 'Test banner images', 'Testing banner image', '18042015005833_Jellyfish2.png', 7, '2015-04-18 00:58:33', '0'),
(9, 'Test banner images', 'Testing banner image', '18042015005836_Jellyfish2.png', 8, '2015-04-18 00:58:37', '0'),
(10, 'Test banner images', 'Testing banner image', '18042015005841_Jellyfish2.png', 9, '2015-04-18 00:58:41', '0'),
(11, 'Test banner images', 'Testing banner image', '18042015005848_Jellyfish2.png', 10, '2015-04-18 00:58:48', '0'),
(12, 'Test banner images', 'Testing banner image', '18042015005852_Jellyfish2.png', 11, '2015-04-18 00:58:52', '0'),
(13, 'Test banner images', 'Testing banner image', '18042015005856_Jellyfish2.png', 12, '2015-04-18 00:58:56', '0'),
(14, 'Test banner images', 'Testing banner image', '18042015005907_Jellyfish2.png', 13, '2015-04-18 00:59:07', '0');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_localities`
--

CREATE TABLE IF NOT EXISTS `tbl_localities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `city_id` int(11) NOT NULL,
  `locality` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `tbl_localities`
--

INSERT INTO `tbl_localities` (`id`, `city_id`, `locality`) VALUES
(1, 96, 'Yas Marina Circuit'),
(2, 96, 'Safaris'),
(3, 96, 'The Corniche'),
(4, 96, 'Ferrari World'),
(5, 96, 'Qasr Al Hosn'),
(6, 115, 'Yas Marina Circuit '),
(7, 115, 'Safaris'),
(8, 115, 'The Corniche'),
(9, 115, 'Ferrari World'),
(10, 115, 'Qasr Al Hosn'),
(11, 116, 'Yas Marina Circuit '),
(12, 116, 'Safaris'),
(13, 369, 'Yas Marina Circuit'),
(14, 369, 'Safaris'),
(15, 404, 'Yas Marina Circuit'),
(16, 404, 'Safaris'),
(17, 712, 'Yas Marina Circuit'),
(18, 712, 'Safaris'),
(19, 765, 'Yas Marina Circuit'),
(20, 765, 'Safaris'),
(21, 812, 'Yas Marina Circuit'),
(22, 812, 'Safaris');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_locality`
--

CREATE TABLE IF NOT EXISTS `tbl_locality` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `sub_title` varchar(255) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `locality_image` varchar(255) NOT NULL,
  `locality_icon_large` varchar(255) NOT NULL,
  `locality_order` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_active` enum('0','1') NOT NULL DEFAULT '1',
  `is_deleted` enum('0','1') NOT NULL DEFAULT '0',
  `deleted_date_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=140 ;

--
-- Dumping data for table `tbl_locality`
--

INSERT INTO `tbl_locality` (`id`, `title`, `sub_title`, `parent_id`, `locality_image`, `locality_icon_large`, `locality_order`, `created_by`, `updated_by`, `created_date`, `updated_date`, `is_active`, `is_deleted`, `deleted_date_time`) VALUES
(1, 'California', '', 1, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(2, 'Carapichiama', '', 1, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(3, 'Caroni', '', 1, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(4, 'Chaguanas', '', 1, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(5, 'Charlieville', '', 1, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(6, 'Chase Village', '', 1, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(7, 'Claxton Bay', '', 1, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(8, 'Couva', '', 1, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(9, 'Cunupia', '', 1, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(10, 'Freeport', '', 1, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(11, 'Gasparillo', '', 1, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(12, 'Gran Couva', '', 1, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(13, 'Kelly Village', '', 1, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(14, 'Longdenville', '', 1, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(15, 'Piparo', '', 1, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(16, 'Point Lisas', '', 1, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(17, 'St.Helena', '', 1, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(18, 'Tabaquite', '', 1, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(19, 'Talparo', '', 1, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(20, 'Aranguez', '', 2, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(21, 'Arima', '', 2, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(22, 'Arouca', '', 2, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(23, 'Barataria', '', 2, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(24, 'Biche', '', 2, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(25, 'Blanchisseuse', '', 2, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(26, 'Champs Fleurs', '', 2, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(27, 'Cumuto', '', 2, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(28, 'Curepe', '', 2, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(29, 'El Dorado', '', 2, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(30, 'El Socorro', '', 2, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(31, 'La Horquetta', '', 2, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(32, 'Las Cuevas', '', 2, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(33, 'Las Lomas', '', 2, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(34, 'Lopinot', '', 2, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(35, 'Macoya', '', 2, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(36, 'Manzanilla', '', 2, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(37, 'Maracas', '', 2, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(38, 'Mt. Hope', '', 2, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(39, 'Piarco', '', 2, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(40, 'Roxborough', '', 2, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(41, 'Salybia', '', 2, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(42, 'San Juan', '', 2, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(43, 'Sangre Grande', '', 2, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(44, 'Santa Cruz', '', 2, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(45, 'Santa Rosa', '', 2, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(46, 'St. Augustine', '', 2, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(47, 'St. Joseph', '', 2, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(48, 'Tacariqua', '', 2, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(49, 'Toco', '', 2, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(50, 'Trinicity', '', 2, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(51, 'Tunapuna', '', 2, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(52, 'Valencia', '', 2, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(53, 'Valsayn', '', 2, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(54, 'Alyce Glen', '', 3, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(55, 'Bayshore', '', 3, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(56, 'Belmont', '', 3, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(57, 'Carenage', '', 3, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(58, 'Cascade', '', 3, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(59, 'Chaguaramus', '', 3, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(60, 'Cocorite', '', 3, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(61, 'Diego Martin', '', 3, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(62, 'Glencoe', '', 3, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(63, 'Hillsboro', '', 3, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(64, 'Lavantile', '', 3, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(65, 'Long Circular', '', 3, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(66, 'Maraval', '', 3, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(67, 'Moka', '', 3, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(68, 'Morvant', '', 3, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(69, 'Mucurapo', '', 3, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(70, 'Port Of Spain', '', 3, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(71, 'St. Anns', '', 3, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(72, 'St. James', '', 3, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(73, 'Westmorrings', '', 3, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(74, 'Woodbrook', '', 3, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(75, 'Cumberbach', '', 4, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(76, 'Galeota', '', 4, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(77, 'Guanapo', '', 4, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(78, 'Guayaguayare', '', 4, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(79, 'Mayaro', '', 4, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(80, 'Rio Claro', '', 4, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(81, 'Willamsville', '', 4, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(82, 'Aripero', '', 5, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(83, 'Avocat', '', 5, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(84, 'Barrackpore', '', 5, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(85, 'Bel Air', '', 5, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(86, 'Cedros', '', 5, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(87, 'Cocoyea', '', 5, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(88, 'Cross Crossings', '', 5, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(89, 'Debe', '', 5, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(90, 'Duncan Village', '', 5, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(91, 'Fyzabad', '', 5, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(92, 'Gopaul Lands', '', 5, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(93, 'Gulf View', '', 5, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(94, 'La Brea', '', 5, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(95, 'La Romain', '', 5, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(96, 'Lange Park', '', 5, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(97, 'Marabella', '', 5, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(98, 'Moruga', '', 5, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(99, 'Palmiste', '', 5, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(100, 'Penal', '', 5, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(101, 'Phillipine', '', 5, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(102, 'Point Fortin', '', 5, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(103, 'Point-a-Pierre', '', 5, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(104, 'Princes Town', '', 5, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(105, 'San Fernando', '', 5, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(106, 'Siparia', '', 5, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(107, 'Oropouche', '', 5, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(108, 'Ste. Madeleine', '', 5, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(109, 'Union Hall', '', 5, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(110, 'Vistabella', '', 5, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(111, 'Castara', '', 6, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(112, 'Charlotteville', '', 6, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(113, 'Englishmans’ Bay', '', 6, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(114, 'Goodwood', '', 6, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(115, 'Roxborough', '', 6, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(116, 'Speyside', '', 6, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(117, 'Arnos Vale', '', 7, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(118, 'Bacolet', '', 7, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(119, 'Bacolet Point', '', 7, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(120, 'Bethany', '', 7, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(121, 'Black Rock', '', 7, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(122, 'Bon Accord', '', 7, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(123, 'Carnbee', '', 7, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(124, 'Concordia', '', 7, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(125, 'Cove', '', 7, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(126, 'Crown Point', '', 7, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(127, 'Grafton ', '', 7, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(128, 'Granby', '', 7, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(129, 'Grange', '', 7, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(130, 'Lambeau', '', 7, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(131, 'Lowlands', '', 7, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(132, 'Mason Hall', '', 7, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(133, 'Mt. Irvine', '', 7, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(134, 'Orange Hill', '', 7, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(135, 'Plymouth', '', 7, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(136, 'Sandy Point', '', 7, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(137, 'Scarborough', '', 7, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(138, 'Signal Hill ', '', 7, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00'),
(139, 'Plantations', '', 7, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-07-31 12:24:40', '1', '0', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_locality_old`
--

CREATE TABLE IF NOT EXISTS `tbl_locality_old` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `sub_title` varchar(255) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `locality_image` varchar(255) NOT NULL,
  `locality_icon_large` varchar(255) NOT NULL,
  `locality_order` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_active` enum('0','1') NOT NULL DEFAULT '1',
  `is_deleted` enum('0','1') NOT NULL DEFAULT '0',
  `deleted_date_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

--
-- Dumping data for table `tbl_locality_old`
--

INSERT INTO `tbl_locality_old` (`id`, `title`, `sub_title`, `parent_id`, `locality_image`, `locality_icon_large`, `locality_order`, `created_by`, `updated_by`, `created_date`, `updated_date`, `is_active`, `is_deleted`, `deleted_date_time`) VALUES
(1, 'Yas Marina Circuit', '', 116, '', '', 0, 2, 2, '2015-06-13 12:29:22', '2015-06-14 06:28:34', '1', '0', '0000-00-00 00:00:00'),
(2, 'Safaris', '', 96, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-06-10 04:50:05', '1', '0', '0000-00-00 00:00:00'),
(3, 'The Corniche', '', 96, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-06-10 04:50:05', '1', '0', '0000-00-00 00:00:00'),
(4, 'Ferrari World', '', 96, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-06-10 04:50:05', '1', '0', '0000-00-00 00:00:00'),
(5, 'Qasr Al Hosn', '', 96, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-06-10 04:50:05', '1', '0', '0000-00-00 00:00:00'),
(6, 'Yas Marina Circuit ', '', 115, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-06-10 04:50:05', '1', '0', '0000-00-00 00:00:00'),
(7, 'Safaris', '', 115, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-06-10 04:50:05', '1', '0', '0000-00-00 00:00:00'),
(8, 'The Corniche', '', 115, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-06-10 04:50:05', '1', '0', '0000-00-00 00:00:00'),
(9, 'Ferrari World', '', 115, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-06-10 04:50:05', '1', '0', '0000-00-00 00:00:00'),
(10, 'Qasr Al Hosn', '', 115, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-06-10 04:50:05', '1', '0', '0000-00-00 00:00:00'),
(11, 'Yas Marina Circuit ', '', 116, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-06-10 04:50:05', '1', '0', '0000-00-00 00:00:00'),
(12, 'Safaris', '', 116, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-06-10 04:50:05', '1', '0', '0000-00-00 00:00:00'),
(13, 'Yas Marina Circuit', '', 369, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-06-10 04:50:05', '1', '0', '0000-00-00 00:00:00'),
(14, 'Safaris', '', 369, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-06-10 04:50:05', '1', '0', '0000-00-00 00:00:00'),
(15, 'Yas Marina Circuit', '', 404, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-06-10 04:50:05', '1', '0', '0000-00-00 00:00:00'),
(16, 'Safaris', '', 404, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-06-10 04:50:05', '1', '0', '0000-00-00 00:00:00'),
(17, 'Yas Marina Circuit', '', 712, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-06-10 04:50:05', '1', '0', '0000-00-00 00:00:00'),
(18, 'Safaris', '', 712, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-06-10 04:50:05', '1', '0', '0000-00-00 00:00:00'),
(19, 'Yas Marina Circuit', '', 765, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-06-10 04:50:05', '1', '0', '0000-00-00 00:00:00'),
(20, 'Safaris', '', 765, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-06-10 04:50:05', '1', '0', '0000-00-00 00:00:00'),
(21, 'Yas Marina Circuit', '', 812, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-06-10 04:50:05', '1', '0', '0000-00-00 00:00:00'),
(22, 'Safaris', '', 812, '', '', 0, 0, 0, '0000-00-00 00:00:00', '2015-06-10 04:50:05', '1', '0', '0000-00-00 00:00:00'),
(24, 'Ferrari World', '', 96, '', '', 0, 2, 2, '2015-06-08 21:01:30', '2015-06-10 04:50:05', '1', '0', '0000-00-00 00:00:00'),
(25, 'Karama', '', 369, '', '', 0, 2, 2, '2015-06-13 19:38:48', '2015-06-14 13:38:00', '1', '0', '0000-00-00 00:00:00'),
(26, 'Burdubai', '', 369, '', '', 0, 2, 2, '2015-06-13 19:39:00', '2015-06-14 13:38:12', '1', '0', '0000-00-00 00:00:00'),
(27, 'san fernando', '', 96, '', '', 0, 2, 2, '2015-07-07 18:30:13', '2015-07-08 05:29:49', '1', '0', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_member`
--

CREATE TABLE IF NOT EXISTS `tbl_member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `gender` enum('Male','Female') NOT NULL DEFAULT 'Male',
  `date_of_birth` date DEFAULT NULL,
  `contact_no` varchar(50) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `business_id` int(11) DEFAULT NULL,
  `address_1` varchar(255) DEFAULT NULL,
  `address_2` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `city` varchar(255) DEFAULT NULL,
  `postal_code` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `country` int(11) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `facebook_id` varchar(255) NOT NULL,
  `is_facebook_login` enum('0','1') NOT NULL DEFAULT '0',
  `registration_ip` varchar(255) DEFAULT NULL,
  `updated_ip` varchar(255) NOT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_login_date` datetime DEFAULT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `last_login_ip` varchar(255) NOT NULL,
  `is_active` enum('0','1') NOT NULL DEFAULT '0',
  `emailToken` varchar(255) DEFAULT NULL,
  `emailVerified` enum('0','1') NOT NULL,
  `subscribe_intimation` tinyint(1) NOT NULL DEFAULT '0',
  `password_token` varchar(250) NOT NULL,
  `is_deleted` enum('0','1') NOT NULL DEFAULT '0',
  `deleted_date_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=39 ;

--
-- Dumping data for table `tbl_member`
--

INSERT INTO `tbl_member` (`id`, `first_name`, `last_name`, `gender`, `date_of_birth`, `contact_no`, `company`, `business_id`, `address_1`, `address_2`, `email`, `city`, `postal_code`, `state`, `country`, `password`, `facebook_id`, `is_facebook_login`, `registration_ip`, `updated_ip`, `created_date`, `last_login_date`, `updated_date`, `last_login_ip`, `is_active`, `emailToken`, `emailVerified`, `subscribe_intimation`, `password_token`, `is_deleted`, `deleted_date_time`) VALUES
(2, 'Testing13', 'Webmunky', 'Female', '1988-10-03', '989808980', 'Web Munky', 0, 'Ahmedabad', 'Ahmedabad', 'testing.webmunky@gmail.com', 'Ahmedabad', '380004', 'Gujarat', 98, '55ebe52d22222e6a6a1802daf99d1605', '', '0', '127.0.0.1', '::1', '2015-02-22 08:49:24', NULL, '2015-06-14 10:36:33', '', '1', '', '0', 0, 'mmfhahrld25im3b0ajdtcw==', '0', '0000-00-00 00:00:00'),
(3, 'Muhammed', 'Anas', 'Male', '2015-05-21', '8888', 'Qburst Technologies', 2, 'asdf#7777', 'Kerala', 'saifumhd+1@gmail.com', 'AlQusais', '1986', 'Dubai', 225, '365bf98ab80bb5334835e715a76b02ec', '', '0', '::1', '::1', '2015-04-17 22:52:10', '2015-04-25 04:01:57', '2015-04-25 20:31:33', '::1', '1', '', '1', 0, '', '0', '0000-00-00 00:00:00'),
(4, 'Muhammed', 'Anas', 'Male', '1970-01-01', '5545', '', 0, 'l,ll', '', 'anas.muhammed@gmail.com', '', '', '', 9, 'e10adc3949ba59abbe56e057f20f883e', '', '0', '::1', '::1', '2015-04-20 15:26:06', '2015-06-13 16:46:10', '2015-10-08 01:39:45', '192.168.1.104', '1', '', '0', 1, 'nhuyzmrrchbjyjzmdw40na==', '0', '0000-00-00 00:00:00'),
(18, 'Test', 'Account', 'Male', '1987-05-26', '+971502908381', 'Test company', 0, 'Qusais', '', 'anas.muhammed+981@gmail.com', '369', '', '0', 225, '098f6bcd4621d373cade4e832627b4f6', '', '0', '192.168.1.104', '', '2015-05-31 11:02:39', '2015-05-31 11:08:12', '2015-06-01 05:07:48', '192.168.1.104', '1', '', '1', 1, '', '0', '0000-00-00 00:00:00'),
(7, 'Saifu', 'Mhd', 'Male', '2015-12-10', '9898989899', 'Test company', 1, 'Address one', 'Address two', 'anas2job@gmail.com', 'Dyuffayi', '690869', 'Kerala', 225, 'd5f2e05c9267fc9090bc90c3f2555c6c', '', '0', '::1', '', '2015-04-23 17:26:21', '2015-05-05 09:17:06', '2015-05-06 03:16:42', '::1', '1', '', '0', 1, '', '0', '0000-00-00 00:00:00'),
(8, 'Saifu', 'Mhd', 'Male', '2015-12-10', '9898989899', 'Test company', 1, 'Address one', 'Address two', 'anas2joba@gmail.com', 'Dyuffayi', '690869', 'Kerala', 225, '07c6083ea5315bb037c5f81e7fffe7e8', '', '0', '::1', '', '2015-04-23 17:29:17', NULL, '2015-04-24 09:58:29', '', '', '', '0', 1, '', '0', '0000-00-00 00:00:00'),
(9, 'Saifu', 'Mhd', 'Male', '2015-12-10', '9898989899', 'Test company', 1, 'Address one', 'Address two', 'anas2jobas@gmail.com', 'Dyuffayi', '690869', 'Kerala', 225, '8d854196f9d84e6718f474ef5f6444f9', '', '0', '::1', '', '2015-04-23 17:34:38', NULL, '2015-04-24 10:03:50', '', '', '', '0', 1, '', '0', '0000-00-00 00:00:00'),
(23, 'Anas', 'Code', 'Male', '1987-01-01', '9999999999', 'xxxxxxxxxxxxx', 0, '0', '0', 'anas1@codeandco.ae', '115', '0', '0', 0, '97679a8b17a7714f5541eb0b4191f1e0', '', '0', '192.168.1.104', '192.168.1.104', '2015-05-31 12:30:17', '2015-06-13 16:16:45', '2015-06-14 10:18:25', '192.168.1.104', '1', '', '1', 1, '', '0', '0000-00-00 00:00:00'),
(19, 'Test', 'User', 'Male', '2015-05-04', '+97150 2908381', 'dsad', 1, 'ssss', '', 'anas.muhammed+985@gmail.com', '369', '', '0', 225, 'e10adc3949ba59abbe56e057f20f883e', '', '0', '192.168.1.104', '', '2015-05-31 12:26:47', '2015-05-31 16:43:11', '2015-06-01 10:42:47', '192.168.1.104', '1', '', '1', 1, '', '0', '0000-00-00 00:00:00'),
(13, 'ssssssssssss', 'sssssssss', 'Male', '1990-02-02', '9898989899', 'Test company', 2, 'Address one', 'Address two', 'anas2jobb@gmail.com', '', '690869', 'Kerala', 2, '61864e71cf9f78a1c816d1289ae2459c', '', '0', '::1', '', '2015-04-24 05:02:37', NULL, '2015-04-24 21:31:49', '', '', 'ntuzotgxmtviotvmntgunte2ndezmzctmjaxnta0mjqwntaymzc%3D', '0', 1, '', '0', '0000-00-00 00:00:00'),
(14, 'ssssssssssss', 'sssssssss', 'Male', '1990-02-02', '9898989899', 'Test company', 2, 'Address one', 'Address two', 'anas2job+1@gmail.com', '', '690869', 'Kerala', 2, '61864e71cf9f78a1c816d1289ae2459c', '', '0', '::1', '', '2015-04-24 05:04:41', NULL, '2015-04-24 21:33:53', '', '', 'ntuzotgxotewmty3zduumji3nzc0njktmjaxnta0mjqwnta0nde%3D', '0', 1, '', '0', '0000-00-00 00:00:00'),
(15, 'ssssssssssss', 'sssssssss', 'Male', '1990-02-02', '9898989899', 'Test company', 2, 'Address one', 'Address two', 'anas2job+2@gmail.com', '', '690869', 'Kerala', 2, '61864e71cf9f78a1c816d1289ae2459c', '', '0', '::1', '', '2015-04-24 05:05:36', NULL, '2015-04-24 21:35:47', '', '0', '', '1', 1, '', '0', '0000-00-00 00:00:00'),
(16, 'XXXXXXXXX', 'YYYYYYYYYY', 'Male', '1990-02-02', '9898989899', '', 0, 'Address one', '', 'anas2job+4@gmail.com', '', '', '', 1, '61864e71cf9f78a1c816d1289ae2459c', '', '0', '::1', '', '2015-04-24 05:08:59', NULL, '2015-04-24 21:38:11', '', '', 'ntuzotgyotnizmvkmjiuntiymdy3nzgtmjaxnta0mjqwnta4ntk%3D', '0', 1, '', '0', '0000-00-00 00:00:00'),
(17, 'Anas', 'Mohd', 'Male', '1987-08-13', '9898989899', 'Test company', 1, 'Address', '', 'anas.muhammed+10@gmail.com', 'Qusais', '6666', 'Dubai', 225, 'ad31f727e6fcc37662ba7e671cb9ceeb', '', '0', '::1', '::1', '2015-04-24 17:49:21', '2015-04-30 10:58:44', '2015-05-01 03:28:20', '::1', '1', '', '1', 1, '', '0', '0000-00-00 00:00:00'),
(27, 'Anas', 'Code', 'Male', '0000-00-00', '0', '0', 0, '0', '0', 'anas@codeandco.ae', '0', '0', '0', 0, '', '1412632239063144', '1', '192.168.1.104', '5.32.65.74', '2015-06-13 17:53:22', '2015-11-23 17:05:01', '2015-11-24 00:05:01', '5.32.65.74', '', NULL, '0', 0, '', '0', '0000-00-00 00:00:00'),
(25, 'Anas', 'Here', '', '2015-06-08', '0', '0', 0, '0', '0', 'anas+1@codeandco.ae', '115', '0', '0', 225, 'e10adc3949ba59abbe56e057f20f883e', '', '0', '192.168.1.104', '', '2015-06-08 18:14:17', NULL, '2015-06-09 12:13:29', '', '', 'ntu3nwezmzk2mjqxmjaumzgwndixmdktmjaxnta2mdgxode0mtc=', '0', 0, '', '0', '0000-00-00 00:00:00'),
(26, 'Anas', 'Mohd', '', '2015-06-08', '0', '0', 0, '0', '0', 'anas+2@codeandco.ae', '115', '0', '0', 225, 'e10adc3949ba59abbe56e057f20f883e', '', '0', '192.168.1.104', '', '2015-06-08 18:19:33', NULL, '2015-06-09 12:19:47', '', '1', '', '1', 0, '', '0', '0000-00-00 00:00:00'),
(28, 'Mohd', 'Anas', '', '2015-06-18', '0', '0', 0, '0', '0', 'anas.muhammed+97@gmail.com', '116', '0', '0', 225, 'e10adc3949ba59abbe56e057f20f883e', '', '0', '5.32.65.74', '', '2015-06-14 06:50:10', '2015-06-13 23:51:56', '2015-06-14 17:51:32', '5.32.65.74', '1', '', '1', 0, '', '0', '0000-00-00 00:00:00'),
(29, 'Puneet', 'Sakhuja', 'Male', '0000-00-00', '0', '0', 0, '0', '0', 'contact.sakhuja@gmail.com', '0', '0', '0', 0, '', '10153439070412174', '0', '5.32.65.74', '5.32.65.74', '2015-06-22 09:01:58', NULL, '2015-06-23 03:03:51', '', '', NULL, '0', 0, '', '0', '0000-00-00 00:00:00'),
(30, 'Vidushi', 'Balani', 'Female', '0000-00-00', '0', '0', 0, '0', '0', 'sombra.del.amo@gmail.com', '0', '0', '0', 0, '', '923679837673239', '0', '5.32.65.74', '5.32.65.74', '2015-06-24 13:40:07', NULL, '2015-06-25 07:45:18', '', '', NULL, '0', 0, '', '0', '0000-00-00 00:00:00'),
(31, 'Girish', 'Nathaney', 'Male', NULL, NULL, NULL, NULL, NULL, NULL, 'girish.nathaney@gmail.com', NULL, NULL, NULL, NULL, '', '10153391316732464', '1', '5.32.65.74', '', '2015-06-24 14:00:39', '2015-11-19 08:15:26', '2015-11-19 15:15:26', '5.32.65.74', '1', NULL, '0', 0, '', '0', '0000-00-00 00:00:00'),
(32, 'Anas', 'Coder', '', '2015-06-10', '0', '0', 0, '0', '0', 'anas2job+3@gmail.com', '116', '0', '0', 0, 'e10adc3949ba59abbe56e057f20f883e', '', '0', '5.32.65.74', '', '2015-06-25 06:25:55', '2015-06-24 23:26:48', '2015-06-25 17:26:24', '5.32.65.74', '1', '', '1', 0, '', '0', '0000-00-00 00:00:00'),
(33, 'Richard', 'Mohammed', '', '1975-09-13', '0', '0', 0, '0', '0', 'snd2k6@gmail.com', '369', '0', '0', 0, 'cc03e747a6afbbcbf8be7668acfebee5', '', '0', '190.213.101.243', '', '2015-06-25 12:04:47', '2016-04-02 15:16:41', '2016-04-02 22:16:41', '95.154.201.143', '1', '', '1', 0, '', '0', '0000-00-00 00:00:00'),
(34, 'Brandon', 'Ramdeo', '', '1996-03-02', '0', '0', 0, '0', '0', 'aviramdeo@gmail.com', '369', '0', '0', 0, 'bcf5e8874ba19398a978e3485d597d4e', '', '0', '186.45.20.201', '', '2015-07-07 01:07:03', '2015-07-16 18:31:58', '2015-07-17 12:31:34', '186.45.48.178', '1', '', '1', 0, '', '0', '0000-00-00 00:00:00'),
(35, 'Fill', 'Crop', 'Male', NULL, NULL, NULL, NULL, NULL, NULL, 'htfortunato@yahoo.com.ph', NULL, NULL, NULL, NULL, '', '998763266834817', '1', '5.32.65.74', '', '2015-07-30 12:33:06', NULL, '2015-07-30 23:32:42', '', '1', NULL, '0', 0, '', '0', '0000-00-00 00:00:00'),
(36, 'shelly', 'mohammed', '', '1982-04-26', '0', '0', 0, '0', '0', 'snd2k6@hotmail.com', '4', '0', '0', 225, 'c40f6309e18d89f808c78ec5998c5ab7', '', '0', '94.207.225.101', '', '2015-08-18 11:00:47', NULL, '2015-08-18 22:00:23', '', '', 'ntvkmzewnwy0nmixyzgunzywmdm3njatmjaxnta4mtgxmtawndc=', '0', 0, '', '0', '0000-00-00 00:00:00'),
(37, 'Brandon', 'Ramdeo', 'Male', NULL, NULL, NULL, NULL, NULL, NULL, 'aviramdeo@gmail.com', NULL, NULL, NULL, NULL, '', '617817185027524', '1', '186.45.14.100', '', '2015-08-28 05:13:06', NULL, '2015-08-28 16:12:42', '', '1', NULL, '0', 0, '', '0', '0000-00-00 00:00:00'),
(38, 'Dummy', 'Test', '', '1990-01-01', '0', '0', 0, '0', '0', 'dummytest360@gmail.com', '1', '0', '0', 0, 'ce488772806bbb92d7c5c3e5949df983', '', '0', '5.32.65.74', '', '2016-03-31 08:11:53', '2016-04-04 22:12:37', '2016-04-05 05:12:37', '5.32.65.74', '1', 'ntzmy2riyzkynzezotyumjqzmjy4otetmjaxnjazmzewodexntm=', '0', 0, '', '0', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_member_log`
--

CREATE TABLE IF NOT EXISTS `tbl_member_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `member_id` int(11) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `gender` enum('Male','Female') NOT NULL DEFAULT 'Male',
  `date_of_birth` date DEFAULT NULL,
  `contact_no` varchar(50) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `business_id` int(11) DEFAULT NULL,
  `address_1` varchar(255) DEFAULT NULL,
  `address_2` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `city` varchar(255) DEFAULT NULL,
  `postal_code` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `country` int(11) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=61 ;

--
-- Dumping data for table `tbl_member_log`
--

INSERT INTO `tbl_member_log` (`id`, `member_id`, `first_name`, `last_name`, `gender`, `date_of_birth`, `contact_no`, `company`, `business_id`, `address_1`, `address_2`, `email`, `city`, `postal_code`, `state`, `country`, `password`, `created_date`) VALUES
(1, 1, 'Munir', 'Vora', 'Male', '1988-10-03', '989808980', 'Web Munky', 0, 'Ahmedabad', 'Ahmedabad', 'munirsunni@gmail.com', '0', '380004', 'Gujarat', 98, '', '2015-02-22 08:38:44'),
(2, 2, 'Testing', 'Webmunky', 'Male', '1988-10-03', '989808980', 'Web Munky', 0, 'Ahmedabad', 'Ahmedabad', 'munirsunni@gmail.com', '0', '380004', 'Gujarat', 3, '55ebe52d22222e6a6a1802daf99d1605', '2015-02-22 09:03:08'),
(3, 22, 'Anas Code', NULL, 'Male', NULL, NULL, NULL, NULL, NULL, NULL, 'anas@codeandco.ae', NULL, NULL, NULL, NULL, '', '2015-05-31 17:00:48'),
(4, 22, 'Anas', 'Code', 'Male', '0000-00-00', '0', '0', 0, '0', '0', 'anas@codeandco.ae', '0', '0', '0', 0, '', '2015-05-31 17:01:06'),
(5, 22, 'Anas', 'Code', 'Male', '0000-00-00', '0', '0', 0, '0', '0', 'anas@codeandco.ae', '0', '0', '0', 0, '', '2015-05-31 18:50:25'),
(6, 23, 'Anas Code', NULL, 'Male', NULL, NULL, NULL, NULL, NULL, NULL, 'anas@codeandco.ae', NULL, NULL, NULL, NULL, '', '2015-06-08 14:47:01'),
(7, 23, 'Anas', 'Code', 'Male', '0000-00-00', '0', '0', 0, '0', '0', 'anas@codeandco.ae', '0', '0', '0', 0, '', '2015-06-08 15:25:25'),
(8, 23, 'Anas', 'Code', 'Male', '0000-00-00', '0', '0', 0, '0', '0', 'anas@codeandco.ae', '0', '0', '0', 0, '', '2015-06-08 15:29:44'),
(9, 23, 'Anas', 'Code', 'Male', '0000-00-00', '0', '0', 0, '0', '0', 'anas@codeandco.ae', '0', '0', '0', 0, '', '2015-06-08 15:51:08'),
(10, 23, 'Anas', 'Code', 'Male', '0000-00-00', '0', '0', 0, '0', '0', 'anas@codeandco.ae', '0', '0', '0', 0, '', '2015-06-08 18:21:43'),
(11, 23, 'Anas', 'Code', 'Male', '0000-00-00', '0', '0', 0, '0', '0', 'anas@codeandco.ae', '0', '0', '0', 0, '', '2015-06-08 19:17:08'),
(12, 23, 'Anas', 'Code', 'Male', '0000-00-00', '0', '0', 0, '0', '0', 'anas@codeandco.ae', '0', '0', '0', 0, '', '2015-06-09 12:07:38'),
(13, 23, 'Anas', 'Code', 'Male', '0000-00-00', '0', '0', 0, '0', '0', 'anas@codeandco.ae', '0', '0', '0', 0, '', '2015-06-09 15:32:51'),
(14, 23, 'Anas', 'Code', 'Male', '0000-00-00', '0', '0', 0, '0', '0', 'anas@codeandco.ae', '0', '0', '0', 0, '', '2015-06-11 18:25:50'),
(15, 23, 'Anas', 'Code', 'Male', '0000-00-00', '0', '0', 0, '0', '0', 'anas@codeandco.ae', '0', '0', '0', 0, '', '2015-06-13 11:56:49'),
(16, 23, 'Anas', 'Code', 'Male', '0000-00-00', '0', '0', 0, '0', '0', 'anas@codeandco.ae', '0', '0', '0', 0, '', '2015-06-13 12:34:30'),
(17, 23, 'Anas', 'Code', 'Male', '0000-00-00', '0', '0', 0, '0', '0', 'anas@codeandco.ae', '0', '0', '0', 0, '', '2015-06-13 13:02:56'),
(18, 23, 'Anas', 'Code', 'Male', '0000-00-00', '0', '0', 0, '0', '0', 'anas@codeandco.ae', '0', '0', '0', 0, '', '2015-06-13 13:21:39'),
(19, 23, 'Anas', 'Code', 'Male', '0000-00-00', '0', '0', 0, '0', '0', 'anas@codeandco.ae', '0', '0', '0', 0, '', '2015-06-13 13:26:54'),
(20, 23, 'Anas', 'Code', 'Male', '0000-00-00', '0', '0', 0, '0', '0', 'anas@codeandco.ae', '0', '0', '0', 0, '', '2015-06-13 13:43:23'),
(21, 23, 'Muhammed Anas', 'Here', 'Male', '2015-05-27', '+97555555888', 'dsad', 2, 'Qusais', 'dd', 'anas@codeandco.ae', '96', '', '0', 225, '194585b5215aea447389c5fefca09c61', '2015-06-13 14:34:47'),
(22, 23, 'Anas', 'Code', 'Male', '1970-01-01', '9999999999', '', 0, '0', '0', 'anas@codeandco.ae', '0', '0', '0', 0, '194585b5215aea447389c5fefca09c61', '2015-06-13 14:35:52'),
(23, 23, 'Anas', 'Code', 'Male', '1970-01-01', '9999999999', '', 0, '0', '0', 'anas@codeandco.ae', '0', '0', '0', 0, '194585b5215aea447389c5fefca09c61', '2015-06-13 14:36:15'),
(24, 23, 'Anas', 'Code', 'Male', '1987-01-01', '9999999999', '', 0, '0', '0', 'anas@codeandco.ae', '0', '0', '0', 0, '194585b5215aea447389c5fefca09c61', '2015-06-13 14:37:17'),
(25, 23, 'Anas', 'Code', 'Male', '1987-01-01', '9999999999', '', 0, '0', '0', 'anas@codeandco.ae', '115', '0', '0', 0, '194585b5215aea447389c5fefca09c61', '2015-06-13 14:37:29'),
(26, 23, 'Anas', 'Code', 'Male', '1987-01-01', '9999999999', '', 0, '0', '0', 'anas@codeandco.ae', '115', '0', '0', 0, '194585b5215aea447389c5fefca09c61', '2015-06-13 14:37:37'),
(27, 23, 'Anas', 'Code', 'Male', '1987-01-01', '9999999999', '', 0, '0', '0', 'anas@codeandco.ae', '115', '0', '0', 0, '194585b5215aea447389c5fefca09c61', '2015-06-13 14:37:48'),
(28, 27, 'Anas Code', NULL, 'Male', NULL, NULL, NULL, NULL, NULL, NULL, 'anas@codeandco.ae', NULL, NULL, NULL, NULL, '', '2015-06-14 05:58:37'),
(29, 27, 'Anas', 'Code', 'Male', '0000-00-00', '0', '0', 0, '0', '0', 'anas@codeandco.ae', '0', '0', '0', 0, '', '2015-06-14 06:17:27'),
(30, 27, 'Anas', 'Code', 'Male', '0000-00-00', '0', '0', 0, '0', '0', 'anas@codeandco.ae', '0', '0', '0', 0, '', '2015-06-14 06:20:51'),
(31, 27, 'Anas', 'Code', 'Female', '0001-11-30', '999999999', '', 0, '0', '0', 'anas@codeandco.ae', '', '0', '0', 0, '', '2015-06-14 06:21:09'),
(32, 27, 'Anas', 'Code', 'Female', '1987-11-30', '999999999', '', 0, '0', '0', 'anas@codeandco.ae', '', '0', '0', 0, '', '2015-06-14 06:21:17'),
(33, 27, 'Anas', 'Code', 'Female', '1987-11-30', '999999999', '', 0, '0', '0', 'anas@codeandco.ae', '115', '0', '0', 0, '', '2015-06-14 06:21:26'),
(34, 27, 'Anas', 'Code', 'Female', '1987-11-30', '999999999', 'Test', 0, '0', '0', 'anas@codeandco.ae', '115', '0', '0', 0, '', '2015-06-16 16:18:05'),
(35, 27, 'Anas', 'Code', 'Male', '0000-00-00', '0', '0', 0, '0', '0', 'anas@codeandco.ae', '0', '0', '0', 0, '', '2015-06-17 05:32:45'),
(36, 27, 'Anas', 'Code', 'Male', '0000-00-00', '0', '0', 0, '0', '0', 'anas@codeandco.ae', '0', '0', '0', 0, '', '2015-06-21 10:22:56'),
(37, 27, 'Anas', 'Code', 'Male', '0000-00-00', '0', '0', 0, '0', '0', 'anas@codeandco.ae', '0', '0', '0', 0, '', '2015-06-21 10:35:48'),
(38, 29, 'Puneet Sakhuja', NULL, 'Male', NULL, NULL, NULL, NULL, NULL, NULL, 'contact.sakhuja@gmail.com', NULL, NULL, NULL, NULL, '', '2015-06-22 09:04:15'),
(39, 27, 'Anas', 'Code', 'Male', '0000-00-00', '0', '0', 0, '0', '0', 'anas@codeandco.ae', '0', '0', '0', 0, '', '2015-06-23 08:27:31'),
(40, 27, 'Anas', 'Code', 'Male', '0000-00-00', '0', '0', 0, '0', '0', 'anas@codeandco.ae', '0', '0', '0', 0, '', '2015-06-24 10:23:49'),
(41, 27, 'Anas', 'Code', 'Male', '0000-00-00', '0', '0', 0, '0', '0', 'anas@codeandco.ae', '0', '0', '0', 0, '', '2015-06-24 13:02:56'),
(42, 27, 'Anas', 'Code', 'Male', '0000-00-00', '0', '0', 0, '0', '0', 'anas@codeandco.ae', '0', '0', '0', 0, '', '2015-06-24 13:31:49'),
(43, 27, 'Anas', 'Code', 'Male', '0000-00-00', '0', '0', 0, '0', '0', 'anas@codeandco.ae', '0', '0', '0', 0, '', '2015-06-24 13:38:11'),
(44, 27, 'Anas', 'Code', 'Male', '0000-00-00', '0', '0', 0, '0', '0', 'anas@codeandco.ae', '0', '0', '0', 0, '', '2015-06-24 13:39:05'),
(45, 30, 'Vidushi Balani', NULL, 'Male', NULL, NULL, NULL, NULL, NULL, NULL, 'sombra.del.amo@gmail.com', NULL, NULL, NULL, NULL, '', '2015-06-24 13:45:42'),
(46, 27, 'Anas', 'Code', 'Male', '0000-00-00', '0', '0', 0, '0', '0', 'anas@codeandco.ae', '0', '0', '0', 0, '', '2015-06-25 06:22:27'),
(47, 27, 'Anas', 'Code', 'Male', '0000-00-00', '0', '0', 0, '0', '0', 'anas@codeandco.ae', '0', '0', '0', 0, '', '2015-06-25 07:16:49'),
(48, 27, 'Anas', 'Code', 'Male', '0000-00-00', '0', '0', 0, '0', '0', 'anas@codeandco.ae', '0', '0', '0', 0, '', '2015-07-25 10:30:41'),
(49, 27, 'Anas', 'Code', 'Male', '0000-00-00', '0', '0', 0, '0', '0', 'anas@codeandco.ae', '0', '0', '0', 0, '', '2015-07-30 12:29:53'),
(50, 27, 'Anas', 'Code', 'Male', '0000-00-00', '0', '0', 0, '0', '0', 'anas@codeandco.ae', '0', '0', '0', 0, '', '2015-08-10 15:37:38'),
(51, 27, 'Anas', 'Code', 'Male', '0000-00-00', '0', '0', 0, '0', '0', 'anas@codeandco.ae', '0', '0', '0', 0, '', '2015-10-07 14:16:01'),
(52, 27, 'Anas', 'Code', 'Male', '0000-00-00', '0', '0', 0, '0', '0', 'anas@codeandco.ae', '0', '0', '0', 0, '', '2015-10-07 15:56:41'),
(53, 31, 'Girish', 'Nathaney', 'Male', NULL, NULL, NULL, NULL, NULL, NULL, 'girish.nathaney@gmail.com', NULL, NULL, NULL, NULL, '', '2015-11-19 08:15:26'),
(54, 27, 'Anas', 'Code', 'Male', '0000-00-00', '0', '0', 0, '0', '0', 'anas@codeandco.ae', '0', '0', '0', 0, '', '2015-11-19 13:21:12'),
(55, 27, 'Anas', 'Code', 'Male', '0000-00-00', '0', '0', 0, '0', '0', 'anas@codeandco.ae', '0', '0', '0', 0, '', '2015-11-19 13:22:56'),
(56, 27, 'Anas', 'Code', 'Male', '0000-00-00', '0', '0', 0, '0', '0', 'anas@codeandco.ae', '0', '0', '0', 0, '', '2015-11-22 14:00:35'),
(57, 27, 'Anas', 'Code', 'Male', '0000-00-00', '0', '0', 0, '0', '0', 'anas@codeandco.ae', '0', '0', '0', 0, '', '2015-11-23 06:06:21'),
(58, 27, 'Anas', 'Code', 'Male', '0000-00-00', '0', '0', 0, '0', '0', 'anas@codeandco.ae', '0', '0', '0', 0, '', '2015-11-23 06:55:57'),
(59, 27, 'Anas', 'Code', 'Male', '0000-00-00', '0', '0', 0, '0', '0', 'anas@codeandco.ae', '0', '0', '0', 0, '', '2015-11-23 16:59:28'),
(60, 27, 'Anas', 'Code', 'Male', '0000-00-00', '0', '0', 0, '0', '0', 'anas@codeandco.ae', '0', '0', '0', 0, '', '2015-11-23 17:05:01');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_module`
--

CREATE TABLE IF NOT EXISTS `tbl_module` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `tbl_module`
--

INSERT INTO `tbl_module` (`id`, `module_name`) VALUES
(1, 'Admin Module'),
(2, 'Member Module'),
(3, 'Classified Module'),
(4, 'CMS Module'),
(5, 'Setting Module'),
(6, 'Home Page Image Gallery'),
(7, 'Advertize Model');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pages`
--

CREATE TABLE IF NOT EXISTS `tbl_pages` (
  `page_id` int(11) NOT NULL AUTO_INCREMENT,
  `page_title` varchar(255) NOT NULL,
  `page_slug` varchar(255) NOT NULL,
  `page_desc` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `page_parent` int(11) NOT NULL,
  `on_home_footer` enum('0','1') NOT NULL DEFAULT '0',
  `on_header` enum('0','1') NOT NULL DEFAULT '1',
  `on_footer` enum('0','1') NOT NULL DEFAULT '1',
  `on_home` enum('0','1') NOT NULL DEFAULT '0',
  `on_left` enum('0','1') NOT NULL DEFAULT '0',
  `page_desc_small` text NOT NULL,
  `page_banner_desc` varchar(255) NOT NULL,
  `page_meta_title` text,
  `page_meta_keywords` text,
  `page_meta_desc` text,
  `page_meta_keyphase` text NOT NULL,
  `page_meta_abstract` text NOT NULL,
  `page_order` int(11) NOT NULL DEFAULT '0',
  `page_banner` varchar(255) DEFAULT NULL,
  `page_banner_small` varchar(255) NOT NULL,
  `page_status` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`page_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=63 ;

--
-- Dumping data for table `tbl_pages`
--

INSERT INTO `tbl_pages` (`page_id`, `page_title`, `page_slug`, `page_desc`, `page_parent`, `on_home_footer`, `on_header`, `on_footer`, `on_home`, `on_left`, `page_desc_small`, `page_banner_desc`, `page_meta_title`, `page_meta_keywords`, `page_meta_desc`, `page_meta_keyphase`, `page_meta_abstract`, `page_order`, `page_banner`, `page_banner_small`, `page_status`) VALUES
(1, 'Home', 'home', '', 0, '0', '1', '1', '0', '0', '', '', 'AL KAMEL ASSOCIATES Management Consultants International Corporate Seminars', 'AL KAMEL ASSOCIATES Management Consultants International Corporate Seminars', 'AL KAMEL ASSOCIATES Management Consultants International Corporate Seminars', '', '', 1, '', '', '1'),
(2, 'About Us', 'about-us', '<div class="col-md-4">\r\n	<p class="about_big">\r\n		At Al Kamel Associates, we emphasize on laying the foundation of good client relationship based on mutual trust and confidence building measures.</p>\r\n	<center>\r\n		<img class="img-responsive" src="images/about_us.png" width="236" /></center>\r\n</div>\r\n<div class="col-md-5 ">\r\n	<p>\r\n		We work closely with all our clients to build a solid and professional working relationship. Clients benefit from our rich and varied experience and expertise in the financial domain. As a result we have been able to oversee the growth of clients&#39; businesses from mere ideas on a notepad right through to the formation of a successful venture.</p>\r\n	<p>\r\n		Since our focus is on Customer Delight by adding value to the existing business environment that our clients operate, we invest significant resources in developing an experienced team for auditing services, financial analysis and planning services, offshore consulting services in the UAE and worldwide.</p>\r\n	<p>\r\n		Our comprehensive Financial Accounting and Reporting solutions form more than useful conglomerate with the Audit and Advisory services to go beyond the routine preparation of financial statements. From serving as an extension of your own accounting department to designing customized reports and their analysis, we can tailor our Management Assurance solutions to meet your organisational needs. We undertake to provide you with the accurate and objective financial information needed to make key business decisions. It is our endeavour to enable your company to relive the financial freedom.</p>\r\n</div>\r\n', 0, '0', '1', '1', '0', '0', ' <p class="darkgrey">At Al Kamel Associates, we emphasize on laying the foundation of good client relationship based on mutual trust and confidence building measures. We work closely with all our clients to build a solid and professional working relationship.</p>\r\n          <p>Clients benefit from our rich and varied experience and expertise in the financial domain. <a href="about-us.html" class="btn-block">read more</a></p>', '<span class="banner_title">An Independent audit is the foundation for</span>\r\n                    <span class="banner_title" style="font-size:24px;">decision - making in the capital markets.</span>', 'ABOUT US', 'ABOUT US', 'ABOUT US', '', '', 2, '1418623699_about_us.jpg', '', '1'),
(5, 'Services', 'services', '<p class="foresnic">\r\n	Forensic Accounting &amp; Fraud Risk Assessment</p>\r\n<p class="font_23px">\r\n	Understanding vulnerabilities to fraud and misconduct is essential in today&rsquo;s global market place.</p>\r\n<p>\r\n	The dynamic and enthusiastic network of Forensic Accounting Professionals, Fraud Risk Assessment Experts and Fraud Examiners at Al Kamel Associates FZE demonstrate highly competent experience in honestly helping organisations and litigators to conduct investigations and respond to the allegations requesting a variety of financial analyses.</p>\r\n<center>\r\n	<img class="img-responsive" src="images/service_img_01.png" /></center>\r\n<p>\r\n	<br />\r\n	We work with our clients to ensure that a sound response and case management systems are in place to support your investigative protocols and procedures. Being one of the torch-bearers in risk management and internal audit function can help you build sustainable fraud risk assessment processes, as well as evaluate and strengthen your anti-fraud program and controls worldwide.</p>\r\n<div aria-multiselectable="true" class="panel-group" id="accordion" role="tablist">\r\n	<div id="accordion">\r\n		<div class="main_accoudina">\r\n			<dl>\r\n				<dt>\r\n					Financial Investigations</dt>\r\n				<dd>\r\n					<br />\r\n					Risks can fester and spread anywhere inside an organization, such as industry- specific which has the regulatory concerns within financial services and healthcare or common to all industries.<br />\r\n					&nbsp;</dd>\r\n			</dl>\r\n		</div>\r\n		<div class="main_accoudina">\r\n			<dl>\r\n				<dt>\r\n					Business Risk</dt>\r\n				<dd>\r\n					<p>\r\n						<br />\r\n						Risks can fester and spread anywhere inside an organization, such as industry- specific which has the regulatory concerns within financial services and healthcare or common to all industries.</p>\r\n					<div class="list_bussnies">\r\n						<ul>\r\n							<li>\r\n								Accountant malpractices</li>\r\n							<li>\r\n								Asset misappropriations</li>\r\n							<li>\r\n								Asset reconstruction, location and recovery</li>\r\n							<li>\r\n								Financial statement manipulation</li>\r\n							<li>\r\n								Fraud investigations</li>\r\n							<li>\r\n								Fraudulent transfers</li>\r\n							<li>\r\n								Government Contract claims</li>\r\n						</ul>\r\n						<ul style="margin:0 0 0 4%;">\r\n							<li>\r\n								Healthcare fraud and claims</li>\r\n							<li>\r\n								Insurance fraud and claims</li>\r\n							<li>\r\n								Internal investigations</li>\r\n							<li>\r\n								Lost profit claims</li>\r\n							<li>\r\n								Partnership disputes</li>\r\n							<li>\r\n								Professional malpractice claims</li>\r\n							<li>\r\n								Shareholder disputes</li>\r\n							<li>\r\n								Structured finance transaction</li>\r\n						</ul>\r\n					</div>\r\n				</dd>\r\n			</dl>\r\n		</div>\r\n		<div class="main_accoudina">\r\n			<dl>\r\n				<dt>\r\n					Asset Tracking &amp; Recovery</dt>\r\n				<dd>\r\n					<br />\r\n					Risks can fester and spread anywhere inside an organization, such as industry- specific which has the regulatory concerns within financial services and healthcare or common to all industries.<br />\r\n					&nbsp;</dd>\r\n			</dl>\r\n		</div>\r\n		<div class="main_accoudina">\r\n			<dl>\r\n				<dt>\r\n					Fraud Risk Management</dt>\r\n				<dd>\r\n					<br />\r\n					Risks can fester and spread anywhere inside an organization, such as industry- specific which has the regulatory concerns within financial services and healthcare or common to all industries.<br />\r\n					&nbsp;</dd>\r\n			</dl>\r\n		</div>\r\n		<div class="main_accoudina">\r\n			<dl>\r\n				<dt>\r\n					Litigation Consulting</dt>\r\n				<dd>\r\n					<br />\r\n					Risks can fester and spread anywhere inside an organization, such as industry- specific which has the regulatory concerns within financial services and healthcare or common to all industries.<br />\r\n					&nbsp;</dd>\r\n			</dl>\r\n		</div>\r\n		<div class="main_accoudina">\r\n			<dl>\r\n				<dt>\r\n					Dispute Risk Assessment</dt>\r\n				<dd>\r\n					<br />\r\n					Risks can fester and spread anywhere inside an organization, such as industry- specific which has the regulatory concerns within financial services and healthcare or common to all industries.<br />\r\n					&nbsp;</dd>\r\n			</dl>\r\n		</div>\r\n		<div class="main_accoudina">\r\n			<dl>\r\n				<dt>\r\n					Expert Testimony</dt>\r\n				<dd>\r\n					<br />\r\n					Risks can fester and spread anywhere inside an organization, such as industry- specific which has the regulatory concerns within financial services and healthcare or common to all industries.<br />\r\n					&nbsp;</dd>\r\n			</dl>\r\n		</div>\r\n		<div class="main_accoudina">\r\n			<dl>\r\n				<dt>\r\n					Audit Services</dt>\r\n				<dd>\r\n					<br />\r\n					Risks can fester and spread anywhere inside an organization, such as industry- specific which has the regulatory concerns within financial services and healthcare or common to all industries.<br />\r\n					&nbsp;</dd>\r\n			</dl>\r\n		</div>\r\n		<div class="main_accoudina">\r\n			<dl>\r\n				<dt>\r\n					Forensic Accounting</dt>\r\n				<dd>\r\n					<br />\r\n					Risks can fester and spread anywhere inside an organization, such as industry- specific which has the regulatory concerns within financial services and healthcare or common to all industries.<br />\r\n					&nbsp;</dd>\r\n			</dl>\r\n		</div>\r\n		<dl class="accordion" id="slider1">\r\n		</dl>\r\n	</div>\r\n</div>\r\n', 0, '1', '1', '1', '0', '0', '', '', 'Services', 'Services', 'Services', '', '', 106, '1418646361_services_banner.jpg', '', '1'),
(62, 'Study Tours & Workshops', 'study-tours-and-workshops', '<p>\r\n	Study Tours &amp; Workshops</p>\r\n', 18, '0', '0', '0', '0', '1', '', '', '', '', '', '', '', 151, '', '', '1'),
(18, ' Corporate Seminars', 'corporate-seminars', '<div>\r\n	WE: Deliver professional and quality training to our clients with business and professional solutions as well as expertise in the organization, management and development of human resources.&nbsp;</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Provide comprehensive, leading edge and relevant training programs to the major Oil &amp; Gas, Power, Petrochemical &amp; Other Major industries in Middle East, Africa &amp; Asia.&nbsp;</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Support to strengthen and update the knowledge and skills of staff in an organization through professional training and development methodologies.&nbsp;</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Offer highly cost-effective trainings for our esteemed clients without compromising on the quality.&nbsp;</div>\r\n<div>\r\n	&nbsp;</div>\r\n<div>\r\n	Analyze and learn the customer requirements and deliver appropriate training to meet the expected level of competencies set by the organization. Our experienced multilingual consultants offer an impressive range of training, consulting and research services to a large number of organizations, both private and public. We pride ourselves on our track record and long-standing relationships with our clients.</div>\r\n', 0, '1', '1', '1', '0', '0', ' Corporate Seminars', '<span class="banner_title">An IndEpendent audit is the foundation for</span>\r\n                    <span class="banner_title" style="font-size:24px;">decision - making in the capital markets.</span>', ' Corporate Seminars', ' Corporate Seminars', ' Corporate Seminars', ' Corporate Seminars', '', 107, '1418646382_corporate.jpg', '', '1'),
(19, 'Training Programs', 'training-programs', '<p>\r\n	Training Programs</p>\r\n', 0, '0', '0', '0', '0', '0', 'Training Programs', '', 'Training Programs', 'Training Programs', 'Training Programs', '', '', 108, '', '', '1'),
(7, 'Contact Us', 'contact', '<p>\r\n	Thank you for visiting our web site. We welcome your inquiries, either by phone or by e-mail at <a class="btn-block" href="mailto:info@aka-dubai.com">info@aka-dubai.com</a>. Alternatively, please fill in the form below and we will get back to you as soon as possible.</p>\r\n', 0, '0', '1', '1', '0', '0', '', '<span class="banner_title">An IndEpendent audit is the foundation for</span>\r\n                    <span class="banner_title" style="font-size:24px;">decision - making in the capital markets.</span>', 'Contact Us', 'Contact Us', 'Contact Us', '', '', 109, '1418646396_contact_banner.jpg', '', '1'),
(44, 'Technical Courses', 'technical-courses-main', '<p>\r\n	Technical Courses</p>\r\n', 0, '0', '0', '0', '0', '0', '', '', 'Technical Courses', 'Technical Courses', 'Technical Courses', '', '', 134, '', '', '1'),
(50, 'Study Tours Courses', 'study-tours-courses-training', '<p>\r\n	Trainings courses are being updated</p>\r\n', 19, '1', '0', '0', '0', '0', ' Trainings courses are being updated ', '', '', '', '', '', '', 139, '', '', '1'),
(48, 'Management Courses', 'management-courses-training', '<p>\r\n	Trainings courses are being updated</p>\r\n', 19, '1', '0', '0', '0', '0', ' Trainings courses are being updated ', '', '', '', '', '', '', 137, '', '', '1'),
(49, 'Technical Courses', 'technical-courses-training', '<p>\r\n	Trainings courses are being updated</p>\r\n', 19, '1', '0', '0', '0', '0', ' Trainings courses are being updated ', '', '', '', '', '', '', 138, '', '', '1'),
(20, 'Management Consultancy', 'management-consultancy', '<p>\r\n	Management Consultancy</p>\r\n', 5, '0', '0', '0', '1', '1', 'Management Consultancy', '', 'Management Consultancy', 'Management Consultancy', 'Management Consultancy', '', '', 110, '', '1418622874_Management-Consultancy.png', '1'),
(21, 'Financial Accounting and Reporting', 'financial-accounting-reporting', '<p>With the move to International Financial Reporting Standards (IFRS) for many of the world''s public companies and the far-reaching mandates of Sarbanes-Oxley, financial accounting has become a lot more complicated. And if the past few years are any guide, the future holds more regulations, new requirements, and an increasing compliance burden. As business becomes more global, with new markets opening up seemingly every day, a companyâ€™s financial accounting must meet global standards.<br>If this is your situation<br>â€¢ You want to keep up-to-date on developments in domestic and international financial reporting. <br>â€¢ You made an acquisition and need to harmonise your accounting policies. <br>â€¢ The conversion to IFRS has raised the issue of staff training and the need for reliable and comprehensive accounting guidance and training. <br>â€¢ You are already familiar with your national GAAP, but need to understand how IFRS compares. <br>â€¢ You have a small finance function and would appreciate support from financial reporting specialists. <br>â€¢ You have a difficult technical accounting issue that you cannot solve in-house. <br><br>How Al Kamel Associates can provided value added benefits?Accounting &amp; B<br><br>Back-Office Support and Supervision Services <br>Having realised that the owner/managers of small and medium-sized family owned and managed organisations do not find it viable to employ a full-time accountant nor do they afford to spend time to look after their accounting function.<br><br>Thus, in such cases the important accounting function takes a back seat. AL KAMEEL provides specialised services in such cases by filling the vacuum of a qualified accountant to make the owners and management free to concentrate on business. Our Back-Office Support division offers full-range of our services, comprising the following: <br>  <br>â€¢ Installation of accounting software<br>â€¢ Training to the data entry clerks/accountants<br>â€¢ Monthly accounting, documentation<br>â€¢ Preparation of reconciliation statements<br>â€¢ Job costing<br>â€¢ Closure of accounts on monthly/quarterly basis<br>â€¢ Formatting management reports tailored to client needs<br>â€¢ Preparation of monthly management reports <br></p>\r\n', 5, '0', '0', '0', '1', '1', 'Financial Accounting and Reporting', '', 'Financial Accounting and Reporting', 'Financial Accounting and Reporting', 'Financial Accounting and Reporting', '', '', 111, '', '1418622962_Financial_Accounting.png', '1'),
(22, 'Internal Audit and Advisory', 'internal-audit-advisory', '<p>\r\n	As companies comply with the reporting requirements of Sections 302 and 404 of the US Sarbanes-Oxley Act, internal auditors are coming to grips with their role and involvement in these initiatives. These questions include both short-term issues during the implementation phase, as well as longer-term questions on the role and responsibilities of internal audit in the process. Although the Sarbanes-Oxley Act spells out the various roles of management, the audit committee, and the external auditors, it does not specifically address the role of internal auditors. In a post Sarbanes-Oxley world, the internal audit function needs to walk a fine line between providing assurance and consulting to management without impairing its objectivity and independence.<br />\r\n	<br />\r\n	<strong>If this is your situation</strong><br />\r\n	<br />\r\n	You need to supplement your in-house internal audit function with specialist skills.<br />\r\n	You need to assess the effectiveness of your risk management, internal audit, and corporate governance procedures.<br />\r\n	Your internal audit function has insufficient resources to cover the geographic scope of your organisation.<br />\r\n	You&rsquo;re concerned that your internal audit function can&rsquo;t keep up with the changing risks facing your business.<br />\r\n	You find it difficult to recruit and retain internal audit professionals in all of your key skill areas.</p>\r\n<p>\r\n	Your organisation considers internal audit to be a non-core activity so you want to find an external provider.</div>\r\n<p>\r\n\r\n\r\n<p>\r\n	<strong>How Al Kamel Associates can help you</strong><br />\r\n	<br />\r\n	Maximising the value and effectiveness of the internal audit function requires an understanding of an organisation&#39;s objectives, risks, risk management priorities, regulatory environment, and the diverse needs of critical stakeholders including executive management, the board, employees, and shareholders. Ultimately, these needs determine the risk profile of the organisation and the strategic focus, organisation, resources and practices required of its internal audit department.<br />\r\n	<br />\r\n	We can assist organisations that need help improving the quality and effectiveness of their internal audit processes in a number of ways. First, by advising and assisting in the development of internal audit and risk management methodologies, including assessing whether the internal audit function is delivering effectively to stakeholders.<br />\r\n	<br />\r\n	Second, by providing internal audit resourcing solutions, including full outsourcing or complementing in-house functions with specialist skills or geographical coverage. Third, by supporting internal audit functions with software to enhance and support their work. In addition, we can develop training for internal auditors using our extensive market and industry knowledge to create highly-tailored solutions.<br />\r\n	<br />\r\n	Internal audit outsourcing<br />\r\n	Internal audit advisory<br />\r\n	Sarbanes-Oxley advisory<br />\r\n	Technical advice</p>\r\n<p>\r\n	&nbsp;</p>\r\n', 5, '0', '0', '0', '1', '1', 'Internal Audit and Advisory', '', 'Internal Audit and Advisory', 'Internal Audit and Advisory', 'Internal Audit and Advisory', '', '', 112, '', '1418623007_Internal_Audit.png', '1'),
(23, 'Forensic Accounting and Fraud Risk Assessment', 'forensic-accounting-fraud-risk-assessment', '<p class="font_23px">Understanding vulnerabilities to fraud and misconduct is essential in todayâ€™s global market place. </p>\n    <p>The dynamic and enthusiastic network of Forensic Accounting Professionals, Fraud Risk Assessment Experts and Fraud Examiners at Al Kamel Associates FZE demonstrate highly competent experience in honestly helping organisations and litigators to conduct investigations and respond to the allegations requesting a variety of financial analyses.  </p>\n    <center><img src="images/service_img_01.png" class="img-responsive"></center>\n    <p><br>\nWe work with our clients to ensure that a sound response and case management systems are in place to support your investigative protocols and procedures. Being one of the torch-bearers in risk management and internal audit function can help you build sustainable fraud risk assessment processes, as well as evaluate and strengthen your anti-fraud program and controls worldwide.</p>\n    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">\n      <div id="accordion">\n                          <dl class="accordion" id="slider1">\n                           <div class="main_accoudina">\n                           <dt>Financial Investigations</dt>\n                           <dd><br>Risks can fester and spread anywhere inside an organization, such as industry-\n    specific which has the regulatory concerns within financial services and healthcare \n    or common to all industries.<br><br></dd>\n                           </div>\n                           <div class="main_accoudina">\n                           <dt>Business Risk</dt>\n                          <dd>\n                          <p><br>Risks can fester and spread anywhere inside an organization, such as industry-\n    specific which has the regulatory concerns within financial services and healthcare \n    or common to all industries.</p>\n                            <div class="list_bussnies">\n                              <ul>\n                                <li>Accountant malpractices</li>\n                                <li>Asset misappropriations</li>\n                                <li>Asset reconstruction, location and recovery</li>\n                                <li>Financial statement manipulation</li>\n                                <li>Fraud investigations</li>\n                                <li>Fraudulent transfers</li>\n                                <li>Government Contract claims</li>\n                             </ul>\n                             <ul style="margin:0 0 0 4%;">\n                                <li>Healthcare fraud and claims</li>\n                                <li>Insurance fraud and claims</li>\n                                <li>Internal investigations</li>\n                                <li>Lost profit claims</li>\n                                <li>Partnership disputes</li>\n                                <li>Professional malpractice claims</li>\n                                <li>Shareholder disputes</li>\n                                <li>Structured finance transaction</li>\n                             </ul>\n                            </div>\n                          </dd>\n                           </div>\n                           <div class="main_accoudina">\n                           <dt>Asset Tracking & Recovery</dt>\n                           <dd><br>Risks can fester and spread anywhere inside an organization, such as industry-\n    specific which has the regulatory concerns within financial services and healthcare \n    or common to all industries.<br><br></dd>\n                            </div>\n                            <div class="main_accoudina">\n                           <dt>Fraud Risk Management</dt>\n                         <dd><br>Risks can fester and spread anywhere inside an organization, such as industry-\n    specific which has the regulatory concerns within financial services and healthcare \n    or common to all industries.<br><br></dd>\n                           </div>\n                           <div class="main_accoudina">\n                           <dt>Litigation Consulting</dt>\n                           <dd><br>Risks can fester and spread anywhere inside an organization, such as industry-\n    specific which has the regulatory concerns within financial services and healthcare \n    or common to all industries.<br><br></dd>\n                           </div>\n                           <div class="main_accoudina">\n                           <dt>Dispute Risk Assessment</dt>\n                          <dd><br>Risks can fester and spread anywhere inside an organization, such as industry-\n    specific which has the regulatory concerns within financial services and healthcare \n    or common to all industries.<br><br></dd>\n                           </div>\n                           <div class="main_accoudina">\n                           <dt>Expert Testimony</dt>\n                           <dd><br>Risks can fester and spread anywhere inside an organization, such as industry-\n    specific which has the regulatory concerns within financial services and healthcare \n    or common to all industries.<br><br></dd>\n                           </div>\n                           <div class="main_accoudina">\n                           <dt>Audit Services</dt>\n                           <dd><br>Risks can fester and spread anywhere inside an organization, such as industry-\n    specific which has the regulatory concerns within financial services and healthcare \n    or common to all industries.<br><br></dd>\n                           </div>\n                           <div class="main_accoudina">\n                           <dt>Forensic Accounting</dt>\n                           <dd><br>Risks can fester and spread anywhere inside an organization, such as industry-\n    specific which has the regulatory concerns within financial services and healthcare \n    or common to all industries.<br><br></dd>          \n                           </div>\n                         </dl>\n    </div>  \n    </div>', 5, '0', '0', '0', '1', '1', 'Forensic Accounting and Fraud Risk Assessment', '', 'Forensic Accounting and Fraud Risk Assessment', 'Forensic Accounting and Fraud Risk Assessment', 'Forensic Accounting and Fraud Risk Assessment', '', '', 113, '', '1418623064_Forensic.png', '1'),
(24, 'Business Set Up Services', 'business-set-up-services', '<p>\r\n	Offering professional assistance in starting a business is one of our areas of specialisation. To begin with, a detailed project report is prepared along-with a feasibility study. We consider this, the beginning of a long term partnership.<br />\r\n	<br />\r\n	The firm assists in drafting the Memorandum of Association, obtaining the Trade License and completing other procedures in local and federal government departments. For offshore business, we help in securing the required license.<br />\r\n	<br />\r\n	We also checkout the cost of project and advice on means of finance. This is Stage I.<br />\r\n	<br />\r\n	In Stage II, the project gets underway. It&#39;s here that many projects, losing control of the costs and time factor, falter and fall. Our guidance on cost and time control will ensure the project is completed within the set deadline and do not exceed the envisaged cost.<br />\r\n	<br />\r\n	In Stage III, we guide the new born in setting up and streamlining management and financial control systems including internal control systems, policies, procedures, guidelines pertaining to its areas of operations as well as accounting, inventory, payroll, costing, budgetary control and management information systems. We further undertake audits regularly to ensure that the systems are followed and the financial transactions are recorded properly.<br />\r\n	<br />\r\n	Once business operations are stabilised, the company&#39;s performance is monitored on a constant basis and continued advise is offered on future expansion and diversification plans. We also ensure that the company&#39;s systems keep pace with the changing times. This is Stage IV.<br />\r\n	<br />\r\n	<br />\r\n	<strong>Project Appraisals &amp; Feasibility Studies</strong><br />\r\n	Project appraisal is an essential tool in regeneration and neighbourhood renewal, but audits of appraisal practice have found significant weaknesses. Effective project appraisal offers significant benefits to partnerships and, most importantly, to local communities. A good appraisal justifies spending money on a project. It is an important tool in decision making and lays the foundation for delivery and evaluation. Getting the design and operation of appraisal systems right is important.<br />\r\n	<br />\r\n	The proper consideration of each of the key components of project appraisal is essential. These are<br />\r\n	<br />\r\n	&bull; need, targeting and objectives<br />\r\n	&bull; context and connections<br />\r\n	&bull; consultation<br />\r\n	&bull; options<br />\r\n	&bull; inputs<br />\r\n	&bull; outputs and outcomes<br />\r\n	<br />\r\n	<br />\r\n	Al Kamel Associates FZE provides a thorough appraisal on any kind of new project referred to us. This is carried out by conducting an in-depth feasibility study to assess viability of the project with due care. Our appraisal proves useful in securing the expected finance from banks and financial institutions.<br />\r\n	<br />\r\n	Our dedicated team of professionals have a thorough understanding of the management and financial issues that face corporate entities. We bring an entrepreneurial spirit and vision to help strategise solutions, seize new business opportunities and optimize profits to help our clients expand their businesses confidently and successfully.<br />\r\n	<br />\r\n	Whether you are entering international markets or developing your existing business, we provide feasibility analysis on set-up and investment and on-going support including tools for contract procurement, project collaboration and supply chain management. We can also represent you at negotiations with potential joint venture partners. Our Feasibility Report can include market research, cost analysis including plant evaluation and process assessment, financial projections and raw material sourcing.</p>\r\n', 5, '0', '0', '0', '1', '1', 'Business Set Up Services', '', 'Business Set Up Services', 'Business Set Up Services', 'Business Set Up Services', '', '', 114, '', '1418623108_Business_Se_-Up.png', '1'),
(25, 'Business Valuations and Due Dilligence', 'business-valuations-and-due-dilligence', '<p>When a company is involved in a merger, business start-up, acquisition or liquidation, it is important that it gets expert advice in properly structuring the transition. We act as independent consultants, offering you objective assistance to establish methods of business valuation and to perform due diligence.<br>\r\n<br>\r\nWe have significantly re-engineered traditional valuation processes to achieve an unbeatable combination of accuracy, consistent high quality, transparency, speed, and cost-effectiveness for our business valuation services.<br>\r\n<br>\r\nWe have leveraged technology, expert systems, adept data mining techniques, Financial Modeling, and the latest financial research to become the leaders in business valuation. We meet almost every valuation need with our standard services. We are also equipped to provide customised services for more complex situations.<br>\r\n<br>\r\nOur valuation technique utilises established valuation models (reports generally incorporate a number of different methodologies as an added check on the result) including analysis of both past and projected figures. Valuations also include a background of the company, future prospects and risk management profiles.<br>\r\n</p>\r\n', 5, '0', '0', '0', '1', '1', 'Business Valuations and Due Dilligence', '', 'Business Valuations and Due Dilligence', 'Business Valuations and Due Dilligence', 'Business Valuations and Due Dilligence', '', '', 115, '', '1418623190_Business_Valuations.png', '1'),
(26, 'Corporate Training and Consultancy', 'corporate-training-consultancy', '<p>\r\n	Al Kamel Associates was established in response to the growing need for quality training to support the business community within your Company, Department or Organization by providing a high standard of Training Programs, Courses, Seminars, Workshops and Consultancy Services to employees in a very competitive business environment.<br />\r\n	<br />\r\n	Following the latest trends and ideas in Technology and Management techniques, and with its in-depth knowledge of the business environment, Al Kamel Associates is able to meet the special requirements of both local and international business throughout the Middle East.<br />\r\n	<br />\r\n	Our Objectives<br />\r\n	<br />\r\n	&bull; Bringing your staff up to date with the latest techniques.<br />\r\n	&bull; Improve their performance.<br />\r\n	&bull; Deepen their understanding of their working environment.<br />\r\n	&bull; Provide new ideas they can use in their daily tasks.<br />\r\n	<br />\r\n	Experience of Al Kamel Associates<br />\r\n	<br />\r\n	Al Kamel Associates is an International Organization based in Dubai (Head Office). We have regional office at Ras Al Khaimah and associates&rsquo; network in Nigeria, UK and India. Al Kamel Associates is an organization of joint venture companies joined together to conduct and market their specialized courses in-group under their trade name.<br />\r\n	<br />\r\n	Our Consultants are Qualified and experienced international speakers, have been selected from UK, USA, Canada, and they are academic instructors. Some of them possess experience of more than 30 years in training in their specialized field for and oriented to our Gulf Area. Our panel of speakers run courses in UK, USA, Canada, South Africa, UAE and the Gulf States (Bahrain and Qatar, Saudi Kingdom, Oman and Kuwait).<br />\r\n	<br />\r\n	Our Customers<br />\r\n	<br />\r\n	&bull; Oil, Gas, Petrochemicals, Refinery Companies<br />\r\n	&bull; The private sector, Major SME Companies<br />\r\n	&bull; Industrial and Commercial Companies<br />\r\n	&bull; The Government Ministries and Departments<br />\r\n	&bull; National and Commercial Banks in GCC<br />\r\n	<br />\r\n	Our Goal<br />\r\n	<br />\r\n	Our goal is the provision of the highest level of service, which equips you with the best possible training packages you require. All of our staff is highly qualified, and all our packages are designed by professionals in conjunction with academics in various industries, thus providing a unique combination. This blend of vocational, personal and academic experience means that the training packages offered by Al Kamel Associates are always of the highest quality, and we aim to constantly upgrade our courses and seminars to keep abreast of, and sometimes pre-empt, the latest trends in Management, Industry and Technology.<br />\r\n	<br />\r\n	Corporate Training Courses<br />\r\n	<br />\r\n	Al Kamel Associates provides a supportive environment in which to make the most of the learning opportunities available. Al Kamel Associates courses, seminars and programs give delegates the chance to develop the wider skills and creativity that the modern workplace demands. Participants will benefit from sharing experiences with other people attending the course.<br />\r\n	<br />\r\n	In - Company Courses<br />\r\n	<br />\r\n	Al Kamel Associates courses are designed to provide customized programs to meet the specific needs of your Company. Selected topics can be combined to form a coherent program in the most cost effective way. The main objective of such training is to orient participants to the latest techniques and to ensure their full understanding of established working practices in the fields in which they are specialized or are operating.<br />\r\n	<br />\r\n	Al Kamel Associates Trained Experts are available to offer advice on designed programs and courses. We will work with you to create to your requirements, tailored Training Packages to be conducted. If you would like to take advantage of this service please contact our Courses Co-ordinators. In-Company Courses are very cost effective and organizations have the opportunity to screen and select delegates to ensure maximum benefits.<br />\r\n	<br />\r\n	Mission and Vision<br />\r\n	<br />\r\n	Building people.. Building Future<br />\r\n	<br />\r\n	To provide top Quality Training, Consultancy, Courses, Seminars and Workshops delivered by a widely - based International Organization which is competitive, in touch with local needs and is committed to excellence.<br />\r\n	<br />\r\n	Clientele that we cater to:<br />\r\n	<br />\r\n	&bull; We understand that our success is solely dependent on our meticulous attention to customer needs:<br />\r\n	&bull; In a training room, our consultants are keen to establish then meet the needs of the delegates attending a program, aiming to transfer knowledge to raise professional competence.<br />\r\n	&bull; At all times, we are conscious of the needs of your organization, that you are looking to achieve value for money and are looking to us to equip your people to help you achieve your goals<br />\r\n	&bull; As responsible corporate citizens, we also recognize that we are serving the needs of the region and community in which we operate.<br />\r\n	<br />\r\n	Why Al Kamel Associates?<br />\r\n	<br />\r\n	Al Kamel Associates has always been a great place to work but we have worked very hard during the last two years to make it even better. We have recruited a number of well trained, highly motivated innovative managers and trainers to cope with our ever increasing project demands. We are committed to global unification of work practice and standards, offering template solutions to regional offices and clients alike. And we have implemented a program of high-involvement innovation through policies which:<br />\r\n	<br />\r\n	Make people management our highest priority Link staff training to global and regional business planning Install TEAM structures formally train our leaders to assist quicker TEAM cohesiveness.<br />\r\n	<br />\r\n	Focus on TEAM work rather than individual tasks through the incorporation of project management concepts Shift focus to knowledge management rather than task management because we see our people as &#39;assets&#39; to be cultivated rather than &#39;labour costs&#39; to be minimized<br />\r\n	<br />\r\n	Training Areas :<br />\r\n	<br />\r\n	1) OIL AND GAS INDUSTRIES-RISK ASSESSMENT<br />\r\n	2) RISK MANAGEMENT-PRACTICES AND TRENDS<br />\r\n	3) FINANCE &amp; BUDGETING<br />\r\n	4) MANAGEMENT &amp; LEADERSHIP |<br />\r\n	5) PROJECT, CONTRACTS &amp; TENDERS MANAGEMENT<br />\r\n	6) PURCHASING, INVENTORY &amp; MATERIAL MANAGEMENT<br />\r\n	7) HUMAN RESOURCES MANAGEMENT &amp; TRAINING SKILLS<br />\r\n	8) PUBLIC RELATIONS<br />\r\n	9) SALES, MARKETING &amp; CUSTOMER RELATIONSHIP<br />\r\n	10) ADMINISTRATION, SECRETARIAL &amp; WRITING SKILLS<br />\r\n	<br />\r\n	&nbsp;</p>\r\n', 5, '0', '0', '0', '1', '1', 'Corporate Training and Consultancy', '', 'Corporate Training and Consultancy', 'Corporate Training and Consultancy', 'Corporate Training and Consultancy', '', '', 116, '', '1418623234_Corporate.png', '1'),
(27, 'International Corporate Seminars', 'international-corporate-seminars', '<p>\r\n	International Corporate Seminars</p>\r\n', 5, '0', '0', '0', '1', '1', 'International Corporate Seminars', '', 'International Corporate Seminars', 'International Corporate Seminars', 'International Corporate Seminars', '', '', 117, '', '1418623285_International.png', '1'),
(17, 'Profile', 'profile', '<div class="col-md-4 top_alemnt" style="margin:0 4% 0  0">\r\n	<p>\r\n		Al Kamel Associates FZE started with the best intentions. Our comprehensive Financial Accounting and Reporting solutions form more than useful conglomerate with the Audit and Advisory services to go beyond the routine preparation of financial statements. From serving as an extension of your own accounting department to designing customized reports and their analysis, we can tailor our Management Assurance solutions to meet your organisational needs. We undertake to provide you with the accurate and objective financial information needed to make key business decisions. It is our endeavour to enable your company to relive the financial freedom.</p>\r\n</div>\r\n<div class="col-md-4 ">\r\n	<center>\r\n		<img class="img-responsive img-top-bottom" src="images/profile_img_01.png" /></center>\r\n	<p>\r\n		We are committed to provide you highly Professional &amp; Internationally Certified instructors / Trainers who are leaders in their fields of expertise. We select only qualified and dedicated instructors / Trainers to deliver the trainings. We also evaluate the instructors before and after the training planned. We have experienced consultants to assess each trainers/ instructors.</p>\r\n</div>\r\n<p>\r\n	&nbsp;</p>\r\n', 0, '0', '1', '1', '0', '0', 'This is small desc', '<span class="banner_title">An IndEpendent audit is the foundation for</span>\r\n                    <span class="banner_title" style="font-size:24px;">decision - making in the capital markets.</span>', 'Profile', 'Profile', 'Profile', 'Profile', 'Profile', 14, '1418646318_profile_banner.jpg', '1418295622_934891_1031033686952999_1913940623436890964_n.jpg', '1'),
(45, 'External Auditing and Management Assurance', 'external-auditing-and-management-assurance', '<p>\r\n	An independent audit is the foundation for decision-making in the capital markets. Our ethos and working practices are modern and progressive. We are a motivated, cohesive and dynamic firm who work with and for our clients in ways that seek to advance traditional notions of auditing, with a focus on client relationships.<br />\r\n	<br />\r\n	A professional knowledge of principles and business, as well as of the appropriate systems, processes and controls are also critical requirements. Moreover, auditors&rsquo; judgements must be rooted in an in-depth understanding of each client&rsquo;s industry value drivers, competitive positioning and marketplace practice. Al Kamel Associates has the knowledge and experience necessary to help you with complex financial accounting issues related to matters such as valuations, pensions and share plans, listings, IFRS conversions, and corporate treasury and company secretarial functions.<br />\r\n	<br />\r\n	Our audit approach, at the leading edge of best practice, is tailored to suit the size and nature of your organisation and draws upon our extensive industry knowledge. Our deep understanding of regulation and legislation means we can also help with complex reporting issues involving Sarbanes-Oxley and International Financial Reporting Standards (IFRS).<br />\r\n	We, at Al Kamel Associates, execute each assignment in a time-tested, rigorous, and cost-effective methods and procedures. We evaluate the organization&#39;s internal controls and develop a tailored audit program for the client that also enhances financial reporting efficiency.</p>\r\n<p>\r\n	&nbsp;</p>\r\n<p>\r\n	Some of the auditing services provided by us under External Independent Audit domain are:</p>\r\n<p>\r\n	<br />\r\n	&bull; Financial statement audit<br />\r\n	&bull; IFRS reporting<br />\r\n	&bull; Independent controls &amp; systems process assurance<br />\r\n	<br />\r\n	Financial statement audit<br />\r\n	<br />\r\n	The financial statement audit has never been more important. In today&#39;s business environment there is more scrutiny and scepticism of a company&#39;s financial statements than ever before. Investors have lost faith in corporate governance and reporting and they expect more: greater reliability, more oversight and clear evidence of internal controls. Corporate management, boards and audit committees, internal and external auditors, analysts and other investment professionals all have important roles to play in rebuilding investor trust by executing their respective responsibilities, keeping in mind both legal obligations and the heightened expectations of investors. Meeting investor expectations begins with the completeness and accuracy of information contained in a company&rsquo;s financial statements.<br />\r\n	<br />\r\n	IFRS reporting<br />\r\n	<br />\r\n	In 2005, the vast majority of EU listed companies&mdash;and in many other countries&mdash;made the switch to IFRS. The good news: investors are now able to understand and compare financial statements from companies around the world. Result: lower cost capital more efficiently allocated. The not-so-good news: it&#39;s been a challenge getting through the IFRS conversion process and initial reporting period. Conversion to IFRS is much more than a technical accounting issue. IFRS may significantly affect any number of a company&rsquo;s day-to-day operations or even impact the reported profitability of the business itself.<br />\r\n	<br />\r\n	How Al Kamel Associates can help you<br />\r\n	<br />\r\n	Al Kamel Associates has a proven track record in helping companies successfully complete the transition to new accounting standards. Reflecting the complexity of the task at hand, we have a range of specialists to assist your company&#39;s conversion to IFRS, including: technical accounting, treasury, tax, human resource, M&amp;A valuations and project management specialists. (For audit clients subject to the provisions of the US Sarbanes-Oxley Act, non-audit services, including tax services can be provided by the auditor as long as the services have been pre-approved by the audit committee.)<br />\r\n	<br />\r\n	&bull; Objective evaluation of project processes, controls, and deliverables<br />\r\n	&bull; Training and coaching support<br />\r\n	&bull; Advice on adapting processes, data, and systems for IFRS<br />\r\n	&bull; Technical accounting advice and support tools<br />\r\n	&bull; IAS 39 and financial instruments<br />\r\n	&bull; Employee benefits and share compensation programs<br />\r\n	&bull; Purchase price allocation related to business combinations<br />\r\n	<br />\r\n	Independent Controls Assurance Practice<br />\r\n	<br />\r\n	In today&rsquo;s business world, IT and financial reporting environments are becoming increasingly complex while even greater reliance is being placed on the information produced by these systems and processes. In addition, new regulations in many countries have put a greater emphasis on internal controls and often require independent assurance of the effectiveness of internal controls.<br />\r\n	Attention to the design, documentation and operation of controls is critical to ensuring the accuracy and timeliness of information used for financial reporting and management decision-making.<br />\r\n	<br />\r\n	Our Mission<br />\r\n	<br />\r\n	&bull; To enable the customers&rsquo; confidence in the quality of the information produced by their IT systems.<br />\r\n	&bull; Provide assistance in documenting or testing internal controls over financial reporting.<br />\r\n	&bull; Carry out an independent review of the control structure, including identification of weaknesses and possible design enhancements.<br />\r\n	&bull; Furnish objective third party financial information.<br />\r\n	&bull; If the customer is implementing&mdash;or have just implemented&mdash;a new IT system and want a review of the controls.<br />\r\n	&bull; Companies entering into a joint venture or other transaction and need due diligence on systems and controls.<br />\r\n	<br />\r\n	Our Methodology<br />\r\n	<br />\r\n	Our Controls Assurance practice provides services related to controls around the financial reporting process, including financial business process and IT management controls. Serving both audit and non-audit clients, Al Kamel Associates provide:<br />\r\n	<br />\r\n	&bull; Financial and operation applications/business process controls reviews<br />\r\n	&bull; Database security controls reviews<br />\r\n	&bull; IT general controls reviews<br />\r\n	&bull; Infrastructure security reviews<br />\r\n	&bull; Third party assurance and opinion services<br />\r\n	&bull; Sarbanes-Oxley readiness, process improvement and sustainability services<br />\r\n	&bull; Compliance with other regulatory requirements (e.g., Turnbull, Basel II, King)<br />\r\n	&bull; Due diligence on systems and controls<br />\r\n	&bull; Pre- and post-implementation systems reviews<br />\r\n	&bull; Network Security Reviews<br />\r\n	&bull; Data services (e.g., CAATs, data quality reviews)</p>\r\n', 5, '0', '0', '0', '1', '0', 'An independent audit is the foundation for decision-making in the capital markets. Our ethos and working practices are modern and progressive. We are a motivated, cohesive and dynamic firm who work with and for our clients in ways that seek to advance traditional notions of auditing, with a focus on client relationships.', '', 'External Auditing and Management Assurance', 'External Auditing and Management Assurance', 'External Auditing and Management Assurance', '', '', 135, '', '1418819994_Management-Consultancy.png', '1');
INSERT INTO `tbl_pages` (`page_id`, `page_title`, `page_slug`, `page_desc`, `page_parent`, `on_home_footer`, `on_header`, `on_footer`, `on_home`, `on_left`, `page_desc_small`, `page_banner_desc`, `page_meta_title`, `page_meta_keywords`, `page_meta_desc`, `page_meta_keyphase`, `page_meta_abstract`, `page_order`, `page_banner`, `page_banner_small`, `page_status`) VALUES
(46, 'Company Liquidation', 'company-liquidation', '<p>\r\n	Liquidation is a key element of corporate strategic planning.<br />\r\n	<br />\r\n	Liquidation or deregistration, the arduous process by which a company is brought to an end, is not a situation that any organization wishes to find itself in.<br />\r\n	<br />\r\n	But preparing for the worst-case scenario, especially in turbulent times like these, should be a critical part of a company&#39;s strategic planning.<br />\r\n	<br />\r\n	&quot;While liquidation might not have been a familiar term in the UAE just a few years ago thanks to the sky-rocketing economy, it is now an everyday phenomenon ever since the global crisis hit home. Most companies are shocked and surprised by the lengthy procedures involved in closing up a company in the UAE. But all it takes is some advance planning about the liquidation process during the Business Plan stages itself, and most of the ordeal can be avoided&quot;.<br />\r\n	<br />\r\n	While the UAE&#39;s business environment does encourage entrepreneurship and is significantly devoid of red-tapes compared to other world business markets, there are certain aspects of the law and procedure that business houses need to take note at an early stage of their business plan. For example, even after announcing liquidation by placing a notice in the newspapers for the same, the Directors need to have a clean chit from the concerned Government Agencies like Labour, Immigration and the Municipality stating that they have no pending renewals of licenses. Companies should also be aware of the fact that their licenses should be active while applying for closure. Awareness of small but crucial aspects of the law such as these can save a lot of time, money and trouble during liquidation.</p>\r\n', 5, '0', '0', '0', '1', '0', 'Liquidation is a key element of corporate strategic planning.\r\n\r\nLiquidation or deregistration, the arduous process by which a company is brought to an end, is not a situation that any organization wishes to find itself in.', '', 'Company Liquidation', 'Company Liquidation', '', '', '', 136, '', '1418820106_International.png', '1'),
(51, 'Accounting & Finance', 'accounting-finance', '<table border="0" cellpadding="0" cellspacing="0" width="530">\r\n	<colgroup>\r\n		<col />\r\n		<col />\r\n		<col />\r\n		<col />\r\n		<col />\r\n	</colgroup>\r\n	<tbody>\r\n		<tr height="40">\r\n			<td height="40" style="height:41px;width:53px;">\r\n				Code&nbsp;</td>\r\n			<td style="width:240px;">\r\n				&nbsp;Seminar / Workshop</td>\r\n			<td style="width:92px;">\r\n				Dates&nbsp;</td>\r\n			<td style="width:67px;">\r\n				Venue&nbsp;&nbsp;</td>\r\n			<td style="width:79px;">\r\n				Fee/Per Participant&nbsp;</td>\r\n		</tr>\r\n		<tr height="18">\r\n			<td height="55" rowspan="3" style="height:55px;">\r\n				F&amp;A 001</td>\r\n			<td rowspan="3">\r\n				Financial Analysis, Modelling and Forecasting</td>\r\n			<td>\r\n				04 Jan - 08 Jan</td>\r\n			<td rowspan="3">\r\n				Dubai&nbsp;</td>\r\n			<td rowspan="3">\r\n				&nbsp;$&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 4,500</td>\r\n		</tr>\r\n		<tr height="18">\r\n			<td height="18" style="height:18px;">\r\n				9 Mar - 13 Mar</td>\r\n		</tr>\r\n		<tr height="19">\r\n			<td height="19" style="height:19px;">\r\n				17 Aug - 21 Aug</td>\r\n		</tr>\r\n		<tr height="18">\r\n			<td height="55" rowspan="3" style="height:55px;">\r\n				F&amp;A 002</td>\r\n			<td rowspan="3">\r\n				Basic Petroleum Accounting &amp; Economics</td>\r\n			<td>\r\n				11 Jan - 15 Jan</td>\r\n			<td rowspan="3">\r\n				Dubai&nbsp;</td>\r\n			<td rowspan="3">\r\n				&nbsp;$&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 4,500</td>\r\n		</tr>\r\n		<tr height="18">\r\n			<td height="18" style="height:18px;">\r\n				06 Apr - 10 Apr</td>\r\n		</tr>\r\n		<tr height="19">\r\n			<td height="19" style="height:19px;">\r\n				21 Sep - 25 Sep</td>\r\n		</tr>\r\n		<tr height="18">\r\n			<td height="37" rowspan="2" style="height:37px;">\r\n				F&amp;A 003</td>\r\n			<td rowspan="2" style="width:240px;">\r\n				Petroleum Finance &amp; Accounting Principles</td>\r\n			<td>\r\n				12 Jan - 16 Jan</td>\r\n			<td rowspan="2">\r\n				Dubai</td>\r\n			<td rowspan="2">\r\n				&nbsp;$&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 4,500</td>\r\n		</tr>\r\n		<tr height="19">\r\n			<td height="19" style="height:19px;">\r\n				26 Oct - 30 Oct</td>\r\n		</tr>\r\n		<tr height="18">\r\n			<td height="30" rowspan="2" style="height:31px;">\r\n				F&amp;A 004</td>\r\n			<td rowspan="2" style="width:240px;">\r\n				Finance Theory &amp; Financial Management</td>\r\n			<td rowspan="2">\r\n				18 Jan - 22 Jan</td>\r\n			<td rowspan="2">\r\n				Dubai</td>\r\n			<td rowspan="2">\r\n				&nbsp;$&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 4,500</td>\r\n		</tr>\r\n		<tr height="12">\r\n		</tr>\r\n		<tr height="18">\r\n			<td height="73" rowspan="4" style="height:73px;">\r\n				F&amp;A 005</td>\r\n			<td rowspan="4" style="width:240px;">\r\n				Economic Analysis for Business Decisions</td>\r\n			<td>\r\n				19 Jan - 23 Jan</td>\r\n			<td rowspan="4">\r\n				Dubai</td>\r\n			<td rowspan="4">\r\n				&nbsp;$&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 4,750</td>\r\n		</tr>\r\n		<tr height="18">\r\n			<td height="18" style="height:18px;">\r\n				25 May - 29 May</td>\r\n		</tr>\r\n		<tr height="18">\r\n			<td height="18" style="height:18px;">\r\n				03 Aug - 07 Aug</td>\r\n		</tr>\r\n		<tr height="19">\r\n			<td height="19" style="height:19px;">\r\n				16 Nov - 20 Nov</td>\r\n		</tr>\r\n		<tr height="18">\r\n			<td height="73" rowspan="4" style="height:73px;">\r\n				F&amp;A 006</td>\r\n			<td rowspan="4" style="width:240px;">\r\n				Cash Flow and Working Capital Management</td>\r\n			<td>\r\n				19 Jan - 30 Jan</td>\r\n			<td rowspan="4">\r\n				Dubai</td>\r\n			<td rowspan="4">\r\n				&nbsp;$&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 8,500</td>\r\n		</tr>\r\n		<tr height="18">\r\n			<td height="18" style="height:18px;">\r\n				25 May - 05 Jun</td>\r\n		</tr>\r\n		<tr height="18">\r\n			<td height="18" style="height:18px;">\r\n				03 Aug - 14 Aug</td>\r\n		</tr>\r\n		<tr height="19">\r\n			<td height="19" style="height:19px;">\r\n				16 Nov - 27 Nov</td>\r\n		</tr>\r\n		<tr height="18">\r\n			<td height="73" rowspan="4" style="height:73px;">\r\n				F&amp;A 007</td>\r\n			<td rowspan="4" style="width:240px;">\r\n				Budgeting, Planning and Management Reporting</td>\r\n			<td>\r\n				26 Jan - 30 Jan</td>\r\n			<td rowspan="4">\r\n				Dubai</td>\r\n			<td rowspan="4">\r\n				&nbsp;$&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 4,950</td>\r\n		</tr>\r\n		<tr height="18">\r\n			<td height="18" style="height:18px;">\r\n				01 Jun - 05 Jun</td>\r\n		</tr>\r\n		<tr height="18">\r\n			<td height="18" style="height:18px;">\r\n				10 Aug - 14 Aug</td>\r\n		</tr>\r\n		<tr height="19">\r\n			<td height="19" style="height:19px;">\r\n				23 Nov - 27 Nov</td>\r\n		</tr>\r\n		<tr height="39">\r\n			<td height="39" style="height:40px;">\r\n				F&amp;A 008</td>\r\n			<td style="width:240px;">\r\n				Effective Budgeting &amp; Operational Cost Control</td>\r\n			<td>\r\n				15 Feb - 19 Feb</td>\r\n			<td>\r\n				Dubai</td>\r\n			<td>\r\n				&nbsp;$&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 4,500</td>\r\n		</tr>\r\n		<tr height="18">\r\n			<td height="55" rowspan="3" style="height:55px;">\r\n				F&amp;A 009</td>\r\n			<td rowspan="3" style="width:240px;">\r\n				Auditing Corporate Governance</td>\r\n			<td>\r\n				15 Feb - 19 Feb</td>\r\n			<td rowspan="3">\r\n				Dubai</td>\r\n			<td rowspan="3">\r\n				&nbsp;$&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 4,500</td>\r\n		</tr>\r\n		<tr height="18">\r\n			<td height="18" style="height:18px;">\r\n				24 May - 28 May</td>\r\n		</tr>\r\n		<tr height="19">\r\n			<td height="19" style="height:19px;">\r\n				18 Oct - 22 Oct</td>\r\n		</tr>\r\n		<tr height="18">\r\n			<td height="55" rowspan="3" style="height:55px;">\r\n				F&amp;A 010</td>\r\n			<td rowspan="3" style="width:240px;">\r\n				Project Planning, Scheduling &amp; Cost Estimating Skills</td>\r\n			<td>\r\n				23 Feb - 27 Feb</td>\r\n			<td rowspan="3">\r\n				Dubai</td>\r\n			<td rowspan="3">\r\n				&nbsp;$&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 4,500</td>\r\n		</tr>\r\n		<tr height="18">\r\n			<td height="18" style="height:18px;">\r\n				10 Aug - 14 Aug</td>\r\n		</tr>\r\n		<tr height="19">\r\n			<td height="19" style="height:19px;">\r\n				30 Nov - 04 Dec</td>\r\n		</tr>\r\n		<tr height="21">\r\n			<td height="58" rowspan="3" style="height:58px;">\r\n				F&amp;A 011</td>\r\n			<td rowspan="3" style="width:240px;">\r\n				IFRS International Accounting Standards</td>\r\n			<td>\r\n				23 Feb - 06 Mar</td>\r\n			<td rowspan="3">\r\n				Dubai</td>\r\n			<td rowspan="3">\r\n				&nbsp;$&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 7,950</td>\r\n		</tr>\r\n		<tr height="18">\r\n			<td height="18" style="height:18px;">\r\n				10 Aug - 21 Aug</td>\r\n		</tr>\r\n		<tr height="19">\r\n			<td height="19" style="height:19px;">\r\n				30 Nov - 11 Dec</td>\r\n		</tr>\r\n		<tr height="18">\r\n			<td height="55" rowspan="3" style="height:55px;">\r\n				F&amp;A 013</td>\r\n			<td rowspan="3" style="width:240px;">\r\n				Fraud Investigation and Intelligence Report</td>\r\n			<td>\r\n				01 Mar - 05 Mar</td>\r\n			<td rowspan="3">\r\n				Dubai</td>\r\n			<td rowspan="3">\r\n				&nbsp;$&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 4,500</td>\r\n		</tr>\r\n		<tr height="18">\r\n			<td height="18" style="height:18px;">\r\n				02 Aug - 06 Aug</td>\r\n		</tr>\r\n		<tr height="19">\r\n			<td height="19" style="height:19px;">\r\n				06 Dec - 10 Dec</td>\r\n		</tr>\r\n		<tr height="18">\r\n			<td height="55" rowspan="3" style="height:55px;">\r\n				F&amp;A 014</td>\r\n			<td rowspan="3" style="width:240px;">\r\n				Accounting, Decision Making &amp; Financial Communication</td>\r\n			<td>\r\n				02 Mar - 06 Mar</td>\r\n			<td rowspan="3">\r\n				Dubai</td>\r\n			<td rowspan="3">\r\n				&nbsp;$&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 5,000</td>\r\n		</tr>\r\n		<tr height="18">\r\n			<td height="18" style="height:18px;">\r\n				17 Aug - 21 Aug</td>\r\n		</tr>\r\n		<tr height="19">\r\n			<td height="19" style="height:19px;">\r\n				07 Dec - 11 Dec</td>\r\n		</tr>\r\n		<tr height="18">\r\n			<td height="55" rowspan="3" style="height:55px;">\r\n				F&amp;A 015</td>\r\n			<td rowspan="3" style="width:240px;">\r\n				Financial Analysis and Evaluation</td>\r\n			<td>\r\n				08 Mar - 12 Mar</td>\r\n			<td rowspan="3">\r\n				Dubai</td>\r\n			<td rowspan="3">\r\n				&nbsp;$&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 4,750</td>\r\n		</tr>\r\n		<tr height="18">\r\n			<td height="18" style="height:18px;">\r\n				09 Aug - 13 Aug</td>\r\n		</tr>\r\n		<tr height="19">\r\n			<td height="19" style="height:19px;">\r\n				13 Dec - 17 Dec</td>\r\n		</tr>\r\n		<tr height="18">\r\n			<td height="55" rowspan="3" style="height:55px;">\r\n				F&amp;A 016</td>\r\n			<td rowspan="3" style="width:240px;">\r\n				Essential Management Accounting</td>\r\n			<td>\r\n				16 Mar - 20 Mar</td>\r\n			<td rowspan="3">\r\n				Dubai</td>\r\n			<td rowspan="3">\r\n				&nbsp;$&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 4,500</td>\r\n		</tr>\r\n		<tr height="18">\r\n			<td height="18" style="height:18px;">\r\n				31 Aug - 04 Sep</td>\r\n		</tr>\r\n		<tr height="19">\r\n			<td height="19" style="height:19px;">\r\n				30 Nov - 04 Dec</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<p>\r\n	&nbsp;</p>\r\n', 18, '0', '0', '0', '1', '1', 'Accounting &Finance', '', '', '', '', '', '', 140, '', '1419242534_tham_01.png', '1'),
(52, 'Management & Leadership', 'management-leadership', '', 18, '0', '0', '0', '1', '1', 'Management & Leadership', '', '', '', '', '', '', 141, '', '1419242624_tham_05.png', '1'),
(53, 'Administration & Office Management', 'admin-and-office-management', '', 18, '0', '0', '0', '1', '1', '', '', '', '', '', '', '', 142, '', '1419242675_tham_06.png', '1'),
(54, 'Human Resources  Management', 'human-resources-management', '', 18, '0', '0', '0', '1', '1', '', '', '', '', '', '', '', 143, '', '1419242568_tham_04.png', '1'),
(55, 'Public Relations', 'public-realtion', '', 18, '0', '0', '0', '1', '1', '', '', '', '', '', '', '', 144, '', '1419242763_tham_09.png', '1'),
(56, ' I.T  Seminars', 'it-seminars', '', 18, '0', '0', '0', '1', '1', '', '', '', '', '', '', '', 145, '', '1419242800_tham_10.png', '1'),
(57, 'Contracts  & Procurement', 'contract-and-procurement', '', 18, '0', '0', '0', '1', '1', '', '', '', '', '', '', '', 146, '', '1419242879_tham_07.png', '1'),
(58, 'Project Management', 'project-management', '', 18, '0', '0', '0', '1', '1', '', '', '', '', '', '', '', 147, '', '1419259474_tham_08.png', '1'),
(59, ' Health , Safety & Security', 'health-safety-and-security', '', 18, '0', '0', '0', '1', '1', '', '', '', '', '', '', '', 148, '', '1419259486_tham_09.png', '1'),
(60, 'Oil & Gas Technical', 'oil-and-gas-technical', '', 18, '0', '0', '0', '1', '1', '', '', '', '', '', '', '', 149, '', '1419259499_tham_10.png', '1'),
(61, '10 Days', '10-days', '', 18, '0', '0', '0', '1', '1', '', '', '', '', '', '', '', 150, '', '1419259521_tham_12.png', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_saved_search`
--

CREATE TABLE IF NOT EXISTS `tbl_saved_search` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `search_query` text NOT NULL,
  `is_active` enum('0','1') NOT NULL,
  `is_deleted` enum('0','1') NOT NULL,
  `created_date` datetime NOT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `tbl_saved_search`
--

INSERT INTO `tbl_saved_search` (`id`, `user_id`, `search_query`, `is_active`, `is_deleted`, `created_date`, `updated`) VALUES
(1, 3, '0', '1', '0', '2015-04-22 11:47:25', '2015-04-24 11:56:52'),
(2, 3, 'search_city%3DDubai%26amp%3Bsearch_keyword%3Dhere%2Bis%2Byour%2Bsearch', '1', '0', '2015-04-22 12:21:32', '2015-04-24 11:56:52'),
(3, 3, 'search_city%3DDubai%26amp%3Bsearch_keyword%3Dhere%2Bis%2Byour%2Bsearch%2Bin%2Bcars', '1', '0', '2015-04-22 12:22:52', '2015-04-24 11:56:52'),
(4, 3, 'search_city%3DDubai%26amp%3Bsearch_keyword%3Dhere%2Bis%2Bsearch%2Bin%2Bcars', '1', '0', '2015-04-22 12:23:26', '2015-04-24 11:56:52'),
(5, 3, 'category%3D3%26amp%3Border%3D2', '0', '1', '2015-04-22 12:48:36', '2015-04-24 15:40:45'),
(6, 3, 'category%3D3%26amp%3Border%3D2%26amp%3Bsearch_keyword%3DTDI', '1', '0', '2015-04-22 13:06:02', '2015-04-24 11:56:52'),
(7, 3, 'category%3D3%26amp%3Border%3D2%26amp%3Bsearch_keyword%3DTDI%2Bhewr', '1', '0', '2015-04-22 13:07:02', '2015-04-24 11:56:52'),
(8, 3, 'search_city_range%3D10%26amp%3Bsearch_city%3DDubai%26amp%3Bsearch_keyword%3Ddss', '1', '0', '2015-04-22 20:24:26', '2015-04-24 11:56:52'),
(9, 3, 'search_city_range%3D40%26amp%3Bsearch_city%3DSharja', '1', '0', '2015-04-23 20:16:29', '2015-04-24 12:45:41'),
(10, 3, 'search_city_range%3D40%26amp%3Bsearch_city%3DSharja%26amp%3Bsearch_keyword%3Dleather%2Bseat%2Bbelt', '1', '0', '2015-04-23 20:16:57', '2015-04-24 12:46:09'),
(11, 3, 'search_city=Qusais', '1', '0', '2015-04-23 21:41:08', '2015-04-24 14:10:20'),
(12, 3, 'search_city=Qusais&amp;search_keyword=leather+seat+belt', '1', '0', '2015-04-23 21:42:23', '2015-04-24 14:11:35'),
(13, 3, 'search_keyword=here+is+your+search+in+cars&amp;search_city_range=20&amp;search_city=Dubai', '1', '0', '2015-04-23 21:47:58', '2015-04-24 14:17:10'),
(14, 3, 'search_keyword=TDI&amp;search_city_range=20&amp;search_city=Dubai', '1', '0', '2015-04-23 21:48:29', '2015-04-24 14:17:41'),
(15, 3, 'search_keyword=Good+cars&amp;search_city_range=20&amp;search_city=Dubai', '1', '0', '2015-04-23 21:49:02', '2015-04-24 14:18:14'),
(16, 3, 'country=98&amp;search_city=Kolkotta&amp;category=1&amp;brand=Honda&amp;model=Accord+2.3+VTI+AT&amp;manufacture_year=2015&amp;min_amount=2000&amp;max_amount=5000', '1', '0', '2015-04-23 22:56:04', '2015-04-24 15:25:16'),
(17, 3, 'country=98&amp;search_city=Kolkotta&amp;category=1&amp;brand=Honda&amp;model=Accord+2.3+VTI+AT&amp;manufacture_year=2015&amp;min_amount=2000&amp;max_amount=5000&amp;order=2', '0', '1', '2015-04-23 22:58:01', '2015-04-24 15:35:43'),
(18, 17, 'search_keyword=xxx', '0', '1', '2015-04-24 19:52:06', '2015-04-25 12:21:34'),
(19, 17, 'search_keyword=xxx', '0', '1', '2015-04-24 19:52:32', '2015-05-01 05:25:56'),
(20, 17, 'search_keyword=xxx', '1', '0', '2015-04-30 12:56:52', '2015-05-01 05:26:04'),
(21, 17, 'search_keyword=xxx&amp;search_city=xx', '1', '0', '2015-04-30 12:57:09', '2015-05-01 05:26:21'),
(22, 17, 'search_keyword=xxx&amp;search_city_range=50&amp;search_city=xx', '1', '0', '2015-04-30 12:57:21', '2015-05-01 05:26:33');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_secret_question`
--

CREATE TABLE IF NOT EXISTS `tbl_secret_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` varchar(255) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `created_date_time` datetime NOT NULL,
  `updated_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `tbl_secret_question`
--

INSERT INTO `tbl_secret_question` (`id`, `question`, `status`, `created_date_time`, `updated_date_time`) VALUES
(1, 'What street did you live on in third grade?', '1', '2015-06-08 16:46:31', '2015-06-09 10:46:07'),
(2, 'What is the name of your favorite childhood friend?', '1', '2015-06-08 16:46:31', '2015-06-09 10:46:07'),
(3, 'In what city did you meet your spouse/significant other?', '1', '2015-06-08 16:46:31', '2015-06-09 10:46:07'),
(4, 'What was your childhood nickname?', '1', '2015-06-08 16:46:31', '2015-06-09 10:46:07'),
(5, 'What time of the day were you born?', '1', '2015-06-08 16:46:31', '2015-06-09 10:46:07'),
(6, 'In what city or town does your nearest sibling live?', '1', '2015-06-08 16:46:31', '2015-06-09 10:46:07'),
(7, 'What was the name of your elementary / primary school?', '1', '2015-06-08 16:46:31', '2015-06-09 10:46:07'),
(8, 'What is the name of the place your wedding reception was held?', '1', '2015-06-08 16:46:31', '2015-06-09 10:46:07'),
(9, 'What is the last name of the teacher who gave you your first failing grade?', '1', '2015-06-08 16:46:31', '2015-06-09 10:46:07'),
(10, 'What is the first name of the person you first kissed?', '1', '2015-06-08 16:46:31', '2015-06-09 10:46:07'),
(11, 'What is your oldest sibling’s birthday month and year?', '1', '2015-06-08 16:46:31', '2015-06-09 10:46:07'),
(12, 'What is the middle name of your oldest child?', '1', '2015-06-08 16:46:31', '2015-06-09 10:46:07'),
(13, 'What is your oldest sibling''s middle name?', '1', '2015-06-08 16:46:31', '2015-06-09 10:46:07'),
(14, 'What school did you attend for sixth grade?', '1', '2015-06-08 16:46:31', '2015-06-09 10:46:07'),
(15, 'What was your childhood phone number including area code?', '1', '2015-06-08 16:46:31', '2015-06-09 10:46:07'),
(16, 'What is your oldest cousin''s first and last name?', '1', '2015-06-08 16:46:31', '2015-06-09 10:46:07'),
(17, 'What was the name of your first stuffed animal?', '1', '2015-06-08 16:46:31', '2015-06-09 10:46:07'),
(18, 'In what city or town did your mother and father meet?', '1', '2015-06-08 16:46:31', '2015-06-09 10:46:07'),
(19, 'What is the first name of the boy or girl that you first kissed?', '1', '2015-06-08 16:46:31', '2015-06-09 10:46:07'),
(20, 'What is your oldest brother’s birthday month and year?', '1', '2015-06-08 16:46:31', '2015-06-09 10:46:07'),
(21, 'What is your maternal grandmother''s maiden name?', '1', '2015-06-08 16:46:31', '2015-06-09 10:46:07'),
(22, 'In what city or town was your first job?', '1', '2015-06-08 16:46:31', '2015-06-09 10:46:07'),
(23, 'What is the name of a college you applied to but didn''t attend?', '1', '2015-06-08 16:46:31', '2015-06-09 10:46:07'),
(24, 'What was the last name of your third grade teacher?', '1', '2015-06-08 16:46:31', '2015-06-09 10:46:07'),
(25, '', '1', '2015-06-08 16:46:31', '2015-06-09 10:46:07');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_setting`
--

CREATE TABLE IF NOT EXISTS `tbl_setting` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `default_title` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `is_active` enum('0','1') NOT NULL DEFAULT '1',
  `created_date` datetime NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `tbl_setting`
--

INSERT INTO `tbl_setting` (`id`, `created_by`, `updated_by`, `default_title`, `value`, `is_active`, `created_date`, `updated_date`) VALUES
(1, 1, 1, 'NAME_SETTING', 'This is default name', '0', '0000-00-00 00:00:00', '2015-02-27 04:07:06'),
(2, 1, 2, 'DEFAULT_META_TITLE', 'classifie The first line of Lorem Ipsum', '1', '2015-02-21 12:38:19', '2015-02-27 04:08:37'),
(4, 2, 2, 'DEFAULT_META_KEYWORDS', 'test', '1', '2015-02-26 07:09:27', '2015-02-27 04:09:03'),
(5, 2, 2, 'DEFAULT_META_DESCRIPTION', 'description', '1', '2015-02-26 07:10:21', '2015-02-27 04:09:57'),
(6, 2, 2, 'FACEBOOK_PAGE_URL', 'https://www.facebook.com/rollsroycemotorcars', '1', '2015-02-26 07:10:21', '2015-06-23 00:40:49'),
(7, 2, 2, 'MANUFACTURE_YEAR_START', '1950', '1', '2015-02-26 07:10:21', '2015-02-27 04:09:57'),
(8, 2, 2, 'FACEBOOK_PAGE_LINK', 'https://www.facebook.com/rollsroycemotorcars', '1', '2015-06-13 17:53:54', '2015-06-23 00:39:53'),
(9, 2, 2, 'TWITTER_PAGE_LINK', 'https://twitter.com', '1', '2015-06-13 17:56:16', '2015-06-14 18:01:05'),
(10, 2, 2, 'LINKEDIN_PAGE_LINK', 'https://ae.linkedin.com/in', '1', '2015-06-13 17:57:04', '2015-06-14 18:01:19'),
(11, 2, 2, 'INSTAGRAM_PAGE_LINK', 'https://instagram.com/', '1', '2015-06-13 17:57:36', '2015-06-14 11:56:48'),
(12, 2, 2, 'GOODLE_PLUS_LINK', 'https://plus.google.com', '1', '2015-06-13 18:28:16', '2015-06-14 12:28:04'),
(13, 2, 2, 'EXCEED_TITLE', 'Exceed TRADER', '1', '2015-06-14 07:00:46', '2015-06-14 18:00:22');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_subscriber`
--

CREATE TABLE IF NOT EXISTS `tbl_subscriber` (
  `subscriber_id` int(11) NOT NULL AUTO_INCREMENT,
  `subscriber_email_address` varchar(255) NOT NULL,
  `subscriber_status` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`subscriber_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `tbl_subscriber`
--

INSERT INTO `tbl_subscriber` (`subscriber_id`, `subscriber_email_address`, `subscriber_status`) VALUES
(1, 'anas.muhammed@gmail.com', '0'),
(2, 'anas.muhammed1@gmail.com', '0'),
(3, 'anas@codeandco.ae', '0'),
(4, 'sanilal@gmail.com', '0'),
(5, 'anas.muhammed12@gmail.com', '0'),
(7, 'test@test.com', '0'),
(8, 'dasd@sd.jk', '0');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_tips_and_tricks`
--

CREATE TABLE IF NOT EXISTS `tbl_tips_and_tricks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `sub_category_id` int(11) NOT NULL,
  `member_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `url_slug` varchar(300) NOT NULL,
  `author_name` varchar(255) NOT NULL,
  `institution_name` varchar(255) NOT NULL,
  `institution_address` varchar(255) NOT NULL,
  `contact_number` varchar(15) NOT NULL,
  `like` int(11) NOT NULL DEFAULT '0',
  `short_description` varchar(500) DEFAULT NULL,
  `description` text NOT NULL,
  `extra_description` text NOT NULL,
  `country_id` int(11) NOT NULL,
  `image` varchar(200) NOT NULL,
  `city_id` int(11) NOT NULL,
  `review_count` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL,
  `is_active` enum('0','1') NOT NULL DEFAULT '1',
  `is_deleted` enum('0','1') NOT NULL DEFAULT '0',
  `created_date_time` datetime NOT NULL,
  `deleted_date_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=43 ;

--
-- Dumping data for table `tbl_tips_and_tricks`
--

INSERT INTO `tbl_tips_and_tricks` (`id`, `category_id`, `sub_category_id`, `member_id`, `title`, `url_slug`, `author_name`, `institution_name`, `institution_address`, `contact_number`, `like`, `short_description`, `description`, `extra_description`, `country_id`, `image`, `city_id`, `review_count`, `created_by`, `is_active`, `is_deleted`, `created_date_time`, `deleted_date_time`) VALUES
(17, 5, 0, 0, 'Top Pediatrics', 'top-pediatrics', 'De. Biswas Mehtha', 'Amrutha Institute of Medical Science', 'AIMS, New Delhi', '+91000000000', 0, '', '', '', 98, 'blog-img-01.jpg', 1124932, 0, 0, '1', '0', '2015-05-07 09:03:23', '0000-00-00 00:00:00'),
(18, 14, 0, 0, 'Meatball Pasta Bake', 'meatball-pasta-bake', '', '', '', '', 0, 'INGREDIENTS:  \n\nMeatball, 350g lean minced beef, 50g white breadcrumbs, 1 tbsp thyme, chopped, 50g parmesan, grated, 1/2 apple, peeled and grated', '<h1>\n INGREDIENTS</h1>\n<h2>\n Meatball</h2>\n<ul class=\\"recipeContent\\">\n <li>\n  350g lean minced beef</li>\n <li>\n  50g white breadcrumbs</li>\n <li>\n  1 tbsp thyme, chopped</li>\n <li>\n  50g parmesan, grated</li>\n <li>\n  1/2 apple, peeled and grated</li>\n</ul>\n<h2>\n Sauce</h2>\n<ul class=\\"recipeContent\\">\n <li>\n  2 tbsp olive oil</li>\n <li>\n  1 large onion, chopped</li>\n <li>\n  2 cloves garlic, crushed</li>\n <li>\n  2 x 400g tin chopped tomatoes</li>\n <li>\n  1 tbsp sun dried tomato paste</li>\n <li>\n  A dash of sugar</li>\n <li>\n  200g fusilli pasta</li>\n <li>\n  50g cheddar cheese, grated</li>\n <li>\n  2 tbsp basil, chopped</li>\n</ul>', '', 225, 'blog-img-01.jpg', 0, 0, 0, '1', '0', '2015-05-09 02:40:39', '0000-00-00 00:00:00'),
(19, 14, 1, 0, 'Salmon Puree', 'salmon-puree', '', '', '', '', 0, 'Put the carrots into a saucepan, cover with water, bring to the boil and cook over a medium heat for about 20 minutes until tender. Alternatively, place the vegetables in a steamer and cook for 20 minutes.', '<div>\n 200g carrots, peeled and sliced</div>\n<div>\n 125g salmon fillet, skinned</div>\n<div>\n 60ml orange juice</div>\n<div>\n 40g grated Cheddar cheese</div>\n<div>\n 15g unsalted butter</div>\n<div>\n 2 tbsp milk</div>\n<br />', '<div>\n	1. Put the carrots into a saucepan, cover with water, bring to the boil and cook over a medium heat for about 20 minutes until tender. Alternatively, place the vegetables in a steamer and cook for 20 minutes.</div>\n<div>\n	2. Meanwhile, place the salmon in a suitable dish, pour over the orange juice and scatter over the cheese. Cover, leaving an air vent and microwave on high for about 2 minutes or until the fish flakes easily with a fork . Alternatively, cover with foil and cook in a 180&deg;C/ 350&deg;F/Gas 4 pre-heated oven for about 20 minutes.</div>\n<div>\n	3. Flake the fish with a fork, carefully removing any bones. Drain the carrots, mix with the butter and milk and puree in a blender together with the flaked fish and its sauce. For older babies, mash the carrots together with the butter and the milk and then mix the flaked fish with the mashed carrots.</div>\n<div>\n	&nbsp;</div>\n', 225, 'blog-img-01.jpg', 0, 0, 0, '1', '0', '2015-05-30 00:40:54', '0000-00-00 00:00:00'),
(20, 14, 1, 0, 'Lentil Puree', 'lentil-puree', '', '', '', '', 0, 'Heat the oil in a saucepan, Add the onion, pepper, courgette and carrots and fry for 5 minutes', '<div>\n 2 tsp olive oil</div>\n<div>\n 50g red onion, diced</div>\n<div>\n 50g red pepper, diced</div>\n<div>\n 50g courgette, diced</div>\n<div>\n 1 clove garlic, crushed</div>\n<div>\n 3 tbsp red lentils</div>\n<div>\n Quarter tsp ground coriander</div>\n<div>\n 200g chopped tomatoes</div>\n<div>\n 200 ml chicken stock</div>\n<div>\n 1 tsp sundried tomato paste</div>\n<div>\n 2 tsp basil, chopped</div>\n<div>\n 15g parmesan, grated</div>', '<div>\n	1. Heat the oil in a saucepan.</div>\n<div>\n	2. Add the onion, pepper, courgette and carrots and fry for 5 minutes.</div>\n<div>\n	3. Add the garlic and fry for 1 minute.</div>\n<div>\n	4. Add the lentils and coriander , then blend in the tomatoes, stock. tomato paste.</div>\n<div>\n	5. Bring up to the boil, cover with a lid and simmer for 20 minutes.</div>\n<div>\n	6. Blend until smooth using a hand blender.</div>\n<div>\n	7. Add basil and parmesan.</div>\n', 225, 'blog-img-01.jpg', 0, 0, 0, '1', '0', '2015-05-30 01:10:19', '0000-00-00 00:00:00'),
(21, 14, 1, 0, 'Chicken and Apple Balls', 'chicken-and-apple-balls', '', '', '', '', 0, 'Heat the olive oil in a pan and saute half the onion for about 3 minutes. Using your hands, squeeze out a little excess liquid from the grated apple.', '<div>\n 2 tsp light olive oil</div>\n<div>\n 1 onion, finely chopped</div>\n<div>\n 1 large Granny Smith apple, peeled and grated</div>\n<div>\n 2 large chicken breasts, cut into chunks</div>\n<div>\n ½ tbsp fresh parsley </div>\n<div>\n 1 tbsp fresh thyme or sage, chopped, or a pinch of dried mixed herbs</div>\n<div>\n 1 chicken stock cube, crumbled (from 1 year)</div>\n<div>\n 50g fresh white breadcrumbs</div>\n<div>\n Salt and freshly ground pepper (from 1 year)</div>\n<div>\n Plain flour for coating</div>\n<div>\n Vegetable oil for frying</div>', '<div>\n	1. Heat the olive oil in a pan and saute half the onion for about 3 minutes.</div>\n<div>\n	2. Using your hands, squeeze out a little excess liquid from the grated apple.</div>\n<div>\n	3. Mix the apple with the chicken, then add the cooked and remaining raw onion, herbs stock cube (from 1 year) and breadcrumbs and roughly chop in a food processor for a few seconds. Season with a little salt and pepper (from 1 year).</div>\n<div>\n	4. With your hands, form into about 20 little balls, roll in flour and fry in shallow oil for about 5 minutes until lightly golden and cooked through.</div>\n', 225, 'blog-img-01.jpg', 0, 0, 0, '1', '0', '2015-05-30 01:22:17', '0000-00-00 00:00:00'),
(22, 14, 1, 0, 'Risotto with Butternut Squash', 'risotto-with-butternut-squash', '', '', '', '', 0, 'Saute the onion in half the butter until softened, Stir in the rice until well coated. Pour over the boiling water, cover pan with a lid and cook for 8 minutes over a high heat.', '<div>\n 50g onion, chopped</div>\n<div>\n 25g unsalted butter</div>\n<div>\n 110g basmati rice</div>\n<div>\n 450ml boiling water</div>\n<div>\n 150g butternut squash, peeled and chopped</div>\n<div>\n 225g ripe tomatoes, skinned, deseeded and chopped</div>\n<div>\n 50g Cheddar cheese, grated</div>', '<div>\n	1. Saute the onion in half the butter until softened.</div>\n<div>\n	2. Stir in the rice until well coated.</div>\n<div>\n	3. Pour over the boiling water, cover pan with a lid and cook for 8 minutes over a high heat.</div>\n<div>\n	4. Stir in the butternut squash, reduce the heat and cook, covered, for about 12 minutes or until or the water had been absorbed.</div>\n<div>\n	5. Meanwhile, melt the remaining butter in a small saucepan, add the tomatoes and saute for 2-3 minutes,</div>\n<div>\n	6. Stir in the cheese until melted, then stir the tomato and cheese mixture into the cooked rice.</div>\n', 225, 'blog-img-01.jpg', 0, 0, 0, '1', '0', '2015-05-30 01:24:42', '0000-00-00 00:00:00'),
(23, 14, 1, 0, 'Salmon with Tomato and Sweet Potato Sauce', 'salmon-with-tomato-and-sweet-potato-sauce', '', '', '', '', 0, 'Heat the oil in a pan and saute the onion for 5 minutes or until softened but not coloured, Add the garlic and cook for 1 minute. Add the chicken and saute for 2-3 minutes.', '<div>\n 55g Salmon fillet</div>\n<div>\n A knob of butter</div>\n<div>\n 2 x 80g sachets of Annabel Karmel Organic Tomato, Sweet Potato & Cheese Sauce or Carrot and Lentil Sauce</div>', '<div>\n	1. Heat the oil in a pan and saute the onion for 5 minutes or until softened but not coloured.</div>\n<div>\n	2. Add the garlic and cook for 1 minute.</div>\n<div>\n	3. Add the chicken and saute for 2-3 minutes.</div>\n<div>\n	4. Add the sweet potato, apricots, passata and stock or water.</div>\n<div>\n	5. Bring to the boil, then cover and simmer for about 15 minutes or until tender.</div>\n<div>\n	6. Blend to a puree.</div>\n', 225, 'blog-img-01.jpg', 0, 0, 0, '1', '0', '2015-05-30 01:26:19', '0000-00-00 00:00:00'),
(24, 14, 1, 0, 'Krispie Chicken Nuggets', 'krispie-chicken-nuggets', '', '', '', '', 0, 'Cut the chicken breasts into 1.5 cm cubes and put in a bowl. Mix together the milk, garlic, thyme, lemon juice, 1/4 tsp salt and some black pepper (the mixture will separate a little from the lemon juice but this is OK), and pour over the chicken.', '<div>\n 200g boneless skinless, chicken breasts</div>\n<div>\n 100 ml milk, plus 1 tbsp for dipping</div>\n<div>\n 1 garlic clove, crushed</div>\n<div>\n 1 tsp fresh thyme leaves</div>\n<div>\n 1 tbsp fresh lemon juice</div>\n<div>\n 45 g Rice Krispies</div>\n<div>\n 15 g mature Cheddar cheese, finely grated</div>\n<div>\n 1 tbsp grated Parmesan</div>\n<div>\n 1 egg</div>\n<div>\n 4 tbsp plain flour</div>\n<div>\n 3–4 tbsp sunflower oil, for frying</div>\n<div>\n salt and black pepper, to season</div>', '<div>\n	1. Cut the chicken breasts into 1.5 cm cubes and put in a bowl.</div>\n<div>\n	2. Mix together the milk, garlic, thyme, lemon juice, 1/4 tsp salt and some black pepper (the mixture will separate a little from the lemon juice but this is OK), and pour over the chicken.</div>\n<div>\n	3. Cover, and leave to marinate in the fridge for 4 hours or overnight (optional).</div>\n<div>\n	4. Put the Rice Krispies in a food processor and whiz for 1&ndash;2 minutes, until reduced to fine crumbs.</div>\n<div>\n	5. Add the cheeses, plus salt and pepper to taste, and whiz to combine. Transfer to a large plate.</div>\n<div>\n	6. Whisk the egg in a small bowl with the tablespoon of milk.</div>\n<div>\n	7. Mix the flour with a little salt and pepper, and spread out on another large plate.</div>\n<div>\n	8. Remove the chicken pieces from the marinade, shaking off any excess. Toss in the seasoned flour then dip in the egg and then roll in the Rice Krispie coating.</div>\n<div>\n	9. Put the oil in a large non-stick pan over a medium heat. Fry the nuggets for 2&ndash;3 minutes each side, until golden and crisp. Drain on kitchen paper and cool slightly before serving.</div>\n<div>\n	&nbsp;</div>\n<div>\n	To oven cook:</div>\n<div>\n	&nbsp;</div>\n<div>\n	1. If you would like an alternative to frying, pre-heat the oven to 200&ordm;C/ 400&ordm;F/Gas 6/Fan 180&ordm;C. Reduce the oil to two tablespoons. Add one tablespoon to the Kellogg&#39;s Rice Krispie crumbs with the cheese and whiz to combine evenly (you may need to stop and stir a couple of times as the oil can clump a bit).</div>\n<div>\n	2. Grease a baking sheet with the remaining tablespoon of oil. Coat the chicken (as above) and put on the prepared baking sheet.</div>\n<div>\n	3. Bake for 15 minutes, turning over half-way through.</div>\n<div>\n	&nbsp;</div>\n<div>\n	To freeze put uncooked, coated chicken nuggets on baking sheet lined with clingfilm. Cover with more clingfilm and freeze until solid then transfer to a re-sealable box or freezer bag. Cook direct from frozen, adding 1 minute extra cooking time for frying and 3-4 minutes extra for baking.</div>\n', 225, 'blog-img-01.jpg', 0, 0, 0, '1', '0', '2015-05-30 01:28:11', '0000-00-00 00:00:00'),
(25, 14, 1, 0, 'Rice Pudding', 'rice-pudding', '', '', '', '', 0, 'Put the rice, milk and sugar in a heavy bottomed saucepan, Split the vanilla pod, if usng, and scrape the seeds into the pan, or add the vanilla essence.', '<div>\n 50g (2 oz) pudding rice</div>\n<div>\n 600 ml (1 pint) milk</div>\n<div>\n 1 - 2 tbsp caster sugar</div>\n<div>\n 1 vanilla pod or ½ tsp vanilla essence</div>', '<div>\n	1. Put the rice, milk and sugar in a heavy bottomed saucepan.</div>\n<div>\n	2. Split the vanilla pod, if usng, and scrape the seeds into the pan, or add the vanilla essence.</div>\n<div>\n	3. Bring to the boil, then reduce the heat, cover with a lid and simmer for 30 &ndash; 35 minutes, stirring occasionally.</div>\n<div>\n	4. Mix with fruit or a topping of your choice such as strawberry jam or fruit puree.</div>\n', 225, 'blog-img-01.jpg', 0, 0, 0, '1', '0', '2015-05-30 01:29:49', '0000-00-00 00:00:00'),
(26, 14, 1, 0, 'Banana Puree', 'banana-puree', '', '', '', '', 0, 'Peel a small banana and mash with a fork. During the first stages of weaning, add a little milk...', '<p>\n 1 small banana</p>', '<div>\n	1. Peel a small banana and mash with a fork.</div>\n<div>\n	2. During the first stages of weaning, add a little milk if necessary to thin down the consistency and add a familiar taste.</div>\n', 225, 'blog-img-01.jpg', 0, 0, 0, '1', '0', '2015-05-30 01:32:23', '0000-00-00 00:00:00'),
(27, 14, 1, 0, 'Chicken with Carrot, Sweet Pepper and Sweet Potato', 'chicken-with-carrot--sweet-pepper-and-sweet-potato', '', '', '', '', 0, 'Heat the oil in a saucepan. Saute the onion, pepper, carrot, sweet potato and chicken for 2 to 3 minutes, then add 300ml water.', '<div>\n 2 tsp olive oil</div>\n<div>\n 100g onion, chopped</div>\n<div>\n 200g sweet potato, peeled and diced</div>\n<div>\n 175g carrot, peeled and sliced</div>\n<div>\n 175g yellow pepper, diced</div>\n<div>\n 150g chicken breast, diced</div>\n<div>\n 15g parmesan, grated</div>', '<div>\n	1. Heat the oil in a saucepan. Saute the onion, pepper, carrot, sweet potato and chicken for 2 to 3 minutes, then add 300ml water.&nbsp;</div>\n<div>\n	2. Cover, bring up to the boil and simmer for 20 to 25 minutes until tender and cooked through. Blend using an electric hand blender. Stir in the cheese until melted.</div>\n', 225, 'blog-img-01.jpg', 0, 0, 0, '1', '0', '2015-05-30 01:33:31', '0000-00-00 00:00:00'),
(28, 14, 1, 0, 'Fillet of Fish with Carrots and Orange', 'fillet-of-fish-with-carrots-and-orange', '', '', '', '', 0, 'Steam the carrots and potatoes for 15 to 18 minutes or until tender, Put the fish into a suitable gratin dish, pour over the orange juice. Sprinkle with cheese and dot with butter.', '<div>\n 175g carrots, peeled and sliced</div>\n<div>\n 125g potatoes, peeled and chopped</div>\n<div>\n 175g plaice fillets or another white fish</div>\n<div>\n Juice of one orange</div>\n<div>\n 60g Cheddar cheese, grated</div>\n<div>\n A knob of butter</div>', '<div>\n	1. Steam the carrots and potatoes &nbsp;for 15 to 18 &nbsp;minutes or until tender .</div>\n<div>\n	2. Put the fish into a suitable gratin dish, pour over the orange juice. Sprinkle with cheese and dot with butter.</div>\n<div>\n	3. Microwave the fish for 2 to 3 minutes or until the fish flakes easily .</div>\n<div>\n	4. Alternatively, you can cover with foil and oven cook for 20 minutes.</div>\n<div>\n	5. Flake the fish with a fork, checking carefully for bones.</div>\n<div>\n	6. Add the vegetables to the fish and its juices and puree or mash for older babies.</div>\n', 225, 'blog-img-01.jpg', 0, 0, 0, '1', '0', '2015-05-30 01:35:55', '0000-00-00 00:00:00'),
(29, 14, 1, 0, 'Sweet Potato with Broccoli and Peas', 'sweet-potato-with-broccoli-and-peas', '', '', '', '', 0, 'Steam the sweet potato for 3 minutes. Add the broccoli florets and steam for another 4 minutes, Add the frozen peas and steam for about 3 minutes or until cooked.', '<div>\n 1 small sweet potato (300g), peeled and chopped</div>\n<div>\n 60g broccoli florets</div>\n<div>\n 40g frozen peas</div>', '<div>\n	1. Steam the sweet potato for 3 minutes.</div>\n<div>\n	2. Add the broccoli florets and steam for another 4 minutes.</div>\n<div>\n	3. Add the frozen peas and steam for about 3 minutes or until cooked.</div>\n<div>\n	4. Blitz the vegetables using an electric hand blender together with about 75 ml of the water from the bottom of the steamer.</div>\n', 225, 'blog-img-01.jpg', 0, 0, 0, '1', '0', '2015-05-30 01:37:36', '0000-00-00 00:00:00'),
(30, 14, 1, 0, 'Butternut Squash, Carrot, Apple and Prune', 'butternut-squash--carrot--apple-and-prune', '', '', '', '', 0, 'Put the carrot and squash into a steamer and cook for 5 minutes. Add the apples and prunes and continue to steam for 10 minutes.', '<div>\n 1 medium carrot(100g), peeled and sliced</div>\n<div>\n 200g butternut squash, peeled, deseeded and chopped</div>\n<div>\n ½ small dessert apple, peeled, cored and chopped</div>\n<div>\n 10g prunes, chopped</div>', '<p>\n	Put the carrot and squash into a steamer and cook for 5 minutes. Add the apples and prunes and continue to steam for 10 minutes, until all the ingredients are tender. Blend with about 2 tablespoons of water from the steamer.</p>\n', 225, 'blog-img-01.jpg', 0, 0, 0, '1', '0', '2015-05-30 02:01:29', '0000-00-00 00:00:00'),
(31, 14, 1, 0, 'Chicken Delight', 'chicken-delight', '', '', '', '', 0, 'Heat the oil in a frying pan . Add the onion, red pepper, apple and carrot and fry for 2 minutes. Add the chicken, garlic, prunes and cinnamon.', '<div>\n 1 tbsp sunflower oil</div>\n<div>\n ½ medium red onion (75g), diced</div>\n<div>\n ½  red pepper (50g), diced</div>\n<div>\n 75g apple, diced</div>\n<div>\n 1 medium carrot (100g), diced</div>\n<div>\n 150g minced chicken or turkey</div>\n<div>\n 2 cloves garlic, crushed</div>\n<div>\n 10g prunes, chopped</div>\n<div>\n Half tsp ground cinnamon</div>\n<div>\n 400g tin chopped tomatoes</div>\n<div>\n 150 ml water</div>\n<div>\n 1 tsp tomato puree</div>', '<p>\n	Heat the oil in a frying pan . Add the onion, red pepper, apple and carrot and fry for 2 minutes. Add the chicken, garlic, prunes and cinnamon. Add the tomatoes, water and tomato puree. Bring to the boil, then &nbsp;cover and simmer for 15 minutes. Blend until smooth</p>\n', 225, 'blog-img-01.jpg', 0, 0, 0, '1', '1', '2015-05-30 02:02:57', '2015-05-30 02:10:31'),
(32, 14, 1, 0, 'Chicken Delight', 'chicken-delight-1', '', '', '', '', 0, 'Heat the oil in a frying pan . Add the onion, red pepper, apple and carrot and fry for 2 minutes. Add the chicken, garlic, prunes and cinnamon.', '<div>\n 1 tbsp sunflower oil</div>\n<div>\n ½ medium red onion (75g), diced</div>\n<div>\n ½  red pepper (50g), diced</div>\n<div>\n 75g apple, diced</div>\n<div>\n 1 medium carrot (100g), diced</div>\n<div>\n 150g minced chicken or turkey</div>\n<div>\n 2 cloves garlic, crushed</div>\n<div>\n 10g prunes, chopped</div>\n<div>\n Half tsp ground cinnamon</div>\n<div>\n 400g tin chopped tomatoes</div>\n<div>\n 150 ml water</div>\n<div>\n 1 tsp tomato puree</div>', '<p>\n	Heat the oil in a frying pan . Add the onion, red pepper, apple and carrot and fry for 2 minutes. Add the chicken, garlic, prunes and cinnamon. Add the tomatoes, water and tomato puree. Bring to the boil, then &nbsp;cover and simmer for 15 minutes. Blend until smooth</p>\n', 225, 'blog-img-01.jpg', 0, 0, 0, '1', '1', '2015-05-30 02:03:14', '2015-05-30 02:12:10'),
(33, 14, 1, 0, 'Chicken Delight', 'chicken-delight-2', '', '', '', '', 0, 'Heat the oil in a frying pan . Add the onion, red pepper, apple and carrot and fry for 2 minutes. Add the chicken, garlic, prunes and cinnamon.', '<div>\n 1 tbsp sunflower oil</div>\n<div>\n ½ medium red onion (75g), diced</div>\n<div>\n ½  red pepper (50g), diced</div>\n<div>\n 75g apple, diced</div>\n<div>\n 1 medium carrot (100g), diced</div>\n<div>\n 150g minced chicken or turkey</div>\n<div>\n 2 cloves garlic, crushed</div>\n<div>\n 10g prunes, chopped</div>\n<div>\n Half tsp ground cinnamon</div>\n<div>\n 400g tin chopped tomatoes</div>\n<div>\n 150 ml water</div>\n<div>\n 1 tsp tomato puree</div>', '<p>\n	Heat the oil in a frying pan . Add the onion, red pepper, apple and carrot and fry for 2 minutes. Add the chicken, garlic, prunes and cinnamon. Add the tomatoes, water and tomato puree. Bring to the boil, then &nbsp;cover and simmer for 15 minutes. Blend until smooth</p>\n', 225, 'blog-img-01.jpg', 0, 0, 0, '1', '1', '2015-05-30 02:03:40', '2015-05-30 02:13:24'),
(34, 14, 1, 0, 'Chicken Delight', 'chicken-delight-3', '', '', '', '', 0, 'Heat the oil in a frying pan . Add the onion, red pepper, apple and carrot and fry for 2 minutes. Add the chicken, garlic, prunes and cinnamon.', '<div>\n 1 tbsp sunflower oil</div>\n<div>\n ½ medium red onion (75g), diced</div>\n<div>\n ½  red pepper (50g), diced</div>\n<div>\n 75g apple, diced</div>\n<div>\n 1 medium carrot (100g), diced</div>\n<div>\n 150g minced chicken or turkey</div>\n<div>\n 2 cloves garlic, crushed</div>\n<div>\n 10g prunes, chopped</div>\n<div>\n Half tsp ground cinnamon</div>\n<div>\n 400g tin chopped tomatoes</div>\n<div>\n 150 ml water</div>\n<div>\n 1 tsp tomato puree</div>', '<p>\n	Heat the oil in a frying pan . Add the onion, red pepper, apple and carrot and fry for 2 minutes. Add the chicken, garlic, prunes and cinnamon. Add the tomatoes, water and tomato puree. Bring to the boil, then &nbsp;cover and simmer for 15 minutes. Blend until smooth</p>\n', 225, 'blog-img-01.jpg', 0, 0, 0, '1', '1', '2015-05-30 02:04:37', '2015-05-30 02:13:20'),
(35, 14, 1, 0, 'Chicken Delight', 'chicken-delight-4', '', '', '', '', 0, 'Heat the oil in a frying pan . Add the onion, red pepper, apple and carrot and fry for 2 minutes. Add the chicken, garlic, prunes and cinnamon.', '<div>\n 1 tbsp sunflower oil</div>\n<div>\n 1/2 medium red onion (75g), diced</div>\n<div>\n 1/2  red pepper (50g), diced</div>\n<div>\n 75g apple, diced</div>\n<div>\n 1 medium carrot (100g), diced</div>\n<div>\n 150g minced chicken or turkey</div>\n<div>\n 2 cloves garlic, crushed</div>\n<div>\n 10g prunes, chopped</div>\n<div>\n Half tsp ground cinnamon</div>\n<div>\n 400g tin chopped tomatoes</div>\n<div>\n 150 ml water</div>\n<div>\n 1 tsp tomato puree</div>', '<p>\n	Heat the oil in a frying pan . Add the onion, red pepper, apple and carrot and fry for 2 minutes. Add the chicken, garlic, prunes and cinnamon. Add the tomatoes, water and tomato puree. Bring to the boil, then &nbsp;cover and simmer for 15 minutes. Blend until smooth</p>\n', 225, 'blog-img-01.jpg', 0, 0, 0, '1', '0', '2015-05-30 02:07:02', '0000-00-00 00:00:00'),
(36, 14, 1, 0, 'Pumpkin Puree', 'pumpkin-puree', '', '', '', '', 0, 'Preheat the oven to 200&deg;C/400&deg;F/Gas 6. Put the halves of pumpkin sliced side up in a shallow ovenproof dish. Pour in some water – it should be about 1 cm (1?3 in) deep. Put the pumpkin in the oven for 45 minutes to 1 hour.', '<div>\r\n 1 small pumpkin (about 700 g/11?2 lb) cut into quarters and deseeded</div>\r\n<div>\r\n A little sunflower oil</div>\r\n<div>\r\n A little breast milk or formula milk (optional)</div>', '<div>\r\n	1. Preheat the oven to 200&deg;C/400&deg;F/Gas 6. Put the halves of pumpkin sliced side up in a shallow ovenproof dish. Pour in some water &ndash; it should be about 1 cm (1&frasl;3 in) deep. Put the pumpkin in the oven for 45 minutes to 1 hour.</div>\r\n<div>\r\n	2. Remove the dish from the oven and allow the squash to cool. Scoop out the flesh and pur&eacute;e to the desired consistency. You can add a little of your baby&rsquo;s usual milk if you wish.</div>\r\n', 225, 'blog-img-01.jpg', 0, 0, 0, '1', '0', '2015-05-30 11:44:55', '0000-00-00 00:00:00'),
(37, 1, 0, 0, 'dfdf', 'dfdf', 'dfdfd', 'fdfdf', 'dfdf', 'dfdfd', 0, 'fdf', '', '', 225, 'blog-img-01.jpg', 0, 0, 0, '1', '0', '2015-05-30 17:08:41', '0000-00-00 00:00:00'),
(38, 14, 3, 0, 'dfdf', 'dfdf-1', '', '', '', '', 0, 'Is there a way to unset ALL userdata in a session without having to use session destroy? I''m trying to log my user out and need to unset all userdata one at a time. It''s becoming tedious to keep track of all possible userdata set in session. I just want to unset everything that might have been set in userdata', '<div>\r\n Is there a way to unset ALL userdata in a session</div>\r\n<div>\r\n without having to use session destroy?</div>\r\n<div>\r\n I\\''m trying to log my user out and need to unset all userdata one at a time.</div>\r\n<div>\r\n It\\''s becoming tedious to keep track of all possible userdata set in session.</div>\r\n<div>\r\n I just want to unset everything that might have been set in userdata</div>', '', 225, 'blog-img-01.jpg', 0, 0, 0, '1', '0', '2015-05-30 17:10:23', '0000-00-00 00:00:00'),
(39, 1, 7, 0, 'fdsf', 'fdsf', 'dsfsd', 'sdfsd', 'sdfsdf', '', 0, '', '', '', 225, 'blog-img-01.jpg', 0, 0, 0, '1', '0', '2015-05-30 17:11:49', '0000-00-00 00:00:00'),
(40, 1, 0, 0, 'Meatball Pasta Bake', 'meatball-pasta-bake-1', '', '', '', '', 0, 'sdsssssssssssss', '<p>\r\n <p> ssssssssssssss</p></p>', '', 0, '', 0, 0, 0, '1', '0', '2015-06-13 18:21:29', '0000-00-00 00:00:00'),
(41, 1, 0, 0, 'XXXXXxxxxxxx', 'xxxxxxxxxxxx', '', '', '', '', 0, 'xxxxxxxxxxxx', '<p>\r\n xxxxxxxxxxxx</p>', '', 0, '13062015182147_about-us-left01.jpg', 0, 0, 0, '1', '0', '2015-06-13 18:21:47', '0000-00-00 00:00:00'),
(42, 16, 0, 0, '10 new play areas to have fun out of the sun', '10-new-play-areas-to-have-fun-out-of-the-sun', '', '', '', '', 0, 'Gone are the days when indoor play centres meant dingy, noisy caverns with violent games, stomach-churning rides and kids nagging for the chance to win an enormous stuffed toy that you definitely didn''t want to take home. Here, then, are 10 new play zones featuring something for everyone - from the restless teenager who needs a new hobby, to the mum who wants to kick back on a sofa with Wi-Fi and', '<p>\n Gone are the days when indoor play centres meant dingy, noisy caverns with violent games, stomach-churning rides and kids nagging for the chance to win an enormous stuffed toy that you definitely didn&#39;t want to take home. Here, then, are 10 new play zones featuring something for everyone - from the restless teenager who needs a new hobby, to the mum who wants to kick back on a sofa with Wi-Fi and coffee while keeping an eye as her toddler goes exploring.</p>\n<p>\n <span class=\\"Web Bold\\">1. Cheeky Monkeys, Etihad Mall, Dubai</span></p>\n<p>\n This new branch of Cheeky Monkeys claims to be the largest soft-play area in Dubai. As well as the standard soft-play features, there&#39;s a separate play zone for kids under 4, with a waterfall slide (that has real water running beneath the surface), a giant doll&#39;s house, crazy mirrors, a ball pit and craft tables with play dough set out. The cafe seating is conveniently located by windows looking onto the toddler area.</p>\n<p>\n There&#39;s also free Wi-Fi and a salon for kids opening soon.</p>\n<p>\n . Dh45 for one hour or Dh50 for two hours; 9am to 10pm on weekdays, 9am to 11pm on weekends; 04 3850875, <a href=\\"http://www.cheekymonkeys.ae/\\">www.cheekymonkeys.ae</a></p>\n<p>\n <span class=\\"Web Bold\\">2. Kiddies Cafe, Jumeirah Lake Towers, Dubai</span></p>\n<p>\n Though small in comparison with other new offerings, this place has a sense of community charm to it and you can immediately tell it was designed by a mum with small kids of her own. Hiba Momani says she opened Kiddies Cafe to create a cozy space with good-quality toys, where JLT mums could meet up. Momani also organises coffee mornings and movie nights.</p>\n<p>\n Her cafe provides free Wi-Fi and an appetising range of cakes, salads and pastries. In the kids&#39; section, there&#39;s the soft-play area, kiddies&#39; supermarket complete with wooden toy food and craft tables where staff help children decorate paper roses.</p>\n<p>\n Most eye-catching is the life-size doll&#39;s house and garage. Parking can be tricky on weekday mornings.</p>\n<p>\n . Dh45 per hour, Dh30 per additional hour; 10am to 7pm; 04 360 8571,<a href=\\"http://www.kiddiescafe.com/\\">www.kiddiescafe.com</a></p>\n<p>\n <span class=\\"Web Bold\\">3. Kids Connection, Wafi Mall, Dubai</span></p>\n<p>\n This spacious venue is the ultimate indoor park. Children can do everything they&#39;d be doing in an average playground, but without the need for sunscreen - laying on the \\"grass\\" (the smooth, rubbery kind) and playing on wooden swings, slides, see-saws and a climbing tower. There are also a bouncy castle, trampolines, a traversing wall, redemption games, rides and a softball court.</p>\n<p>\n To complete the park feel, trees are illustrated on the walls, blue sky and clouds painted on the ceiling and park benches are dotted around. No trip to the park is complete without an ice cream, which Baskin-Robbins is on hand to deliver.</p>\n<p>\n . Dh35 for two hours for children under 3, Dh45 for 2 hours for those over 3; 10am to 10pm on weekdays, 10am to midnight on weekends; 04 327 9011, <a href=\\"http://www.wafimall.com/\\">www.wafimall.com</a></p>\n<p>\n <span class=\\"Web Bold\\">4. Adventure HQ, Dalma Mall, Abu Dhabi</span></p>\n<p>\n If your kids are bouncing off the walls this summer, this is where they really can bounce off the walls - off a wall-trampoline, that is. A more spacious version of its Dubai counterpart, Adventure HQ in Abu Dhabi lives up to its name. In addition to 22 trampolines, it features a cable climb and the UAE&#39;s first \\"bouldering\\" climbing wall, complete with a bouncy foam floor to protect from falls. The skate park can also be traversed on scooters and bikes and there&#39;s a \\"waving raceway\\" with ridges and furrows for the feel of a real BMX track. Children who might need one-on-one help from the friendly staff are advised to come on weekdays, when it&#39;s quieter.</p>\n<p>\n Children under 4 are not allowed, while 4- to 6-year-olds have to be accompanied by parents.</p>\n<p>\n Adventure sports classes will also be running soon.</p>\n<p>\n . Dh90 for a one-hour session; 10am to 10pm and until midnight on Thursday and Friday; 02 445 6995, <a href=\\"http://www.adventurehq.ae/\\">www.adventurehq.ae</a></p>', '', 0, '25072015104253_blog-img-01.jpg', 0, 0, 0, '1', '0', '2015-06-14 05:55:38', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_tips_and_tricks_category`
--

CREATE TABLE IF NOT EXISTS `tbl_tips_and_tricks_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `sub_title` varchar(255) NOT NULL,
  `url_slug` varchar(255) NOT NULL,
  `description` text,
  `parent_id` int(11) NOT NULL,
  `category_image` varchar(255) NOT NULL,
  `category_order` int(11) NOT NULL,
  `on_header` enum('0','1') NOT NULL DEFAULT '1',
  `meta_title` text,
  `meta_desc` text,
  `meta_keywords` text,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_active` enum('0','1') NOT NULL DEFAULT '0',
  `is_deleted` enum('0','1') NOT NULL DEFAULT '0',
  `deleted_date_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `tbl_tips_and_tricks_category`
--

INSERT INTO `tbl_tips_and_tricks_category` (`id`, `title`, `sub_title`, `url_slug`, `description`, `parent_id`, `category_image`, `category_order`, `on_header`, `meta_title`, `meta_desc`, `meta_keywords`, `created_by`, `updated_by`, `created_date`, `updated_date`, `is_active`, `is_deleted`, `deleted_date_time`) VALUES
(1, 'Top Paeds & IBCLCs', 'Top Pediatrics', 'Top-Pediatrics', '0', 0, '', 0, '1', NULL, NULL, NULL, 1, 3, '0000-00-00 00:00:00', '2015-06-07 08:02:43', '1', '0', '0000-00-00 00:00:00'),
(2, 'Breast feeding', 'Breast feeding', 'Breast feeding', 'Breast feeding', 0, '', 1, '0', NULL, NULL, NULL, 1, 3, '0000-00-00 00:00:00', '2015-06-07 08:45:34', '0', '0', '0000-00-00 00:00:00'),
(3, 'Breastfeeding', 'Milk Supply', 'Milk-Supply', 'Milk Supply', 0, '', 3, '0', NULL, NULL, NULL, 1, 3, '0000-00-00 00:00:00', '2015-06-07 09:36:56', '1', '0', '0000-00-00 00:00:00'),
(4, 'Support Groups', 'Support Groups', 'Support-Groups', 'Support Groups', 0, '', 4, '0', NULL, NULL, NULL, 1, 1, '0000-00-00 00:00:00', '2015-06-07 08:45:40', '0', '0', '0000-00-00 00:00:00'),
(12, 'Top Pediatrics of Delhi', '', 'top-pediatrics-of-delhi', NULL, 0, '', 6, '1', NULL, NULL, NULL, 3, 3, '2015-05-07 08:53:21', '2015-05-10 05:38:01', '1', '1', '2015-05-09 02:38:49'),
(14, 'Recipes', '', '', '0', 0, '', 2, '1', NULL, NULL, NULL, 3, 3, '2015-05-09 02:39:36', '2015-06-07 09:36:45', '1', '0', '0000-00-00 00:00:00'),
(16, 'Play Areas', '', '', NULL, 0, '', 0, '1', NULL, NULL, NULL, 3, 0, '2015-06-06 16:04:07', '2015-06-07 10:12:04', '0', '0', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_tips_review`
--

CREATE TABLE IF NOT EXISTS `tbl_tips_review` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tip_id` int(11) NOT NULL,
  `reviewer_id` int(11) NOT NULL,
  `rate_value` int(11) NOT NULL,
  `created_date_time` datetime NOT NULL,
  `updated_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `tbl_tips_review`
--

INSERT INTO `tbl_tips_review` (`id`, `tip_id`, `reviewer_id`, `rate_value`, `created_date_time`, `updated_date_time`) VALUES
(3, 5, 37, 4, '0000-00-00 00:00:00', '2015-05-28 11:44:14'),
(2, 1, 37, 1, '0000-00-00 00:00:00', '2015-05-28 11:10:53'),
(4, 5, 39, 2, '0000-00-00 00:00:00', '2015-05-28 12:45:14'),
(5, 5, 40, 2, '0000-00-00 00:00:00', '2015-05-28 13:16:48'),
(6, 1, 37, 3, '0000-00-00 00:00:00', '2015-05-28 13:53:29'),
(8, 30, 40, 3, '0000-00-00 00:00:00', '2015-05-28 13:53:29'),
(9, 30, 37, 3, '0000-00-00 00:00:00', '2015-05-28 14:24:49'),
(10, 31, 37, 4, '0000-00-00 00:00:00', '2015-05-29 12:50:46'),
(11, 38, 37, 3, '0000-00-00 00:00:00', '2015-05-31 11:09:54');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_tip_comment`
--

CREATE TABLE IF NOT EXISTS `tbl_tip_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tip_id` int(11) NOT NULL,
  `reviewer_id` int(11) NOT NULL,
  `comment` text NOT NULL,
  `created_date_time` datetime NOT NULL,
  `updated_date_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `tbl_tip_comment`
--

INSERT INTO `tbl_tip_comment` (`id`, `tip_id`, `reviewer_id`, `comment`, `created_date_time`, `updated_date_time`) VALUES
(3, 5, 3, 'Unicare Medical Center, \r\n1st Floor, Bin Ham Bldg, Shk Khalifa Bin Zayed Rd, Near Burjuman Center, Karama, Dubai', '0000-00-00 00:00:00', '2015-05-28 11:44:14'),
(2, 1, 3, 'Unicare Medical Center, \r\n1st Floor, Bin Ham Bldg, Shk Khalifa Bin Zayed Rd, Near Burjuman Center, Karama, Dubai\r\nUnicare Medical Center, \r\n1st Floor, Bin Ham Bldg, Shk Khalifa Bin Zayed Rd, Near Burjuman Center, Karama, Dubai', '0000-00-00 00:00:00', '2015-05-28 11:10:53'),
(4, 5, 3, 'Unicare Medical Center, 1st Floor, Bin Ham Bldg, Shk Khalifa Bin Zayed Rd, Near Burjuman Center, Karama, Dubai', '0000-00-00 00:00:00', '2015-05-28 12:45:14'),
(5, 5, 3, 'Wow! Amazing', '0000-00-00 00:00:00', '2015-05-28 13:16:48'),
(6, 30, 3, 'ghdfghfdg', '0000-00-00 00:00:00', '2015-05-28 14:25:56'),
(7, 1, 3, 'Medical Center, 1st Floor, Bin Ham Bldg, Shk Khalifa Bin Zayed Rd, Near Burjuman Center, Karama, Dub', '0000-00-00 00:00:00', '2015-05-28 15:15:16'),
(8, 1, 3, 'sss', '2015-05-27 21:19:12', '2015-05-28 15:18:24'),
(9, 1, 3, 'M Bldg, Shk Khalifa Bin Zayed Rd, Near Burjuman Center, Karama, Dub m Bldg, Shk Khalifa Bin Zayed Rd, Near Burjuman Center, Karama, Dub m Bldg, Shk Khalifa Bin Zayed Rd, Near Burjuman Center, Karama, Dub m Bldg, Shk Khalifa Bin Zayed Rd, Near Burjuman Center, Karama, Dub m Bldg, Shk Khalifa Bin Zayed Rd, Near Burjuman Center, Karama, Dub m Bldg, Shk Khalifa Bin Zayed Rd, Near Burjuman Center, Karama, Dub m Bldg, Shk Khalifa Bin Zayed Rd, Near Burjuman Center, Karama, Dub m Bldg, Shk Khalifa Bin Zayed Rd, Near Burjuman Center, Karama, Dub m Bldg, Shk Khalifa Bin Zayed Rd, Near Burjuman Center, Karama, Dub', '2015-05-30 11:39:17', '2015-05-31 05:38:29'),
(11, 38, 3, 'sdsd', '2015-05-30 17:10:46', '2015-05-31 11:09:58'),
(12, 36, 23, 'asdddddddddd', '2015-06-09 19:39:11', '2015-06-10 13:38:23'),
(13, 30, 23, 'sdfsdfdsf', '2015-06-13 12:56:51', '2015-06-14 06:56:03'),
(14, 30, 23, 'Preheat the oven to 200C/400F/Gas 6. Put the halves of pumpkin sliced side up in a shallow ovenproof dish. Pour in some water – it should be about 1 cm (1?3 in) deep. Put the pumpkin in the oven for 45 minutes to 1 hour.', '2015-06-13 12:57:02', '2015-06-14 06:56:14'),
(15, 38, 23, 'Is there a way to unset ALL userdata in a session without having to use session destroy?', '2015-06-13 13:03:30', '2015-06-14 07:02:42'),
(16, 39, 27, 'g to use session destroy? I''m trying to log my user out and need to unset all userdata one at a time. It''s becoming tedious to keep track of all possible userdata set in session. I just want to unset everything that might have bee', '2015-06-13 18:07:23', '2015-06-14 12:06:35'),
(17, 42, 27, 'If your kids are bouncing off the walls this summer, this is where they really can bounce off the walls - off a wall-trampoline, that is. A more spacious version of its Dubai counterpart, Adventure HQ in Abu Dhabi lives up to its name. In addition to 22 trampolines, it features a cable climb and the UAE''s first \\"bouldering\\" climbing wall, complete with a bouncy foam floor to protect from falls. The skate park can also be traversed on scooters and bikes and there''s a \\"waving raceway\\" with ridges and furrows for the feel of a real BMX track. Children who might need one-on-one help from the friendly staff are advised to come on weekdays, when it''s quieter.', '2015-06-14 06:22:10', '2015-06-14 17:21:46');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_transmission`
--

CREATE TABLE IF NOT EXISTS `tbl_transmission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `created_date` datetime NOT NULL,
  `is_active` enum('0','1') NOT NULL DEFAULT '1',
  `is_deleted` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tbl_transmission`
--

INSERT INTO `tbl_transmission` (`id`, `title`, `created_date`, `is_active`, `is_deleted`) VALUES
(1, 'Auto', '2015-10-07 13:36:56', '0', '1'),
(2, 'Manual', '2015-10-07 13:36:59', '0', '1'),
(3, 'Both', '2015-10-07 14:13:15', '1', '0');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_vehicle_brand`
--

CREATE TABLE IF NOT EXISTS `tbl_vehicle_brand` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `created_date` datetime NOT NULL,
  `is_active` enum('0','1') NOT NULL DEFAULT '0',
  `is_deleted` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `tbl_vehicle_brand`
--

INSERT INTO `tbl_vehicle_brand` (`id`, `title`, `created_date`, `is_active`, `is_deleted`) VALUES
(1, 'Honda', '2015-03-01 13:02:10', '1', '0'),
(2, 'MAzda', '2015-03-01 13:04:20', '0', '1'),
(3, 'Audi', '2015-03-04 09:06:43', '1', '0'),
(4, 'Toyota', '2015-04-17 23:27:11', '1', '0'),
(5, 'Hyundai', '2015-04-17 23:27:33', '1', '0'),
(6, 'Lamborghini', '2015-04-17 23:28:22', '0', '1'),
(7, 'Mercedes-Benz', '2015-04-20 17:54:05', '1', '0'),
(8, 'Mazda', '2016-03-01 09:11:48', '1', '0'),
(9, 'BMW', '2016-03-01 09:12:07', '1', '0'),
(10, 'Nissan', '2016-03-01 09:12:23', '1', '0'),
(11, 'Subaru', '2016-03-01 09:12:41', '1', '0'),
(12, 'Mitsubishi', '2016-03-01 09:13:43', '1', '0'),
(13, 'Kia', '2016-03-01 09:14:06', '1', '0'),
(14, 'Ford', '2016-03-01 09:14:19', '1', '0'),
(15, 'Suzuki', '2016-03-01 09:14:36', '1', '0'),
(16, 'Skoda', '2016-03-01 09:14:49', '1', '0'),
(17, 'Isuzu', '2016-03-01 09:15:02', '1', '0'),
(18, 'Volkswagen', '2016-03-01 09:16:43', '1', '0'),
(19, 'Porsche', '2016-03-01 09:17:11', '1', '0'),
(20, 'Chevrolet', '2016-03-01 09:17:36', '1', '0'),
(21, 'Mudan', '2016-03-01 09:18:08', '0', '1'),
(22, 'Great Wall', '2016-03-01 09:18:26', '1', '0'),
(23, 'Peugeot', '2016-03-01 11:00:30', '1', '0'),
(24, 'Land Rover', '2016-03-01 11:00:43', '1', '0'),
(25, 'Daihatsu', '2016-03-01 11:00:57', '1', '0'),
(26, 'Renault', '2016-03-01 11:01:07', '1', '0'),
(27, 'Lexus', '2016-03-01 11:01:18', '1', '0'),
(28, 'Range Rover', '2016-03-01 11:01:31', '1', '0'),
(29, 'Jeep', '2016-03-01 11:01:41', '1', '0');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_vehicle_model`
--

CREATE TABLE IF NOT EXISTS `tbl_vehicle_model` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `brand_id` int(11) NOT NULL,
  `is_active` enum('0','1') NOT NULL DEFAULT '0',
  `created_date_time` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=165 ;

--
-- Dumping data for table `tbl_vehicle_model`
--

INSERT INTO `tbl_vehicle_model` (`id`, `title`, `brand_id`, `is_active`, `created_date_time`) VALUES
(3, 'Q7', 3, '1', '2015-03-04 09:08:05'),
(2, 'CRV', 1, '1', '2015-03-01 13:35:04'),
(5, 'CIVIC', 1, '1', '2015-05-31 14:33:27'),
(6, 'Yaris', 4, '1', '2015-06-14 05:48:36'),
(42, 'C180', 7, '1', '2016-03-01 15:43:28'),
(8, 'Grand Cherokee', 29, '1', '2016-03-01 11:02:18'),
(9, 'Laredo', 29, '1', '2016-03-01 11:02:37'),
(10, 'Cherokee', 29, '1', '2016-03-01 11:02:48'),
(11, 'Fit', 1, '1', '2016-03-01 11:03:33'),
(12, 'City', 1, '1', '2016-03-01 11:03:43'),
(13, 'Accent', 1, '1', '2016-03-01 11:03:52'),
(14, 'Accord', 1, '1', '2016-03-01 11:04:01'),
(15, 'Delsol', 1, '1', '2016-03-01 11:04:11'),
(16, 'A4', 3, '1', '2016-03-01 11:04:36'),
(17, 'A3', 3, '1', '2016-03-01 11:06:22'),
(18, 'A5', 3, '1', '2016-03-01 11:06:41'),
(19, 'A6', 3, '1', '2016-03-01 11:06:57'),
(20, 'A7', 3, '1', '2016-03-01 11:07:06'),
(21, 'Q5', 3, '1', '2016-03-01 11:07:17'),
(22, 'Q3', 3, '1', '2016-03-01 11:07:29'),
(23, 'TT', 3, '1', '2016-03-01 11:07:37'),
(24, 'Corolla', 4, '1', '2016-03-01 11:08:22'),
(25, 'Hilux', 4, '1', '2016-03-01 11:08:32'),
(26, 'Crown', 4, '1', '2016-03-01 11:08:42'),
(27, 'Fielder Wagon', 4, '1', '2016-03-01 11:08:58'),
(28, 'Camry', 4, '1', '2016-03-01 11:09:10'),
(29, 'Royal Saloon', 4, '1', '2016-03-01 11:09:21'),
(30, 'Fortuner', 4, '1', '2016-03-01 11:09:34'),
(31, 'Cressida', 4, '1', '2016-03-01 11:09:45'),
(32, 'Prado', 4, '1', '2016-03-01 11:09:52'),
(33, 'Rav 4', 4, '1', '2016-03-01 11:10:05'),
(34, 'Veloster', 5, '1', '2016-03-01 14:02:43'),
(35, 'Tucson', 5, '1', '2016-03-01 14:02:54'),
(36, 'Elantra', 5, '1', '2016-03-01 14:03:08'),
(37, 'SantaFe', 5, '1', '2016-03-01 14:03:18'),
(38, 'Matrix', 5, '1', '2016-03-01 14:03:28'),
(39, 'Trajet', 5, '1', '2016-03-01 14:03:45'),
(40, 'H100 Van', 5, '1', '2016-03-01 14:03:54'),
(41, 'Sonata', 5, '1', '2016-03-01 14:04:03'),
(43, 'C350', 7, '1', '2016-03-01 15:43:47'),
(44, 'E250', 7, '1', '2016-03-01 15:43:57'),
(45, 'S500', 7, '1', '2016-03-01 15:44:06'),
(46, 'CLK200', 7, '1', '2016-03-01 15:44:19'),
(47, 'C230', 7, '1', '2016-03-01 15:44:29'),
(48, 'Sprinter', 7, '1', '2016-03-01 15:58:02'),
(49, 'E200', 7, '1', '2016-03-01 15:58:11'),
(50, 'CLS320', 7, '1', '2016-03-01 15:58:21'),
(51, 'ML280', 7, '1', '2016-03-01 15:58:31'),
(52, '730d', 9, '1', '2016-03-01 17:38:35'),
(53, 'X5', 9, '1', '2016-03-01 17:40:00'),
(54, '320i', 9, '1', '2016-03-01 17:40:15'),
(55, '316i', 9, '1', '2016-03-01 17:40:42'),
(56, 'X6', 9, '1', '2016-03-01 17:41:06'),
(57, '116i', 9, '1', '2016-03-01 17:41:30'),
(58, '525i', 9, '1', '2016-03-01 17:41:50'),
(59, '520i', 9, '1', '2016-03-01 17:42:03'),
(60, '740i', 9, '1', '2016-03-01 17:42:21'),
(61, '530i', 9, '1', '2016-03-01 17:50:37'),
(62, 'RX8', 8, '1', '2016-03-01 17:51:45'),
(63, '3', 8, '1', '2016-03-01 17:51:56'),
(64, 'BT-50', 8, '1', '2016-03-01 17:52:12'),
(65, 'MX3', 8, '1', '2016-03-01 17:52:23'),
(66, '6', 8, '1', '2016-03-01 17:52:34'),
(67, 'B2500', 8, '1', '2016-03-01 17:52:45'),
(68, 'B2200', 8, '1', '2016-03-01 17:52:57'),
(69, '323', 8, '1', '2016-03-01 17:53:08'),
(70, 'RX7', 8, '1', '2016-03-01 17:53:20'),
(71, 'TITAN2', 8, '1', '2016-03-01 17:53:39'),
(72, '626', 8, '1', '2016-03-01 17:53:49'),
(73, 'MX6', 8, '1', '2016-03-01 17:54:03'),
(74, 'XTrail', 10, '1', '2016-03-01 17:56:29'),
(75, 'B13', 10, '1', '2016-03-01 17:56:41'),
(76, 'Maxima', 10, '1', '2016-03-01 17:56:54'),
(77, 'Frontier', 10, '1', '2016-03-01 17:57:07'),
(78, 'B14', 10, '1', '2016-03-01 17:57:18'),
(79, 'Tiida', 10, '1', '2016-03-01 17:57:29'),
(80, 'Teana', 10, '1', '2016-03-01 17:57:44'),
(81, 'Wingroad', 10, '1', '2016-03-01 17:58:01'),
(82, 'Sunny', 10, '1', '2016-03-01 17:58:15'),
(83, 'Qashqai', 10, '1', '2016-03-01 17:58:33'),
(84, 'Almera', 10, '1', '2016-03-01 17:58:45'),
(85, 'Primera', 10, '1', '2016-03-01 17:58:59'),
(86, 'March', 10, '1', '2016-03-01 17:59:40'),
(87, 'Navara', 10, '1', '2016-03-01 17:59:54'),
(88, 'Serena', 10, '1', '2016-03-01 18:00:07'),
(89, 'Versa', 10, '1', '2016-03-01 18:00:39'),
(90, 'E24 Caravan', 10, '1', '2016-03-01 18:00:56'),
(91, 'Cifero', 10, '1', '2016-03-01 18:01:09'),
(92, 'Bluebird', 10, '1', '2016-03-01 18:01:51'),
(93, 'B15', 10, '1', '2016-03-01 18:02:05'),
(94, 'E26 Caravan', 10, '1', '2016-03-01 18:03:15'),
(95, 'E25 Caravan', 10, '1', '2016-03-01 18:03:30'),
(96, 'Silvia', 10, '1', '2016-03-01 18:03:43'),
(97, 'Pathfinder', 10, '1', '2016-03-01 18:03:55'),
(98, 'Terrano', 10, '1', '2016-03-01 18:04:07'),
(99, 'B12', 10, '1', '2016-03-01 18:04:17'),
(100, 'Prairie', 10, '1', '2016-03-01 18:04:31'),
(101, 'Laurel', 10, '1', '2016-03-01 18:04:47'),
(102, 'Note', 10, '1', '2016-03-01 18:04:57'),
(103, 'SkylineG7', 10, '1', '2016-03-01 18:06:08'),
(104, '350Z', 10, '1', '2016-03-01 18:06:20'),
(105, '370Z', 10, '1', '2016-03-01 18:06:33'),
(106, 'Condor Truck', 10, '1', '2016-03-01 18:06:49'),
(107, 'Legacy', 11, '1', '2016-03-01 18:08:23'),
(108, 'Forester', 11, '1', '2016-03-01 18:08:37'),
(109, 'Impreza', 11, '1', '2016-03-01 18:08:51'),
(110, 'Lancer', 12, '1', '2016-03-01 18:28:41'),
(111, 'L200 Sportero', 12, '1', '2016-03-01 18:28:55'),
(112, 'L300 Panel Van', 12, '1', '2016-03-01 18:29:06'),
(113, 'Pajero', 12, '1', '2016-03-01 18:29:15'),
(114, 'Galant', 12, '1', '2016-03-01 18:29:26'),
(115, 'Libero Wagon', 12, '1', '2016-03-01 18:29:38'),
(116, 'Triton', 12, '1', '2016-03-01 18:29:49'),
(117, 'Fuso', 12, '1', '2016-03-01 18:29:59'),
(118, 'ASX', 12, '1', '2016-03-01 18:30:09'),
(119, 'Rio', 13, '1', '2016-03-01 20:04:47'),
(120, 'Sorento', 13, '1', '2016-03-01 20:04:58'),
(121, 'Sportage', 13, '1', '2016-03-01 20:05:39'),
(122, 'Cerato', 13, '1', '2016-03-01 20:05:49'),
(123, 'Optima', 13, '1', '2016-03-01 20:05:59'),
(124, 'Carens', 13, '1', '2016-03-01 20:06:08'),
(125, 'K2700', 13, '1', '2016-03-01 20:06:18'),
(126, 'Pride', 13, '1', '2016-03-01 20:06:31'),
(127, 'Ranger', 14, '1', '2016-03-01 20:07:32'),
(128, 'Everest', 14, '1', '2016-03-01 20:07:43'),
(129, 'Fiesta', 14, '1', '2016-03-01 20:07:51'),
(130, 'Focus', 14, '1', '2016-03-01 20:07:59'),
(131, 'Grand Vitara', 15, '1', '2016-03-01 20:08:42'),
(132, 'Swift', 15, '1', '2016-03-01 20:08:52'),
(133, 'Liana', 15, '1', '2016-03-01 20:09:02'),
(134, 'APV Van', 15, '1', '2016-03-01 20:09:11'),
(135, 'Carry', 15, '1', '2016-03-01 20:09:20'),
(136, 'Jimny', 15, '1', '2016-03-01 20:09:28'),
(137, 'SX4', 15, '1', '2016-03-01 20:09:36'),
(138, 'Celerio', 15, '1', '2016-03-01 20:09:49'),
(139, 'Samurai', 15, '1', '2016-03-01 20:09:59'),
(140, 'Octavia', 16, '1', '2016-03-01 20:10:42'),
(141, 'D-Max Pickup', 17, '1', '2016-03-01 20:11:09'),
(142, 'Wrecker', 17, '1', '2016-03-01 20:13:20'),
(143, 'Jetta', 18, '1', '2016-03-01 20:16:16'),
(144, 'Passat', 18, '1', '2016-03-01 20:16:26'),
(145, 'Golf', 18, '1', '2016-03-01 20:16:38'),
(146, 'Bora', 18, '1', '2016-03-01 20:16:45'),
(147, 'Touareg', 18, '1', '2016-03-01 20:17:15'),
(148, 'Cayenne', 19, '1', '2016-03-01 20:17:48'),
(149, 'Cayman', 19, '1', '2016-03-01 20:17:58'),
(150, 'Cruz', 20, '1', '2016-03-01 20:18:22'),
(151, 'Wingle', 22, '1', '2016-03-01 20:19:17'),
(152, '307', 23, '1', '2016-03-01 20:19:43'),
(153, 'Discovery', 24, '1', '2016-03-01 20:20:28'),
(154, 'Defender', 24, '1', '2016-03-01 20:20:37'),
(155, 'Laguna', 26, '1', '2016-03-01 20:21:11'),
(156, 'Megane', 26, '1', '2016-03-01 20:21:21'),
(157, 'Altezza', 27, '1', '2016-03-01 20:21:52'),
(158, 'IS200', 27, '1', '2016-03-01 20:22:00'),
(159, 'ES300', 27, '1', '2016-03-01 20:22:14'),
(160, 'RX300', 27, '1', '2016-03-01 20:22:23'),
(161, 'RX350', 27, '1', '2016-03-01 20:22:34'),
(162, 'LX470', 27, '1', '2016-03-01 20:22:46'),
(163, 'Sport HSE', 28, '1', '2016-03-01 20:23:11'),
(164, 'Vogue', 28, '1', '2016-03-01 20:23:24');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

/**
 * Created by USER on 11/23/2015.
 */


function create_loadCriteriaLookup(obj, parentId, target, lookupType, callBack, customTargetWrap, selected)
{
    //console.log(callBack);
    loadLookup(obj, parentId, target, lookupType, callBack, customTargetWrap, selected);
    create_manage_lookUpfields($(obj).val());
    create_manage_fileInputFields();
}
function create_manage_fileInputFields()
{
    //alert("Innn");
    var sub_category = $('#sub_category_id').val();
    $("[id^='category_based-file-wrap_']").hide();
    $("[id^='category_based-file-wrap_']").find('input').prop('disabled', true);
    if ($('#category_based-file-wrap_'+sub_category).length > 0) {
        //alert(sub_category);
        $('#category_based-file-wrap_' + sub_category).find('input').prop('disabled', false);
        $('#category_based-file-wrap_' + sub_category).find('textarea').prop('disabled', false);
        $('#category_based-file-wrap_' + sub_category).find('select').prop('disabled', false);
        $('#category_based-file-wrap_' + sub_category).show();
    } else {
        //alert('common');
        $('#category_based-file-wrap_common').find('input').prop('disabled', false);
        $('#category_based-file-wrap_common').find('textarea').prop('disabled', false);
        $('#category_based-file-wrap_common').find('select').prop('disabled', false);
        $('#category_based-file-wrap_common').show();
    }
}

function create_manage_lookUpfields(categ_id)
{
    $("[id^='category_based-filter-wrap_']").hide();
    $("[id^='category_based-filter-wrap_']").find('input').prop('disabled', true);
    $("[id^='category_based-filter-wrap_']").find('textarea').prop('disabled', true);
    $("[id^='category_based-filter-wrap_']").find('select').prop('disabled', true);

    if ($('#category_based-filter-wrap_'+categ_id).length > 0) {
        $('#category_based-filter-wrap_' + categ_id).find('input').prop('disabled', false);
        $('#category_based-filter-wrap_' + categ_id).find('textarea').prop('disabled', false);
        $('#category_based-filter-wrap_' + categ_id).find('select').prop('disabled', false);
        $('#category_based-filter-wrap_' + categ_id).show();
    } else {
        $('#category_based-filter-wrap_common').find('input').prop('disabled', false);
        $('#category_based-filter-wrap_common').find('textarea').prop('disabled', false);
        $('#category_based-filter-wrap_common').find('select').prop('disabled', false);
        $('#category_based-filter-wrap_common').show();
    }
}

/**
 * Function to load subcategories
 *
 * @param obj
 * @param target
 * @param lookupType
 * @param parentId
 * @param selected
 * @param extra
 */
function loadLookup(obj, parentId, target, lookupType, callBack, customTargetWrap, selected)
{
    //alert(obj+ ' - '+parentId+ ' - '+target+ ' - '+lookupType+ ' - '+callBack+ ' - '+customTargetWrap+ ' - '+selected);
    var rootUrl = $('#rootUrlLink').val();
    //alert(rootUrl);
    var loader = $('<img src="' +rootUrl+ 'images/ajax-loader.gif" />');
    $(loader).insertAfter($(target));
    var extra = ' id="'+$(target).attr('id')+'"';
    extra += ' class="'+$(target).attr('class')+'"';
    if (typeof selected == 'undefined') {
        selected = '';
    }
    $(target).hide();
    jQuery.ajax({
        type: "POST",
        url: rootUrl+'home/loadLookupDropDowns',
        data: {lookupType: lookupType, fieldName: $(target).attr('name'),parentId:parentId, selected:selected, extra: extra},
        cache: false,
        success: function(data) {
            if (typeof customTargetWrap != 'undefined' && $(customTargetWrap).length > 0) {
                $(customTargetWrap).html(data);
                $(customTargetWrap).find('select').chosen();
            } else {
                var target_wrap = $(target).parent();
                $(target_wrap).html(data);
                $(target_wrap).find('select').chosen();
            }

            /*$(target).show(function(){
             if (typeof callBack != 'undefined' && callBack != false) {
             hideSubSubCateg();
             }
             });*/
            loader.remove();

            return true;
        }
    });
}
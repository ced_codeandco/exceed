function trim(str, chars) {
	return ltrim(rtrim(str, chars), chars);
}
 
function ltrim(str, chars) {
	chars = chars || "\\s";
	return str.replace(new RegExp("^[" + chars + "]+", "g"), "");
}
 
function rtrim(str, chars) {
	chars = chars || "\\s";
	return str.replace(new RegExp("[" + chars + "]+$", "g"), "");
}
/*
common javascript fuctions 
*/
function validatePassword (pw, options) {
	// default options (allows any password)
	var o = {
		lower:    0,
		upper:    0,
		alpha:    0, /* lower + upper */
		numeric:  0,
		special:  0,
		length:   [0, Infinity],
		custom:   [ /* regexes and/or functions */ ],
		badWords: [],
		badSequenceLength: 0,
		noQwertySequences: false,
		noSequential:      false
	};
	for (var property in options)
		o[property] = options[property];
	var	re = {
			lower:   /[a-z]/g,
			upper:   /[A-Z]/g,
			alpha:   /[A-Z]/gi,
			numeric: /[0-9]/g,
			special: /[\W_]/g
		},
		rule, i;
	// enforce min/max length
	if (pw.length < o.length[0] || pw.length > o.length[1])
		return false;
	// enforce lower/upper/alpha/numeric/special rules
	for (rule in re) {
		if ((pw.match(re[rule]) || []).length < o[rule])
			return false;
	}
	// enforce word ban (case insensitive)
	for (i = 0; i < o.badWords.length; i++) {
		if (pw.toLowerCase().indexOf(o.badWords[i].toLowerCase()) > -1)
			return false;
	}
	// enforce the no sequential, identical characters rule
	if (o.noSequential && /([\S\s])\1/.test(pw))
		return false;
	// enforce alphanumeric/qwerty sequence ban rules
	if (o.badSequenceLength) {
		var	lower   = "abcdefghijklmnopqrstuvwxyz",
			upper   = lower.toUpperCase(),
			numbers = "0123456789",
			qwerty  = "qwertyuiopasdfghjklzxcvbnm",
			start   = o.badSequenceLength - 1,
			seq     = "_" + pw.slice(0, start);
		for (i = start; i < pw.length; i++) {
			seq = seq.slice(1) + pw.charAt(i);
			if (
				lower.indexOf(seq)   > -1 ||
				upper.indexOf(seq)   > -1 ||
				numbers.indexOf(seq) > -1 ||
				(o.noQwertySequences && qwerty.indexOf(seq) > -1)
			) {
				return false;
			}
		}
	}
	// enforce custom regex/function rules
	for (i = 0; i < o.custom.length; i++) {
		rule = o.custom[i];
		if (rule instanceof RegExp) {
			if (!rule.test(pw))
				return false;
		} else if (rule instanceof Function) {
			if (!rule(pw))
				return false;
		}
	}
	// great success!
	return true;
}
function loadEditor(id)
{
    var instance = CKEDITOR.instances[id];
    if(instance)
    {
        CKEDITOR.remove(instance);
    }
    CKEDITOR.replace(id);
}
function formatItem(row) {
	return row[0];
}
function formatResult(row) {
	return row[0].replace(/(<.+?>)/gi, '');
}
function loadNewsCalender(){
	 $("#news_publish_date").date_input();
	 $("#news_unpublish_date").date_input();
}
function pressEnter(e){
	if(e && e.keyCode == 13){
     $("#add").click();
     return false;
   }
}
function validateEmailRegexp(email) { 
	
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
} 
function validateEmail(fld)
{
	var my=fld.value;
	var attherate=my.indexOf("@");
	var lastattherate = my.lastIndexOf("@")
	var dotpos=my.lastIndexOf(".");
	var posspace = my.indexOf(" ");
	var totallen = my.length;
	
	if (attherate<=0 || dotpos<=0 || attherate > dotpos || (dotpos-attherate)<=1 || (dotpos == totallen-1) || posspace > -1 || attherate!=lastattherate)
		return false;
	else
		return true;
}
function validate_register(){
	
	var emailFilter = /^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,4})+$/;
	
	if($("#customer_name").val()==''){
		$("#msg_customer_name").html('<br/>Name is required.');
		$("#customer_name").addClass('jquery_err');
		$("#customer_name").focus();
		return false;
	}else{
		$("#msg_customer_name").html('');
		$("#customer_name").removeClass('jquery_err');
	}
	
	
	if($("#customer_address").val()==''){
		$("#msg_customer_address").html('<br/>Address is required.');
		$("#customer_address").addClass('jquery_err');
		$("#customer_address").focus();
		return false;
	}else{
		$("#msg_customer_address").html('');
		$("#customer_address").removeClass('jquery_err');
	}
	
	if($("#customer_city").val()==''){
		$("#msg_customer_city").html('<br/>City is required.');
		$("#customer_city").addClass('jquery_err');
		$("#customer_city").focus();
		return false;
	}else{
		$("#msg_customer_city").html('');
		$("#customer_city").removeClass('jquery_err');
	}
	
	if($("#customer_area").val()==''){
		$("#msg_customer_area").html('<br/>Area is required.');
		$("#customer_area").addClass('jquery_err');
		$("#customer_area").focus();
		return false;
	}else{
		$("#msg_customer_area").html('');
		$("#customer_area").removeClass('jquery_err');
	}
	
	if($("#customer_country").val()==''){
		$("#msg_customer_country").html('<br/>Country is required.');
		$("#customer_country").addClass('jquery_err');
		$("#customer_country").focus();
		return false;
	}else{
		$("#msg_customer_country").html('');
		$("#customer_country").removeClass('jquery_err');
	}
	
	if($("#customer_state").val()==''){
		$("#msg_customer_state").html('<br/>State is required.');
		$("#customer_state").addClass('jquery_err');
		$("#customer_state").focus();
		return false;
	}else{
		$("#msg_customer_state").html('');
		$("#customer_state").removeClass('jquery_err');
	}
	
	if($("#customer_zip_code").val()==''){
		$("#msg_customer_zip_code").html('<br/>Pincode is required.');
		$("#customer_zip_code").addClass('jquery_err');
		$("#customer_zip_code").focus();
		return false;
	}else{
		$("#msg_customer_zip_code").html('');
		$("#customer_zip_code").removeClass('jquery_err');
	}
	
	
	if($("#customer_email_address").val()==''){
		$("#msg_customer_email_address").html('<br/>Email address is required.');
		$("#customer_email_address").addClass('jquery_err');
		$("#customer_email_address").focus();
		return false;
	}else if($("#customer_email_address").val()!='' && !(emailFilter.test($("#customer_email_address").val())) ){
		$("#msg_customer_email_address").html('<br/>Email address is invalid');
		$("#customer_email_address").addClass('jquery_err');
		$("#customer_email_address").focus();
		return false;
	}else{
		$("#msg_customer_email_address").html('');
		$("#customer_email_address").removeClass('jquery_err');
	}
	
	var password = $("#customer_password").val();
	var passed = validatePassword(password, {
		length:   [6, Infinity],
		numeric:  1,
		special:  1
	});
	
	if($("#customer_password").val()==''){
		$("#msg_customer_password").html('<br/>Password is required.');
		$("#customer_password").addClass('jquery_err');
		$("#customer_password").focus();
		return false;
	}else if(!passed){
		$("#msg_customer_password").html('<br/>Password should have minimum 6 char and atleast one numeric and one special char. ');
		$("#customer_password").addClass('jquery_err');
		$("#customer_password").focus();
		return false;
	}else if($("#customer_password").val()!='' && $("#customer_password").val()!=$("#customer_repassword").val()){
		$("#msg_customer_password").html('<br/>Password and Again password do not matched.');
		$("#customer_password").addClass('jquery_err');
		$("#customer_password").focus();
		return false;
	}else{
		$("#msg_customer_password").html('');
		$("#customer_password").removeClass('jquery_err');
	}
	if(!$("#terms").attr('checked')){
		$("#msg_terms").html('Please read terms and conidtions.');
		$("#terms").addClass('jquery_err');
		$("#terms").focus();
		return false;
	}else{
		$("#msg_customer_state").html('');
		$("#customer_state").removeClass('jquery_err');
	}
}
function validate_profile(){
	
	var emailFilter = /^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,4})+$/;
	
	if($("#customer_name").val()==''){
		$("#msg_customer_name").html('<br/>Name is required.');
		$("#customer_name").addClass('jquery_err');
		$("#customer_name").focus();
		return false;
	}else{
		$("#msg_customer_name").html('');
		$("#customer_name").removeClass('jquery_err');
	}
	
	if($("#customer_address").val()==''){
		$("#msg_customer_address").html('<br/>Address is required.');
		$("#customer_address").addClass('jquery_err');
		$("#customer_address").focus();
		return false;
	}else{
		$("#msg_customer_address").html('');
		$("#customer_address").removeClass('jquery_err');
	}
	
	if($("#customer_city").val()==''){
		$("#msg_customer_city").html('<br/>City is required.');
		$("#customer_city").addClass('jquery_err');
		$("#customer_city").focus();
		return false;
	}else{
		$("#msg_customer_city").html('');
		$("#customer_city").removeClass('jquery_err');
	}
	
	if($("#customer_area").val()==''){
		$("#msg_customer_area").html('<br/>Area is required.');
		$("#customer_area").addClass('jquery_err');
		$("#customer_area").focus();
		return false;
	}else{
		$("#msg_customer_area").html('');
		$("#customer_area").removeClass('jquery_err');
	}
	if($("#customer_country").val()==''){
		$("#msg_customer_country").html('<br/>Country is required.');
		$("#customer_country").addClass('jquery_err');
		$("#customer_country").focus();
		return false;
	}else{
		$("#msg_customer_country").html('');
		$("#customer_country").removeClass('jquery_err');
	}
	if($("#customer_state").val()==''){
		$("#msg_customer_state").html('<br/>State is required.');
		$("#customer_state").addClass('jquery_err');
		$("#customer_state").focus();
		return false;
	}else{
		$("#msg_customer_state").html('');
		$("#customer_state").removeClass('jquery_err');
	}
		
	if($("#customer_zip_code").val()==''){
		$("#msg_customer_zip_code").html('<br/>Zipcode is required.');
		$("#customer_zip_code").addClass('jquery_err');
		$("#customer_zip_code").focus();
		return false;
	}else{
		$("#msg_customer_zip_code").html('');
		$("#customer_zip_code").removeClass('jquery_err');
	}
		
}
function validate_changePassword(){
	
	if($("#change_password").val()==''){
		$("#msg_change_password").html('<br/>New password is required.');
		$("#change_password").addClass('jquery_err');
		$("#change_password").focus();
		return false;
	}else{
		$("#msg_change_password").html('');
		$("#change_password").removeClass('jquery_err');
	}	
	
	if($("#change_cpassword").val()==''){
		$("#msg_change_cpassword").html('<br/>Confirm password is required.');
		$("#change_cpassword").addClass('jquery_err');
		$("#change_cpassword").focus();
		return false;
	}else{
		$("#msg_change_cpassword").html('');
		$("#change_cpassword").removeClass('jquery_err');
	}	
	
	if($("#change_password").val()=='' && $("#change_cpassword").val()=='' && $("#change_password").val()!=$("#change_cpassword").val()){
		$("#msg_change_cpassword").html('<br/>New and confirm password does not matched.');
		$("#change_cpassword").addClass('jquery_err');
		$("#change_cpassword").focus();
		return false;
	}else{
		$("#msg_change_cpassword").html('');
		$("#change_cpassword").removeClass('jquery_err');
	}
}
function validate_subscriber(){
	
	var emailFilter = /^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,4})+$/;
	if($("#subscriber_email").val()=='' || $("#subscriber_email").val()=='Your Email Address'){
		alert('Please enter your email address');
		$("#subscriber_email").focus();
		return false;
	}else if($("#subscriber_email").val()!='' && !(emailFilter.test($("#subscriber_email").val()))){
		alert('Please enter valid email address');		
		$("#subscriber_email").val('');
		$("#subscriber_email").focus();
		return false;
	}
}
function validate_right_quote(){
	
	var emailFilter = /^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,4})+$/;
	if($("#quote_name").val()=='' || $("#quote_name").val()=='Your Name'){
		alert('Please enter your name');
		$("#quote_name").focus();
		return false;
	}
	
	if($("#quote_company").val()=='' || $("#quote_company").val()=='Company Name'){
		alert('Please enter company name');
		$("#quote_company").focus();
		return false;
	}
	
	
	if($("#quote_email").val()=='' || $("#quote_email").val()=='Email Address'){
		alert('Please enter email address');
		$("#quote_email").focus();
		return false;
	}else if($("#quote_email").val()!='' && !(emailFilter.test($("#quote_email").val()))){
		alert('Please enter valid email address');		
		$("#quote_email").val('');
		$("#quote_email").focus();
		return false;
	}
	
	if($("#quote_phone").val()=='' || $("#quote_phone").val()=='Phone'){
		alert('Please enter phone');
		$("#quote_phone").focus();
		return false;
	}
	
	if($("#quote_message").val()=='' || $("#quote_message").val()=='Message'){
		alert('Please enter message');
		$("#quote_message").focus();
		return false;
	}
}
function checkQuoteForm(){
	
var emailFilter = /^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,4})+$/;
	if($("#pquote_name").val()=='' || $("#pquote_name").val() == 'Your Name'){
		alert('Please enter your name.');
		return false;
	}
	if($("#pquote_email").val()=='' || $("#pquote_email").val() == 'Email Address'){
		alert('Please enter your email address.');
		return false;
	}else if($("#pquote_email").val()!='' && !(emailFilter.test($("#pquote_email").val()))){
		alert('Please enter valid Email address');		
		return false;
	}
	if($("#pquote_number").val()=='' || $("#pquote_number").val() == 'Phone / Mobile'){
		alert('Please enter Phone / Mobile.');
		return false;
	}
	if($("#pquote_message").val()=='' || $("#pquote_message").val() == 'Comments'){
		alert('Please enter some text in comments.');
		return false;
	}
	if($("#captcha_data").val()=='' || $("#captcha_data").val() == 'Security Code'){
		alert('Please enter security code');
		return false;
	}
	return true;
}

/**
 * function to validate Email ID[User name].
 * @param obj
 * @param url
 * @param container
 * @param style
 * @return
 */
function validate_user_name(obj, url, container, style)
{
    container = typeof container !== 'undefined' ? container : 'div';
    style = typeof style !== 'undefined' ? style : 'error';
    var email = $(obj).val();
    if(email.length == 0)
    {
        set_message(obj, 'Please input an email', container, style);
        return;
    }
    else if(!validateEmailRegexp(email))
    {
        set_message(obj, 'Invalid/Incomplete Email ID', container, style);
    }
    else
    {
        set_message(obj, 'Checking your Email ID for duplication...', container, style);
        check_duplicate_email(email, url, obj, container, style);
        return;
    }
}
/**
 * function to check duplication of Email ID[User name].
 * @param email
 * @param url
 * @param obj
 * @param container
 * @param style
 * @return string data
 */
function check_duplicate_email(email, url, obj, container, style)
{
    //showWait($(obj));
    jQuery.ajax({
        type: "POST",
        url: url,
        dataType: 'json',
        data: {email:email},
        cache: false,
        success: function(data) {
            //hideWait($(obj));
            var status = data.status;
            var msg = data.message;
            if(status*1 == 1)
            {
                set_message(obj, msg, container, style);
            }
            else
            {
                clear_error_message(obj, container, style);
            }
            return data;
        }
    });
}
/**
 * function to set error message.
 * @param obj
 * @param msg
 * @param container
 * @param style
 * @return void
 */
function set_message(obj, msg, container, style, fade)
{
    container = typeof container !== 'undefined' ? container : 'p';
    style = typeof style !== 'undefined' ? style : 'error';
    if($(obj).next(container).length > 0)
    {
        $(container).show();
        $(obj).next(container).html(msg);
        if (typeof fade != 'undefined' && fade == true) {
            $(container).delay( 1000 ).fadeOut(1000);
        }
    }
    else
    {
        var new_msg = $('<'+container+' class="'+style+'">'+msg+'</'+container+'>');
        $(obj).parent().append(new_msg);
        console.log(fade);
        if (typeof fade != 'undefined' && fade == true) {
            $(new_msg).delay( 1000 ).fadeOut(1000, function(){
                $(new_msg).remove();
            });
        }
    }
}
/**
 * function to clear error message.
 * @param obj
 * @param container
 * @return void
 */
function clear_error_message(obj, container)
{
    container = typeof container !== 'undefined' ? container : 'p';
    if($(obj).next(container).length > 0)
    {
        $(obj).next(container).html('');
        return;
    }
}

/**
 * Function to save search queries
 * @param url
 * @param queryString
 */
function saveSearch(url, queryString, obj, container, style)
{
    jQuery.ajax({
        type: "POST",
        url: url,
        dataType: 'json',
        data: {search_query : queryString, is_active:1},
        cache: false,
        success: function(data) {
            //hideWait($(obj));
            var status = data.status;
            var msg = data.message;
            if(status*1 == 1)
            {
                set_message(obj, msg, container, style, true);
            }
            else
            {
                //clear_error_message(obj, container);
                //alert('ssssssssss');
                set_message(obj, msg, container, style, true);
            }
            return data;
        }
    });
}

function validPhoneNumber(string)
{
    var regex = /^(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d?)\)?)?[\-\.\ \\\/]?)?((?:\(?\d{1,}\)?[\-\.\ \\\/]?){0,})(?:[\-\.\ \\\/]?(?:#|ext\.?|extension|x)[\-\.\ \\\/]?(\d+))?$/i;
    return regex.test(string);
}

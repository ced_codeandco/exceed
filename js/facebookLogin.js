/**
 * Created by USER on 5/10/2015.
 */

window.fbAsyncInit = function() {
    FB.init({
        appId      : '1606664936257258',
        xfbml      : true,
        version    : 'v2.3'
    });
};

(function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

$('#facebookLoginLink').click(function(){
    $('#facebook').find('.loadingFb').removeClass('error')
    $('#facebook').hide();
    $('#loadingIconContainer').show();
    FB.login(function(response) {
        if (response.authResponse) {
            //console.log('Welcome!  Fetching your information.... ');
            $('#loadingIconContainer').find('.loadingFb').html('Fetching Information');
            FB.api('/me', function(response) {
                //console.log('Good to see you, ' + response.name + '.');
                $('#loadingIconContainer').find('.loadingFb').html('Creating Account');
                createUserAccountAndLogin(response);
            });
        } else {
            $('#facebook').find('div.connect').html('Failed to login').addClass('error')
            $('#facebook').show();
            $('#loadingIconContainer').hide();
            //console.log('User cancelled login or did not fully authorize.');
        }
    },{scope: 'public_profile,email',return_scopes: true});
})
function createUserAccountAndLogin(formData)
{
    var rootUrlLink = $('#rootUrlLink').val();
    var loginRedirect = $('#login-redirect').val();
    //showWait($(obj));
    jQuery.ajax({
        type: "POST",
        url: rootUrlLink+'facebookLogin',
        dataType: 'json',
        data: formData,
        cache: false,
        success: function(data) {
            //hideWait($(obj));
            var status = data.status;
            var msg = data.message;
            if(status*1 == 1)
            {
                window.location = loginRedirect;
            }
            else
            {
                alert(msg);
            }
            return data;
        }
    });
}
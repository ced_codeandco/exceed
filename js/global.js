
$(document).ready(function(){
	//To switch directions up/down and left/right just place a "-" in front of the top/left attribute
	$('.boxgrid, .peek').hover(function(){
		$(".cover, .cover2, .cover3, .cover4", this).stop().animate({left:'249px'},{queue:false,duration:300});
		
		//$(".big-nav a.discovery").css({"opacity": "0.5"});
		
	}, function() {
		$(".cover, .cover2, .cover3, .cover4", this).stop().animate({left:'0px'},{queue:false,duration:300});
//		$(".big-nav a.discovery").css({"opacity": "1"});
	});
	
	$('.big-nav a.discovery').hover(function(){
		$(".cover", 0).stop().animate({left:'249px'},{queue:false,duration:300});
	}, function() {
		$(".cover", 0).stop().animate({left:'0px'},{queue:false,duration:300});
	});
	
	$('.big-nav a.development').hover(function(){
		$(".cover2", 0).stop().animate({left:'249px'},{queue:false,duration:300});
	}, function() {
		$(".cover2", 0).stop().animate({left:'0px'},{queue:false,duration:300});
	});
	
	$('.big-nav a.design').hover(function(){
		$(".cover3", 0).stop().animate({left:'249px'},{queue:false,duration:300});
	}, function() {
		$(".cover3", 0).stop().animate({left:'0px'},{queue:false,duration:300});
	});
	
	$('.big-nav a.delivery').hover(function(){
		$(".cover4", 0).stop().animate({left:'249px'},{queue:false,duration:300});
	}, function() {
		$(".cover4", 0).stop().animate({left:'0px'},{queue:false,duration:300});
	});
	
	/* topnav*/
	$('ul.top-nav li').hover(
		function () {
			//show its submenu
			$('ul', this).slideDown(100);

		}, 
		function () {
			//hide its submenu
			$('ul', this).slideUp(100);			
		}
	);
	
	/* photos */
	$('#main-img').fadeIn(3000, function() {
        // Animation complete
      });
	
	 /* text */
	 $('.text-cont').lionbars();
	
});

function mycarousel_initCallback(carousel)
{
    // Disable autoscrolling if the user clicks the prev or next button.
    carousel.buttonNext.bind('click', function() {
        carousel.startAuto(0);
    });

    carousel.buttonPrev.bind('click', function() {
        carousel.startAuto(0);
    });

    // Pause autoscrolling if the user moves with the cursor over the clip.
    carousel.clip.hover(function() {
        carousel.stopAuto();
    }, function() {
        carousel.startAuto();
    });
};

jQuery(document).ready(function() {
    jQuery('#mycarousel').jcarousel({
        auto: 1,
        wrap: 'circular',
		animation: 3000,
        initCallback: mycarousel_initCallback
    });
});

jQuery(document).ready(function() {
    jQuery('#mycarousel2').jcarousel({
        auto: 1,
        wrap: 'circular',
		animation: 1000,
        initCallback: mycarousel_initCallback
    });
});

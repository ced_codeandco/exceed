/**
 * Created by USER on 6/9/2015.
 */


function create_loadCriteriaLookup(obj, parentId, target, lookupType, callBack)
{
    //console.log(callBack);
    loadLookup(obj, parentId, target, lookupType, callBack);
    create_manage_lookUpfields($(obj).val());
    create_manage_fileInputFields();
}
function create_manage_fileInputFields()
{
    //alert("Innn");
    var sub_category = $('#sub_category_id').val();
    $("[id^='category_based-file-wrap_']").hide();
    $("[id^='category_based-file-wrap_']").find('input').prop('disabled', true);
    if ($('#category_based-file-wrap_'+sub_category).length > 0) {
        //alert(sub_category);
        $('#category_based-file-wrap_' + sub_category).find('input').prop('disabled', false);
        $('#category_based-file-wrap_' + sub_category).find('textarea').prop('disabled', false);
        $('#category_based-file-wrap_' + sub_category).find('select').prop('disabled', false);
        $('#category_based-file-wrap_' + sub_category).show();
    } else {
        //alert('common');
        $('#category_based-file-wrap_common').find('input').prop('disabled', false);
        $('#category_based-file-wrap_common').find('textarea').prop('disabled', false);
        $('#category_based-file-wrap_common').find('select').prop('disabled', false);
        $('#category_based-file-wrap_common').show();
    }
}

function create_manage_lookUpfields(categ_id)
{
    $("[id^='category_based-filter-wrap_']").hide();
    $("[id^='category_based-filter-wrap_']").find('input').prop('disabled', true);
    $("[id^='category_based-filter-wrap_']").find('textarea').prop('disabled', true);
    $("[id^='category_based-filter-wrap_']").find('select').prop('disabled', true);

    if ($('#category_based-filter-wrap_'+categ_id).length > 0) {
        $('#category_based-filter-wrap_' + categ_id).find('input').prop('disabled', false);
        $('#category_based-filter-wrap_' + categ_id).find('textarea').prop('disabled', false);
        $('#category_based-filter-wrap_' + categ_id).find('select').prop('disabled', false);
        $('#category_based-filter-wrap_' + categ_id).show();
    } else {
        $('#category_based-filter-wrap_common').find('input').prop('disabled', false);
        $('#category_based-filter-wrap_common').find('textarea').prop('disabled', false);
        $('#category_based-filter-wrap_common').find('select').prop('disabled', false);
        $('#category_based-filter-wrap_common').show();
    }
}
/**
 * Function to load subcategories
 *
 * @param obj
 * @param target
 * @param lookupType
 * @param parentId
 * @param selected
 * @param extra
 */
function loadLookup(obj, parentId, target, lookupType, callBack)
{
    var rootUrl = $('#rootUrlLink').val();
    var loader = $('<img src="' +rootUrl+ 'images/ajax-loader.gif" />');
    $(loader).insertAfter($(target));
    var extra = ' id="'+$(target).attr('id')+'"';
    extra += ' class="'+$(target).attr('class')+'"';
    $(target).hide();
    jQuery.ajax({
        type: "POST",
        url: rootUrl+'home/loadLookupDropDowns',
        data: {lookupType: lookupType, fieldName: $(target).attr('name'),parentId:parentId, selected:'', extra: extra},
        cache: false,
        success: function(data) {
            $(target).replaceWith(data);
            $(target).show(function(){
                if (typeof callBack != 'undefined') {
                    hideSubSubCateg();
                }
            });
            loader.remove();

            return true;
        }
    });
}
$(document).ready(function(){
    $('#sub_category_id').live('change', function(){
        loadTertiaryOnCreate($(this));
    })
    /*$('#sub_category_id:visible').live(function() {
        console.log($('#tertiary_category_id option').size());
    });*/
})
function loadTertiaryOnCreate(obj) {
    console.log('Here-1111');
    loadLookup($(obj), $(obj).val(), $('#tertiary_category_id'), 'subCategory', 1);
    var sub_category = $('#sub_category_id').val();
    create_manage_fileInputFields(sub_category);
}
function hideSubSubCateg() {
    //console.log($('#tertiary_category_id option').size());
    if ($('#tertiary_category_id option').size() == 1) {
        $('#tertiary_category_id').parent().hide();
        //$('#tertiary_category_id option').parent().hide();
    } else {
        $('#tertiary_category_id').parent().show();
    }
}

function validDubaiPhoneNumber(string)
{
    var regex = /^(?:(?:\(?(?:00|\+)([1-4]\d\d|[1-9]\d?)\)?)?[\-\.\ \\\/]?)?((?:\(?\d{1,}\)?[\-\.\ \\\/]?){0,})(?:[\-\.\ \\\/]?(?:#|ext\.?|extension|x)[\-\.\ \\\/]?(\d+))?$/i;
    return regex.test(string);
}


function subscribeme(form) {
    var rootUrl = $('#rootUrlLink').val();
    var email = $(form).find('input[name="email"]');
    var loader = $('<img src="' +rootUrl+ 'images/footer-ajax-loader.gif" />');
    var target = $(form).parent();
    $(target).find('.email-subsc-error').remove();
    $(target).hide();
    $(loader).insertAfter($(target));
    jQuery.ajax({
        type: "POST",
        url: rootUrl+'home/subscribe',
        data: {email: email.val()},
        dataType: 'json',
        cache: false,
        success: function(data) {
            if (data.status == 1) {
                $(target).html('<p class="font-13px">' + data.msg + '</p>');
            } else {
                $('<label class="email-subsc-error" style="color: #ff0000; font-size: 12px;">' +data.msg+ '</label>').insertAfter(email);
            }
            $(target).show();
            loader.remove();
        }
    });
    return false;
}
$(document).ready(function(){
    $('.categ-read-more a').click(function(){
        if ($(this).parent().hasClass('less')) {
            $(this).parent().removeClass('less');
            $(this).parent().parent().find('.hidden-categ-list').slideUp('slow');
            $(this).html('Read more &raquo;&raquo;');
        } else {
            $(this).parent().addClass('less');
            $(this).parent().parent().find('.hidden-categ-list').slideDown('slow');
            $(this).html('Read less &laquo;&laquo;');
        }
    })
})

$(document).ready(function(){
    var url = location.href;
    //console.log(url);
    $('div.red-box').find('ul.nav-bar').find('li').each(function(){
        if ($(this).attr('home') == '1') {
            var homeUrl = $(this).find('a').attr('href');
            homeUrl = homeUrl.replace(/\/$/, "");
            url = url.replace(/#[^#]*$/, "");
            url = url.replace(/\/$/, "");
            if (homeUrl == url) {
                $(this).find('a:first').addClass('active');
            }
        } else if (url.search($(this).find('a').attr('href')) != -1) {
            $(this).find('a:first').addClass('active');
        }
    })
})
$(document).ready(function() {
    $('.tab-controle').click(function (e) {
        e.preventDefault();
        if (!$(this).hasClass('active')) {
            $('.tab-controle').removeClass('active');
            $(this).addClass('active');
            $('[class^="tab-content-"]').removeClass('tab-active').addClass('tab-hidden');
            $('.tab-content-'+$(this).attr('rel')).addClass('tab-active').removeClass('tab-hidden');
        }
    })
})
$(document).ready(function() {
    $(".ajax-popup").colorbox({width: "50%"});
})
$(document).ready(function() {
    $('.more-link-list a').click(function () {
        if (typeof $(this).attr('clicked') == "undefined") {
            $(this).attr('clicked', 'clicked');
            $(this).parent().prev('ul.locations-list').css('height', 'auto');
            $(this).html('Less &laquo;')
        } else {
            $(this).removeAttr('clicked');
            $(this).html('More &raquo;')
            $(this).parent().prev('ul.locations-list').css('height', '165px');
        }
    })
})

var mainImageIndex = 0;
function addfileinputcloned(obj) {
    var maxFiles = 5;
    maxFiles =  maxFiles - $('.preview-images').length;
    if ($('.file-selector-wrap').length < maxFiles) {
        mainImageIndex++;
        var fileInputRow = $('<div class="file-selector-wrap"> <div class="post-field-col-4"> <p class="form"> <input type="text" id="path" /> <label class="add-photo-btn">Select Photo <span> <input type="file" id="myfile' + mainImageIndex + '" name="myfile[' + mainImageIndex + ']" /> </span> </label> </p> </div> <div class="post-field-col-4"><input type="text" readonly id="pathInput' + mainImageIndex + '"></div>  <div class="clerafix"></div> </div>');

        fileInputRow.insertAfter($('.file-selector-wrap:last'));
    }
    if ($('.file-selector-wrap').length == maxFiles) {
        //$(obj).hide();
    }
}
/**
 * Created by USER on 6/7/2015.
 */

$(document).ready(function(){
    var animspeed = 950; // animation speed in milliseconds

    var $blockquote = $('.bigtext');
    var height = $blockquote.height();
    //var mini = $('.bigtext p').eq(590).height() + $('.bigtext p').eq(550).height() + $('.bigtext p').eq(50).height() + $('.bigtext p').eq(50).height();
    var mini = 110;
    $blockquote.attr('data-fullheight',height+'px');
    $blockquote.attr('data-miniheight',mini+'px');
    $blockquote.css('height',mini+'px');


    $('.expand').on('click', function(e){
        $text = $(this).prev();

        $text.animate({
            'height': $text.attr('data-fullheight')
        }, animspeed);
        $(this).next('.contract').removeClass('hide');
        $(this).addClass('hide');
    });

    $('.contract').on('click', function(e){
        $text = $(this).prev().prev();

        $text.animate({
            'height': $text.attr('data-miniheight')
        }, animspeed);
        $(this).prev('.expand').removeClass('hide');
        $(this).addClass('hide');
    });
});
